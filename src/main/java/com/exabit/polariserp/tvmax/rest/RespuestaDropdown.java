/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 */
public class RespuestaDropdown {
     
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    private List<DropdownItem> results = new LinkedList<>();

    public List<DropdownItem> getResults() {
        return results;
    }

    public void setResults(List<DropdownItem> results) {
        this.results = results;
    }
}
