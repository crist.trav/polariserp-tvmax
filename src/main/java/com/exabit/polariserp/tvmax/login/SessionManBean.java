package com.exabit.polariserp.tvmax.login;


import com.exabit.polariserp.tvmax.entidades.CategoriaModulo;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.entidades.TerminalImpresion;
import com.exabit.polariserp.tvmax.funcionalidad.AuxFuncionalidad;
import com.exabit.polariserp.tvmax.sesbeans.FuncionalidadFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ModuloFacade;
import com.exabit.polariserp.tvmax.sesbeans.TerminalImpresionFacade;
import com.exabit.polariserp.tvmax.util.NavegacionManBean;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.Cookie;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author frioss
 */
@Named(value = "sessionManBean")
@SessionScoped
public class SessionManBean implements Serializable {
    
    @Inject
    NavegacionManBean navManBean;

    @EJB
    private TerminalImpresionFacade terminalImpresionFacade;

    @EJB
    private ModuloFacade moduloFacade;

    @EJB
    private FuncionalidadFacade funcionalidadFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

//    public static final String REDIRECCIONAR_INDEX = "/faces/index.xhtml";
//    public static final String REDIRECCIONAR_LOGIN = "/faces/login.xhtml";

    private Funcionario funcionarioVar;
    Funcionalidad funcionalidadPermisoVar = new Funcionalidad();
    private List<Funcionalidad> listafuncionalidadPermisos = new ArrayList<>();
    private List<AuxFuncionalidad> lstAux = new LinkedList();
    private List<Funcionalidad> listFuncionalidad = new LinkedList<>();
    private List<Funcionalidad> listFuncionalidadPermiso = new LinkedList<>();
    private List<Funcionalidad> listFuncionalidadTMP = new LinkedList<>();
    private List<Funcionalidad> listaPermisosAsignados = new ArrayList<>();
    private String modulo;
    private String msg = "Acceso correcto";
//    private boolean isLogged;
    private Integer ci;
    private String pass;
    private Integer validator = 0;
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    private boolean isfunciona;

    /**
     * Creates a new instance of sessionManBean
     */
    public SessionManBean() {
    }

    private String textoBusquedaModulo = "";

    public String getTextoBusquedaModulo() {
        return textoBusquedaModulo;
    }

    public void setTextoBusquedaModulo(String textoBusquedaModulo) {
        this.textoBusquedaModulo = textoBusquedaModulo;
    }

    private String tipoMsg="info";

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public Funcionalidad getFuncionalidadPermisoVar() {
        return funcionalidadPermisoVar;
    }

    public List<Funcionalidad> getListaPermisosAsignados() {
        return listaPermisosAsignados;
    }

    public void setListaPermisosAsignados(List<Funcionalidad> listaPermisosAsignados) {
        this.listaPermisosAsignados = listaPermisosAsignados;
    }

    public void setFuncionalidadPermisoVar(Funcionalidad funcionalidadPermisoVar) {
        this.funcionalidadPermisoVar = funcionalidadPermisoVar;
    }

    public boolean isIsfunciona() {
        return isfunciona;
    }

    public void setIsfunciona(boolean isfunciona) {
        this.isfunciona = isfunciona;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public List<Funcionalidad> getListafuncionalidadPermisos() {
        return listafuncionalidadPermisos;
    }

    public void setListafuncionalidadPermisos(List<Funcionalidad> listafuncionalidadPermisos) {
        this.listafuncionalidadPermisos = listafuncionalidadPermisos;
    }

    public List<AuxFuncionalidad> getLstAux() {
        return lstAux;
    }

    public void setLstAux(List<AuxFuncionalidad> lstAux) {
        this.lstAux = lstAux;
    }

    public List<Funcionalidad> getListFuncionalidad() {
        return listFuncionalidad;
    }

    public void setListFuncionalidad(List<Funcionalidad> listFuncionalidad) {
        this.listFuncionalidad = listFuncionalidad;
    }

    public List<Funcionalidad> getListFuncionalidadPermiso() {
        return listFuncionalidadPermiso;
    }

    public void setListFuncionalidadPermiso(List<Funcionalidad> listFuncionalidadPermiso) {
        this.listFuncionalidadPermiso = listFuncionalidadPermiso;
    }

    public List<Funcionalidad> getListFuncionalidadTMP() {
        return listFuncionalidadTMP;
    }

    public void setListFuncionalidadTMP(List<Funcionalidad> listFuncionalidadTMP) {
        this.listFuncionalidadTMP = listFuncionalidadTMP;
    }

    public Funcionario getFuncionarioVar() {
        return funcionarioVar;
    }

    public void setFuncionarioVar(Funcionario funcionarioVar) {
        this.funcionarioVar = funcionarioVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isIsLogged() {
        return this.funcionarioVar != null;
    }

    public Integer getValidator() {
//        System.out.println("validator: "+validator);
        return validator;
    }

    public void setValidator(Integer validator) {
//         System.out.println("validator: "+validator);
        this.validator = validator;
    }

    public void verificarAcceso() throws NoSuchAlgorithmException, UnsupportedEncodingException, IOException {
        String encryptedPass = DigestUtils.sha256Hex(pass);
        Query q = em.createQuery("SELECT f FROM Funcionario f WHERE f.ci=:ci AND f.password=:pass");
        q.setParameter("ci", ci);
        q.setParameter("pass", encryptedPass);
        List<Funcionario> fLista = q.getResultList();
        if (!fLista.isEmpty()) {
            Funcionario fun = fLista.get(0);
            if (fun.getActivo()) {
                funcionarioVar = fun;
                msg = "Acceso correcto";
                this.tipoMsg = "info";

//                FacesContext.getCurrentInstance().getExternalContext().redirect(REDIRECCIONAR_INDEX);
            } else {
                this.funcionarioVar = null;
                msg = "Usuario desactivado";
                this.tipoMsg = "warning";
            }
        } else {
            this.funcionarioVar = null;
            this.msg = "Credenciales incorrectas";
            this.tipoMsg = "error";
        }
    }

    public String verificarLogueado() throws IOException {
        System.out.println("estado: " + this.isIsLogged());
        if (this.isIsLogged() != true) {
            System.out.println("usuario no logueado");
            FacesContext.getCurrentInstance().getExternalContext().redirect(this.navManBean.getLoginUrl());
        } else {
            return this.navManBean.getIndexUrl();
//            FacesContext.getCurrentInstance().getExternalContext().redirect("/polariserp-web/faces/index.xhtml");
        }
        return this.navManBean.getLoginUrl();
    }

    public void logout() {
        this.ci = null;
        this.pass = null;
        this.funcionarioVar = null;
        this.textoBusquedaModulo = "";
    }

    public void modulosFuncionalidad(Integer idmodulo) throws IOException {
        verificarLogueado();
        if (!this.moduloFacade.isFuncionarioAutorizado(funcionarioVar, idmodulo)) {
            FacesContext.getCurrentInstance().getExternalContext().redirect(this.navManBean.getIndexUrl());
        }
    }

    public boolean permisosAsignados(Integer idfuncionalidadAsignada) {
        return this.funcionalidadFacade.isFuncionarioAutorizado(funcionarioVar, idfuncionalidadAsignada);
    }

    public List<CategoriaModulo> getListaCategoriasModulosAutorizados() {
        System.out.println("Get lista categorias");
        HashMap<Integer, CategoriaModulo> hmCM = new HashMap<>();
        for (Funcionalidad f : this.getFuncionalidades()) {
            hmCM.put(f.getModulo().getCategoriaModulo().getIdcategoriaModulo(), f.getModulo().getCategoriaModulo());
        }
        List<CategoriaModulo> lstCM = new LinkedList<>();
        lstCM.addAll(hmCM.values());
        return lstCM;
    }

    public List<Modulo> getListaModulosAutorizados(Integer categoria) {
        HashMap<Integer, Modulo> hmMod = new HashMap<>();
        for (Funcionalidad f : this.getFuncionalidades()) {
            if (f.getModulo().getCategoriaModulo().getIdcategoriaModulo().equals(categoria)) {
                hmMod.put(f.getModulo().getIdmodulo(), f.getModulo());
            }
        }
        List<Modulo> lstm = new ArrayList<>();
        lstm.addAll(hmMod.values());
        return lstm;
    }

    private List<Funcionalidad> getFuncionalidades() {
        System.out.println("Get funcionaliddades");
        System.out.println("Funcionario logueado: "+this.funcionarioVar);
        System.out.println("Funcionario logueado: "+this.funcionarioVar.getIdfuncionario()+" "+this.funcionarioVar.getNombres());
        if (this.textoBusquedaModulo.isEmpty()) {
            List<Funcionalidad> lstfunc=this.funcionalidadFacade.getFuncionalidades(funcionarioVar);
            System.out.println("Cantiddad de funcionalidades: "+lstfunc.size());
            return lstfunc;
        } else {
            return this.funcionalidadFacade.getFuncionalidadesBusquedaModulo(funcionarioVar, this.textoBusquedaModulo);
        }
    }

    public void verificarTerminalImpresion() {
        System.out.println("Verificar galleta");
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        if (requestCookieMap.get("polariserp-idterminal") != null) {
            Cookie cid = (Cookie) requestCookieMap.get("polariserp-idterminal");
            Cookie ctm = (Cookie) requestCookieMap.get("polariserp-asignacion-timestamp");
            Integer idterminal = Integer.parseInt(cid.getValue());
            Long timeStampAsignacion = Long.parseLong(ctm.getValue());
            Calendar calAsignacion = new GregorianCalendar();
            calAsignacion.setTimeInMillis(timeStampAsignacion);
            TerminalImpresion t = this.terminalImpresionFacade.find(idterminal);
            boolean eliminarAsignacion;
            if (t != null) {
                Calendar calAsignacionBd = new GregorianCalendar();

                if (t.getFechaAsignacion() != null) {
                    calAsignacionBd.setTime(t.getFechaAsignacion());
                    eliminarAsignacion = calAsignacionBd.after(calAsignacion);
                } else {
                    eliminarAsignacion = true;
                }
            } else {
                eliminarAsignacion = true;
            }
            if (eliminarAsignacion) {
                System.out.println("Galletas de terminal se borran");
                Map<String, Object> pars = new HashMap<>();
                pars.put("maxAge", 0);
                FacesContext.getCurrentInstance().getExternalContext().addResponseCookie("polariserp-idterminal", "-1", pars);
                FacesContext.getCurrentInstance().getExternalContext().addResponseCookie("polariserp-asignacion-timestamp", "-1", pars);
            }
        }
    }
}
