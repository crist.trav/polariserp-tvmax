/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 * @param <T>
 */
public class Paginador<T> implements Serializable {

    private final List<T> lista = new ArrayList<>();
    private final List<T> listaPaginada;
    private int cantidadRegistrosPorPagina = 20;
    private int paginaActual = 1;
    private int cantidadPaginasBloque = 5;
    private int posInicioBloquePaginador = 1;

    public Paginador(List<T> lst) {
        listaPaginada = lst;
    }

    public int getCantidadPaginasBloque() {
        return cantidadPaginasBloque;
    }

    public void setCantidadPaginasBloque(int cantidadPaginasBloque) {
        this.cantidadPaginasBloque = cantidadPaginasBloque;
    }

    public void agregarItemsLista(List<T> lst) {
        this.lista.addAll(lst);
        this.listaPaginada.clear();
        this.listaPaginada.addAll(this.getRegistrosPaginaActual());
    }

    public void limpiarLista() {
        this.lista.clear();
        this.setPaginaActual(1);
        this.listaPaginada.clear();
    }

    public int getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
        this.listaPaginada.clear();
        this.listaPaginada.addAll(this.getRegistrosPaginaActual());
    }

    public int getCantidadRegistrosPorPagina() {
        return cantidadRegistrosPorPagina;
    }

    public void setCantidadRegistrosPorPagina(int cantidadRegistrosPorPagina) {
        this.cantidadRegistrosPorPagina = cantidadRegistrosPorPagina;
        this.listaPaginada.clear();
        this.listaPaginada.addAll(this.getRegistrosPaginaActual());
    }

    private List<Integer> getNumeracionDePaginas() {
        double resto=((double)this.lista.size())%((double)this.cantidadRegistrosPorPagina);
        int cantPag = this.lista.size() / this.cantidadRegistrosPorPagina;
        if(resto>0){
            cantPag++;
        }
        List<Integer> lstpaginas = new ArrayList<>();
        if (cantPag > 0) {
            for (int i = 1; i <= cantPag; i++) {
                lstpaginas.add(i);
            }
        } else {
            lstpaginas.add(1);
        }
        return lstpaginas;
    }

    public List<T> getRegistrosPaginaActual() {
        if (!this.lista.isEmpty()) {
            int posinicio = (this.paginaActual - 1) * this.cantidadRegistrosPorPagina;
            int posfin = ((this.paginaActual - 1) * this.cantidadRegistrosPorPagina) + this.cantidadRegistrosPorPagina;
            if (posfin > this.lista.size()) {
                posfin = this.lista.size();
            }
            return this.lista.subList(posinicio, posfin);
        } else {
            return this.lista;
        }
    }

    public List<Integer> getBloqueNumeracionPaginas() {
        if (this.getNumeracionDePaginas().size() <= this.cantidadPaginasBloque) {
            return this.getNumeracionDePaginas();
        } else {
            int posinicio=this.posInicioBloquePaginador - 1;
            int posfin=(this.posInicioBloquePaginador - 1) + this.cantidadPaginasBloque;
            if(posfin>this.getNumeracionDePaginas().size()){
                posfin=this.getNumeracionDePaginas().size();
            }
            return this.getNumeracionDePaginas().subList(posinicio, posfin);
        }
    }

    public void desplazarDerechaPaginador() {
        int tmpNuevaPosicion = this.posInicioBloquePaginador + this.cantidadPaginasBloque;
        if (tmpNuevaPosicion <= this.getNumeracionDePaginas().size()) {
            this.posInicioBloquePaginador = tmpNuevaPosicion;
        }
    }

    public void desplazarIzquierdaPaginador() {
        int tmpNuevaPosicion = this.posInicioBloquePaginador - this.cantidadPaginasBloque;
        if (tmpNuevaPosicion >= 1) {
            this.posInicioBloquePaginador = tmpNuevaPosicion;
        }
    }

    public boolean isDesplazableDerecha() {
        int tmpNuevaPosicion = this.posInicioBloquePaginador + this.cantidadPaginasBloque;
        return tmpNuevaPosicion <= this.getNumeracionDePaginas().size();
    }

    public boolean isDesplazableIzquierda() {
        int tmpNuevaPosicion = this.posInicioBloquePaginador - this.cantidadPaginasBloque;
        return tmpNuevaPosicion >= 1;
    }

    public int getTotalRegistros() {
        return this.lista.size();
    }

    public int getPosicionInicio() {
        if(this.lista.isEmpty()){
            return 0;
        }else{
            return ((this.paginaActual - 1) * this.cantidadRegistrosPorPagina)+1;
        }
    }

    public int getPosicionFin() {
        int posfin = ((this.paginaActual - 1) * this.cantidadRegistrosPorPagina) + this.cantidadRegistrosPorPagina;
        if (posfin > this.lista.size()) {
            posfin = this.lista.size();
        }
        return posfin;
    }
    
    public boolean isEmpty(){
        return this.lista.isEmpty();
    }
    
    public List<T> getListaCompleta(){
        return this.lista;
    }
}
