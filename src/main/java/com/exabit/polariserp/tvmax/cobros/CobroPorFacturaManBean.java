package com.exabit.polariserp.tvmax.cobros;

import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;

/**
 *
 * @author traver
 */
@Named(value = "cobroPorFacturaManBean")
@SessionScoped
public class CobroPorFacturaManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(CobroPorFacturaManBean.class.getName());
    
    @Inject
    private SessionManBean sesManBean;

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private FacturaFacade facturaFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private final List<Factura> lstFacturasCobrar = new ArrayList<>();
    private Integer nroFacturaBuscar;
    private String tipoMsg = "info";
    private String cabeceraMsg = "x";
    private String cuerpoMsg = "y";
    private boolean cobroRegistrado = false;
    private String strFechaCobro;
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public boolean isCobroRegistrado() {
        return cobroRegistrado;
    }

    public void setCobroRegistrado(boolean cobroRegistrado) {
        this.cobroRegistrado = cobroRegistrado;
    }

    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public List<Factura> getLstFacturasCobrar() {
        return lstFacturasCobrar;
    }

    public Integer getNroFacturaBuscar() {
        return nroFacturaBuscar;
    }

    public void setNroFacturaBuscar(Integer nroFacturaBuscar) {
        this.nroFacturaBuscar = nroFacturaBuscar;
    }

    public String getStrFechaCobro() {
        return strFechaCobro;
    }

    public void setStrFechaCobro(String strFechaCobro) {
        this.strFechaCobro = strFechaCobro;
    }
    //</editor-fold>

    /**
     * Creates a new instance of CobroPorFacturaManBean
     */
    public CobroPorFacturaManBean() {
        this.strFechaCobro = this.sdf.format(new Date());
    }

    public void guardarCobros() {
        try {
            this.facturaFacade.registrarCobroFacturas(lstFacturasCobrar, this.sdf.parse(this.strFechaCobro), this.sesManBean.getFuncionarioVar());
            this.cobroRegistrado = true;
            this.cabeceraMsg = "Éxito";
            this.tipoMsg = "info";
            this.cuerpoMsg = "Guardados cobros de " + this.lstFacturasCobrar.size() + " facturas";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al registrar cobros", ex);
            this.cobroRegistrado = true;
            this.cabeceraMsg = "Error al guardar cobros";
            this.tipoMsg = "error";
            this.cuerpoMsg = ex.getMessage();
        }

    }

    public void quitarFactura(int index) {
        this.lstFacturasCobrar.remove(index);
    }

    public void agregarFactura() {
        Factura f = this.facturaFacade.getFacturaPendientePorNro(this.nroFacturaBuscar);
        if (f != null) {
            boolean existe = false;
            for (Factura fa : this.lstFacturasCobrar) {
                if (fa.getIdfactura().equals(f.getIdfactura())) {
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                this.lstFacturasCobrar.add(0, f);
                this.cabeceraMsg = "Éxito";
                this.cuerpoMsg = "Factura agregada correctamente";
                this.tipoMsg = "info";
            } else {
                this.cabeceraMsg = "Error al agregar";
                this.cuerpoMsg = "La factura ya se encuentra en la lista";
                this.tipoMsg = "error";
            }
        } else {
            this.cabeceraMsg = "Error al agregar";
            this.cuerpoMsg = "No se encuentra ninguna factura pendiente con número " + this.nroFacturaBuscar;
            this.tipoMsg = "error";
        }
    }

    public void limpiar() {
        this.cobroRegistrado = false;
        this.lstFacturasCobrar.clear();
        this.nroFacturaBuscar = null;
        this.strFechaCobro = this.sdf.format(new Date());
        System.out.println("Limpiar");
    }

}
