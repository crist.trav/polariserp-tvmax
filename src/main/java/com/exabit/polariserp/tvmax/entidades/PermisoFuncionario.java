/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "permiso_funcionario")
@NamedQueries({
    @NamedQuery(name = "PermisoFuncionario.findAll", query = "SELECT p FROM PermisoFuncionario p")})
public class PermisoFuncionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PermisoFuncionarioPK permisoFuncionarioPK;
    @JoinColumn(name = "funcionalidad", referencedColumnName = "idfuncionalidad", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Funcionalidad funcionalidad1;
    @JoinColumn(name = "funcionario", referencedColumnName = "idfuncionario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Funcionario funcionario1;

    public PermisoFuncionario() {
    }

    public PermisoFuncionario(PermisoFuncionarioPK permisoFuncionarioPK) {
        this.permisoFuncionarioPK = permisoFuncionarioPK;
    }

    public PermisoFuncionario(int funcionario, int funcionalidad) {
        this.permisoFuncionarioPK = new PermisoFuncionarioPK(funcionario, funcionalidad);
    }

    public PermisoFuncionarioPK getPermisoFuncionarioPK() {
        return permisoFuncionarioPK;
    }

    public void setPermisoFuncionarioPK(PermisoFuncionarioPK permisoFuncionarioPK) {
        this.permisoFuncionarioPK = permisoFuncionarioPK;
    }

    public Funcionalidad getFuncionalidad1() {
        return funcionalidad1;
    }

    public void setFuncionalidad1(Funcionalidad funcionalidad1) {
        this.funcionalidad1 = funcionalidad1;
    }

    public Funcionario getFuncionario1() {
        return funcionario1;
    }

    public void setFuncionario1(Funcionario funcionario1) {
        this.funcionario1 = funcionario1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permisoFuncionarioPK != null ? permisoFuncionarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermisoFuncionario)) {
            return false;
        }
        PermisoFuncionario other = (PermisoFuncionario) object;
        if ((this.permisoFuncionarioPK == null && other.permisoFuncionarioPK != null) || (this.permisoFuncionarioPK != null && !this.permisoFuncionarioPK.equals(other.permisoFuncionarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.PermisoFuncionario[ permisoFuncionarioPK=" + permisoFuncionarioPK + " ]";
    }
    
}
