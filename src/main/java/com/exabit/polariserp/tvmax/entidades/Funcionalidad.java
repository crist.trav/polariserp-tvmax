/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "funcionalidad")
@NamedQueries({
    @NamedQuery(name = "Funcionalidad.findAll", query = "SELECT f FROM Funcionalidad f")
    , @NamedQuery(name = "Funcionalidad.findByIdfuncionalidad", query = "SELECT f FROM Funcionalidad f WHERE f.idfuncionalidad = :idfuncionalidad")
    , @NamedQuery(name = "Funcionalidad.findByNombre", query = "SELECT f FROM Funcionalidad f WHERE f.nombre = :nombre")})
public class Funcionalidad implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionalidad1")
    private List<PermisoFuncionario> permisoFuncionarioList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idfuncionalidad")
    private Integer idfuncionalidad;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @JoinTable(name = "permiso_funcionario", joinColumns = {
        @JoinColumn(name = "funcionalidad", referencedColumnName = "idfuncionalidad")}, inverseJoinColumns = {
        @JoinColumn(name = "funcionario", referencedColumnName = "idfuncionario")})
    @ManyToMany
    private List<Funcionario> funcionarioList;
    @JoinColumn(name = "modulo", referencedColumnName = "idmodulo")
    @ManyToOne
    private Modulo modulo;

    public Funcionalidad() {
    }

    public Funcionalidad(Integer idfuncionalidad) {
        this.idfuncionalidad = idfuncionalidad;
    }

    public Integer getIdfuncionalidad() {
        return idfuncionalidad;
    }

    public void setIdfuncionalidad(Integer idfuncionalidad) {
        this.idfuncionalidad = idfuncionalidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Funcionario> getFuncionarioList() {
        return funcionarioList;
    }

    public void setFuncionarioList(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfuncionalidad != null ? idfuncionalidad.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionalidad)) {
            return false;
        }
        Funcionalidad other = (Funcionalidad) object;
        if ((this.idfuncionalidad == null && other.idfuncionalidad != null) || (this.idfuncionalidad != null && !this.idfuncionalidad.equals(other.idfuncionalidad))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Funcionalidad[ idfuncionalidad=" + idfuncionalidad + " ]";
    }

    public List<PermisoFuncionario> getPermisoFuncionarioList() {
        return permisoFuncionarioList;
    }

    public void setPermisoFuncionarioList(List<PermisoFuncionario> permisoFuncionarioList) {
        this.permisoFuncionarioList = permisoFuncionarioList;
    }
    
}
