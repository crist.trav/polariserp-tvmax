/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "cuotatemporal")
@NamedQueries({
    @NamedQuery(name = "Cuotatemporal.findAll", query = "SELECT c FROM Cuotatemporal c")
    , @NamedQuery(name = "Cuotatemporal.findByIdcuota", query = "SELECT c FROM Cuotatemporal c WHERE c.idcuota = :idcuota")})
public class Cuotatemporal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuota")
    private Integer idcuota;

    public Cuotatemporal() {
    }

    public Cuotatemporal(Integer idcuota) {
        this.idcuota = idcuota;
    }

    public Integer getIdcuota() {
        return idcuota;
    }

    public void setIdcuota(Integer idcuota) {
        this.idcuota = idcuota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcuota != null ? idcuota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuotatemporal)) {
            return false;
        }
        Cuotatemporal other = (Cuotatemporal) object;
        if ((this.idcuota == null && other.idcuota != null) || (this.idcuota != null && !this.idcuota.equals(other.idcuota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Cuotatemporal[ idcuota=" + idcuota + " ]";
    }
    
}
