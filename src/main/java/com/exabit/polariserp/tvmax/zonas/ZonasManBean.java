/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.zonas;

import com.exabit.polariserp.tvmax.entidades.Zona;
import com.exabit.polariserp.tvmax.sesbeans.ZonaFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "zonasManBean")
@ViewScoped
public class ZonasManBean implements Serializable {
    
    private String txtBusqueda;

    public String getTxtBusqueda() {
        return txtBusqueda;
    }

    public void setTxtBusqueda(String txtBusqueda) {
        this.txtBusqueda = txtBusqueda;
    }

    private List<Zona> lstZonas=new ArrayList<>();

    public List<Zona> getLstZonas() {
        return lstZonas;
    }

    public void setLstZonas(List<Zona> lstZonas) {
        this.lstZonas = lstZonas;
    }


    @EJB
    private ZonaFacade zonaFacade;
    
    private String selElimIDJSON;

    public String getSelElimIDJSON() {
        return selElimIDJSON;
    }

    public void setSelElimIDJSON(String selElimIDJSON) {
        this.selElimIDJSON = selElimIDJSON;
    }

    
    private Zona zonaVar = new Zona();

    public Zona getZonaVar() {
        return zonaVar;
    }

    public void setZonaVar(Zona zonaVar) {
        this.zonaVar = zonaVar;
    }
    
        private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


    /**
     * Creates a new instance of ZonasManBean
     */
    public ZonasManBean() {
    }
    
    @PostConstruct
    private void postConstruct(){
        System.out.println("Postconstruct");
        this.lstZonas.addAll(this.zonaFacade.findAll());
    }

    public void agregarZona(){
        System.out.println("Se creara zona: "+this.zonaVar.getIdzona()+" "+this.zonaVar.getNombre());
        this.zonaFacade.create(zonaVar);
        this.msg="Zona registrada con éxito: "+this.zonaVar.getIdzona()+"-"+this.zonaVar.getNombre();
        this.recargarZonas();
    }

    
    public void eliminarZonas(){
        Gson gson=new Gson();
        Integer[] idsEliminar=gson.fromJson(this.selElimIDJSON, Integer[].class);
        for(Integer id:idsEliminar){
            System.out.println("eliminar: "+id);
            Zona zElim=this.zonaFacade.find(id);
            this.zonaFacade.remove(zElim);
            this.recargarZonas();
        }
    }
    
    public void modificarZona(){
        System.out.println("Modificando zona: "+this.zonaVar.getIdzona()+" "+this.zonaVar.getNombre());
        this.zonaFacade.edit(zonaVar);
        this.msg="Guardado con éxito: "+this.zonaVar.getIdzona()+"-"+this.zonaVar.getNombre();
        for(Zona z:this.lstZonas){
            if(z.getIdzona().equals(zonaVar.getIdzona())){
                int indiceZona=this.lstZonas.indexOf(z);
                this.lstZonas.remove(z);
                this.lstZonas.add(indiceZona, this.zonaVar);
                break;
            }
        }
    }
    
    private void recargarZonas(){
        this.lstZonas.clear();
        this.lstZonas.addAll(this.zonaFacade.findAll());
    }
    
    public void buscarZona(){
        System.out.println("Se busca zona: "+this.txtBusqueda);
        this.lstZonas.clear();
        if(this.txtBusqueda.isEmpty()){
            this.lstZonas.addAll(this.zonaFacade.findAll());
        }else{
            this.lstZonas.addAll(this.zonaFacade.buscarZona(this.txtBusqueda));
        }
    }
    
}
