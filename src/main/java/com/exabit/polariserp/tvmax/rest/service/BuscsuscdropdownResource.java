/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.service;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.google.gson.Gson;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cristhian-kn
 */
@Path("buscsuscdropdown")
public class BuscsuscdropdownResource {
    
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of BuscsuscdropdownResource
     */
    public BuscsuscdropdownResource() {
    }

    /**
     * Retrieves representation of an instance of com.polariserp.web.rest.service.BuscsuscdropdownResource
     * @param q
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText(@QueryParam("q") String q) {
        System.out.println("REST dropdown llamado: "+q);
        TypedQuery<Suscripcion> query=this.em.createQuery("SELECT s FROM Suscripcion s WHERE SQL('CAST(? AS CHAR(11))', s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.idcliente) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx", Suscripcion.class);
//        TypedQuery<Suscripcion> query=this.em.createQuery("SELECT s FROM Suscripcion s WHERE SQL('CAST(? AS CHAR(11))', s.idsuscripcion) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.idcliente) OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx", Suscripcion.class);
        query.setParameter("tx", "%"+q.toLowerCase()+"%");
        Gson gson=new Gson();
        ResultBusquedaSuscDropdown rs=new ResultBusquedaSuscDropdown();
        List<SuscripcionBasicDropdown> lstSuscBas=new LinkedList<>();
        List<Suscripcion> lstSusc=query.setMaxResults(100).getResultList();
        for(Suscripcion s:lstSusc){
            lstSuscBas.add(new SuscripcionBasicDropdown(s));
        }
        System.out.println("cantidad resultados: "+lstSuscBas.size());
        rs.setLstSuscripciones(lstSuscBas);
        return gson.toJson(rs);
    }

    /**
     * PUT method for updating or creating an instance of BuscsuscdropdownResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
