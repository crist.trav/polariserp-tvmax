package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.ConceptoFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "nuevaFacturaManBean")
@ViewScoped
public class NuevaFacturaManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(NuevaFacturaManBean.class.getName());

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @Inject
    SessionManBean sessionManBean;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private ConceptoFacade conceptoFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private ClienteFacade clienteFacade;

    private Integer tmpIdTalonario;

    public Integer getTmpIdTalonario() {
        return tmpIdTalonario;
    }

    public void setTmpIdTalonario(Integer tmpIdTalonario) {
        this.tmpIdTalonario = tmpIdTalonario;
    }

    private Integer tmpIdTerminal;

    public Integer getTmpIdTerminal() {
        return tmpIdTerminal;
    }

    public void setTmpIdTerminal(Integer tmpIdTerminal) {
        this.tmpIdTerminal = tmpIdTerminal;
    }

    private Integer dvRuc;

    public Integer getDvRuc() {
        return dvRuc;
    }

    public void setDvRuc(Integer dvRuc) {
        this.dvRuc = dvRuc;
    }

    private Integer idTalonarioSelNf;

    public Integer getIdTalonarioSelNf() {
        return idTalonarioSelNf;
    }

    public void setIdTalonarioSelNf(Integer idTalonarioSelNf) {
        this.idTalonarioSelNf = idTalonarioSelNf;
    }

    private String razonSocialNuevo;

    public String getRazonSocialNuevo() {
        return razonSocialNuevo;
    }

    public void setRazonSocialNuevo(String razonSocialNuevo) {
        this.razonSocialNuevo = razonSocialNuevo;
    }

    private Integer nroCiNuevo;

    public Integer getNroCiNuevo() {
        return nroCiNuevo;
    }

    public void setNroCiNuevo(Integer nroCiNuevo) {
        this.nroCiNuevo = nroCiNuevo;
    }

    private String fechaFactStr;

    public String getFechaFactStr() {
        return fechaFactStr;
    }

    public void setFechaFactStr(String fechaFactStr) {
        this.fechaFactStr = fechaFactStr;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private Integer idsuscripcion;

    public Integer getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;

    }

    private Factura facturaVar;

    public Factura getFacturaVar() {
        return facturaVar;
    }

    public void setFacturaVar(Factura facturaVar) {
        this.facturaVar = facturaVar;
    }

    private List<Cuota> lstCuotasCli = new ArrayList<>();

    public List<Cuota> getLstCuotasCli() {
        return lstCuotasCli;
    }

    public void setLstCuotasCli(List<Cuota> lstCuotasCli) {
        this.lstCuotasCli = lstCuotasCli;
    }

    private List<Servicio> lstServicios = new LinkedList<>();

    public List<Servicio> getLstServicios() {
        return lstServicios;
    }

    public void setLstServicios(List<Servicio> lstServicios) {
        this.lstServicios = lstServicios;
    }

    private String tipoCliente;

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    private Integer idCobradorNF;

    public Integer getIdCobradorNF() {
        return idCobradorNF;
    }

    public void setIdCobradorNF(Integer idCobradorNF) {
        this.idCobradorNF = idCobradorNF;
    }

    /**
     * Creates a new instance of NuevaFacturaManBean
     */
    public NuevaFacturaManBean() {
        this.facturaVar = new Factura();
        this.facturaVar.setDetalleFacturaList(new LinkedList<DetalleFactura>());
        this.fechaFactStr = this.sdf.format(new Date());
        this.tipoCliente = "e";

    }

    @PostConstruct
    private void postConstruct() {
        limpiarNuevaFactura();
//        this.idCobradorNF = this.sessionManBean.getFuncionarioVar().getIdfuncionario();
//        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();
//        if (requestCookieMap.get("polariserp-idterminal") != null) {
//            Cookie cid = (Cookie) requestCookieMap.get("polariserp-idterminal");
//            TalonarioFactura t = this.talonarioFacturaFacade.getTalonarioPorIdTerminal(Integer.parseInt(cid.getValue()));
//            if (t != null) {
//                this.idTalonarioSelNf = t.getIdtalonario();
//            } else {
        TalonarioFactura tf = this.talonarioFacturaFacade.getTalonarioActivo();
        if (tf != null) {
            this.idTalonarioSelNf = this.talonarioFacturaFacade.getTalonarioActivo().getIdtalonario();
        } else {
            this.idTalonarioSelNf = -1;
        }
//            }
//        } else {
//            System.out.println("Galleta null en nueva factura :(");
//        }
        this.seleccionarTalonarioNf();
    }

    public void seleccionarSuscripcion() {
        System.out.println("seleccion de cliente");
        this.lstCuotasCli.clear();
        this.lstServicios.clear();
        this.lstServicios.addAll(this.servicioFacade.getOtrosCobros());
        this.facturaVar.getDetalleFacturaList().clear();
        if (this.idsuscripcion != null) {
            Suscripcion s = this.suscripcionFacade.find(this.idsuscripcion);
            this.lstCuotasCli.addAll(this.cuotaFacade.getCuotasPendientes(s));
            EstadoSuscripcion es = s.getEstado();
            System.out.println("El estado es " + es);
            if (es.equals(EntidadesEstaticas.ESTADO_DESCONECTADO) || es.equals(EntidadesEstaticas.ESTADO_RECONECTADO)) {
                System.out.println("reconexion");
//                this.lstServicios.add(this.servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_RECONEXION));
                this.agregarServicio(EntidadesEstaticas.ID_SERVICIO_RECONEXION);
                this.generarCuotaMesActual();
            } else {
                System.out.println("No coincide como descectado");
            }
            this.idCobradorNF = s.getCobrador().getIdfuncionario();
            System.out.println("Nuevo cobrador seleccionado para suscripcion:  " + this.idCobradorNF);
        }
        System.out.println("Elementos en tabla servicio: " + this.lstServicios.size());
    }

    public void agregarCuotaPorIndice(int index) {
        System.out.println("Indice de cuota a agregar");
        Cuota c = this.lstCuotasCli.get((int) index);
        if (!this.existeFacturaPendiente(c.getIdcuota())) {
            System.out.println("Cuota encontrada");
            DetalleFactura df = new DetalleFactura();
            df.setFactura(this.facturaVar);
            df.setCantidad(1);
            df.setServicio(servicioFacade.find(1));
            df.setDescripcion("Cuota mes de " + EntidadesEstaticas.getNombreMes(c.getMesCuota()) + " " + c.getAnioCuota());
            if (c.getMontoEntrega() != null) {
                df.setMonto(c.getMontoCuota() - c.getMontoEntrega());
            } else {
                df.setMonto(c.getMontoCuota());
            }
            df.setSubtotal(df.getMonto() * df.getCantidad());
            df.setCuota(c);
            this.facturaVar.getDetalleFacturaList().add(df);
            this.lstCuotasCli.remove(index);

            this.calcularTotalFactura();
        }
    }

    public void agregarTodasLasCuotas() {
        int totalitems = this.lstCuotasCli.size();
        for (int i = 0; i < totalitems; i++) {
            this.agregarCuotaPorIndice(0);
        }
    }

    public void agregarCuota(Cuota c) {
        System.out.println("Agregar cuota");

        System.out.println("Cuota encontrada");
        DetalleFactura df = new DetalleFactura();
        df.setFactura(this.facturaVar);
        df.setCantidad(1);
        df.setServicio(servicioFacade.find(1));
        df.setDescripcion("Cuota mes de " + EntidadesEstaticas.getNombreMes(c.getMesCuota()) + " " + c.getAnioCuota());
        df.setMonto(c.getMontoCuota());
        df.setSubtotal(df.getMonto() * df.getCantidad());
        df.setCuota(c);
        this.facturaVar.getDetalleFacturaList().add(df);

        this.calcularTotalFactura();
    }

    public void quitarDetalleFactura(int index) {
        DetalleFactura df = this.facturaVar.getDetalleFacturaList().get(index);
        if (df != null) {
            if (df.getCantidad() > 1) {
                df.setCantidad(df.getCantidad() - 1);
                df.setSubtotal(df.getCantidad() * df.getMonto());
            } else {
                if (df.getCuota() != null) {
                    this.lstCuotasCli.add(df.getCuota());
                }
                this.facturaVar.getDetalleFacturaList().remove(index);
            }
        }
        this.calcularTotalFactura();
    }

    public void agregarServicio(int idserv) {
        System.out.println("agregar servicio");
        Servicio se = this.servicioFacade.find(idserv);
        boolean existeServicio = false;
        for (DetalleFactura d : this.facturaVar.getDetalleFacturaList()) {
            if (d.getServicio().getIdservicio().equals(idserv)) {
                d.setCantidad(d.getCantidad() + 1);
                d.setSubtotal(d.getMonto() * d.getCantidad());
                existeServicio = true;
                break;
            }
        }
        if (!existeServicio) {
            DetalleFactura df = new DetalleFactura();
            df.setFactura(this.facturaVar);
            df.setCantidad(1);
            df.setServicio(servicioFacade.find(idserv));
            df.setDescripcion(se.getNombre());
            df.setMonto(se.getPrecio());
            df.setSubtotal(df.getMonto() * df.getCantidad());
            this.facturaVar.getDetalleFacturaList().add(df);
        }
        this.calcularTotalFactura();
    }

    private void calcularTotalFactura() {
        Integer tot = 0;
        Integer totiva5 = 0;
        Integer totiva10 = 0;
        for (DetalleFactura df : this.facturaVar.getDetalleFacturaList()) {
            tot = tot + df.getSubtotal();
            if (df.getServicio().getPorcentajeIva() == 10) {
                totiva10 = totiva10 + (int) Math.round((((double) df.getSubtotal()) * 10) / ((double) 110));
            } else if (df.getServicio().getPorcentajeIva() == 5) {
                totiva5 = totiva5 + (int) Math.round((((double) df.getSubtotal()) * 5) / ((double) 105));
            }
        }
        this.facturaVar.setIva10(totiva10);
        this.facturaVar.setIva5(totiva5);
        this.facturaVar.setTotal(tot);
    }

    public void guardarNuevaFactura() {
        System.out.println("guardar nueva  factura, funcionario logueado: " + this.sessionManBean.getFuncionarioVar());
        try {
            this.facturaVar.setFecha(this.sdf.parse(this.fechaFactStr));
        } catch (ParseException ex) {
            System.out.println("Error al convertir fecha de string a date: " + ex);
        }
        this.facturaVar.setTalonarioFactura(this.talonarioFacturaFacade.find(this.idTalonarioSelNf));
        if (this.tipoCliente.equals("n")) {
            System.out.println("guardar nuevo cliente");

            Cliente cli = new Cliente();
            cli.setRazonSocial(this.razonSocialNuevo);
            cli.setCi(this.nroCiNuevo);
            cli.setDvRuc(this.dvRuc.shortValue());
            cli.setBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO);
            cli.setCategoria(EntidadesEstaticas.CATEGORIA_CLIENTE_SIN_CATEGORIA);
            this.facturaVar.setCliente(cli);

            this.facturaFacade.registrarFacturaNuevaConexion(this.facturaVar, this.funcionarioFacade.find(this.idCobradorNF), this.sessionManBean.getFuncionarioVar());
        } else {
            System.out.println("guardar nueva factura cliente existente: " + this.sessionManBean);
            this.facturaVar.setCliente(this.suscripcionFacade.find(this.idsuscripcion).getCliente());

            this.facturaFacade.registrarNuevaFacturaExistente(facturaVar, this.funcionarioFacade.find(this.idCobradorNF), this.suscripcionFacade.find(this.idsuscripcion), this.sessionManBean.getFuncionarioVar());

        }

        this.msg = "Se registró exitosamente factura con código: " + this.facturaVar.getIdfactura();

    }

    public void limpiarNuevaFactura() {
        System.out.println("Limpiar nueva factura");
        this.facturaVar.setIdfactura(null);
        this.facturaVar.setAnulado(false);
        this.facturaVar.setCancelado(false);
        this.facturaVar.setContado(true);
        this.facturaVar.setFecha(new Date());
        this.facturaVar.setIva10(0);
        this.facturaVar.setIva5(0);
//        TalonarioFactura t = this.talonarioFacturaFacade.getTalonarioActivo();
//
//        if (t != null) {
//            this.idTalonarioSelNf = t.getIdtalonario();
//            this.facturaVar.setTalonarioFactura(t);
//            this.facturaVar.setNroFactura(t.getNroActual());
//        }
        this.facturaVar.setTotal(0);
        this.facturaVar.getDetalleFacturaList().clear();
        this.lstCuotasCli.clear();
        this.idsuscripcion = null;
        this.msg = "";
        this.idCobradorNF = this.sessionManBean.getFuncionarioVar().getIdfuncionario();
        System.out.println("cobrador logueado seleccionado: " + this.idCobradorNF);
    }

    public void imprimirNuevaFactura() {
        this.generarPDFPendSeleccionado();
    }

    public void generarPDFPendSeleccionado() {
//        String rutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
        HashMap<Integer, String> hmCobrador = new HashMap<>();
        for (DetalleFactura df : this.facturaVar.getDetalleFacturaList()) {
            if (df.getCuota() != null) {
                Funcionario cobrador = df.getCuota().getSuscripcion().getCobrador();
                String cobra = cobrador.getNombres();
                if (cobrador.getApellidos() != null) {
                    cobra = cobra + " " + cobrador.getApellidos();
                }
                hmCobrador.put(this.facturaVar.getIdfactura(), cobra);
            }
        }
        List<Factura> lstFacturasGeneradas = new ArrayList<>();
        lstFacturasGeneradas.add(this.facturaVar);
        List<JasperPrint> lstJP = UtilReportes.generarJPListFacturas(lstFacturasGeneradas, hmCobrador);
        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=facturas.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
        }
    }

    public void seleccionTipoCliente() {
        System.out.println("Seleccion tipo cliente: " + this.tipoCliente);
        this.lstServicios.clear();
        this.lstServicios.addAll(this.servicioFacade.getOtrosCobros());
        this.limpiarNuevaFactura();
        if (this.tipoCliente.equals("n")) {
            this.lstServicios.add(this.servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_CONEXION));
            this.agregarServicio(EntidadesEstaticas.ID_SERVICIO_CONEXION);
            this.generarCuotaMesActual();
        } else {
            this.limpiarNuevaFactura();
        }
//        this.lstCuotasCli.add(c);
    }

    private void generarCuotaMesActual() {
        Cuota c = new Cuota();
        c.setMontoCuota(this.servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_TVCABLE).getPrecio());
        Calendar ahora = new GregorianCalendar();
        c.setCancelado(true);
        c.setFechaPago(ahora.getTime());
        Calendar vencimiento = new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        vencimiento.add(Calendar.MONTH, 1);

        c.setFechaVencimiento(vencimiento.getTime());
        c.setMesCuota((short) (ahora.get(Calendar.MONTH) + 1));
        c.setAnioCuota((short) ahora.get(Calendar.YEAR));
        c.setMontoEntrega(c.getMontoCuota());
        this.agregarCuota(c);
        if (this.idsuscripcion != null) {
            c.setSuscripcion(this.suscripcionFacade.find(this.idsuscripcion));
        }
    }

    public List<TalonarioFactura> getLstTalonariosActivos() {
        return this.talonarioFacturaFacade.getTalonariosDisponibles();
    }

    public void seleccionarTalonarioNf() {
        if (this.idTalonarioSelNf == null) {
            this.idTalonarioSelNf = this.talonarioFacturaFacade.getTalonarioActivo().getIdtalonario();
        }
        System.out.println("Seleccion de nuevo talonario id: " + this.idTalonarioSelNf);

        if (this.idTalonarioSelNf != -1) {
            this.facturaVar.setTalonarioFactura(this.talonarioFacturaFacade.find(this.idTalonarioSelNf));
            this.facturaVar.setNroFactura(this.facturaVar.getTalonarioFactura().getNroActual());
        }
        System.out.println("nuevo numero: " + this.facturaVar.getNroFactura());
    }

    public void editarDetalleFactura() {
        System.out.println("Editar detalle factura");
        for (DetalleFactura df : this.facturaVar.getDetalleFacturaList()) {
            df.setSubtotal(df.getCantidad() * df.getMonto());
            System.out.println("Monto: " + df.getMonto());
        }
        this.calcularTotalFactura();
    }

    public List<Funcionario> getLstCobradores() {
        return this.funcionarioFacade.getCobradoresActivos();
    }

    public String getNroFacturaPendiente(Integer idCuota) {
        if (idCuota != null) {
            Factura f = this.facturaFacade.getFacturaPendientePorCuota(idCuota);
            String nroFactura = "";
            if (f != null) {
                DecimalFormat df = new DecimalFormat("0000000");
                nroFactura = f.getTalonarioFactura().getCodEstablecimiento() + "-" + df.format(f.getNroFactura());
            }
            return nroFactura;
        } else {
            return null;
        }
    }

    public boolean existeFacturaPendiente(Integer idcuota) {
        if (idcuota != null) {
            return !this.getNroFacturaPendiente(idcuota).isEmpty();
        } else {
            return false;
        }
    }

    public void seleccionarTerminalAsignada() {
        System.out.println("IdCaja: " + this.tmpIdTerminal);
        if (this.tmpIdTerminal != null) {
            TalonarioFactura t = this.talonarioFacturaFacade.getTalonarioPorIdTerminal(this.tmpIdTerminal);
            if (t != null) {
                this.idTalonarioSelNf = t.getIdtalonario();
                this.tmpIdTalonario = t.getIdtalonario();
            } else {
                this.idTalonarioSelNf = this.talonarioFacturaFacade.getTalonarioActivo().getIdtalonario();
                this.tmpIdTalonario = this.talonarioFacturaFacade.getTalonarioActivo().getIdtalonario();
            }
        } else {
            System.out.println("Terminal obtenidad NULL");
        }
    }

}
