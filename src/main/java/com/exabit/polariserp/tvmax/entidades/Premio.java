/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "premio")
@NamedQueries({
    @NamedQuery(name = "Premio.findAll", query = "SELECT p FROM Premio p")
    , @NamedQuery(name = "Premio.findByIdpremios", query = "SELECT p FROM Premio p WHERE p.idpremios = :idpremios")
    , @NamedQuery(name = "Premio.findByDescripcion", query = "SELECT p FROM Premio p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Premio.findByFechaHora", query = "SELECT p FROM Premio p WHERE p.fechaHora = :fechaHora")
    , @NamedQuery(name = "Premio.findByNumeroPremio", query = "SELECT p FROM Premio p WHERE p.numeroPremio = :numeroPremio")})
public class Premio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idpremio", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idpremio", table = "idsequences", allocationSize = 1, initialValue = 21)
    @Basic(optional = false)
    @Column(name = "idpremios")
    private Integer idpremios;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Column(name = "numero_premio")
    private Integer numeroPremio;
    @JoinColumn(name = "ganador", referencedColumnName = "idsuscripcion")
    @ManyToOne
    private Suscripcion ganador;
    @JoinColumn(name = "sorteo_idsorteo", referencedColumnName = "idsorteo")
    @ManyToOne
    private Sorteo sorteoIdsorteo;

    public Premio() {
    }

    public Premio(Integer idpremios) {
        this.idpremios = idpremios;
    }

    public Integer getIdpremios() {
        return idpremios;
    }

    public void setIdpremios(Integer idpremios) {
        this.idpremios = idpremios;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Integer getNumeroPremio() {
        return numeroPremio;
    }

    public void setNumeroPremio(Integer numeroPremio) {
        this.numeroPremio = numeroPremio;
    }

    public Suscripcion getGanador() {
        return ganador;
    }

    public void setGanador(Suscripcion ganador) {
        this.ganador = ganador;
    }

    public Sorteo getSorteoIdsorteo() {
        return sorteoIdsorteo;
    }

    public void setSorteoIdsorteo(Sorteo sorteoIdsorteo) {
        this.sorteoIdsorteo = sorteoIdsorteo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpremios != null ? idpremios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Premio)) {
            return false;
        }
        Premio other = (Premio) object;
        if ((this.idpremios == null && other.idpremios != null) || (this.idpremios != null && !this.idpremios.equals(other.idpremios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Premio[ idpremios=" + idpremios + " ]";
    }
    
}
