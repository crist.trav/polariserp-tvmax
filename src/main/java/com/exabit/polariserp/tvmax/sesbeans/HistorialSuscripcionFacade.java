/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.CampoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.HistorialSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class HistorialSuscripcionFacade extends AbstractFacade<HistorialSuscripcion> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HistorialSuscripcionFacade() {
        super(HistorialSuscripcion.class);
    }
    
    public List<HistorialSuscripcion> getHistorialSuscripcionListOrdenado(Suscripcion s){
        TypedQuery<HistorialSuscripcion> q=this.em.createQuery("SELECT hs FROM HistorialSuscripcion hs WHERE hs.suscripcion=:susc ORDER BY hs.fechaHoraCambio DESC", HistorialSuscripcion.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }
    public List<HistorialSuscripcion> getHistorialSuscripcionListOrdenado(Suscripcion s, CampoSuscripcion cs){
        TypedQuery<HistorialSuscripcion> q=this.em.createQuery("SELECT hs FROM HistorialSuscripcion hs WHERE hs.suscripcion=:susc AND hs.campoSuscripcion=:campo ORDER BY hs.fechaHoraCambio DESC", HistorialSuscripcion.class);
        q.setParameter("susc", s);
        q.setParameter("campo", cs);
        return q.getResultList();
    }
    
    
    
}
