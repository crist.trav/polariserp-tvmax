/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "anulacionesManBean")
@SessionScoped
public class AnulacionesManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(AnulacionesManBean.class.getName());
    
    

    @Inject
    SessionManBean sessionManBean;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;

    @EJB
    private FacturaFacade facturaFacade;
    
        private String strFechaAnulacion;

    public String getStrFechaAnulacion() {
        return strFechaAnulacion;
    }

    public void setStrFechaAnulacion(String strFechaAnulacion) {
        this.strFechaAnulacion = strFechaAnulacion;
    }


    private List<Factura> lstFacturasAnular = new LinkedList<>();

    public List<Factura> getLstFacturasAnular() {
        return lstFacturasAnular;
    }

    public void setLstFacturasAnular(List<Factura> lstFacturasAnular) {
        this.lstFacturasAnular = lstFacturasAnular;
    }

    private Integer nroFactura;

    public Integer getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(Integer nroFactura) {
        this.nroFactura = nroFactura;
    }

    private Integer idTalonarioFactura;

    public Integer getIdTalonarioFactura() {
        return idTalonarioFactura;
    }

    public void setIdTalonarioFactura(Integer idTalonarioFactura) {
        this.idTalonarioFactura = idTalonarioFactura;
    }

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String tipoMsg = "info";

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    private String observacionAnulacion;

    public String getObservacionAnulacion() {
        return observacionAnulacion;
    }

    public void setObservacionAnulacion(String observacionAnulacion) {
        this.observacionAnulacion = observacionAnulacion;
    }

    private boolean yaRegistrado = false;

    public boolean isYaRegistrado() {
        return yaRegistrado;
    }

    public void setYaRegistrado(boolean yaRegistrado) {
        this.yaRegistrado = yaRegistrado;
    }

    SimpleDateFormat sdf=new SimpleDateFormat("MMMM dd, yyyy");
    
    /**
     * Creates a new instance of AnulacionesManBean
     */
    public AnulacionesManBean() {
        this.strFechaAnulacion=sdf.format(new Date());
    }

    public void agregarFacturaAnular() {
        if (this.nroFactura != null) {
            if (this.idTalonarioFactura != null) {
                boolean yaEnLista = false;
                for (Factura f : this.lstFacturasAnular) {
                    if (f.getTalonarioFactura().getIdtalonario().equals(this.idTalonarioFactura) && f.getNroFactura().equals(this.nroFactura)) {
                        yaEnLista = true;
                        break;
                    }
                }
                if (yaEnLista) {
                    this.tipoMsg = "warning";
                    this.msg = "La factura ya se encuentra en la lista";
                } else {
                    if (this.facturaFacade.existeFacturaAnulada(this.talonarioFacturaFacade.find(this.idTalonarioFactura), this.nroFactura)) {
                        this.tipoMsg = "warning";
                        this.msg = "La factura ya se encuentra registrada y anulada";
                    } else {
                        Factura fAnul = new Factura();
                        try{
                            fAnul.setFechaHoraAnulacion(this.sdf.parse(this.strFechaAnulacion));
                        }catch(ParseException ex){
                            fAnul.setFechaHoraAnulacion(new Date());
                            LOG.log(Level.SEVERE, "Error al convertir fecha de anulacion de string a date", ex);
                        }
                        fAnul.setTalonarioFactura(this.talonarioFacturaFacade.find(this.idTalonarioFactura));
                        fAnul.setNroFactura(this.nroFactura);
                        fAnul.setCliente(this.clienteFacade.find(EntidadesEstaticas.CLIENTE_ANULACION_FACTURA.getIdcliente()));
                        fAnul.setAnulado(true);
                        fAnul.setFechaHoraRegistro(new Date());
//                        fAnul.setFechaHoraAnulacion(new Date());
                        fAnul.setFecha(fAnul.getFechaHoraAnulacion());
                        fAnul.setCancelado(false);
                        fAnul.setTotal(0);
                        fAnul.setIva10(0);
                        fAnul.setIva5(0);
                        fAnul.setContado(true);
                        fAnul.setObservacion(this.observacionAnulacion);
                        fAnul.setFuncionarioRegistro(this.sessionManBean.getFuncionarioVar());
                        this.lstFacturasAnular.add(fAnul);
                        DecimalFormat df = new DecimalFormat("0000000");
                        this.tipoMsg = "info";
                        this.msg = "Agregada factura " + fAnul.getTalonarioFactura().getCodEstablecimiento() + "-" + df.format(fAnul.getNroFactura());
                    }
                }
            }
        } else {
            this.tipoMsg = "warning";
            this.msg = "Numero de factura en blanco";
        }
    }

    public List<TalonarioFactura> getLstTalonarios() {
        return this.talonarioFacturaFacade.findAll();
    }

    public void confirmarAnulacion() {
        for (Factura f : this.lstFacturasAnular) {
            this.facturaFacade.create(f);
        }
        this.msg = "Se registraron " + this.lstFacturasAnular.size() + " facturas anuladas";
        this.tipoMsg = "info";
        this.yaRegistrado = true;
    }

    public void quitarFacturaLista(int indice) {
        this.lstFacturasAnular.remove(indice);
        this.tipoMsg = "info";
        this.msg = "Se quitó la factura de la lista";
    }

    public void limpiar() {
        this.tipoMsg = "info";
        this.msg = "";
        this.lstFacturasAnular.clear();
        this.idTalonarioFactura = null;
        this.nroFactura = null;
        this.yaRegistrado = false;
    }

}
