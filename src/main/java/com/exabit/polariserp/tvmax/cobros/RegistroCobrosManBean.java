/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.cobros;

import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.MedioPago;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CobroFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.DetalleFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.MedioPagoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.google.gson.Gson;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "registroCobrosManBean")
@SessionScoped
public class RegistroCobrosManBean implements Serializable {

    private List<Cuota> lstCuotasPendientes = new LinkedList<>();

    public List<Cuota> getLstCuotasPendientes() {
        return lstCuotasPendientes;
    }

    public void setLstCuotasPendientes(List<Cuota> lstCuotasPendientes) {
        this.lstCuotasPendientes = lstCuotasPendientes;
    }

    @EJB
    private FacturaFacade facturaFacade;

    Suscripcion suscSeleccionada;

    public Suscripcion getSuscSeleccionada() {
        return suscSeleccionada;
    }

    public void setSuscSeleccionada(Suscripcion suscSeleccionada) {
        this.suscSeleccionada = suscSeleccionada;
    }

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @EJB
    private CobroFacade cobroFacade;

    @EJB
    private MedioPagoFacade medioPagoFacade;

    public List<MedioPago> getLstMediosPagos() {
        return medioPagoFacade.findAll();
    }

    private Integer idMedioPagoSelect;

    public Integer getIdMedioPagoSelect() {
        return idMedioPagoSelect;
    }

    public void setIdMedioPagoSelect(Integer idMedioPagoSelect) {
        this.idMedioPagoSelect = idMedioPagoSelect;
    }

    private final SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");

    private String txtFechaCobro;

    public String getTxtFechaCobro() {
        return txtFechaCobro;
    }

    public void setTxtFechaCobro(String txtFechaCobro) {
        this.txtFechaCobro = txtFechaCobro;
    }

//    private String txtSuscCliente;
//
//    public String getTxtSuscCliente() {
//        return txtSuscCliente;
//    }
//
//    public void setTxtSuscCliente(String txtSuscCliente) {
//        this.txtSuscCliente = txtSuscCliente;
//    }
    @EJB
    private DetalleFacturaFacade detalleFacturaFacade;

    private Cobro cobroVar = new Cobro();

    public Cobro getCobroVar() {
        return cobroVar;
    }

    public void setCobroVar(Cobro cobroVar) {
        this.cobroVar = cobroVar;
    }

    private List<DetalleFactura> lstDetalleFactura = new LinkedList<>();

    public List<DetalleFactura> getLstDetalleFactura() {
        return lstDetalleFactura;
    }

    public void setLstDetalleFactura(List<DetalleFactura> lstDetalleFactura) {
        this.lstDetalleFactura = lstDetalleFactura;
    }

    private String JSONIdDetallesCobrar;

    public String getJSONIdDetallesCobrar() {
        return JSONIdDetallesCobrar;
    }

    public void setJSONIdDetallesCobrar(String JSONIdDetallesCobrar) {
        this.JSONIdDetallesCobrar = JSONIdDetallesCobrar;
    }

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    private String msgCuotas;

    public String getMsgCuotas() {
        return msgCuotas;
    }

    public void setMsgCuotas(String msgCuotas) {
        this.msgCuotas = msgCuotas;
    }

    public RegistroCobrosManBean() {
        Date fechaact = new Date();
        this.cobroVar.setFecha(fechaact);
        this.txtFechaCobro = sdf.format(fechaact);
        this.cobroVar.setMedioPago(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO);
        this.idMedioPagoSelect = EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago();
//        this.cobroVar.setCuotaList(new LinkedList<Cuota>());
    }

    private Integer idSuscSelec;

    public Integer getIdSuscSelec() {
        return idSuscSelec;
    }

    public void setIdSuscSelec(Integer idSuscSelec) {
        this.idSuscSelec = idSuscSelec;
    }

    public void seleccionSuscripcion() {
        System.out.println("Seleccion Suscripcion");
        List<Integer> lstIdsCobrar = new LinkedList<>();
        lstCuotasPendientes.clear();
        this.lstDetalleFactura.clear();
        if (this.idSuscSelec != null) {

            
            this.suscSeleccionada = this.suscripcionFacade.find(this.idSuscSelec);
            lstCuotasPendientes.addAll(this.cuotaFacade.getCuotasPendientesPrincipal(suscSeleccionada));
            System.out.println("La suscripcion cuenta con "+lstCuotasPendientes.size()+" cuotas pendientes");
            for (Cuota c : lstCuotasPendientes) {
                //lstIdsCobrar.add(c.getIdcuota());
                DetalleFactura df = this.detalleFacturaFacade.getDetallePorCuota(c);
                if (df != null) {
                    this.lstDetalleFactura.add(df);
                    lstIdsCobrar.add(c.getIdcuota());
//                    lstIdsCobrar.add(df.getIddetalleFactura());
                } else {
                    System.out.println("DetalleFacturaNull");
                }
            }

            Gson gson = new Gson();
            this.JSONIdDetallesCobrar = gson.toJson(lstIdsCobrar.toArray(), Integer[].class);
            System.out.println("El nuevo JSON: " + this.JSONIdDetallesCobrar);
            this.calculoTotalCobro();
            DecimalFormat df = new DecimalFormat("###,###,###,##");
//            this.txtSuscCliente=suscSeleccionada.getIdsuscripcion()+"-"+suscSeleccionada.getCliente().getNombres()+" "+suscSeleccionada.getCliente().getApellidos()+"("+df.format(suscSeleccionada.getPrecio())+")";
        } else {
            System.out.println("ID SUSC NULL");
        }
        this.msgCuotas = "Seleccion suscripcion";
        System.out.println("Seleccion suscripcion " + this.idSuscSelec);
    }

    public void calculoTotalCobro() {
        System.out.println("Calculo total cobro");
        Gson gson = new Gson();
        Integer total = 0;
        System.out.println("JSON iddetallefactura: " + this.JSONIdDetallesCobrar);
        Integer[] lstIdCuotasCobrar = gson.fromJson(this.JSONIdDetallesCobrar, Integer[].class);
//        for (Integer iddetafact : lstIdDetallesCobrar) {
//            for (DetalleFactura df : this.lstDetalleFactura) {
//                System.out.println("Se cobra iddetallefactura: " + df.getIddetalleFactura());
//                if (df.getIddetalleFactura().equals(iddetafact)) {
//                    total = total + df.getSubtotal();
//                }
//            }
//        }
        for (Integer idcuota : lstIdCuotasCobrar) {
            for (Cuota cu : this.lstCuotasPendientes) {
                System.out.println("Se cobra idcuota: " + cu.getIdcuota());
                if (cu.getIdcuota().equals(idcuota)) {
                    total = total + cu.getMontoCuota();
                }
            }
        }
        this.cobroVar.setMonto(total);
    }

    public boolean existeIdDetFacturaCobrar(Integer id) {
        System.out.println("Se comprueba si existe id para cobrar: " + id);
        boolean existe = false;
        if (this.JSONIdDetallesCobrar != null) {
            Gson gson = new Gson();
            Integer[] lstIdDetallesCobrar = gson.fromJson(this.JSONIdDetallesCobrar, Integer[].class);

            for (Integer iddfc : lstIdDetallesCobrar) {
                if (iddfc.equals(id)) {
                    existe = true;
                    System.out.println("Se encontro id");
                }
            }
        }
        return existe;
    }

    public void registrarCobro() {
        System.out.println("registro cobro");
        this.msg = "exitooo!";
        this.cobroVar.setAnulado(false);
        Gson gson = new Gson();
        Integer[] lstIdDetallesCobrar = gson.fromJson(this.JSONIdDetallesCobrar, Integer[].class);
        System.out.println("Ids Cuotass a cobrar: "+this.JSONIdDetallesCobrar);
        for (Integer idcuo : lstIdDetallesCobrar) {
            Cuota c=this.cuotaFacade.find(idcuo);
            DetalleFactura detf=this.detalleFacturaFacade.getDetallePorCuota(c);
//            for (DetalleFactura detf : this.lstDetalleFactura) {
//                if (idcuo.equals(detf.getIddetalleFactura())) {
//                    Cuota c = detf.getCuota();
                    c.setMontoEntrega(c.getMontoCuota());
                    c.setFechaPago(this.cobroVar.getFecha());
                    c.setCancelado(true);
                    this.cuotaFacade.edit(c);
                    cobroVar.setCuota(c);
//                    cobroVar.getCuotaList().add(c);
                    cobroVar.setCobrador(c.getSuscripcion().getCobrador());
                    System.out.println("Cobrador "+cobroVar.getCobrador());
                    System.out.println("Porce "+cobroVar.getCobrador().getPorcentajeComision());
                    
//                    cobroVar.setPorceComisionAplicado(cobroVar.getCobrador().getPorcentajeComision());
                    if(cobroVar.getCobrador().getPorcentajeComision()==null){
                        System.out.println("porce nuyll");
                        cobroVar.setPorceComisionAplicado((float) 0.0);
                    }else{
                        cobroVar.setPorceComisionAplicado(cobroVar.getCobrador().getPorcentajeComision());
                    }
                    Factura fa = detf.getFactura();
                    fa.setCancelado(true);
                    this.facturaFacade.edit(fa);
//                }
//            }
        }
        try {
            this.cobroVar.setFecha(this.sdf.parse(this.txtFechaCobro));
        } catch (ParseException ex) {
            System.out.println("Error al convertir fecha: " + ex.getMessage());
        }
        this.cobroVar.setFechaHoraRegistro(new Date());
        
        this.cobroFacade.create(cobroVar);
        this.lstCuotasPendientes.clear();
        lstCuotasPendientes.addAll(this.cuotaFacade.getCuotasPendientesPrincipal(suscSeleccionada));
        this.msg = "Se registró cobro de "+cobroVar.getMonto()+" gs. para "+lstIdDetallesCobrar.length+" cuotas.";
    }

    public void reiniciarCampos() {
        System.out.println("reiniciar campos");
        this.cobroVar.setFecha(new Date());
        this.txtFechaCobro = sdf.format(new Date());
        this.idMedioPagoSelect = EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago();
        this.cobroVar.setMedioPago(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO);
        this.idSuscSelec = null;
        this.lstDetalleFactura.clear();
        this.cobroVar.setMonto(0);
        this.cobroVar.setIdcobro(null);
//        this.cobroVar.getCuotaList().clear();
        this.cobroVar.setCuota(null);
        this.cobroVar.setFechaHoraRegistro(null);
        this.msg = "";
        this.suscSeleccionada = null;
        this.lstCuotasPendientes.clear();
    }

    public String getDetalleFCuota(Integer idcuota) {
        DetalleFactura df = this.detalleFacturaFacade.getDetallePorCuota(this.cuotaFacade.find(idcuota));
        if (df != null) {
            return df.getFactura().getTalonarioFactura().getCodEstablecimiento() + "-" + df.getFactura().getNroFactura();
        } else {
            return null;
        }
    }

    public Integer getIdFactura(Integer idcuota) {
        DetalleFactura df = this.detalleFacturaFacade.getDetallePorCuota(this.cuotaFacade.find(idcuota));
        if (df != null) {
            return df.getFactura().getIdfactura();
        } else {
            return null;
        }
    }

//    public List<Cuota> getLstSinFactura() {
//        List<Cuota> lstCu = new LinkedList<>();
//        Gson gson = new Gson();
//        if (this.JSONIdDetallesCobrar != null) {
//            Integer[] lstIdDetallesCobrar = gson.fromJson(this.JSONIdDetallesCobrar, Integer[].class);
//            for (Integer iddfc : lstIdDetallesCobrar) {
//                Cuota c = this.cuotaFacade.find(iddfc);
//                if (c.getDetalleFacturaList() == null) {
//                    System.out.println("Agregar cuota a lita");
//                    lstCu.add(c);
//                } else if (c.getDetalleFacturaList().isEmpty()) {
//                    System.out.println("Agregar cuota a lita");
//                    lstCu.add(c);
//                }
//            }
//        } else {
//            System.out.println("json null");
//        }
//        return lstCu;
//    }

    public List<DetalleFactura> getLstDetFactExistentes() {
        List<DetalleFactura> lstDetFact = new LinkedList<>();
        Gson gson = new Gson();
        if (this.JSONIdDetallesCobrar != null) {
            System.out.println("json cuotas: "+this.JSONIdDetallesCobrar);
            Integer[] lstIdDetallesCobrar = gson.fromJson(this.JSONIdDetallesCobrar, Integer[].class);
            for (Integer iddfc : lstIdDetallesCobrar) {
                System.out.println("Siguiente id cuota: "+iddfc);
                Cuota c = this.cuotaFacade.find(iddfc);
                System.out.println("Cuota a buscar factura: "+c);
                if (c.getDetalleFacturaList() != null) {
                    System.out.println("Agregar cuota a lita");
                    
                    lstDetFact.add(this.detalleFacturaFacade.getDetallePorCuota(c));
                }
            }
        } else {
            System.out.println("json null");
        }
        return lstDetFact;
    }

    public void actionVerDlgFinTrans() {
        System.out.println("El json: " + this.JSONIdDetallesCobrar);
        this.registrarCobro();
    }
}
