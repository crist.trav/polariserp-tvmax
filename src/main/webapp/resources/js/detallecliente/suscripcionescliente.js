var formatomescal = {
    days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    months: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    today: 'Hoy',
    now: 'Ahora',
    am: 'AM',
    pm: 'PM'
};

$(document).ready(function () {
    $("#cmbOtroServicioGenerar").dropdown({
        onChange: function (value) {
            var monto = Number($("#precioServicio-" + value).val());
            console.log("monto: " + monto);
            $("#in-montootrascuotas").val(monto);
        }
    });
    $("#cmbServ").dropdown({
        onChange: function () {
            $("#cmlCargarPrecioServicio").click();
        }
    });
});

function mostrarDetalleSuscripcion(id) {
    $("#suscripcionActual").empty();
    if (typeof id !== 'undefined') {
        $("#txfIdSuscripcionCargar").val(id);
        $("#btnCargarDetalleSuscripcion").click();
        $("#suscripcionActual").append(id);
    } else {
        $("#suscripcionActual").append("Nueva suscripción");
    }


    $('#tblSuscripciones').hide();
    $("#sgmDetalleSuscripcion").show();

    $("#iconFlechaSuscripciones").show();
    $("#suscripcionActual").show();
}

function mostrarTablaSuscripciones() {
    $("#sgmDetalleSuscripcion").hide();
    $('#tblSuscripciones').show();

    $("#iconFlechaSuscripciones").hide();
    $("#suscripcionActual").hide();

    $("#menuSuscripcion").dropdown();
}

function ajaxMostrarTablaSuscripciones(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            mostrarTablaSuscripciones();
            break;
        }
    }
}

function ajaxCargarDetalleSuscripcion(data) {
    var status = data.status;
    switch (status) {
        case "begin":
        {
            $("#formDetalleSuscripcion").addClass("loading");
            break;
        }
        case "success":
        {
            $("#formDetalleSuscripcion").removeClass("loading");
            activarDropdowns();
            break;
        }
    }
}

function ajaxNuevaSuscripcion(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbBarrio").dropdown("set selected", $("#in-idbarriocli").val());
            $("#in-direccion").val($("#in-barriocli").val());
            mostrarDetalleSuscripcion();
            activarDropdowns();
            $("#tabDatosSuscripcion").click();
            break;
        }
    }
}

function activarDropdowns() {
    $("#cmbServ").dropdown({
        onChange: function () {
            $("#cmlCargarPrecioServicio").click();
        }
    });
    $("#cmb-tiposusc").dropdown();
    $("#cmbBarrio").dropdown();
    $("#tipoViv").dropdown();
    $("#cmbCobrador").dropdown();
    $('#calFechaSuscripcion').calendar({
        type: "date",
        monthFirst: false,
        formatter: dateFormatter
    });
    $('#searchDireccion').search({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/direcciones?q={query}'
        }
    });
}

function evaluarAccionNuevo() {
    if ($("#tblSuscripciones").is(":visible")) {
        console.log("tablavisible");
        $("#btnAgregarSuscripcion").click();
    } else {
        console.log("tabla no visible");
        if ($("#tabDatosSuscripcion").hasClass("active")) {
            console.log("tab datos activo");
            $("#btnAgregarSuscripcion").click();
        }
        if ($("#tabOtrasCuotas").hasClass("active")) {
            $("#modalGenerarOtrasCuotas").modal("show");
            $('#cal-priven-otrascuotas').calendar({
                type: 'month',
                text: formatomescal
            });
        }
        if ($("#tabCuotasSuscripcion").hasClass("active")) {
            $("#modalGenerarCuotas").modal("show");
            $("#cal-desde-gencuota").calendar({
                type: 'month',
                text: formatomescal,
                endCalendar: $("#cal-hasta-gencuota")
            });
            $("#cal-hasta-gencuota").calendar({
                type: 'month',
                text: formatomescal,
                startCalendar: $("#cal-desde-gencuota")
            });
            $("#cmlCargarPrecioGenerarCuota").click();
        }

    }
}

function ajaxGenerarCuotasOtrosServ(data) {
    console.log("ajax generar otras cuotas");
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            break;
        }
    }
}

function ajaxEliminarCuota(data) {
    console.log("ajax eliminar cuota");
    var status = data.status;
    switch (status) {
        case 'success':
        {
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            break;
        }
    }
}

function ajaxGenerarCuotas(data) {
    var status = data.status;
    console.log("ajaxstatus: " + status);
    switch (status) {
        case "success":
        {
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            break;
        }
    }
}

function mostrarModalCambioEstado(idsuscripcion) {
    $("#in-idsuscripcioncambioestado").val(idsuscripcion);
    $("#modalCambioEstado").modal("show");
    $('#calFechaCambioEstado').calendar({
        type: "date",
        monthFirst: false,
        formatter: {
            date: function (date, settings) {
                if (!date)
                    return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        }
    });
}

function validarCambioEstado() {
    validado = true;
    if ($("#in-idestadocambio").val() === "") {
        validado = false;
        $("#fieldEstadoCambio").addClass("error");
    } else {
        $("#fieldEstadoCambio").removeClass("error");
    }

    if ($("#in-fechacambioestado").val() === "") {
        validado = false;
        $("#fieldFechaCambioEstado").addClass("error");
    } else {
        $("#fieldFechaCambioEstado").removeClass("error");
    }
    if (!validado) {
        if ($("#msgCambioEstado").hasClass("hidden")) {
            $("#msgCambioEstado").transition("fade");
        }
    }
    return validado;
}

function ajaxCambiarEstado(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            $("#modalCambioEstado").modal("hide");
            $("#menuSuscripcion").dropdown();
            break;
        }
    }
}

function mostrarModalCambioTitular(idsuscripcion) {
    $("#in-idsuscripcioncambiotitular").val(idsuscripcion);
    $("#modalCambioTitular").modal("show");
    $('#cmbClienteCambioTitular').dropdown({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/buscarClienteDropdown?q={query}'
        },
        showOnFocus: false
    });
    $('#cmbClienteCambioTitular').dropdown("clear");
}

function ajaxCambiarTitular(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            $("#menuSuscripcion").dropdown();
            $("#modalCambioTitular").modal("hide");
            break;
        }
    }
}
