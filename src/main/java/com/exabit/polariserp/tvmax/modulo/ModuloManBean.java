/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.modulo;

import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.sesbeans.ModuloFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.google.gson.Gson;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author frioss
 */
@Named(value="moduloManbean")
@RequestScoped
public class ModuloManBean {  
    @EJB
    private ModuloFacade moduloFacade;
    private Modulo moduloVar = new Modulo();
    private String msg = "";
    private String seleccionarIDmoduloJSON;

    public Modulo getModuloVar() {
        return moduloVar;
    }

    public void setModuloVar(Modulo moduloVar) {
        this.moduloVar = moduloVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionarIDmoduloJSON() {
        return seleccionarIDmoduloJSON;
    }

    public void setSeleccionarIDmoduloJSON(String seleccionarIDmoduloJSON) {
        this.seleccionarIDmoduloJSON = seleccionarIDmoduloJSON;
    }

    public ModuloManBean() {
        
    }
    
    public List<Modulo> getModulos(){
        return moduloFacade.findAll();
    }
    
    
    public void registrarModulos(){
        this.moduloFacade.create(moduloVar);
        this.msg = "Registrado Correctamente: "+moduloVar.getIdmodulo()+", "+moduloVar.getNombre();
    }
    
    public void modificarModulo(){
//        System.out.println("id: "+moduloVar.getIdmodulo()+", nombre:"+moduloVar.getNombre());
        this.moduloFacade.edit(moduloVar);
        this.msg = "Modificado exitosamente: "+moduloVar.getIdmodulo()+", "+moduloVar.getNombre();
    }
    
    public void eliminarModulo(){
//        System.out.println("Eliminar :"+this.seleccionarIDmoduloJSON);
        Gson gson = new Gson();
        Integer [] idsEliminar = gson.fromJson(this.seleccionarIDmoduloJSON, Integer[].class);
        for (Integer ide : idsEliminar) {
            this.moduloFacade.remove(this.moduloFacade.find(ide));
        }
    }
    
    public List<Modulo> getListaModulos(){
        return EntidadesEstaticas.getListaModulos();
    }
}
