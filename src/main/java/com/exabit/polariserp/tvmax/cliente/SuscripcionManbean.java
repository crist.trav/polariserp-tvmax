/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.cliente;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author frioss
 */
@Named(value = "listaSuscripcionParaClientes")
@RequestScoped
public class SuscripcionManbean {

    @EJB
    private SuscripcionFacade suscripcionFacade;
    private List<Suscripcion> listaSuscripcion = new ArrayList<>();

    public List<Suscripcion> getListaSuscripcion() {
        listaSuscripcion.addAll(suscripcionFacade.findAll());
        return listaSuscripcion;
    }

    public void setListaSuscripcion(List<Suscripcion> listaSuscripcion) {
        this.listaSuscripcion = listaSuscripcion;
    }
    
    public void listaClientesActivos(){
        listaSuscripcion.clear();
        List<Suscripcion> list = new ArrayList<>();
        list.addAll(suscripcionFacade.findAll());
        for (Suscripcion suscripcion : list) {
            if (suscripcion.getEstado().getRecibeServicio()==true) {
                System.out.println("lista de activos: "+suscripcion.getEstado().getRecibeServicio());
                listaSuscripcion.add(suscripcion);
            }
        }
    }
    public void listaClientesInactivos(){
        listaSuscripcion.clear();
        listaSuscripcion.removeAll(listaSuscripcion);
        List<Suscripcion> list = new ArrayList<>();
        list.addAll(suscripcionFacade.findAll());
        for (Suscripcion suscripcion : list) {
            if (suscripcion.getEstado().getRecibeServicio()==false) {
                System.out.println("lista de inactivos: "+suscripcion.getEstado().getRecibeServicio());
                listaSuscripcion.add(suscripcion);
            }
        }
    }
}
