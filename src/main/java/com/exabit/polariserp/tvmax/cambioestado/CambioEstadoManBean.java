/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.cambioestado;

import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.SolicitudCambioEstadoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "cambioEstadoManBean")
@ViewScoped
public class CambioEstadoManBean implements Serializable {

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;
    
        private Integer tipoMsg=1;

    public Integer getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(Integer tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    
        private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    
    private Suscripcion suscSeleccionada;

    public Suscripcion getSuscSeleccionada() {
        return suscSeleccionada;
    }

    public void setSuscSeleccionada(Suscripcion suscSeleccionada) {
        this.suscSeleccionada = suscSeleccionada;
    }


    private List<SolicitudCambioEstado> lstSolicitudes = new ArrayList<>();

    public List<SolicitudCambioEstado> getLstSolicitudes() {
        return lstSolicitudes;
    }

    public void setLstSolicitudes(List<SolicitudCambioEstado> lstSolicitudes) {
        this.lstSolicitudes = lstSolicitudes;
    }

    private boolean pendientesFiltro = true;

    public boolean isPendientesFiltro() {
        return pendientesFiltro;
    }

    public void setPendientesFiltro(boolean pendientesFiltro) {
        this.pendientesFiltro = pendientesFiltro;
    }

        private boolean concretadosFiltro;

    public boolean isConcretadosFiltro() {
        return concretadosFiltro;
    }

    public void setConcretadosFiltro(boolean concretadosFiltro) {
        this.concretadosFiltro = concretadosFiltro;
    }

        private boolean todasSuscFiltro = true;

    public boolean isTodasSuscFiltro() {
        return todasSuscFiltro;
    }

    public void setTodasSuscFiltro(boolean todasSuscFiltro) {
        this.todasSuscFiltro = todasSuscFiltro;
    }

        private Integer idSuscripcionCrear;

    public Integer getIdSuscripcionCrear() {
        return idSuscripcionCrear;
    }

    public void setIdSuscripcionCrear(Integer idSuscripcionCrear) {
        this.idSuscripcionCrear = idSuscripcionCrear;
    }

        private Integer idNuevoEstado;

    public Integer getIdNuevoEstado() {
        return idNuevoEstado;
    }

    public void setIdNuevoEstado(Integer idNuevoEstado) {
        this.idNuevoEstado = idNuevoEstado;
    }

        private String observacionNuevo;

    public String getObservacionNuevo() {
        return observacionNuevo;
    }

    public void setObservacionNuevo(String observacionNuevo) {
        this.observacionNuevo = observacionNuevo;
    }
    
        private boolean efectoInmediato = false;

    public boolean isEfectoInmediato() {
        return efectoInmediato;
    }

    public void setEfectoInmediato(boolean efectoInmediato) {
        this.efectoInmediato = efectoInmediato;
    }

    
    /**
     * Creates a new instance of CambioEstadoManBean
     */
    public CambioEstadoManBean() {
    }
    
    @PostConstruct
    private void postConstruct(){
        this.lstSolicitudes.addAll(this.solicitudCambioEstadoFacade.getSolicitudesPendientes());
    }
    
    public void filtrar(){
        this.lstSolicitudes.clear();
        System.out.println("Filtrar: pendientes"+this.pendientesFiltro+" concretados: "+this.concretadosFiltro);
        if(this.concretadosFiltro && this.pendientesFiltro){
            this.lstSolicitudes.addAll(this.solicitudCambioEstadoFacade.getSolicitudesOrdenFecha());
        }else if(this.concretadosFiltro){
            this.lstSolicitudes.addAll(this.solicitudCambioEstadoFacade.getSolicitudesConcretadas());
        }else if(this.pendientesFiltro){
            this.lstSolicitudes.addAll(this.solicitudCambioEstadoFacade.getSolicitudesPendientes());
        }
    }
    
    public void registrarSolicitud(){
        System.out.println("Nueva solicitud");
        if(this.idSuscripcionCrear!=null & this.idNuevoEstado!=null){
            Suscripcion s=this.suscripcionFacade.find(this.idSuscripcionCrear);
            EstadoSuscripcion e=this.estadoSuscripcionFacade.find(this.idNuevoEstado);
            SolicitudCambioEstado sce=new SolicitudCambioEstado();
            sce.setEstadoAnterior(s.getEstado());
            sce.setEstadoNuevo(e);
            sce.setFechaSolicitud(new Date());
            sce.setObservacion(this.observacionNuevo);
            sce.setSuscripcion(s);
            if(this.efectoInmediato){
                sce.setConcretado(true);
                s.setEstado(e);
                sce.setFechaConcrecion(new Date());
                s.setFechaCambioEstado(sce.getFechaConcrecion());
                this.suscripcionFacade.edit(s);
            }else{
                sce.setConcretado(false);
            }
            
            this.solicitudCambioEstadoFacade.create(sce);
             this.tipoMsg=1;
             if(this.efectoInmediato){
                 this.msg="Solicitud con código "+s.getIdsuscripcion()+" registrada y cambio aplicado correctamente";
             }else{
                this.msg="Solicitud con código "+s.getIdsuscripcion()+" registrada correctamente";
             }
             
        }
       
        this.filtrar();
    }
    
    public void seleccionSuscripcion(){
        System.out.println("seleccion suscripcion");
        if(this.idSuscripcionCrear!=null){
            this.suscSeleccionada=this.suscripcionFacade.find(this.idSuscripcionCrear);
        }else{
            this.suscSeleccionada=null;
        }
    }
    
    public void aprobarSolicitud(Integer idSol){
        SolicitudCambioEstado sce=this.solicitudCambioEstadoFacade.find(idSol);
        sce.setConcretado(true);
        sce.setFechaConcrecion(new Date());
        Suscripcion s=sce.getSuscripcion();
        s.setEstado(sce.getEstadoNuevo());
        s.setFechaCambioEstado(sce.getFechaConcrecion());
        this.suscripcionFacade.edit(s);
        this.solicitudCambioEstadoFacade.edit(sce);
        this.tipoMsg=1;
        this.msg="Solicitud aprobada. Código: "+s.getIdsuscripcion();
        this.filtrar();
    }
    
    public void eliminarSolicitud(Integer idSol){
        SolicitudCambioEstado sce=this.solicitudCambioEstadoFacade.find(idSol);
        this.solicitudCambioEstadoFacade.remove(sce);
        this.tipoMsg=1;
        this.msg="Solicitud eliminada: Código: "+sce.getIdsolicitud();
        this.filtrar();
    }
    
    public void limpiarFormulario(){
        this.idNuevoEstado=null;
        this.observacionNuevo="";
        this.suscSeleccionada=null;
        this.efectoInmediato=false;
        this.idSuscripcionCrear=null;
    }
    
}
