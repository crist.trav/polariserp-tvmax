/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.util;

import com.exabit.polariserp.tvmax.sesbeans.UsoModuloFacade;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author traver
 */
@Named(value = "usoModuloManBean")
@RequestScoped
public class usoModuloManBean {

    @EJB
    private UsoModuloFacade usoModuloFacade;
    
    

    /**
     * Creates a new instance of usoModuloManBean
     */
    public usoModuloManBean() {
    }
    
    public void incrementarUso(int idfuncionario, int idmodulo){
        System.out.println("incrementar uso");
        this.usoModuloFacade.incrementarContadorUso(idfuncionario, idmodulo);
    }
    
}
