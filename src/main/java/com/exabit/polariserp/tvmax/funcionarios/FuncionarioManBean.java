package com.exabit.polariserp.tvmax.funcionarios;

import com.exabit.polariserp.tvmax.entidades.Cargo;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.sesbeans.CargoFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author traver
 */
@Named(value = "funcionarioManBean")
@ViewScoped
public class FuncionarioManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(FuncionarioManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private FuncionarioFacade funcionarioFacade;
    @EJB
    private CargoFacade cargoFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private Integer idfuncionario;
    private String nombres;
    private String apellidos;
    private Integer idcargo;
    private String telefono1;
    private String telefono2;
    private String password;
    private Integer ci;
    private boolean activo = false;
    private String jsonSelecionFuncionario;
    private String tipoMsg;
    private String cabeceraMsg;
    private String cuerpoMsg;
    private String busqueda = "";
    //</editor-fold> 

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public String getJsonSelecionFuncionario() {
        return jsonSelecionFuncionario;
    }

    public void setJsonSelecionFuncionario(String jsonSelecionFuncionario) {
        this.jsonSelecionFuncionario = jsonSelecionFuncionario;
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(Integer idcargo) {
        this.idcargo = idcargo;
    }

    public Integer getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Integer idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }
    //</editor-fold>

    /**
     * Creates a new instance of FuncionarioManBean
     */
    public FuncionarioManBean() {
    }

    public List<Funcionario> getLstFuncionarios() {
        if (this.busqueda.isEmpty()) {
            return funcionarioFacade.findAll();
        } else {
            return funcionarioFacade.buscarFuncionarios(this.busqueda);
        }
    }

    public List<Cargo> getLstCargos() {
        return this.cargoFacade.findAll();
    }

    public void guardarFuncionario() {
        try {
            System.out.println("Guardar func: " + this.idfuncionario + " " + this.nombres + " " + this.apellidos + " " + this.ci + " " + this.telefono1 + " " + this.telefono2 + " " + this.activo + " " + this.password);
            Funcionario f;
            if (this.idfuncionario == null) {
                f = new Funcionario();
            } else {
                f = this.funcionarioFacade.find(this.idfuncionario);
            }
            f.setIdfuncionario(this.idfuncionario);
            f.setNombres(this.nombres);
            f.setApellidos(this.apellidos);
            f.setCi(this.ci);
            f.setCargo(this.cargoFacade.find(this.idcargo));
            f.setTelefono1(this.telefono1);
            f.setTelefono2(this.telefono2);
            f.setActivo(this.activo);
            if (!this.password.isEmpty()) {
                f.setPassword(DigestUtils.sha256Hex(this.password));
            }
            if (f.getIdfuncionario() == null) {
                this.funcionarioFacade.create(f);
                this.idfuncionario=f.getIdfuncionario();
            } else {
                this.funcionarioFacade.edit(f);
            }
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Funcionario guardado correctamente.";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al guardar funcionario", ex);
            this.tipoMsg = "negative";
            this.cabeceraMsg = "Error al guardar funcionario";
            this.cuerpoMsg = ex.getMessage();
        }
    }

    public void eliminarFuncionario() {
        System.out.println("Eliminar: " + this.jsonSelecionFuncionario);
        try {
            Gson gson = new Gson();
            Integer[] ids = gson.fromJson(this.jsonSelecionFuncionario, Integer[].class);
            for (Integer id : ids) {
                this.funcionarioFacade.remove(this.funcionarioFacade.find(id));
            }
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Funcionarios eliminados correctamente.";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al eliminar", ex);
            this.tipoMsg = "negative";
            this.cabeceraMsg = "Error al eliminar";
            this.cuerpoMsg = ex.getMessage();
        }
    }

    public void cargarDatos() {
        System.out.println("Cargar datos: " + this.idfuncionario);
        Funcionario f = this.funcionarioFacade.find(this.idfuncionario);
        this.nombres = f.getNombres();
        this.apellidos = f.getApellidos();
        this.ci = f.getCi();
        this.idcargo = f.getCargo().getIdcargo();
        this.telefono1 = f.getTelefono1();
        this.telefono2 = f.getTelefono2();
        this.activo = f.getActivo();
        this.password = "";
    }

    public void limpiarDatos() {
        this.idfuncionario = null;
        this.nombres = null;
        this.apellidos = null;
        this.ci = null;
        this.telefono1 = null;
        this.telefono2 = null;
        this.idcargo = null;
        this.password = null;
        this.activo = false;
    }

    public void filtrar() {
        System.out.println("filtrar: " + this.busqueda);
    }

}
