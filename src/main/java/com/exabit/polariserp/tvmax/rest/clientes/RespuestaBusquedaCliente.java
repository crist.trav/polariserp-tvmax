/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.clientes;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 */
public class RespuestaBusquedaCliente {
    
        private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    private List<ClienteBasicDropdown> results = new LinkedList<>();

    public List<ClienteBasicDropdown> getResults() {
        return results;
    }

    public void setResults(List<ClienteBasicDropdown> results) {
        this.results = results;
    }


}
