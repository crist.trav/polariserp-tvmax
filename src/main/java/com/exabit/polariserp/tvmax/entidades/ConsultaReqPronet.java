/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "consulta_req_pronet")
@NamedQueries({
    @NamedQuery(name = "ConsultaReqPronet.findAll", query = "SELECT c FROM ConsultaReqPronet c")
    , @NamedQuery(name = "ConsultaReqPronet.findByIdconsultaReq", query = "SELECT c FROM ConsultaReqPronet c WHERE c.idconsultaReq = :idconsultaReq")
    , @NamedQuery(name = "ConsultaReqPronet.findByCodServicio", query = "SELECT c FROM ConsultaReqPronet c WHERE c.codServicio = :codServicio")
    , @NamedQuery(name = "ConsultaReqPronet.findByTipoTrx", query = "SELECT c FROM ConsultaReqPronet c WHERE c.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "ConsultaReqPronet.findByUsuario", query = "SELECT c FROM ConsultaReqPronet c WHERE c.usuario = :usuario")
    , @NamedQuery(name = "ConsultaReqPronet.findByPassword", query = "SELECT c FROM ConsultaReqPronet c WHERE c.password = :password")
    , @NamedQuery(name = "ConsultaReqPronet.findByNroDocumento", query = "SELECT c FROM ConsultaReqPronet c WHERE c.nroDocumento = :nroDocumento")
    , @NamedQuery(name = "ConsultaReqPronet.findByMoneda", query = "SELECT c FROM ConsultaReqPronet c WHERE c.moneda = :moneda")
    , @NamedQuery(name = "ConsultaReqPronet.findByFechaHora", query = "SELECT c FROM ConsultaReqPronet c WHERE c.fechaHora = :fechaHora")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsultaReqPronet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idconsultareqpronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idconsultareqpronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idconsulta_req")
    @JsonIgnore
    private Integer idconsultaReq;
    @Size(max = 5)
    @Column(name = "codServicio")
    private String codServicio;
    @Column(name = "tipoTrx")
    private Integer tipoTrx;
    @Size(max = 20)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 70)
    @Column(name = "password")
    private String password;
    @Size(max = 15)
    @Column(name = "nroDocumento")
    private String nroDocumento;
    @Size(max = 1)
    @Column(name = "moneda")
    private String moneda;
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "consultaReqPronet")
    @JsonIgnore
    private List<ConsultaResPronet> consultaResPronetList;

    public ConsultaReqPronet() {
    }

    public ConsultaReqPronet(Integer idconsultaReq) {
        this.idconsultaReq = idconsultaReq;
    }

    public Integer getIdconsultaReq() {
        return idconsultaReq;
    }

    public void setIdconsultaReq(Integer idconsultaReq) {
        this.idconsultaReq = idconsultaReq;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public List<ConsultaResPronet> getConsultaResPronetList() {
        return consultaResPronetList;
    }

    public void setConsultaResPronetList(List<ConsultaResPronet> consultaResPronetList) {
        this.consultaResPronetList = consultaResPronetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconsultaReq != null ? idconsultaReq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultaReqPronet)) {
            return false;
        }
        ConsultaReqPronet other = (ConsultaReqPronet) object;
        if ((this.idconsultaReq == null && other.idconsultaReq != null) || (this.idconsultaReq != null && !this.idconsultaReq.equals(other.idconsultaReq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.ConsultaReqPronet[ idconsultaReq=" + idconsultaReq + " ]";
    }
    
}
