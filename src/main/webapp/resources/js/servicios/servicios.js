$(document).ready(function () {
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    $("#elementos-tabla tbody :checkbox").prop("checked", false);
    if ($("#in-idservicio").val() !== "") {
        seleccionUnica($("#in-idservicio").val());
    }
});

function limpiarSeleccion() {
    $("[id^='fila-']").removeClass("active");
    $("[id^='chk-serv-']").prop("checked", false);
}
function seleccionarTodo() {
    $("[id^='fila-']").addClass("active");
    $("[id^='chk-serv-']").prop("checked", true);
}
function seleccionUnica(idservicio) {
    limpiarSeleccion();
    $("#fila-" + idservicio).addClass("active");
    $("#chk-serv-" + idservicio).prop("checked", true);
    $("#in-idserviciocargar").val(idservicio);
    $("#cmlCargarServicio").click();
    evaluarMostrarEliminar();
}

function agregarSeleccionCheck(idservicio) {
    if ($("#chk-serv-" + idservicio).prop("checked")) {
        $("#fila-" + idservicio).addClass("active");
    } else {
        $("#fila-" + idservicio).removeClass("active");
    }
    evaluarMostrarEliminar();
}

function agregarSeleccion(idservicio) {
    if ($("#fila-" + idservicio).hasClass("active")) {
        $("#chk-serv-" + idservicio).prop("checked", false);
        $("#fila-" + idservicio).removeClass("active");
    } else {
        $("#chk-serv-" + idservicio).prop("checked", true);
        $("#fila-" + idservicio).addClass("active");
    }
    evaluarMostrarEliminar();
}

function ajaxCargarServicio(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbIva").dropdown();
            $("#cmbConceptoCobro").dropdown();
            break;
        }
    }
}

function ajaxNuevoServicio(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            limpiarSeleccion();
            $("#cmbIva").dropdown();
            $("#cmbConceptoCobro").dropdown();
            if (!$("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $("#cmbConceptoCobro").dropdown();
            $("#nombreservicio").focus();
            break;
        }
    }
}

function ajaxGuardarServicio(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            if ($("#in-idservicio").val() !== "") {
                seleccionUnica($("#in-idservicio").val());
            }
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function setTipoMsg(tipo) {
    $("#msg").removeClass("error");
    $("#msg").removeClass("info");
    $("#msg").removeClass("warning");
    $("#msg").addClass(tipo);
}

function mostrarMsg(tipo, cabecera, cuerpo) {
    setTipoMsg(tipo);
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);

    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function evaluarMostrarEliminar() {
    if ($("[id^='chk-serv-']:checked").length === 0) {
        $("#btnEliminar").hide();
    } else {
        $("#btnEliminar").show();
    }
}

function checkSeleccionarTodo() {
    if ($("#chk-selec-todo").prop("checked")) {
        seleccionarTodo();
    } else {
        limpiarSeleccion();
    }
    evaluarMostrarEliminar();
}

function mostrarConfElim() {
    $('#modalEliminarServicios').modal("show");
    var ids = new Array;
    $("#elementos-tabla tbody tr.active [id^='col-id-servicio-']").each(function (index, value) {
        ids.push(parseInt($(this).html()));
    });
    $("#in-servicioselim").val(JSON.stringify(ids));
}

function ajaxEliminar(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function validar() {
    var validado = true;
    if ($("#nombreservicio").val() === "") {
        validado = false;
        $("#nombreServ").addClass("error");
    } else {
        $("#nombreServ").removeClass("error");
    }

    if ($("#precioservicio").val() === "") {
        validado = false;
        $("#precioServ").addClass("error");
    } else {
        $("#precioServ").removeClass("error");
    }
    if ($("#in-ivaservicio").val() === "") {
        validado = false;
        $("#ivaServ").addClass("error");
    } else {
        $("#ivaServ").removeClass("error");
    }
    if ($("#in-idconcepto").val() === "") {
        validado = false;
        $("#cmbConceptoCobro").addClass("error");
    } else {
        $("#cmbConceptoCobro").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
    }
    return validado;
}

function validarNuevoConcepto() {
    var validado = true;
    if ($("#in-nombrenuevoconcepto").val() === "") {
        validado = false;
        $("#fieldNombreNuevoConcepto").addClass("error");
    } else {
        $("#fieldNombreNuevoConcepto").removeClass("error");
    }
    if (!validado) {
        if ($("#msgErrorNuevoConcepto").hasClass("hidden")) {
            $("#msgErrorNuevoConcepto").transition("fade");
        }
    }
    return validado;
}

function ajaxBuscar(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            evaluarMostrarEliminar();
            break;
        }
    }
}

function mostrarModalNuevoConcepto(){
    $("#in-nombrenuevoconcepto").val("");
    $("#modalAgregarConcepto").modal("show");
}

function ajaxGuardarNuevoConcepto(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $("#cmbConceptoCobro").dropdown();
            setTipoMsg($("#tipoMsg").val());
            if (!$("#msgErrorNuevoConcepto").hasClass("hidden")) {
                $("#msgErrorNuevoConcepto").transition("fade");
            }
//            $("#modalAgregarConcepto").modal("hide");
            break;
        }
    }
}

var timeOut = 0;
$('#txtBuscar').on('input', function () {
    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        $('#cmlBuscar').click();
    }, 500);
});
