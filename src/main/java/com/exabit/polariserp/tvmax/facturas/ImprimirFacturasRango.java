/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.sesbeans.AsignacionFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "imprimirFacturasRango")
@ViewScoped
public class ImprimirFacturasRango implements Serializable {

    private static final Logger LOG = Logger.getLogger(ImprimirFacturasRango.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private AsignacionFacturaFacade asignacionFacturaFacade;
    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;
    @EJB
    private FacturaFacade facturaFacade;
    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;
    //</editor-fold>

    private Integer idTalonarioSel;
    private Integer nroInicio;
    private Integer nroFin;
    private boolean existenFacturas;
    
    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public boolean isExistenFacturas() {
        return existenFacturas;
    }

    public void setExistenFacturas(boolean existenFacturas) {
        this.existenFacturas = existenFacturas;
    }

    public Integer getIdTalonarioSel() {
        return idTalonarioSel;
    }

    public void setIdTalonarioSel(Integer idTalonarioSel) {
        this.idTalonarioSel = idTalonarioSel;
    }

    public Integer getNroInicio() {
        return nroInicio;
    }

    public void setNroInicio(Integer nroInicio) {
        this.nroInicio = nroInicio;
    }

    public Integer getNroFin() {
        return nroFin;
    }

    public void setNroFin(Integer nroFin) {
        this.nroFin = nroFin;
    }
    //</editor-fold>

    /**
     * Creates a new instance of ImprimirFacturasRango
     */
    public ImprimirFacturasRango() {
    }

    public List<TalonarioFactura> getLstTalonarios() {
        return this.talonarioFacturaFacade.findAll();
    }

    public void generarPDFPendSeleccionado() {
        TalonarioFactura t=this.talonarioFacturaFacade.find(this.idTalonarioSel);
        List<Factura> lstFacturas = this.facturaFacade.getRangoFactura(t, nroInicio, nroFin);
        System.out.println("talonario: " + t);
        System.out.println("nro inicio: " + this.nroInicio);
        System.out.println("nro fin: " + this.nroFin);
        System.out.println("Cantidad de facturas: " + lstFacturas.size());
        this.generarPDFPendSeleccionado(lstFacturas);
    }

    public void validarExistenciaFacturas() {
        System.out.println(this.idTalonarioSel+" "+this.nroInicio+" "+this.nroFin);
        if (this.idTalonarioSel != null && this.nroInicio != null && nroFin != null) {
            TalonarioFactura t=this.talonarioFacturaFacade.find(this.idTalonarioSel);
            List<Factura> lstFacturas = this.facturaFacade.getRangoFactura(t, nroInicio, nroFin);
            this.existenFacturas=!lstFacturas.isEmpty();
        } else {
            this.existenFacturas=false;
        }

    }

    private void generarPDFPendSeleccionado(List<Factura> lstFacturas) {
        if (!lstFacturas.isEmpty()) {
            List<JasperPrint> lstJP = UtilReportes.generarJPListFacturas(lstFacturas);
            try {
                HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.addHeader("content-disposition", "inline; filename=facturas.pdf");
                ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
                JRPdfExporter exporter = new JRPdfExporter();
                SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
                configExport.setCreatingBatchModeBookmarks(true);
                exporter.setConfiguration(configExport);
                exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
                SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
                exporter.setExporterOutput(out);
                exporter.exportReport();
            } catch (IOException | JRException ex) {
                LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
            }
        } else {
            System.out.println("Lista de facturas vacia");
        }
    }
}
