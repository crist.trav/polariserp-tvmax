/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.funcionalidad;

/**
 *
 * @author frioss
 */
public class AuxFuncionalidad {

    public AuxFuncionalidad(String nombre, Integer idFuncionalidad, boolean estado) {
        this.nombre = nombre;
        this.idFuncionalidad = idFuncionalidad;
        this.estado = estado;
    }
    
    
    private String nombre;
    
    private Integer idFuncionalidad;

    public Integer getIdFuncionalidad() {
        return idFuncionalidad;
    }
    
    public void setIdFuncionalidad(Integer idFuncionalidad) {
        this.idFuncionalidad = idFuncionalidad;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
        private boolean estado;

    public boolean isEstado() {
        return estado;
    }
    
    public void setEstado(boolean estado) {
        this.estado = estado;
    }

}
