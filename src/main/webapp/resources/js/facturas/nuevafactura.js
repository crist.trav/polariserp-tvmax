$(document).ready(function () {

    $('#example2').calendar({
        type: "date",
        monthFirst: false,
        formatter: {
            date: function (date, settings) {
                if (!date)
                    return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        }
    });
    $('#cmbCliente').dropdown({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/buscarClienteDropdown?q={query}'
        },
        showOnFocus: false
    });
    $("#cmbCliente").keypress(function (e) {
        if (e.which == 13) {
            $("#cmlSelecCli").click();
        }
    });
    $("#cmbCliente").focusout(function (e) {
        if (e.which == 13) {
            $("#cmlSelecCli").click();
        }
    });

    $("#tabCuotas").tab();
    $("#tabOtrosCobros").tab();

    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });

    if (typeof (Storage) !== "undefined") {
        console.log("ID " + localStorage.getItem('idterminal'));
        $("#in-idterminal").val(localStorage.getItem('idterminal'));
//        console.log("Fecha " + localStorage.getItem('fechahoraasignacionterminal'));
    } else {
        console.log("No soporte para local storage :(");
    }

    $("#cmbTalonarioFactura").dropdown({
        onChange: function (value, text, choice) {
            $("#cmlActNumeroFactura").click();
        }
    });

    $("#cmlActualizarTalonario").click();
});

function ajaxAgregarCliente(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbSuscripcionOtrosCobros").dropdown();
            break;
        }
    }
}

function validarAgregarOtrosCobros() {
    console.log("susc otros cobros: " + $("#in-idsuscotroscobros").val());
    var validado = true;
    if ($("#in-idsuscotroscobros").val() === "") {
        validado = false;
        $("#cmbSuscripcionOtrosCobros").addClass("error");
    } else {
        $("#cmbSuscripcionOtrosCobros").removeClass("error");
    }
    if (!validado) {
        if ($("#msgAgregarOtroCobro").hasClass("hidden")) {
            $("#msgAgregarOtroCobro").transition("fade");
        }
    } else {
        if (!$("#msgAgregarOtroCobro").hasClass("hidden")) {
            $("#msgAgregarOtroCobro").transition("fade");
        }
    }
    return validado;
}

function ajaxActualizarTalonario(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbTalonarioFactura").dropdown({
                onChange: function (value, text, choice) {
                    $("#cmlActNumeroFactura").click();
                }
            });
            break;
        }
    }
}

function validarFactura() {
    var validado = true;
    if ($("#in-fechafact").val() === "") {
        $("#inputFechaFactura").addClass("error");
        validado = false;
    } else {
        $("#inputFechaFactura").removeClass("error");
    }
    if ($("#in-idtalonario").val() === "") {
        $("#cmbTalonarioFactura").addClass("error");
        validado = false;
    } else {
        $("#cmbTalonarioFactura").removeClass("error");
    }
    if ($("#in-numerofactura").val() === "") {
        $("#inputFactura").addClass("error");
        validado = false;
    } else {
        $("#inputFactura").removeClass("error");
    }
    if ($("#in-idclienteagregar").val() === "") {
        $("#cmbCliente").addClass("error");
        validado = false;
    } else {
        $("#cmbCliente").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("Error de validación", "Complete los campos obligatorios", "error");
    }
    return validado;
}

function cambiarTipoMsg(tipo) {
    $("#msg").removeClass("error");
    $("#msg").removeClass("warning");
    $("#msg").removeClass("info");
    $("#msg").removeClass("success");
    $("#msg").addClass(tipo);
}

function mostrarMsg(cabecera, cuerpo, tipo) {
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);
    cambiarTipoMsg(tipo);
    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function ajaxGuardarFactura(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbTalonarioFactura").dropdown({
                onChange: function (value, text, choice) {
                    $("#cmlActNumeroFactura").click();
                }
            });
            $("#cmbTalonarioFactura").dropdown({
                onChange: function (value, text, choice) {
                    $("#cmlActNumeroFactura").click();
                }
            });
            cambiarTipoMsg($("#in-tipomsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function ajaxNuevaFactura(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbTalonarioFactura").dropdown({
                onChange: function (value, text, choice) {
                    $("#cmlActNumeroFactura").click();
                }
            });
            $("#cmbTalonarioFactura").dropdown({
                onChange: function (value, text, choice) {
                    $("#cmlActNumeroFactura").click();
                }
            });
            if (!$("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $('#cmbCliente').dropdown({
                apiSettings: {
                    url: '//' + location.host + restRootPath + '/buscarClienteDropdown?q={query}'
                },
                showOnFocus: false
            });
            $("#cmbCliente").keypress(function (e) {
                if (e.which == 13) {
                    $("#cmlSelecCli").click();
                }
            });
            $("#cmbCliente").focusout(function (e) {
                if (e.which == 13) {
                    $("#cmlSelecCli").click();
                }
            });
            break;
        }
    }
}