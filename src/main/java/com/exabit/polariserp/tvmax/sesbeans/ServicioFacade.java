/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Servicio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author frioss
 */
@Stateless
public class ServicioFacade extends AbstractFacade<Servicio> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ServicioFacade() {
        super(Servicio.class);
    }
    
    public List<Servicio> getOtrosCobros(){
        TypedQuery<Servicio> q=this.em.createQuery("SELECT s FROM Servicio s WHERE s.idservicio!=1", Servicio.class);
        return q.getResultList();
    }
    
    public List<Servicio> getServiciosSuscribibles(){
        TypedQuery<Servicio> q=this.em.createQuery("SELECT s FROM Servicio s WHERE s.suscribible=true", Servicio.class);
        return q.getResultList();
    }
    public List<Servicio> getServiciosNoSuscribibles(){
        TypedQuery<Servicio> q=this.em.createQuery("SELECT s FROM Servicio s WHERE s.suscribible=false", Servicio.class);
        return q.getResultList();
    }
    
    public List<Servicio> buscarServicio(String busqueda){
        TypedQuery<Servicio> qs=this.em.createQuery("SELECT s FROM Servicio s WHERE FUNCTION('REPLACE', LOWER(s.nombre), ' ', '') LIKE :busq", Servicio.class);
        qs.setParameter("busq", "%"+busqueda.replace(" ", "").toLowerCase()+"%");
        return qs.getResultList();
    }
}
