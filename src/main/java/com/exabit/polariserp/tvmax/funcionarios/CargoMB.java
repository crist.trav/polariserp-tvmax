/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.funcionarios;

import com.exabit.polariserp.tvmax.entidades.Cargo;
import com.exabit.polariserp.tvmax.sesbeans.CargoFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author gemacron
 */
@ManagedBean(name = "cargo")
@SessionScoped
public class CargoMB implements Serializable {

    private List<Cargo> listadecargos = new ArrayList<>();

    private String idcargo;
    private String nombre;
    
    @EJB
    private CargoFacade cargoFacade;
    public String seleccionCargoIDJson;

    public String getSeleccionCargoIDJson() {
        return seleccionCargoIDJson;
    }

    public void setSeleccionCargoIDJson(String seleccionCargoIDJson) {
        this.seleccionCargoIDJson = seleccionCargoIDJson;
    }
    
    public String getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(String idcargo) {
        this.idcargo = idcargo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private Cargo cargo_temp = new Cargo();

    public Cargo getCargo_temp() {
        return cargo_temp;
    }

    public void setCargo_temp(Cargo cargo_temp) {
        this.cargo_temp = cargo_temp;
    }

    public List<Cargo> mostrarCargos() {
        return cargoFacade.findAll();
    }

    public void registrarCargo() {
        this.cargoFacade.create(cargo_temp);
    }

    public List<Cargo> listadeCargos() {
        return cargoFacade.findAll();
    }

    public void modificarCargo()  {
        Cargo contenedorcargo = new Cargo();
        contenedorcargo.setIdcargo(cargo_temp.getIdcargo());
        contenedorcargo.setNombre(cargo_temp.getNombre());
        this.cargoFacade.edit(contenedorcargo);
    }
    
        public void eliminarCargos() {
        Gson gson = new Gson();
        Integer idEliminar = gson.fromJson(this.seleccionCargoIDJson, Integer.class);
        this.cargoFacade.remove(this.cargoFacade.find(idEliminar));
        
    }
}
