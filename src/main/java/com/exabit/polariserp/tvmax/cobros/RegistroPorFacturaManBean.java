package com.exabit.polariserp.tvmax.cobros;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.util.UtilConversion;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "registroPorFacturaManBean")
@SessionScoped
public class RegistroPorFacturaManBean implements Serializable {

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private ClienteFacade clienteFacade;
    
    
    
        private String msgNuevoCobro;

    public String getMsgNuevoCobro() {
        return msgNuevoCobro;
    }

    public void setMsgNuevoCobro(String msgNuevoCobro) {
        this.msgNuevoCobro = msgNuevoCobro;
    }

        private int tipomsg = 1;

    public int getTipomsg() {
        return tipomsg;
    }

    public void setTipomsg(int tipomsg) {
        this.tipomsg = tipomsg;
    }

    
    private List<Factura> lstFacturasCobro = new ArrayList<>();

    public List<Factura> getLstFacturasCobro() {
        return lstFacturasCobro;
    }

    public void setLstFacturasCobro(List<Factura> lstFacturasCobro) {
        this.lstFacturasCobro = lstFacturasCobro;
    }
    
        private Integer nroFactura;

    public Integer getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(Integer nroFactura) {
        this.nroFactura = nroFactura;
    }



    /**
     * Creates a new instance of RegistroPorFacturaManBean
     */
    public RegistroPorFacturaManBean() {
    }
    
    public String getDescripcionCobradorCliente(int idcli){
        Funcionario co=this.clienteFacade.getCobrador(this.clienteFacade.find(idcli));
        String cobrador=co.getIdfuncionario()+"-"+co.getNombres()+" "+(co.getApellidos()!=null?co.getApellidos():"");
        if(co.getCi()!=null){
         cobrador=cobrador+"("+co.getCi()+")";
        }
        return cobrador;
        
    }
    
    public void agregarFactura(){
        System.out.println("Buscar factura");
        Factura f=this.facturaFacade.getFacturaPendientePorNro(this.nroFactura);
        boolean existe=false;
        for(Factura fa:this.lstFacturasCobro){
            if(fa!=null){
                if(fa.getIdfactura().equals(f.getIdfactura())){
                    existe=true;
                }
            }
        }
        if(!existe){
            if(f!=null){
                if(!f.getAnulado()){
                    this.lstFacturasCobro.add(f);
                    Cuota cuofact=null;
                    for(DetalleFactura detf:f.getDetalleFacturaList()){
                        if(detf.getCuota()!=null){
                            cuofact=detf.getCuota();
                            break;
                        }
                    }
                    if(cuofact!=null){
                        List<Cuota> lstCuoAnt=this.cuotaFacade.getCuotasPendientesAnteriores(cuofact);
                        if(lstCuoAnt.isEmpty()){
                            this.msgNuevoCobro="Agregada factura "+f.getTalonarioFactura().getCodEstablecimiento()+"-"+f.getNroFactura();
                            this.tipomsg=1;
                        }else{
                            this.msgNuevoCobro="Existen cuotas más antiguas pendientes de cobro:";
                            this.tipomsg=2;
                            UtilConversion uc=new UtilConversion();
                            for(Cuota c:lstCuoAnt){
                                System.out.println("Cuota nro "+c.getIdcuota());
                                Factura fp=this.facturaFacade.getFacturaPendientePorCuota(c);
                                this.msgNuevoCobro=this.msgNuevoCobro+" -"+uc.getNombreMes(c.getMesCuota().intValue())+" "+c.getAnioCuota()+" (";
                                if(fp!=null){
                                    DecimalFormat decf=new DecimalFormat("0000000");
                                    this.msgNuevoCobro=this.msgNuevoCobro+"Factura "+fp.getTalonarioFactura().getCodEstablecimiento()+"-"+decf.format(fp.getNroFactura())+")";
                                }else{
                                    this.msgNuevoCobro=this.msgNuevoCobro+"Sin Factura)";
                                }
                            }
                        }
                    }else{
                        this.msgNuevoCobro="Agregada factura "+f.getTalonarioFactura().getCodEstablecimiento()+"-"+f.getNroFactura();
                        this.tipomsg=1;
                    }
                }else{
                    DecimalFormat df=new DecimalFormat("0000000");
                    this.msgNuevoCobro="La factura "+f.getTalonarioFactura().getCodEstablecimiento()+"-"+df.format(f.getNroFactura())+" está anulada";
                    this.tipomsg=3;
                }
            }else{
                System.out.println("no se encuentra pendiente");
                this.msgNuevoCobro="No se encontró factura pendiente con número "+this.nroFactura;
                this.tipomsg=3;
            }
        }
    }
    
    public void quitarFactura(int index){
        Factura f=this.lstFacturasCobro.get(index);
        DecimalFormat df=new DecimalFormat("0000000");
        this.msgNuevoCobro="Quitada factura "+f.getTalonarioFactura().getCodEstablecimiento()+"-"+df.format(f.getNroFactura())+" de la lista.";
        this.tipomsg=1;
        this.lstFacturasCobro.remove(index);
    }
    
    
    public void registrarCobrosFacturas(){
        System.out.println("registrar cobros");
        this.facturaFacade.registrarPagoFacturas(lstFacturasCobro);
        this.lstFacturasCobro.clear();
    }
    
    
    
}
