/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author frioss
 */
@Stateless
public class FuncionalidadFacade extends AbstractFacade<Funcionalidad> {

    @EJB
    private ModuloFacade moduloFacade;
    
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FuncionalidadFacade() {
        super(Funcionalidad.class);
    }

    public List<Funcionalidad> getFuncionalidadesBusquedaModulo(Funcionario funcionario, String busqueda) {
        TypedQuery<Funcionalidad> q = this.em.createQuery("SELECT f FROM Funcionalidad f WHERE LOWER(f.modulo.nombre) LIKE :tx AND f.modulo.activo=true AND :funcionario MEMBER OF f.funcionarioList", Funcionalidad.class);
        q.setParameter("funcionario", funcionario);
        q.setParameter("tx", "%" + busqueda.toLowerCase() + "%");
        return q.getResultList();
    }
    
    public List<Funcionalidad> getFuncionalidades(Funcionario f){
//        Funcionario fun=this.em.find(Funcionario.class, f.getIdfuncionario());
//        List<Funcionalidad> lstfun=new ArrayList<>();
//        for(Funcionalidad fu:fun.getFuncionalidadList()){
//            if(fu.getModulo().getActivo()){
//                //System.out.println("Modulo activo se agrega: "+fu.getModulo().getNombre());
//                lstfun.add(fu);
//            }else{
//                //System.out.println("Modulo inactivo se omite: "+fu.getModulo().getNombre());
//            }
//        }
//        return fun.getFuncionalidadList();
        TypedQuery<Funcionalidad> q = this.em.createQuery("SELECT f FROM Funcionalidad f WHERE f.modulo.activo=true AND :funcionario MEMBER OF f.funcionarioList", Funcionalidad.class);
        q.setParameter("funcionario", f);        
        return q.getResultList();
    }
    
    public boolean isFuncionarioAutorizado(Funcionario f, Integer idfuncionalidad){
        TypedQuery<Funcionalidad> q = this.em.createQuery("SELECT f FROM Funcionalidad f WHERE f.idfuncionalidad=:idfuncionalidad AND :funcionario MEMBER OF f.funcionarioList", Funcionalidad.class);
        q.setParameter("funcionario", f);
        q.setParameter("idfuncionalidad", idfuncionalidad);
        return !q.getResultList().isEmpty();
    }
    
    
}
