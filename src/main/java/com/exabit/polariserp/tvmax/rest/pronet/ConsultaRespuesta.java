package com.exabit.polariserp.tvmax.rest.pronet;

import java.util.List;

/**
 *
 * @author traver
 */
public class ConsultaRespuesta {
    
    private String codServicio;
    private Integer tipoTrx;
    private String codRetorno;
    private String desRetorno;
    private String nombreApellido;
    private Integer cantFilas;

    public Integer getCantFilas() {
        return cantFilas;
    }

    public void setCantFilas(Integer cantFilas) {
        this.cantFilas = cantFilas;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public String getDesRetorno() {
        return desRetorno;
    }

    public void setDesRetorno(String desRetorno) {
        this.desRetorno = desRetorno;
    }

    public String getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }
    
    private List<ConsultaRespuestaDetalle> detalles;

    public List<ConsultaRespuestaDetalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<ConsultaRespuestaDetalle> Detalles) {
        this.detalles = Detalles;
    }


}
