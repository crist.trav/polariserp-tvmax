/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.service;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 */
public class ResultBusquedaSuscDropdown {
    
        private boolean success=true;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

        private List<SuscripcionBasicDropdown> results = new LinkedList<>();

    public List<SuscripcionBasicDropdown> getResults() {
        return results;
    }

    public void setLstSuscripciones(List<SuscripcionBasicDropdown> lstSuscripciones) {
        this.results = lstSuscripciones;
    }

    
}
