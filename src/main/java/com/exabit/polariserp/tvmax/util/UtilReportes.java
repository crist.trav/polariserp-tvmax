package com.exabit.polariserp.tvmax.util;

import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.reportes.DetalleFacturaReporteBean;
import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author cristhian-kn
 */
public class UtilReportes {

    private static final Logger LOG = Logger.getLogger(UtilReportes.class.getName());

    public static List<JasperPrint> generarJPListFacturas(List<Factura> lstFact, Map<Integer, String> hmCobrador) {

        System.out.println("a generar reporte");
        List<JasperPrint> lstJasperPrint = new LinkedList<>();
        Integer idsuscripcion = null;
        for (Factura f : lstFact) {
            String cobrador = null;
            List<DetalleFacturaReporteBean> lstDFReporte = new LinkedList<>();
            for (DetalleFactura def : f.getDetalleFacturaList()) {
                lstDFReporte.add(new DetalleFacturaReporteBean(def));
                if (def.getCuota() != null) {
                    System.out.println("Cuota No null: " + def.getCuota().getSuscripcion().getIdsuscripcion());
                    idsuscripcion = def.getCuota().getSuscripcion().getIdsuscripcion();
                    Funcionario c = def.getCuota().getSuscripcion().getCobrador();
                    cobrador = c.getNombres();
                    if (c.getApellidos() != null) {
                        cobrador = cobrador + " " + c.getApellidos();
                    }
                } else {
                    System.out.println("Cuota null");
                }
            }
            Map<String, Object> params = new HashMap<>();
            params.put("razonsocial", f.getCliente().getRazonSocial());
            params.put("idsuscripcion", idsuscripcion);
            if (hmCobrador.get(idsuscripcion) != null) {
                cobrador = hmCobrador.get(idsuscripcion);
            }
            params.put("cobrador", cobrador);
            String ruc = "";
            if (f.getCliente().getCi() != null) {
                ruc = f.getCliente().getCi().toString();
            }
            if (f.getCliente().getDvRuc() != null) {
                ruc = ruc + "-" + f.getCliente().getDvRuc();
            }
            params.put("ruc", ruc);
            params.put("fecha", f.getFecha());
            params.put("montototal", f.getTotal());
            params.put("montoiva5", f.getIva5());
            params.put("montoiva10", f.getIva10());
            params.put("direccion", f.getCliente().getDireccion().toUpperCase());
            params.put("telefono", f.getCliente().getTelefono1());
            params.put("nrofactura", f.getNroFactura());
            params.put("totalIva", f.getIva10() + f.getIva5());
            if (f.getContado()) {
                params.put("contado", "x");
                params.put("credito", null);
            } else {
                params.put("credito", "x");
                params.put("contado", null);
            }
            ULocale locale = new ULocale("es_PY");  //SPANISH

//            NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
            NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
            String montoTexto = formatter.format(f.getTotal());
            System.out.println("Monto texto: " + montoTexto);
            params.put("montoLetras", montoTexto);
            for (String key : params.keySet()) {
                Object par = params.get(key);
                if (par instanceof String) {
                    par = ((String) par).toUpperCase();
                    params.put(key, par);
                }
            }
            JRDataSource datos = new JRBeanCollectionDataSource(lstDFReporte);
            try {
                String nombreReporte = "rpFactura.jasper";
                if (f.getTalonarioFactura().getNombreFormato() != null) {
                    if (!f.getTalonarioFactura().getNombreFormato().isEmpty()) {
                        nombreReporte = f.getTalonarioFactura().getNombreFormato() + ".jasper";
                    }
                }

                JasperReport jr = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + nombreReporte));
//                JasperReport jr = (JasperReport) JRLoader.loadObject(new File(rutaReporte+nombreReporte));
                JasperPrint jpr = JasperFillManager.fillReport(jr, params, datos);
                lstJasperPrint.add(jpr);
            } catch (JRException ex) {
                LOG.log(Level.SEVERE, "Error al crear reporte", ex);
                System.out.println("Error al crear reporte: " + ex.getMessage());
            }
        }
        return lstJasperPrint;
    }
    public static List<JasperPrint> generarJPListFacturas(List<Factura> lstFact) {

        System.out.println("a generar reporte");
        List<JasperPrint> lstJasperPrint = new LinkedList<>();
        for (Factura f : lstFact) {
            List<DetalleFacturaReporteBean> lstDFReporte = new LinkedList<>();
            for (DetalleFactura def : f.getDetalleFacturaList()) {
                lstDFReporte.add(new DetalleFacturaReporteBean(def));
            }
            Map<String, Object> params = new HashMap<>();
            params.put("razonsocial", f.getCliente().getRazonSocial());
            params.put("idsuscripcion", f.getCliente().getIdcliente());
            if(f.getCliente().getCobrador()!=null){
                String co=f.getCliente().getCobrador().getNombres();
                if(f.getCliente().getCobrador().getApellidos()!=null){
                    co=co+" "+f.getCliente().getCobrador().getApellidos();
                }
                params.put("cobrador", co);
            }
            String ruc = "";
            if (f.getCliente().getCi() != null) {
                DecimalFormat df=new DecimalFormat("#,###,###,###");
                ruc = df.format(f.getCliente().getCi());
            }
            if (f.getCliente().getDvRuc() != null) {
                ruc = ruc + "-" + f.getCliente().getDvRuc();
            }
            params.put("ruc", ruc);
            params.put("fecha", f.getFecha());
            params.put("montototal", f.getTotal());
            params.put("montoiva5", f.getIva5());
            params.put("montoiva10", f.getIva10());
            params.put("direccion", f.getCliente().getDireccion().toUpperCase());
            params.put("telefono", f.getCliente().getTelefono1());
            params.put("nrofactura", f.getNroFactura());
            params.put("totalIva", f.getIva10() + f.getIva5());
            if (f.getContado()) {
                params.put("contado", "x");
                params.put("credito", null);
            } else {
                params.put("credito", "x");
                params.put("contado", null);
            }
            ULocale locale = new ULocale("es_PY");  //SPANISH

//            NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
            NumberFormat formatter = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);
            String montoTexto = formatter.format(f.getTotal());
            System.out.println("Monto texto: " + montoTexto);
            params.put("montoLetras", montoTexto);
            for (String key : params.keySet()) {
                Object par = params.get(key);
                if (par instanceof String) {
                    par = ((String) par).toUpperCase();
                    params.put(key, par);
                }
            }
            JRDataSource datos = new JRBeanCollectionDataSource(lstDFReporte);
            try {
                String nombreReporte = "rpFactura.jasper";
                if (f.getTalonarioFactura().getNombreFormato() != null) {
                    if (!f.getTalonarioFactura().getNombreFormato().isEmpty()) {
                        nombreReporte = f.getTalonarioFactura().getNombreFormato() + ".jasper";
                    }
                }

                JasperReport jr = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + nombreReporte));
//                JasperReport jr = (JasperReport) JRLoader.loadObject(new File(rutaReporte+nombreReporte));
                JasperPrint jpr = JasperFillManager.fillReport(jr, params, datos);
                lstJasperPrint.add(jpr);
            } catch (JRException ex) {
                LOG.log(Level.SEVERE, "Error al crear reporte", ex);
                System.out.println("Error al crear reporte: " + ex.getMessage());
            }
        }
        return lstJasperPrint;
    }

    

}
