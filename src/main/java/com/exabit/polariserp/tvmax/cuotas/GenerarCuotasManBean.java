package com.exabit.polariserp.tvmax.cuotas;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "generarCuotasManBean")
@RequestScoped
public class GenerarCuotasManBean {
    
    private static final Logger LOG = Logger.getLogger(GenerarCuotasManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private GenerarCuotasSesBeanSingleton generarCuotasSesBeanSingleton;
    @EJB
    private SuscripcionFacade suscripcionFacade;
    @EJB
    private CuotaFacade cuotaFacade;
    //</editor-fold>
    
    //<editor-fold desc="Properties" defaultstate="collapsed">
    //    public static final int TIPO_MSG_INFO_HIDDEN=1;
    public static final int TIPO_MSG_INFO = 1;
    public static final int TIPO_MSG_WARNING = 2;
    public static final int TIPO_MSG_ERROR = 3;
    private Integer tipoMensajeGenerar = TIPO_MSG_INFO;
    private boolean chkGenerarTodo = true;
    private Integer idSuscripcionSel;
    private String msgGen;
    private boolean chkGenerarMasReciente = true;
    private String strFDesdeGCuota;
    private String strFHastaGCuota;
    private final SimpleDateFormat sdfGC = new SimpleDateFormat("MMMM yyyy");
    //</editor-fold>
    
    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public boolean getChkGenerarTodo() {
        return chkGenerarTodo;
    }

    public void setChkGenerarTodo(boolean chkGenerarTodo) {
        this.chkGenerarTodo = chkGenerarTodo;
    }
    
    public Integer getIdSuscripcionSel() {
        return idSuscripcionSel;
    }

    public void setIdSuscripcionSel(Integer idSuscripcionSel) {
        this.idSuscripcionSel = idSuscripcionSel;
    }

    public String getMsgGen() {
        return msgGen;
    }

    public void setMsgGen(String msgGen) {
        this.msgGen = msgGen;
    }

    public Integer getTipoMensajeGenerar() {
        return tipoMensajeGenerar;
    }

    public void setTipoMensajeGenerar(Integer tipoMensajeGenerar) {
        this.tipoMensajeGenerar = tipoMensajeGenerar;
    }
    
    public boolean isChkGenerarMasReciente() {
        return chkGenerarMasReciente;
    }

    public void setChkGenerarMasReciente(boolean chkGenerarMasReciente) {
        this.chkGenerarMasReciente = chkGenerarMasReciente;
    }
    
    public String getStrFDesdeGCuota() {
        return strFDesdeGCuota;
    }

    public void setStrFDesdeGCuota(String strFDesdeGCuota) {
        this.strFDesdeGCuota = strFDesdeGCuota;
    }
    
    public String getStrFHastaGCuota() {
        return strFHastaGCuota;
    }

    public void setStrFHastaGCuota(String strFHastaGCuota) {
        this.strFHastaGCuota = strFHastaGCuota;
    }
    //</editor-fold>

    /**
     * Creates a new instance of generarCuotasManBean
     */
    public GenerarCuotasManBean() {
        Calendar c = new GregorianCalendar();
        this.strFDesdeGCuota = this.sdfGC.format(c.getTime());
        this.strFHastaGCuota = this.sdfGC.format(c.getTime());
    }

    private void generarTodos() {
        System.out.println("Generar cuotas general");
        try {
            Date dd = this.sdfGC.parse(this.strFDesdeGCuota);
            Date dh = this.sdfGC.parse(this.strFHastaGCuota);
//            Integer totCuotas = this.cuotaFacade.generarCuotas(dd, dh);
            Calendar ch = new GregorianCalendar();
            ch.setTime(dh);
            ch.add(Calendar.MONTH, 1);
            ch.add(Calendar.DAY_OF_MONTH, -1);
            this.generarCuotasSesBeanSingleton.generarCuotas(dd, ch.getTime(), this.chkGenerarMasReciente);
            this.tipoMensajeGenerar = GenerarCuotasManBean.TIPO_MSG_INFO;
            DecimalFormat df = new DecimalFormat("###,###,###,###");
            this.msgGen = "Fueron generadas " + df.format(this.generarCuotasSesBeanSingleton.getCantCuotasGeneradas()) + " cuotas para el intervalo: " + this.strFDesdeGCuota + " a " + this.strFHastaGCuota;
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir rango de mes/anio", ex);
        }

    }

    private void generarUnico(Suscripcion s) {
        System.out.println("Generar cuotas unico");
        try {
            Date dd = this.sdfGC.parse(this.strFDesdeGCuota);
            Date dh = this.sdfGC.parse(this.strFHastaGCuota);
            Calendar ch = new GregorianCalendar();
            ch.setTime(dh);
            ch.add(Calendar.MONTH, 1);
            ch.add(Calendar.DAY_OF_MONTH, -1);
            this.generarCuotasSesBeanSingleton.generarCuotas(s, dd, ch.getTime(), this.chkGenerarMasReciente);

            this.tipoMensajeGenerar = GenerarCuotasManBean.TIPO_MSG_INFO;
            DecimalFormat df = new DecimalFormat("###,###,###,###");
            this.msgGen = "Fueron generadas " + df.format(this.generarCuotasSesBeanSingleton.getCantCuotasGeneradas()) + " cuotas para el intervalo: " + this.strFDesdeGCuota + " a " + this.strFHastaGCuota + ". Suscripción: " + s.getIdsuscripcion();
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir rango de mes/anio", ex);
        }
    }

    public void generarCuotas() {
        if (this.chkGenerarTodo) {
            System.out.println("Chk todos");
            this.generarTodos();
        } else {
            if (this.idSuscripcionSel != null) {
                System.out.println("idSuscripcion: " + this.idSuscripcionSel);
                Suscripcion s = this.suscripcionFacade.find(this.idSuscripcionSel);

                if (s != null) {
                    System.out.println("suscripcion: " + s);
                    this.generarUnico(s);
                } else {
                    System.out.println("suscripcion nulll ");
                    this.generarTodos();
                }
            } else {
                System.out.println("id suscripcion null");
                this.generarTodos();
            }

        }
    }

    public Integer getUltimoPorcentaje() {
        return this.generarCuotasSesBeanSingleton.getUltPorcEnviado();
    }

    public boolean isGenerando() {
        return this.generarCuotasSesBeanSingleton.isGenerando();
    }

    public String getMsgCantidadGenerado() {
        DecimalFormat df = new DecimalFormat("###,###,###,###");
        return this.msgGen = "Fueron generadas " + df.format(this.generarCuotasSesBeanSingleton.getCantCuotasGeneradas()) + " cuotas para el intervalo: " + this.sdfGC.format(this.generarCuotasSesBeanSingleton.getDesdeFechaGenActual()) + " - " + this.sdfGC.format(this.generarCuotasSesBeanSingleton.getHastaFechaGenActual());
    }
}
