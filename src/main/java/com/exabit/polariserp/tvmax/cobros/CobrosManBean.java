package com.exabit.polariserp.tvmax.cobros;

import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.reportes.UtilReportesSesBean;
import com.exabit.polariserp.tvmax.sesbeans.CobroFacade;
import com.exabit.polariserp.tvmax.sesbeans.ConceptoFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.ibm.icu.text.RuleBasedNumberFormat;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "cobrosManBean")
@ViewScoped
public class CobrosManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(CobrosManBean.class.getName());

    @EJB
    private ConceptoFacade conceptoFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private UtilReportesSesBean utilReportesSesBean;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private CobroFacade cobroFacade;

    private List<ItemResumenCobro> lstResumenCobros = new ArrayList<>();

    public List<ItemResumenCobro> getLstResumenCobros() {
        return lstResumenCobros;
    }

    public void setLstResumenCobros(List<ItemResumenCobro> lstResumenCobros) {
        this.lstResumenCobros = lstResumenCobros;
    }

    private List<Cobro> lstCobros = new LinkedList<>();

    public List<Cobro> getLstCobros() {
        return lstCobros;
    }

    public void setLstCobros(List<Cobro> lstCobros) {
        this.lstCobros = lstCobros;
    }

    private Integer idCobradorSel;

    public Integer getIdCobradorSel() {
        return idCobradorSel;
    }

    public void setIdCobradorSel(Integer idCobradorSel) {
        this.idCobradorSel = idCobradorSel;
    }

    private String fechaInicio;

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    private String fechaFin;

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    private Paginador paginador;

    public Paginador getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador paginador) {
        this.paginador = paginador;
    }

    private Integer idConceptoFiltro;

    public Integer getIdConceptoFiltro() {
        return idConceptoFiltro;
    }

    public void setIdConceptoFiltro(Integer idConceptoFiltro) {
        this.idConceptoFiltro = idConceptoFiltro;
    }

    private boolean anuladosFiltro = false;

    public boolean isAnuladosFiltro() {
        return anuladosFiltro;
    }

    public void setAnuladosFiltro(boolean anuladosFiltro) {
        this.anuladosFiltro = anuladosFiltro;
    }

    private String txBusquedaCobro = "";

    public String getTxBusquedaCobro() {
        return txBusquedaCobro;
    }

    public void setTxBusquedaCobro(String txBusquedaCobro) {
        this.txBusquedaCobro = txBusquedaCobro;
    }

    private Integer idFuncionarioRegistroSel;

    public Integer getIdFuncionarioRegistroSel() {
        return idFuncionarioRegistroSel;
    }

    public void setIdFuncionarioRegistroSel(Integer idFuncionarioRegistroSel) {
        this.idFuncionarioRegistroSel = idFuncionarioRegistroSel;
    }

    /**
     * Creates a new instance of CobrosManBean
     */
    private final SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

    public CobrosManBean() {
        Calendar cinicio;
        Calendar cfin;
        Calendar c = new GregorianCalendar();
        cinicio = new GregorianCalendar();
        cfin = new GregorianCalendar();
        cinicio.set(Calendar.DAY_OF_MONTH, 1);
        cfin.set(Calendar.DAY_OF_MONTH, 1);
        cfin.add(Calendar.MONTH, 1);
        cfin.add(Calendar.DAY_OF_MONTH, -1);
        this.fechaInicio = this.sdf.format(cinicio.getTime());
        this.fechaFin = this.sdf.format(cfin.getTime());
        this.paginador = new Paginador(this.lstCobros);
    }

    @PostConstruct
    private void postConstruct() {
        this.filtrarCobros();
    }

    public List<Funcionario> getLstCobradores() {
        return this.funcionarioFacade.getCobradores();
    }

    public List<Funcionario> getLstFuncionarios() {
        return this.funcionarioFacade.findAll();
    }

    public void filtrarCobros() {
        boolean ordenAscendente=true;
        System.out.println("Se filstra: " + this.idCobradorSel + " " + this.fechaInicio + " " + this.fechaFin);
        List<Cobro> lstOrd = new ArrayList<>();
        this.paginador.limpiarLista();
        if (this.idCobradorSel == null) {
            if (this.idConceptoFiltro == null) {
                try {
                    lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));

                } catch (ParseException ex) {
                    System.out.println("Error al convertir string a date: " + ex);
                }
            } else {
                Concepto c = this.conceptoFacade.find(this.idConceptoFiltro);
                try {
                    lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(c, this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));
                } catch (ParseException ex) {
                    System.out.println("Error al convertir string a date: " + ex);
                }
            }
        } else {
            Funcionario cobrador = this.funcionarioFacade.find(this.idCobradorSel);
            try {
                if (cobrador == null) {
                    if (this.idConceptoFiltro == null) {
                        lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));
                    } else {
                        Concepto c = this.conceptoFacade.find(this.idConceptoFiltro);
                        lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(c, this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));
                    }
                } else {
                    if (this.idConceptoFiltro == null) {
                        lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(cobrador, this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));
                    } else {
                        Concepto c = this.conceptoFacade.find(this.idConceptoFiltro);
                        lstOrd.addAll(this.ordenarPorFactura(this.cobroFacade.getCobrosOrdenFechaDesc(cobrador, c, this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin), this.txBusquedaCobro), ordenAscendente));
                    }
                }

            } catch (ParseException ex) {
                System.out.println("Error al convertir string a date: " + ex);
            }
        }
        this.paginador.agregarItemsLista(lstOrd);
        System.out.println("Filtro anulados: " + this.anuladosFiltro);
        List<Cobro> lstCobrosFinal = new ArrayList<>();
        for (Object c : this.paginador.getListaCompleta()) {
            Cobro co = (Cobro) c;
            if (this.anuladosFiltro) {
                if (co.getAnulado()) {
                    lstCobrosFinal.add(co);
                }
            } else {
                if (!co.getAnulado()) {
                    lstCobrosFinal.add(co);
                }
            }
        }
        if (this.idFuncionarioRegistroSel != null) {
            List<Cobro> lstCobroFR = new ArrayList<>();
            for (Cobro co : lstCobrosFinal) {
                if (co.getFuncionarioRegistro() != null) {
                    if (co.getFuncionarioRegistro().getIdfuncionario().equals(this.idFuncionarioRegistroSel)) {
                        lstCobroFR.add(co);
                    }
                }
            }
            lstCobrosFinal.clear();
            lstCobrosFinal.addAll(lstCobroFR);
        }
        this.paginador.limpiarLista();
        this.paginador.agregarItemsLista(lstCobrosFinal);
    }

    public Integer getTotalCobros() {
        Integer tot = 0;
        for (Cobro c : this.lstCobros) {
            tot = tot + c.getMonto();
        }
        return tot;
    }

    public void vistaPreviaImpresion() {

        try {
            Date d = this.sdf.parse(fechaInicio);
            Date h = this.sdf.parse(fechaFin);
            Map<String, Object> par = new HashMap<>();
            par.put("fdesde", d);
            par.put("fhasta", h);
            if (this.idFuncionarioRegistroSel == null) {
                par.put("funcionarioregistro", "(Cualquier usuario)");
            } else {
                Funcionario f = this.funcionarioFacade.find(this.idFuncionarioRegistroSel);
                String fr = f.getIdfuncionario() + "-" + f.getNombres();
                if (f.getApellidos() != null) {
                    fr = fr + " " + f.getApellidos();
                }
                par.put("funcionarioregistro", fr);
            }
            if (this.idCobradorSel != null) {
                Funcionario c = this.funcionarioFacade.find(this.idCobradorSel);
                String nc = c.getNombres();
                if (c.getApellidos() != null) {
                    nc = nc + " " + c.getApellidos();
                }
                if (c.getCi() != null) {
                    DecimalFormat df = new DecimalFormat("###,###,###,###");
                    nc = nc + "(ci:" + df.format(c.getCi()) + ")";
                }
                par.put("cobrador", nc);
            }
            if (this.idConceptoFiltro != null) {
                Concepto con = this.conceptoFacade.find(this.idConceptoFiltro);
                par.put("concepto", con.getNombre());
            }
            Integer totalCobro = 0;
            for (Object co : this.paginador.getListaCompleta()) {
                Cobro c = (Cobro) co;
                totalCobro = totalCobro + c.getMonto();
            }
            System.out.println("total de cobro para reporte: " + totalCobro);
            RuleBasedNumberFormat formatter = new RuleBasedNumberFormat(RuleBasedNumberFormat.SPELLOUT);
            par.put("totalletras", formatter.format(totalCobro).toUpperCase());
            if (this.idCobradorSel == null && this.idConceptoFiltro == null) {
                System.out.println("lista cobros " + this.paginador.getListaCompleta());
                this.utilReportesSesBean.generarReporteCobro(this.paginador.getListaCompleta(), par, UtilReportesSesBean.REPORTE_COBRO_GENERAL);
            } else if (this.idConceptoFiltro != null && this.idCobradorSel != null) {
                this.utilReportesSesBean.generarReporteCobro(this.paginador.getListaCompleta(), par, UtilReportesSesBean.REPORTE_COBRO_COBRADOR_CONCEPTO);
            } else if (this.idCobradorSel == null && this.idConceptoFiltro != null) {
                this.utilReportesSesBean.generarReporteCobro(this.paginador.getListaCompleta(), par, UtilReportesSesBean.REPORTE_COBRO_CONCEPTO);
            } else if (this.idCobradorSel != null && this.idConceptoFiltro == null) {
                this.utilReportesSesBean.generarReporteCobro(this.paginador.getListaCompleta(), par, UtilReportesSesBean.REPORTE_COBRO_COBRADOR);
            }

        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir fecha desde string", ex);
        }
    }

//    public void vistaPreviaImpresion(){
//        this.utilReportesSesBean.generarReporteCobro(this.lstCobros);
//    }
    public void recargarCobros() {
        this.paginador.limpiarLista();
//        try {
//            this.paginador.agregarItemsLista(this.cobroFacade.getCobrosOrdenFechaDesc(this.sdf.parse(this.fechaInicio), this.sdf.parse(this.fechaFin)));
        this.filtrarCobros();
//        } catch (ParseException ex) {
//            System.out.println("Error al convertir string a date: " + ex);
//        }
    }

    public void actualizarResumen() {
        Map<Integer, ItemResumenCobro> hmResumen = new HashMap<>();
        List<Cobro> lstc = new ArrayList<>();
        lstc.addAll(this.paginador.getListaCompleta());
        for (Cobro c : lstc) {
            if (hmResumen.get(c.getConcepto().getIdconcepto()) == null) {
                ItemResumenCobro irc = new ItemResumenCobro();
                irc.setConcepto(c.getConcepto().getNombre());
                irc.setMonto(c.getMonto());
                hmResumen.put(c.getConcepto().getIdconcepto(), irc);
            } else {
                ItemResumenCobro irc = hmResumen.get(c.getConcepto().getIdconcepto());
                irc.setMonto(irc.getMonto() + c.getMonto());
                hmResumen.put(c.getConcepto().getIdconcepto(), irc);
            }
        }
        this.lstResumenCobros.clear();
        this.lstResumenCobros.addAll(hmResumen.values());
    }

    public Integer getTotalResumen() {
        int total = 0;
        for (ItemResumenCobro irc : this.lstResumenCobros) {
            total = total + irc.getMonto();
        }
        return total;
    }

    public List<Cobro> ordenarPorFactura(List<Cobro> lst, boolean ascendente) {
        List<Cobro> lstCobroOrdenado = new ArrayList<>();
        Map<Integer, List<Cobro>> mpEstFactura = new HashMap<>();
        List<Cobro> lstCobrosSinFactura = new ArrayList<>();
        for (Cobro c : lst) {
            if (c.getDetalleFactura() == null) {
                lstCobrosSinFactura.add(c);
            } else {
                if (mpEstFactura.get(c.getDetalleFactura().getFactura().getTalonarioFactura().getIdtalonario()) == null) {
                    mpEstFactura.put(c.getDetalleFactura().getFactura().getTalonarioFactura().getIdtalonario(), new ArrayList<>());
                }
                mpEstFactura.get(c.getDetalleFactura().getFactura().getTalonarioFactura().getIdtalonario()).add(c);
            }
        }
        lstCobroOrdenado.addAll(lstCobrosSinFactura);
        for (Integer codEst : mpEstFactura.keySet()) {
            List<Cobro> lstco = mpEstFactura.get(codEst);
            int listSize=lstco.size();
            for (int x = 0; x < listSize - 1; x++) {
                for (int y = x + 1; y < listSize; y++) {
                    Cobro cx = lstco.get(x);
                    Cobro cy = lstco.get(y);
                    Factura fx = lstco.get(x).getDetalleFactura().getFactura();
                    Factura fy = lstco.get(y).getDetalleFactura().getFactura();
                    if (ascendente) {
                        if (fx.getNroFactura() > fy.getNroFactura()) {
                            lstco.set(x, cy);
                            lstco.set(y, cx);
                        }
                    } else {
                        if (fx.getNroFactura() < fy.getNroFactura()) {
                            lstco.set(x, cy);
                            lstco.set(y, cx);
                        }
                    }

                }
            }
            lstCobroOrdenado.addAll(lstco);
        }
        return lstCobroOrdenado;
    }
}
