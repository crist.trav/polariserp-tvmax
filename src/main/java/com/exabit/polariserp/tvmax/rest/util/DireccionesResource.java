package com.exabit.polariserp.tvmax.rest.util;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.google.gson.Gson;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author traver
 */
@Path("direcciones")
public class DireccionesResource {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of DireccionesResource
     */
    public DireccionesResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.polariserp.web.DireccionesResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText(@QueryParam("q") String q) {
        System.out.println("Se busca direcciones: "+q);
        Gson gson=new Gson();
        Results c=new Results();
        TypedQuery<Suscripcion> qs=em.createQuery("SELECT s FROM Suscripcion s WHERE FUNCTION('REPLACE', LOWER(s.direccion), ' ','') LIKE :q", Suscripcion.class);
        TypedQuery<Cliente> qc=em.createQuery("SELECT c FROM Cliente c WHERE FUNCTION('REPLACE', LOWER(c.direccion), ' ','') LIKE :q", Cliente.class);
        qs.setParameter("q", q.replace(" ", "").toLowerCase()+"%");
        qc.setParameter("q", q.replace(" ", "").toLowerCase()+"%");
        qs.setMaxResults(50);
        qs.setMaxResults(50);
        for(Suscripcion s:qs.getResultList()){
            c.getResults().add(new ContentItem(s.getDireccion()));
        }
        for(Cliente cl:qc.getResultList()){
            c.getResults().add(new ContentItem(cl.getDireccion()));
        }
        return gson.toJson(c);
    }
}

class Results {
    
    private final Set<ContentItem> results = new HashSet<>();

    public Set<ContentItem> getResults() {
        return results;
    }

}

class ContentItem {
    
    private String title;
    
    public ContentItem(String title){
        this.title=title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContentItem other = (ContentItem) obj;
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        return true;
    }
}
