/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sorteo;

/**
 *
 * @author frioss
 */
public class AuxExcluidos {

    public AuxExcluidos(Integer idSuscripcion, String razonSocial, String descripcionSorteo) {
        this.idSuscripcion = idSuscripcion;
        this.razonSocial = razonSocial;
        this.descripcionSorteo = descripcionSorteo;
    }
    
    private Integer idSuscripcion;
    private String razonSocial;
    private String descripcionSorteo;

    public Integer getIdSuscripcion() {
        return idSuscripcion;
    }

    public void setIdSuscripcion(Integer idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDescripcionSorteo() {
        return descripcionSorteo;
    }

    public void setDescripcionSorteo(String descripcionSorteo) {
        this.descripcionSorteo = descripcionSorteo;
    }
    
}
