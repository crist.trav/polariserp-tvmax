/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "suscripciones_excluidas_sorteo")
@NamedQueries({
    @NamedQuery(name = "SuscripcionesExcluidasSorteo.findAll", query = "SELECT s FROM SuscripcionesExcluidasSorteo s")})
public class SuscripcionesExcluidasSorteo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SuscripcionesExcluidasSorteoPK suscripcionesExcluidasSorteoPK;
    @JoinColumn(name = "sorteo_idsorteo", referencedColumnName = "idsorteo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sorteo sorteo;
    @JoinColumn(name = "suscripcion_idsuscripcion", referencedColumnName = "idsuscripcion", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Suscripcion suscripcion;

    public SuscripcionesExcluidasSorteo() {
    }

    public SuscripcionesExcluidasSorteo(SuscripcionesExcluidasSorteoPK suscripcionesExcluidasSorteoPK) {
        this.suscripcionesExcluidasSorteoPK = suscripcionesExcluidasSorteoPK;
    }

    public SuscripcionesExcluidasSorteo(int suscripcionIdsuscripcion, int sorteoIdsorteo) {
        this.suscripcionesExcluidasSorteoPK = new SuscripcionesExcluidasSorteoPK(suscripcionIdsuscripcion, sorteoIdsorteo);
    }

    public SuscripcionesExcluidasSorteoPK getSuscripcionesExcluidasSorteoPK() {
        return suscripcionesExcluidasSorteoPK;
    }

    public void setSuscripcionesExcluidasSorteoPK(SuscripcionesExcluidasSorteoPK suscripcionesExcluidasSorteoPK) {
        this.suscripcionesExcluidasSorteoPK = suscripcionesExcluidasSorteoPK;
    }

    public Sorteo getSorteo() {
        return sorteo;
    }

    public void setSorteo(Sorteo sorteo) {
        this.sorteo = sorteo;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (suscripcionesExcluidasSorteoPK != null ? suscripcionesExcluidasSorteoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuscripcionesExcluidasSorteo)) {
            return false;
        }
        SuscripcionesExcluidasSorteo other = (SuscripcionesExcluidasSorteo) object;
        if ((this.suscripcionesExcluidasSorteoPK == null && other.suscripcionesExcluidasSorteoPK != null) || (this.suscripcionesExcluidasSorteoPK != null && !this.suscripcionesExcluidasSorteoPK.equals(other.suscripcionesExcluidasSorteoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.SuscripcionesExcluidasSorteo[ suscripcionesExcluidasSorteoPK=" + suscripcionesExcluidasSorteoPK + " ]";
    }
    
}
