package com.exabit.polariserp.tvmax.cobros;

/**
 *
 * @author cristhian-kn
 */
public class ItemResumenCobro {
    
        private String concepto;

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }


    private Integer monto = 0;

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    
}
