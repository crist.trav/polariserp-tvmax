/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.tiposuscripcion;

import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "tipoSuscripcionManBean")
@ViewScoped
public class TipoSuscripcionManBean implements Serializable {

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;
    
    
    
    /**
     * Creates a new instance of TipoSuscripcionManBean
     */
    public TipoSuscripcionManBean() {
    }
    
    public List<TipoSuscripcion> getLstTipoSuscripcion(){
        return this.tipoSuscripcionFacade.findAll();
        
    }
    
}
