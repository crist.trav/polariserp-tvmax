/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.reportes;

import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.reportes.cobros.CobroPorCobradorItem;
import com.exabit.polariserp.tvmax.reportes.detallesuscripcion.DetalleSuscripcionItemRp;
import com.exabit.polariserp.tvmax.reportes.suscripciones.SuscripcionCantidadCuotasItem;
import com.exabit.polariserp.tvmax.sesbeans.CobroFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.suscripciones.ItemResumenSuscripciones;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.UtilConversion;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class UtilReportesSesBean {

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private CobroFacade cobroFacade;

    private static final Logger LOG = Logger.getLogger(UtilReportesSesBean.class.getName());

    public static String REPORTE_COBRO_GENERAL = "rpCobro.jasper";
    public static String REPORTE_COBRO_COBRADOR_CONCEPTO = "rpCobroPorCobradorConcepto.jasper";
    public static String REPORTE_COBRO_CONCEPTO = "rpCobroConcepto.jasper";
    public static String REPORTE_COBRO_COBRADOR = "rpCobroCobrador.jasper";

    public static String REPORTE_SUSCRIPCIONES_CUOTAS = "rpSuscripcionesCuoPend.jasper";
    public static String REPORTE_SUSCRIPCIONES_CUOTAS_GRAL = "rpSuscripcionesCuoPendGral.jasper";
    public static String REPORTE_SUSCRIPCIONES_CUOTAS_GRAL_CON_COBRO = "rpSuscripcionesCuoPendGralConCobro.jasper";
    public static String REPORTE_SUSCRIPCIONES_CUOTAS_CON_COBRO = "rpSuscripcionesCuoPendConCobro.jasper";

    public static String REPORTE_DETALLE_SUSCRIPCION = "rpDetalleSuscripcion.jasper";
    public static String SUBREPORTE_RESUMEN_SUSCRIPCION = "subrpResumenSuscripciones.jasper";

//    public void generarReporteCobro(Funcionario cobrador, Date desde, Date hasta){
//        DecimalFormat df=new DecimalFormat("0000000");
//        List<Cobro> lstc=this.cobroFacade.getCobrosOrdenFechaDesc(cobrador, desde, hasta);
//        this.generarReporteCobro(lstc);
//    }
    public void generarReporteCobro(List<Cobro> lstCobros, Map<String, Object> parametros, String reporte) {

        DecimalFormat df = new DecimalFormat("0000000");
        List<CobroPorCobradorItem> lstItemsReport = new ArrayList<>();
        for (Cobro c : lstCobros) {
            CobroPorCobradorItem ci = new CobroPorCobradorItem();
            ci.setConcepto(c.getConcepto().getNombre());
            String cobra = c.getCobrador().getNombres();
            if (c.getCobrador().getApellidos() != null) {
                cobra = cobra + " " + c.getCobrador().getApellidos();
            }
            ci.setCobrador(cobra);
            String cli = "";
            String ruc = "";
            if (c.getSuscripcion() != null) {
                cli = c.getSuscripcion().getCliente().getRazonSocial();
                if (c.getSuscripcion().getCliente().getCi() != null) {
                    DecimalFormat def = new DecimalFormat("#,###,###,###");
                    ruc = def.format(c.getSuscripcion().getCliente().getCi());
                    if (c.getSuscripcion().getCliente().getDvRuc() != null) {
                        ruc = ruc + "-" + c.getSuscripcion().getCliente().getDvRuc();
                    }

                }
            }
            ci.setCliente(cli);
            ci.setRuc(ruc);
            if (c.getCuota() != null) {
                Cuota cu = c.getCuota();
                ci.setFechaven(cu.getFechaVencimiento());
                ci.setMontocuota(cu.getMontoCuota());
                ci.setIdsuscripcion(cu.getSuscripcion().getIdsuscripcion());
            } else {
                ci.setMontocuota(0);
            }
            if (c.getDetalleFactura() != null) {
                ci.setFactura(c.getDetalleFactura().getFactura().getTalonarioFactura().getCodEstablecimiento() + "-" + df.format(c.getDetalleFactura().getFactura().getNroFactura()));
            } else {
                ci.setFactura("(Sin factura)");
            }
            ci.setFechapago(c.getFecha());
            ci.setMontopagado(c.getMonto());
            lstItemsReport.add(ci);
        }
        JRBeanCollectionDataSource jrdata = new JRBeanCollectionDataSource(lstItemsReport);
        try {
            String uriReporte = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES) + reporte;
            JasperReport jp = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + reporte));
            JasperPrint jprint = JasperFillManager.fillReport(jp, parametros, jrdata);
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=reportecobrador.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            List<JasperPrint> lstJP = new ArrayList<>();
            lstJP.add(jprint);
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al generar reporte de cobros", ex);
        }
    }

    public void generarReporteSuscCuotasPend(List<Suscripcion> lsts, Map<String, Object> parametros, Date desde, Date hasta, Funcionario cobrador, String reporte) {
        if (cobrador != null) {

            String nombrecobra = cobrador.getNombres();
            if (cobrador.getApellidos() != null) {
                nombrecobra = nombrecobra + " " + cobrador.getApellidos();
            }
            parametros.put("cobrador", nombrecobra);
        } else {
            parametros.put("cobrador", "(Todos)");
        }
        List<SuscripcionCantidadCuotasItem> lstSCCI = new ArrayList<>();
        int cantCuotasO = 0;
        int totalOtrosCobros=0;
        int cantSuscOtrosCobros=0;
        for (Suscripcion s : lsts) {
            SuscripcionCantidadCuotasItem scci = new SuscripcionCantidadCuotasItem();
            scci.setIdsuscripcion(s.getIdsuscripcion());
            scci.setDireccion(s.getDireccion());
            scci.setCliente(s.getCliente().getRazonSocial());
            scci.setFechaingreso(s.getFechaSuscripcion());
//            scci.setMonto(this.cuotaFacade.getMontoDeuda(s, desde, hasta));
            scci.setMonto(this.getMontoDeuda(s.getIdsuscripcion(), desde, hasta));
            scci.setFechaultimopago(this.cuotaFacade.getFechaUltimoPago(s));
//            scci.setCantcuotas(this.cuotaFacade.getCantidadCuotasPendientes(s, desde, hasta));
            scci.setCantcuotas(this.getCantidadCuotasPendientes(s.getIdsuscripcion(), desde, hasta));
            if (scci.getCantcuotas().equals(0)) {
                cantCuotasO++;
            }
            scci.setFechavenc(this.cuotaFacade.getFechaCuotaPendienteAntigua(s));
            if (scci.getFechavenc() == null) {
                Calendar cHasta = new GregorianCalendar();
                cHasta.setTime(this.cuotaFacade.getFechaCuotaReciente(s));
                cHasta.add(Calendar.MONTH, 1);
                scci.setFechavenc(cHasta.getTime());
            }
//            scci.setMontocuota(this.cuotaFacade.getUltimoPrecioCuotaPend(s));
            scci.setMontocuota(s.getPrecio());
            scci.setTelefono(s.getCliente().getTelefono1());
            scci.setServicio(s.getServicio().getNombre().toUpperCase());
            if(s.getCliente().getCi()!=null){
                DecimalFormat df=new DecimalFormat("###,###,###,###");
                String ruc=df.format(s.getCliente().getCi());
                if(s.getCliente().getDvRuc()!=null){
                    ruc=ruc+"-"+s.getCliente().getDvRuc();
                }
                scci.setRuc(ruc);
            }
            lstSCCI.add(scci);
            List<Cuota>lstCuotasOtrosServ=this.cuotaFacade.getCuotasPendientesOtrosServicios(s);
            if(!lstCuotasOtrosServ.isEmpty()){
                SuscripcionCantidadCuotasItem scciSec=new SuscripcionCantidadCuotasItem();
                scciSec.setServicio(lstCuotasOtrosServ.get(0).getServicio().getNombre());
                scciSec.setCliente(s.getCliente().getRazonSocial());
                scciSec.setDireccion(s.getDireccion());
                scciSec.setFechaingreso(s.getFechaSuscripcion());
                scciSec.setFechaultimopago(this.cuotaFacade.getFechaUltimoPagoOtrosServ(s));
                scciSec.setMonto(this.cuotaFacade.getMontoDeudaOtrosServicios(s));
                scciSec.setCantcuotas(lstCuotasOtrosServ.size());
                scciSec.setMontocuota(lstCuotasOtrosServ.get(0).getMontoCuota());
                scciSec.setIdsuscripcion(s.getIdsuscripcion());
                scciSec.setRuc(scci.getRuc());
                scciSec.setTelefono(scci.getTelefono());
                scciSec.setFechavenc(this.cuotaFacade.getFechaCuotaPendienteAntiguaOtrosServ(s, new Date()));
                lstSCCI.add(scciSec);
                totalOtrosCobros=totalOtrosCobros+scciSec.getMonto();
                cantSuscOtrosCobros++;
            }
        }
//        parametros.put("montoOtrosCobros", totalOtrosCobros);
        parametros.put("cantsuscaldia", cantCuotasO);
        JRBeanCollectionDataSource jrdata = new JRBeanCollectionDataSource(lstSCCI);
        try {
            Map<String, Object> paramSubreport=new HashMap<>();
            paramSubreport.put("montoOtrosCobros", totalOtrosCobros);
            paramSubreport.put("cantSuscOtrosCobros", cantSuscOtrosCobros);
            parametros.put("parametrosSubreporte", paramSubreport);
            //Subreporte
            JasperReport srResumen = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + SUBREPORTE_RESUMEN_SUSCRIPCION));
            parametros.put("subrResumen", srResumen);

            List<ItemResumenSuscripciones> lstResSusc = new LinkedList<>();

            lstResSusc.addAll(this.getResumen(lsts, desde, hasta));

            JRBeanCollectionDataSource dsResumenSusc = new JRBeanCollectionDataSource(lstResSusc);

            parametros.put("datasourceSubreporte", dsResumenSusc);

            InputStream is = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + reporte);
            JasperReport jp = (JasperReport) JRLoader.loadObject(is);

            JasperPrint jprint = JasperFillManager.fillReport(jp, parametros, jrdata);
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=reportesuscripcioncuota.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            List<JasperPrint> lstJP = new ArrayList<>();
            lstJP.add(jprint);
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al generar reporte de cobros", ex);
        }
    }

    public void generarReporteSuscCuotasPend(List<Suscripcion> lsts, Map<String, Object> parametros, Date desde, Date hasta, Date desdeCobro, Date hastaCobro, Funcionario cobrador, String reporte) {
        List<Cobro> lstCobros = new LinkedList<>();
        if (cobrador != null) {
            lstCobros.addAll(this.cobroFacade.getCobrosOrdenFechaDesc(cobrador, EntidadesEstaticas.CONCEPTO_COBRO_CUOTA, desdeCobro, hastaCobro, ""));
        } else {
            lstCobros.addAll(this.cobroFacade.getCobrosOrdenFechaDesc(EntidadesEstaticas.CONCEPTO_COBRO_CUOTA, desdeCobro, hastaCobro, ""));
        }
        Integer totalCobro = 0;
        for (Cobro c : lstCobros) {
            totalCobro = totalCobro + c.getMonto();
        }
        parametros.put("totalcobro", totalCobro);
        parametros.put("cobrodesde", desdeCobro);
        parametros.put("cobrohasta", hastaCobro);
        this.generarReporteSuscCuotasPend(lsts, parametros, desde, hasta, cobrador, reporte);
    }

    public void generarReporteDetalleSuscripcion(Integer ids) {
        Suscripcion s = this.suscripcionFacade.find(ids);
        List<Cuota> lstCuotas = new ArrayList<>();
        lstCuotas.addAll(this.cuotaFacade.getCuotaOrdenVencimiento(s));

        Map<String, Object> par = new HashMap<>();
        par.put("idsuscripcion", s.getIdsuscripcion());
        par.put("cliente", s.getCliente().getRazonSocial());
        par.put("fechasuscripcion", s.getFechaSuscripcion());
        par.put("estado", s.getEstado().getNombre());
        par.put("tipovivienda", s.getTipoVivienda().getNombre());
        String cobrador = s.getCobrador().getNombres();
        if (s.getCobrador().getApellidos() != null) {
            cobrador = cobrador + " " + s.getCobrador().getApellidos();
        }
        par.put("cobrador", cobrador);
        par.put("barrio", s.getBarrio().getNombre());
        par.put("direccion", s.getDireccion());
        par.put("nromedidor", s.getNroMedidorElectrico());

        List<DetalleSuscripcionItemRp> lstDetalleSusc = new ArrayList<>();
        for (Cuota c : lstCuotas) {
            DetalleSuscripcionItemRp ds = new DetalleSuscripcionItemRp();
            UtilConversion uc = new UtilConversion();
            String mescuota = c.getAnioCuota() + "/" + uc.getNombreMes((int) c.getMesCuota());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MMM");
            Calendar cal = new GregorianCalendar(c.getAnioCuota(), c.getMesCuota() - 1, 1);
            ds.setMescuota(sdf.format(cal.getTime()));
            ds.setFechavencimiento(c.getFechaVencimiento());
            ds.setFechapago(c.getFechaPago());
            ds.setMonto(c.getMontoCuota());
            ds.setObservacion(c.getObservacion());
            ds.setPagado(c.getMontoEntrega());
            if (c.getMontoEntrega() != null) {
                ds.setSaldo(c.getMontoCuota() - c.getMontoEntrega());
            } else {
                ds.setSaldo(c.getMontoCuota());
            }
            if (!c.getDetalleFacturaList().isEmpty()) {
                Factura f = null;
                for (DetalleFactura df : c.getDetalleFacturaList()) {
                    if (!df.getFactura().getAnulado()) {
                        if (f == null) {
                            f = df.getFactura();
                        } else {
                            if (df.getFactura().getFecha().after(f.getFecha())) {
                                f = df.getFactura();
                            }
                        }
                    }
                }
                String fact = "";
                if (f != null) {
                    fact = f.getTalonarioFactura().getCodEstablecimiento() + "-" + f.getNroFactura();
                    if (!f.getCancelado()) {
                        ds.setObservacion("(Pend.)" + (ds.getObservacion() != null ? ds.getObservacion() : ""));
                    }

                }

                ds.setFactura(fact);
            }
            lstDetalleSusc.add(ds);
        }

        try {
            JasperReport jp = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/" + REPORTE_DETALLE_SUSCRIPCION));
            JRBeanCollectionDataSource jrdata = new JRBeanCollectionDataSource(lstDetalleSusc);
            JasperPrint jprint = JasperFillManager.fillReport(jp, par, jrdata);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=reportedetallesuscripcion.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            List<JasperPrint> lstJP = new ArrayList<>();
            lstJP.add(jprint);
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "error al cargar reporte de detalle de suscripcion", ex);
        }
    }

    public Integer getCantidadCuotasPendientes(Integer idsuscripcion, Date desde, Date hasta) {
        Suscripcion s = this.suscripcionFacade.find(idsuscripcion);
        Date fdesde = desde;
        Date fhasta = hasta;
        Calendar chasta = new GregorianCalendar();
        chasta.setTime(fhasta);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        return this.cuotaFacade.getCantidadCuotasPendientes(s, fdesde, chasta.getTime());
    }

    public Integer getMontoDeuda(Integer idsuscripcion, Date desde, Date hasta) {
        Suscripcion s = this.suscripcionFacade.find(idsuscripcion);
        Date fhasta = hasta;
        Calendar chasta = new GregorianCalendar();
        chasta.setTime(fhasta);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        return this.cuotaFacade.getTotalDeuda(s, desde, chasta.getTime());
    }
    
    public List<ItemResumenSuscripciones> getResumen(List<Suscripcion> lstSusc, Date desde, Date hasta) {

        Map<Integer, ItemResumenSuscripciones> mpResumen = new HashMap<>();
        for (Suscripcion s : lstSusc) {
            Integer cantcuo = this.getCantidadCuotasPendientes(s.getIdsuscripcion(), desde, hasta);
            ItemResumenSuscripciones ir = mpResumen.get(cantcuo);
            if (ir != null) {
                ir.setCantSuscripciones(ir.getCantSuscripciones() + 1);
                ir.setMonto(ir.getMonto() + this.getMontoDeuda(s.getIdsuscripcion(), desde, hasta));
                mpResumen.put(cantcuo, ir);
            } else {
                ItemResumenSuscripciones ire = new ItemResumenSuscripciones();
                ire.setNroCuotas(cantcuo);
                ire.setCantSuscripciones(1);
                ire.setMonto(this.getMontoDeuda(s.getIdsuscripcion(), desde, hasta));
                //                lstr.add(ire);
                mpResumen.put(cantcuo, ire);
            }
        }
        List<ItemResumenSuscripciones> lstresumen = new LinkedList<>();
        lstresumen.addAll(mpResumen.values());
        return lstresumen;
    }
}
