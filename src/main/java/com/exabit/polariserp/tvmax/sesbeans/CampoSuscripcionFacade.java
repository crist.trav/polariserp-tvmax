/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.CampoSuscripcion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class CampoSuscripcionFacade extends AbstractFacade<CampoSuscripcion> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CampoSuscripcionFacade() {
        super(CampoSuscripcion.class);
    }
    
    public List<CampoSuscripcion> getCamposSuscripcionOrdenado(){
        TypedQuery<CampoSuscripcion> q=this.em.createQuery("SELECT cs FROM CampoSuscripcion cs ORDER BY cs.nombre ASC", CampoSuscripcion.class);
        return q.getResultList();
    }
    
}
