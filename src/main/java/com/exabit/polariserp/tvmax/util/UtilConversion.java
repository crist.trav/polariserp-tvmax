/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "utilConversion")
@RequestScoped
public class UtilConversion {

    /**
     * Creates a new instance of UtilConversion
     */
    public UtilConversion() {
    }
    
    public String getNombreMes(Integer nroMes){
        return EntidadesEstaticas.getNombreMes(nroMes);
    }
    public String getNombreMesCorto(Integer nroMes){
        return EntidadesEstaticas.getNombreMesCorto(nroMes);
    }
    
    
    public String getColorEstado(int cod){
        String color="";
        switch(cod){
            case 1:
                color="positive";
                break;
            case 2:
                color="warning";
                break;
            case 3:
                color="error";
                break;
                
        }
        return color;
    }
    
    public String getColorLabelEstado(int idestado){
        String color="ui fluid label";
        switch(idestado){
            case 1:
                color="ui fluid green label";
                break;
            case 2:
                color="ui fluid yellow label";
                break;
            case 3:
                color="ui fluid red label";
                break;
            case 4:
                color="ui fluid blue label";
                break;
        }
        return color;
    }
    
    public String getTipoMsg(int tipo){
        String color="ui info message";
        switch(tipo){
            case 1:
                color="ui info message";
                break;
            case 2:
                color="ui warning message";
                break;
            case 3:
                color="ui negative message";
                break;
                
        }
        return color;
    }
    
    public String getFormatoMoneda(Integer monto){
        if(monto!=null){
            DecimalFormat df=new DecimalFormat("#,###,###,###");
            return df.format(monto);
        }else{
            return "";
        }
    }
    
    public String getFechaFormateada(Date f){
        if(f!=null){
            SimpleDateFormat sdf=new SimpleDateFormat("dd/MMM/yyyy");
            return sdf.format(f);
        }else{
            return "";
        }
    }
}
