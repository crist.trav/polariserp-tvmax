/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class TalonarioFacturaFacade extends AbstractFacade<TalonarioFactura> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TalonarioFacturaFacade() {
        super(TalonarioFactura.class);
    }
    
    public TalonarioFactura getTalonarioActivo(){
        TypedQuery<TalonarioFactura> qt=this.em.createQuery("SELECT t FROM TalonarioFactura t WHERE t.activo=true", TalonarioFactura.class);
        List<TalonarioFactura> lstTalon=qt.getResultList();
        if(lstTalon.isEmpty()){
            return null;
        }else{
            return lstTalon.get(0);
        }
    }
    
    public List<TalonarioFactura> getTalonariosDisponibles(){
        TypedQuery<TalonarioFactura> qtf=this.em.createQuery("SELECT tf FROM TalonarioFactura tf WHERE tf.activo=true AND tf.nroActual<tf.nroFin AND tf.finVigencia>=:fa", TalonarioFactura.class);
        qtf.setParameter("fa", new Date());
        return qtf.getResultList();
    }
    
    public TalonarioFactura getTalonarioPorIdTerminal(Integer idt){
        TypedQuery<TalonarioFactura> q=this.em.createQuery("SELECT tf FROM TalonarioFactura tf WHERE tf.terminalImpresion.idterminalImpresion=:id AND tf.activo=true", TalonarioFactura.class);
        q.setParameter("id", idt);
        List<TalonarioFactura> lstTalon=q.getResultList();
        if(lstTalon.isEmpty()){
            return null;
        }else{
            return lstTalon.get(0);
        }
    }
    
    public List<TalonarioFactura> buscarTalonario(String busqueda){
        TypedQuery<TalonarioFactura> q=this.em.createQuery("SELECT tf FROM TalonarioFactura tf WHERE SQL('CAST(? AS CHAR(11))', tf.timbrado) LIKE :busq OR tf.codEstablecimiento LIKE :busq", TalonarioFactura.class);
        q.setParameter("busq", "%"+busqueda.toLowerCase()+"%");
        return q.getResultList();
    }
}
