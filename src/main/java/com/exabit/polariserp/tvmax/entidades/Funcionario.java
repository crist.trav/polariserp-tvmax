/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "funcionario")
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
    , @NamedQuery(name = "Funcionario.findByIdfuncionario", query = "SELECT f FROM Funcionario f WHERE f.idfuncionario = :idfuncionario")
    , @NamedQuery(name = "Funcionario.findByActivo", query = "SELECT f FROM Funcionario f WHERE f.activo = :activo")
    , @NamedQuery(name = "Funcionario.findByApellidos", query = "SELECT f FROM Funcionario f WHERE f.apellidos = :apellidos")
    , @NamedQuery(name = "Funcionario.findByCi", query = "SELECT f FROM Funcionario f WHERE f.ci = :ci")
    , @NamedQuery(name = "Funcionario.findByNombres", query = "SELECT f FROM Funcionario f WHERE f.nombres = :nombres")
    , @NamedQuery(name = "Funcionario.findByPassword", query = "SELECT f FROM Funcionario f WHERE f.password = :password")
    , @NamedQuery(name = "Funcionario.findByPorcentajeComision", query = "SELECT f FROM Funcionario f WHERE f.porcentajeComision = :porcentajeComision")
    , @NamedQuery(name = "Funcionario.findByTelefono1", query = "SELECT f FROM Funcionario f WHERE f.telefono1 = :telefono1")
    , @NamedQuery(name = "Funcionario.findByTelefono2", query = "SELECT f FROM Funcionario f WHERE f.telefono2 = :telefono2")})
public class Funcionario implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionario1")
    private List<PermisoFuncionario> permisoFuncionarioList;

    @OneToMany(mappedBy = "cobrador")
    private List<Cliente> clienteList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idfuncionario", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idfuncionario", table = "idsequences", allocationSize = 1, initialValue = 1008)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idfuncionario")
    private Integer idfuncionario;
    @Column(name = "activo")
    private Boolean activo;
    @Size(max = 255)
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "ci")
    private Integer ci;
    @Size(max = 255)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 255)
    @Column(name = "password")
    private String password;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "porcentaje_comision")
    private Float porcentajeComision;
    @Size(max = 255)
    @Column(name = "telefono1")
    private String telefono1;
    @Size(max = 255)
    @Column(name = "telefono2")
    private String telefono2;
    @ManyToMany(mappedBy = "funcionarioList")
    private List<Funcionalidad> funcionalidadList;
    @OneToMany(mappedBy = "cobrador")
    private List<Suscripcion> suscripcionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionario1")
    private List<UsoModulo> usoModuloList;
    @OneToMany(mappedBy = "funcionarioRegistro")
    private List<Factura> facturaList;
    @OneToMany(mappedBy = "cobrador")
    private List<Cobro> cobroList;
    @OneToMany(mappedBy = "funcionarioRegistro")
    private List<Cobro> cobroList1;
    @JoinColumn(name = "cargo", referencedColumnName = "idcargo")
    @ManyToOne
    private Cargo cargo;
    @OneToMany(mappedBy = "cobrador")
    private List<AsignacionFactura> asignacionFacturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "funcionario")
    private List<HistorialSuscripcion> historialSuscripcionList;

    public Funcionario() {
    }

    public Funcionario(Integer idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Integer getIdfuncionario() {
        return idfuncionario;
    }

    public void setIdfuncionario(Integer idfuncionario) {
        this.idfuncionario = idfuncionario;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Float getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setPorcentajeComision(Float porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public List<Funcionalidad> getFuncionalidadList() {
        return funcionalidadList;
    }

    public void setFuncionalidadList(List<Funcionalidad> funcionalidadList) {
        this.funcionalidadList = funcionalidadList;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    public List<UsoModulo> getUsoModuloList() {
        return usoModuloList;
    }

    public void setUsoModuloList(List<UsoModulo> usoModuloList) {
        this.usoModuloList = usoModuloList;
    }

    public List<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<Factura> facturaList) {
        this.facturaList = facturaList;
    }

    public List<Cobro> getCobroList() {
        return cobroList;
    }

    public void setCobroList(List<Cobro> cobroList) {
        this.cobroList = cobroList;
    }

    public List<Cobro> getCobroList1() {
        return cobroList1;
    }

    public void setCobroList1(List<Cobro> cobroList1) {
        this.cobroList1 = cobroList1;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public List<AsignacionFactura> getAsignacionFacturaList() {
        return asignacionFacturaList;
    }

    public void setAsignacionFacturaList(List<AsignacionFactura> asignacionFacturaList) {
        this.asignacionFacturaList = asignacionFacturaList;
    }

    public List<HistorialSuscripcion> getHistorialSuscripcionList() {
        return historialSuscripcionList;
    }

    public void setHistorialSuscripcionList(List<HistorialSuscripcion> historialSuscripcionList) {
        this.historialSuscripcionList = historialSuscripcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfuncionario != null ? idfuncionario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionario)) {
            return false;
        }
        Funcionario other = (Funcionario) object;
        if ((this.idfuncionario == null && other.idfuncionario != null) || (this.idfuncionario != null && !this.idfuncionario.equals(other.idfuncionario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Funcionario[ idfuncionario=" + idfuncionario + " ]";
    }

    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    public List<PermisoFuncionario> getPermisoFuncionarioList() {
        return permisoFuncionarioList;
    }

    public void setPermisoFuncionarioList(List<PermisoFuncionario> permisoFuncionarioList) {
        this.permisoFuncionarioList = permisoFuncionarioList;
    }
    
}
