package com.exabit.polariserp.tvmax.cliente;

import com.exabit.polariserp.tvmax.entidades.Barrio;
import com.exabit.polariserp.tvmax.entidades.CategoriaCliente;
import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.CategoriaClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SolicitudCambioEstadoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author frioss
 */
@Named(value = "clienteManBean")
@ViewScoped
public class ClienteManBean implements  Serializable{

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private TipoViviendaFacade tipoViviendaFacade;

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;
    
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    @EJB
    private BarrioFacade barrioFacade;

    @EJB
    private CategoriaClienteFacade categoriaClienteFacade;
    Integer cantidadClientesActivos;
    @EJB
    private ClienteFacade clienteFacade;
    private static final Logger LOG = Logger.getLogger(ClienteManBean.class.getName());
    private List<Cliente> listacliente = new LinkedList<>();
    private List<Cliente> listaclientesActivos = new ArrayList<>();
    private List<Cliente> listclientes = new ArrayList<>();
    private List<Cliente> listaclientes = new ArrayList<>();
    private List<Cliente> listAuxGentileza = new ArrayList<>();
    private List<Cliente> listCantidadActivo = new ArrayList<>();
    private List<Suscripcion> suscripciones = new ArrayList<>();

    public List<Suscripcion> getSuscripciones() {
        return suscripciones;
    }

    public void setSuscripciones(List<Suscripcion> suscripciones) {
        this.suscripciones = suscripciones;
    }
    private Paginador<Cliente> paginador;
    public List<Cliente> getListAuxGentileza() {
        return listAuxGentileza;
    }

    public List<Cliente> getListCantidadActivo() {
        return listCantidadActivo;
    }

    public void setListCantidadActivo(List<Cliente> listCantidadActivo) {
        this.listCantidadActivo = listCantidadActivo;
    }

    public void setListAuxGentileza(List<Cliente> listAuxGentileza) {
        this.listAuxGentileza = listAuxGentileza;
    }
    private List<GentilezaListAux> listaclientesGentileza = new ArrayList<>();
    private Suscripcion suscripcionVar;
    private Suscripcion suscripciondate = new Suscripcion();
    private Cuota cuotaVar = new Cuota();
    private SolicitudCambioEstado solictudCmbE = new SolicitudCambioEstado();
    private String desde;
    private String hasta;
    private Integer cantidadTotalSuscriptosActivos = 0;
    private Integer cantidadgentilezaActivas;
    private Integer cantidadNuevosSuscriptosPorFecha;
    private Cliente clienteVar = new Cliente();
    private String msg = "";
    private String seleccionarClienteIDJSON;
    private Integer idClienteTmp=1;
    private Integer idClienteBTmp=1;
    private Integer mes;
    private Integer anio;
    private Integer idServicio=1;
    private Integer idTipoVivienda=1;
    private Integer idCobrador=1000;
    private Integer cantidadTVs=1;
    private Short diaVencimiento=1;
    private Integer anioVencimiento;
    private Integer mesGenerarcuotas;
    private Integer idTipoSuscripcion=1;
    private Integer montoCuota;
    private String numeroMedidor;
    private boolean isAgregarSuscripcion;
    private final Date fechaActual =  new Date();
    private String fechaPrimerVencimiento;

    public Integer getMontoCuota() {
        return montoCuota;
    }

    public void setMontoCuota(Integer montoCuota) {
        this.montoCuota = montoCuota;
    }

    public String getNumeroMedidor() {
        return numeroMedidor;
    }

    public void setNumeroMedidor(String numeroMedidor) {
        this.numeroMedidor = numeroMedidor;
    }

    public String getFechaPrimerVencimiento() {
        return fechaPrimerVencimiento;
    }

    public void setFechaPrimerVencimiento(String fechaPrimerVencimiento) {
        this.fechaPrimerVencimiento = fechaPrimerVencimiento;
    }
    

    public Integer getAnioVencimiento() {
        return anioVencimiento;
    }

    public void setAnioVencimiento(Integer anioVencimiento) {
        this.anioVencimiento = anioVencimiento;
    }

    public Integer getMesGenerarcuotas() {
        return mesGenerarcuotas;
    }

    public void setMesGenerarcuotas(Integer mesGenerarcuotas) {
        this.mesGenerarcuotas = mesGenerarcuotas;
    }
    
    
    public Cuota getCuotaVar() {
        return cuotaVar;
    }

    public void setCuotaVar(Cuota cuotaVar) {
        this.cuotaVar = cuotaVar;
    }

    public SolicitudCambioEstado getSolictudCmbE() {
        return solictudCmbE;
    }

    public void setSolictudCmbE(SolicitudCambioEstado solictudCmbE) {
        this.solictudCmbE = solictudCmbE;
    }

    public Paginador<Cliente> getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador<Cliente> paginador) {
        this.paginador = paginador;
    }

    public Short getDiaVencimiento() {
        return diaVencimiento;
    }

    public void setDiaVencimiento(Short diaVencimiento) {
        this.diaVencimiento = diaVencimiento;
    }

    public Integer getCantidadTVs() {
        return cantidadTVs;
    }

    public void setCantidadTVs(Integer cantidadTVs) {
        this.cantidadTVs = cantidadTVs;
    }
    
    public boolean isIsAgregarSuscripcion() {
        return isAgregarSuscripcion;
    }

    public void setIsAgregarSuscripcion(boolean isAgregarSuscripcion) {
        this.isAgregarSuscripcion = isAgregarSuscripcion;
    }
    
    public Integer getIdTipoSuscripcion() {
        return idTipoSuscripcion;
    }

    public void setIdTipoSuscripcion(Integer idTipoSuscripcion) {
        this.idTipoSuscripcion = idTipoSuscripcion;
    }
    
    public Integer getIdCobrador() {
        return idCobrador;
    }

    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }
    public Integer getIdTipoVivienda() {
        return idTipoVivienda;
    }

    public void setIdTipoVivienda(Integer idTipoVivienda) {
        this.idTipoVivienda = idTipoVivienda;
    }
    SimpleDateFormat f = new SimpleDateFormat("MMMM d, yyyy");

    public Integer getCantidadgentilezaActivas() {
        return cantidadgentilezaActivas;
    }

    public void setCantidadgentilezaActivas(Integer cantidadgentilezaActivas) {
        this.cantidadgentilezaActivas = cantidadgentilezaActivas;
    }

    public List<GentilezaListAux> getListaclientesGentileza() {
        return listaclientesGentileza;
    }

    public void setListaclientesGentileza(List<GentilezaListAux> listaclientesGentileza) {
        this.listaclientesGentileza = listaclientesGentileza;
    }

    public Integer getCantidadTotalSuscriptosActivos() {
        return cantidadTotalSuscriptosActivos;
    }

    public void setCantidadTotalSuscriptosActivos(Integer cantidadTotalSuscriptosActivos) {
        this.cantidadTotalSuscriptosActivos = cantidadTotalSuscriptosActivos;
    }

    public Integer getCantidadNuevosSuscriptosPorFecha() {
        return cantidadNuevosSuscriptosPorFecha;
    }

    public void setCantidadNuevosSuscriptosPorFecha(Integer cantidadNuevosSuscriptosPorFecha) {
        this.cantidadNuevosSuscriptosPorFecha = cantidadNuevosSuscriptosPorFecha;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public List<Cliente> getListaclientesActivos() {
        listaclientesActivos.addAll(clienteFacade.getClientesOrdenadoRazonSocial());
        return listaclientesActivos;
    }

    public List<Cliente> getListclientes() {
        return listclientes;
    }

    public void setListclientes(List<Cliente> listclientes) {
        this.listclientes = listclientes;
    }

    public List<Cliente> getListaclientes() {
        return listaclientes;
    }

    public void setListaclientes(List<Cliente> listaclientes) {
        this.listaclientes = listaclientes;
    }

    public Suscripcion getSuscripciondate() {
        return suscripciondate;
    }

    public void setSuscripciondate(Suscripcion suscripciondate) {
        this.suscripciondate = suscripciondate;
    }

    public void setListaclientesActivos(List<Cliente> listaclientesActivos) {
        this.listaclientesActivos = listaclientesActivos;
    }

    public Integer getCantidadClientesActivos() {
        return cantidadClientesActivos;
    }

    public void setCantidadClientesActivos(Integer cantidadClientesActivos) {
        this.cantidadClientesActivos = cantidadClientesActivos;
    }

    public List<Cliente> getListacliente() {
        return listacliente;
    }

    public void setListacliente(List<Cliente> listacliente) {
        this.listacliente = listacliente;
    }

    public Suscripcion getSuscripcionVar() {
        return suscripcionVar;
    }

    public void setSuscripcionVar(Suscripcion suscripcionVar) {
        this.suscripcionVar = suscripcionVar;
    }
    
    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Cliente getClienteVar() {
        return clienteVar;
    }

    private String txtBusqueda="";

    public String getTxtBusqueda() {
        return txtBusqueda;
    }

    public void setTxtBusqueda(String txtBusqueda) {
        this.txtBusqueda = txtBusqueda;
    }

    public void setClienteVar(Cliente clienteVar) {
        this.clienteVar = clienteVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionarClienteIDJSON() {
        return seleccionarClienteIDJSON;
    }

    public void setSeleccionarClienteIDJSON(String seleccionarClienteIDJSON) {
        this.seleccionarClienteIDJSON = seleccionarClienteIDJSON;
    }

    public Integer getIdClienteTmp() {
        return idClienteTmp;
    }

    public void setIdClienteTmp(Integer idClienteTmp) {
        this.idClienteTmp = idClienteTmp;
    }

    public Integer getIdClienteBTmp() {
        return idClienteBTmp;
    }

    public void setIdClienteBTmp(Integer idClienteBTmp) {
        this.idClienteBTmp = idClienteBTmp;
    }

    @PostConstruct
    private void postConstruct() {
        try {
        this.paginador.agregarItemsLista(clienteFacade.getClientesOrdenadoRazonSocial());
        } catch (Exception e) {
            System.out.println("error al obtener clientes, "+e);
        }
    }

    public void registrarCliente() {
        try {
            System.out.println("Barrio cliente" + clienteVar.getBarrio());
            List<Cliente> listAux = new ArrayList<>();
            Date priVencimiento = this.f.parse(this.fechaPrimerVencimiento);
            Integer ci = clienteVar.getCi();
            Query query = em.createQuery("SELECT c FROM Cliente c WHERE c.ci=:ci");
            query.setParameter("ci", ci);
            listAux.addAll(query.getResultList());
            if (listAux.isEmpty()) {
                if (isAgregarSuscripcion) {
                    
                    System.out.println("Registando Cliente y suscripcion");
                    CategoriaCliente categoria = categoriaClienteFacade.find(this.idClienteTmp);
                    this.clienteVar.setCategoria(categoria);
                    Barrio barrio = barrioFacade.find(this.idClienteBTmp);
                    this.clienteVar.setBarrio(barrio);
                    suscripcionVar = new Suscripcion();
                    this.suscripcionVar.setCliente(clienteVar);
                    this.suscripcionVar.setTipoSuscripcion(tipoSuscripcionFacade.find(idTipoSuscripcion));
                    this.suscripcionVar.setBarrio(this.barrioFacade.find(idClienteBTmp));
                    this.suscripcionVar.setCobrador(this.funcionarioFacade.find(this.idCobrador));
                    this.suscripcionVar.setEstado(EntidadesEstaticas.ESTADO_CONECTADO);
                    this.suscripcionVar.setFechaSuscripcion(fechaActual);
                    this.suscripcionVar.setFechaCambioEstado(fechaActual);
                    this.suscripcionVar.setCantidadTvs(cantidadTVs);
                    this.suscripcionVar.setDiaVencimientoMes(diaVencimiento);
                    this.suscripcionVar.setServicio(this.servicioFacade.find(this.idServicio));
                    this.suscripcionVar.setTipoVivienda(this.tipoViviendaFacade.find(this.idTipoVivienda));
                    this.suscripcionVar.setTipoSuscripcion(this.tipoSuscripcionFacade.find(this.idTipoSuscripcion));
                    this.suscripcionVar.setDireccion(clienteVar.getDireccion());
                    this.suscripcionVar.setPrecio(montoCuota);
                    this.suscripcionVar.setNroMedidorElectrico(numeroMedidor);

                    System.out.println("Numero de medidor: " + suscripcionVar.getNroMedidorElectrico() + ", precio: " + suscripcionVar.getPrecio());
                    this.clienteFacade.create(clienteVar);
                    suscripcionFacade.registrarSuscripcion(suscripcionVar, priVencimiento, cuotaVar.getMontoCuota());
                    this.msg = "Registrado correctamente: " + clienteVar.getNombres() + "- [ " + clienteVar.getRazonSocial() + " ]" + "- Numero de Suscripcion: " + suscripcionVar.getIdsuscripcion();
                    
                } else {
                    CategoriaCliente categoria = categoriaClienteFacade.find(this.idClienteTmp);
                    this.clienteVar.setCategoria(categoria);
                    Barrio barrio = barrioFacade.find(this.idClienteBTmp);
                    this.clienteVar.setBarrio(barrio);
                    this.suscripcionVar.setCliente(clienteVar);
                    this.suscripcionVar.setTipoSuscripcion(tipoSuscripcionFacade.find(idTipoSuscripcion));
                    this.clienteFacade.create(clienteVar);
                    this.msg = "Registrado correctamente: " + "-" + clienteVar.getNombres() + "- [ " + clienteVar.getRazonSocial() + " ]";
                }

            } else {
                System.out.println("el cliente: " + listAux.iterator().next().getRazonSocial() + ", ya se encuentra registrado.");
                this.msg = "El cliente ya se encuentra registrado.";
            }
        } catch (Exception e) {
        }
    }

    public ClienteManBean() {
        Calendar calendar = new GregorianCalendar();
        this.mes = calendar.get(Calendar.MONTH) + 1;
        this.anio = calendar.get(Calendar.YEAR);

        Calendar ahora = new GregorianCalendar();
        Calendar dateDesde = new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        Calendar dateHasta = new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        dateHasta.add(Calendar.MONTH, 1);
        dateHasta.add(Calendar.DAY_OF_MONTH, -1);

        this.desde = f.format(new Date(dateDesde.getTimeInMillis()));
        this.hasta = f.format(new Date(dateHasta.getTimeInMillis()));
        this.fechaPrimerVencimiento = this.f.format(new Date());
        this.anioVencimiento = calendar.get(Calendar.YEAR);
        this.paginador = new Paginador(listacliente);
    }

    public void editarCliente() {
        CategoriaCliente categoria = categoriaClienteFacade.find(this.idClienteTmp);
        this.clienteVar.setCategoria(categoria);
        Barrio barrio = barrioFacade.find(this.idClienteBTmp);
        this.clienteVar.setBarrio(barrio);
        this.clienteFacade.edit(clienteVar);
        this.msg = "Modificado exitosamente" + clienteVar.getIdcliente() + "-" + clienteVar.getNombres() + "-" + clienteVar.getRazonSocial();
    }

    public void eliminarCliente() {
//        System.out.println("Eliminar Cliente" +seleccionarClienteIDJSON);
        Gson gson = new Gson();
        Integer[] idsEliminar = gson.fromJson(this.seleccionarClienteIDJSON, Integer[].class);
        for (Integer ide : idsEliminar) {
//            System.out.println("ide: "+ide);
            this.clienteFacade.remove(this.clienteFacade.find(ide));
        }
        this.buscarClientes();
    }


    public void buscarClientes() {
        System.out.println("buscando Cliente por: "+txtBusqueda);
        this.paginador.limpiarLista();
        if (this.txtBusqueda.isEmpty()) {
            this.paginador.agregarItemsLista(clienteFacade.getClientesOrdenadoRazonSocial());
//            Query q = em.createQuery("SELECT c FROM Cliente c");
//            q.setMaxResults(50);
//            listacliente.addAll(q.getResultList());
        } else {
            this.paginador.limpiarLista();
//            this.listacliente.addAll();
            this.paginador.agregarItemsLista(clienteFacade.buscarCliente(txtBusqueda));
//            this.listacliente.addAll(this.clienteFacade.buscarCliente(this.txtBusqueda));
//            }
        }
        System.out.println("Buscar: " + this.txtBusqueda);
    }

    public void buscaClientePOrci() {
        this.paginador.limpiarLista();
        if (this.txtBusqueda.isEmpty()) {
            Query q = em.createQuery("SELECT c FROM Cliente c");
            this.paginador.agregarItemsLista(q.getResultList());
//            q.setMaxResults(50);
//            listacliente.addAll(q.getResultList());
        }
//        }else{
//            listacliente.addAll(this.clienteFacade.busuqedaClientePorCI(Integer.parseInt(txtBusqueda)));
//        }
    }

    public void clientesActivos() throws ParseException {
        System.out.println("desde: " + desde + ", hasta: " + hasta);
        try {
            Date dateDesde = this.f.parse(this.desde);
            Date dateHasta = this.f.parse(this.hasta);

            System.out.println("datedesde: " + dateDesde + ", dateHasta: " + dateHasta);
            TypedQuery qcliente = em.createQuery("SELECT c FROM Cliente c", Cliente.class);
            listclientes.addAll(qcliente.getResultList());
//        clientes.size();
//        System.out.println("cantidad cliente: "+clientes.size());
            List<Suscripcion> suscripciones = new ArrayList<>();
            List<Suscripcion> listasuscripciones = new ArrayList<>();
            TypedQuery<Suscripcion> qsuscripciones = em.createQuery("SELECT s FROM Suscripcion s WHERE s.fechaSuscripcion>=:dateDesde AND s.fechaSuscripcion<=:dateHasta", Suscripcion.class);
//        WHERE s.fechaSuscripcion>=:desde AND s.fechaSuscripcion<=:hasta
            qsuscripciones.setParameter("dateDesde", dateDesde);
            qsuscripciones.setParameter("dateHasta", dateHasta);
            suscripciones.addAll(qsuscripciones.getResultList());
//        suscripciones.size();
            System.out.println("cantidad suscripciones seleccionadas: " + suscripciones.size());

            for (Suscripcion suscripcione : suscripciones) {
                for (Cliente cliente : listclientes) {
                    if (cliente != null && suscripcione != null) {
                        if (suscripcione.getEstado().getRecibeServicio() == true) {
                            if (Objects.equals(cliente.getIdcliente(), suscripcione.getCliente().getIdcliente())) {
                                listaclientes.add(cliente);
                                listaclientes.size();
//                            System.out.println("cantidad de clientes activos: "+listaclientes.size());
                                cantidadNuevosSuscriptosPorFecha = listaclientes.size();
                            }
                        }
                    }
                }
            }
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir a fecha desde formato", ex);
//            return hasta;
        }

//       return 1;
    }

    public void clientesTotalActivos() {
        listCantidadActivo.clear();
        cantidadTotalSuscriptosActivos = 0;
        listclientes.clear();
        suscripciones.clear();
        TypedQuery qcliente = em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        listclientes.addAll(qcliente.getResultList());
        TypedQuery<Suscripcion> qsuscripciones = em.createQuery("SELECT s FROM Suscripcion s", Suscripcion.class);
        suscripciones.addAll(qsuscripciones.getResultList());
        for (Suscripcion suscripcione : suscripciones) {
            for (Cliente cliente : listclientes) {
                if (cliente != null && suscripcione != null) {
                    if (suscripcione.getEstado().getRecibeServicio() == true && suscripcione.getTipoSuscripcion().getIdtipoSuscripcion().equals(1)) {
                        if (Objects.equals(cliente.getIdcliente(), suscripcione.getCliente().getIdcliente())) {
                            listCantidadActivo.add(cliente);
                            listCantidadActivo.size();
                             System.out.println("cantidad de clientes activos: " + listCantidadActivo.size());
                            cantidadTotalSuscriptosActivos = listCantidadActivo.size();
                        }
                    }
                }
            }
        }

    }

    public void gentileza() {
        listaclientesGentileza.clear();
        SimpleDateFormat fechaS = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        TypedQuery qcliente = em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        listAuxGentileza.addAll(qcliente.getResultList());
        List<Suscripcion> suscripciones = new ArrayList<>();
        List<Suscripcion> listasuscripciones = new ArrayList<>();
        TypedQuery<Suscripcion> qsuscripciones = em.createQuery("SELECT s FROM Suscripcion s", Suscripcion.class);
        System.out.println("cantidad suscripciones seleccionadas: " + suscripciones.size());
        suscripciones.addAll(qsuscripciones.getResultList());
        for (Suscripcion suscripcione : suscripciones) {
            for (Cliente cliente : listAuxGentileza) {
                if (cliente != null && suscripcione != null) {
                    if (suscripcione.getEstado().getRecibeServicio() == true && suscripcione.getTipoSuscripcion().getIdtipoSuscripcion().equals(2)) {
                        if (Objects.equals(cliente.getIdcliente(), suscripcione.getCliente().getIdcliente())) {
//                            listaclientesGentileza.add(suscripcione);
                            System.out.println("cantidad de clientes activos y con gentileza: " + listaclientesGentileza.size());
                            Integer id = suscripcione.getCliente().getIdcliente();
                            String razonSocial= suscripcione.getCliente().getRazonSocial();
//                            Integer ci = suscripcione.getCliente().getCi();
                            String fechaSuscripcion = fechaS.format(suscripcione.getFechaSuscripcion());
                            boolean estado = suscripcione.getEstado().getRecibeServicio();
                            String direccion = suscripcione.getCliente().getDireccion();
                            String barrio = suscripcione.getCliente().getBarrio().getNombre();
                            String telefono = suscripcione.getCliente().getTelefono1();
                            System.out.println("datos: "+id+", "+estado+", "+razonSocial+", "+direccion+", "+barrio+", "+telefono+", "+fechaSuscripcion);
                            GentilezaListAux gentilezaListAux = new GentilezaListAux(id, razonSocial, fechaSuscripcion, direccion, barrio, estado, telefono);
                            listaclientesGentileza.add(gentilezaListAux);
                            listaclientesGentileza.size();
                            cantidadgentilezaActivas = listaclientesGentileza.size();
                        }
                    }
                }
            }
        }
    }

    private void generarPdfImpresionClientes(){
        List<Cliente> lstCliente = new LinkedList<>();
        if (!clienteFacade.getClientesOrdenadoRazonSocial().isEmpty()) {
            for (Cliente cliente : clienteFacade.findAll()) {
                lstCliente.add(cliente);
            }
        }
        this.generarPDFClientes(lstCliente);
    }
    
    private void generarPDFClientes(List<Cliente> lstCliente){
        List<JasperPrint> listJapersPrint = new LinkedList<>();
        for (Cliente c : lstCliente) {
            Map<String, Object> params = new HashMap<>();
            params.put("razonSocial", c.getRazonSocial());
            String ruc = c.getCi().toString();
            if (c.getDvRuc()!=null) {
                ruc = ruc + "-"+ c.getDvRuc();
            }
            params.put("ruc", ruc);
            params.put("nombre", c.getNombres());
            params.put("apellido", c.getApellidos());
            params.put("direccion", c.getDireccion());
            params.put("telefono", c.getTelefono1());
            params.put("barrio", c.getBarrio().getNombre());
//            JRDataSource datos = new JRBeanCollectionDataSource(lstCliente);
            try {
                String ruta = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
                JasperReport jr = (JasperReport) JRLoader.loadObject(new File(ruta+"lista_cliente.jasper"));
                JasperPrint jpr = JasperFillManager.fillReport(jr, params);
                listJapersPrint.add(jpr);
            } catch (JRException e) {
                LOG.log(Level.SEVERE, "Error al crear reporte", e);
                System.out.println("Error al crear reporte"+ e.getMessage());
            }
        }
        try {
            HttpServletResponse httpServletResponse  = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=report.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            configuration.setCreatingBatchModeBookmarks(true);
            exporter.setExporterInput(SimpleExporterInput.getInstance(listJapersPrint));
            SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(output);
            exporter.exportReport();
        } catch (IOException | JRException e) {
            LOG.log(Level.SEVERE, "Error al exportar pdf Clientes", e);
        }
    }
    
    public int getCantSuscripcionesActivas(Integer ids){
        return this.suscripcionFacade.getSuscripcionesConServicio(this.clienteFacade.find(ids)).size();
    }
}
