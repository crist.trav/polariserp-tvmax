/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.service;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import java.text.DecimalFormat;

/**
 *
 * @author cristhian-kn
 */
public class SuscripcionBasicDropdown {
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

        private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

        private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SuscripcionBasicDropdown(Suscripcion s) {
        DecimalFormat df=new DecimalFormat("###,###,###,###");
        
        this.name="[susc:"+s.getIdsuscripcion()+"]-"+s.getCliente().getRazonSocial();
        if(s.getCliente().getCi()!=null){
            this.name=this.name+" (ci:"+df.format(s.getCliente().getCi())+")";
        }
//        this.name=this.name+"Dir: "+s.getDireccion();
        this.value=s.getIdsuscripcion().toString();
        this.text=this.name;
    }
    
    

}
