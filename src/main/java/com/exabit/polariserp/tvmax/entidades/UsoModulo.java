/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "uso_modulo")
@NamedQueries({
    @NamedQuery(name = "UsoModulo.findAll", query = "SELECT u FROM UsoModulo u")
    , @NamedQuery(name = "UsoModulo.findByFuncionario", query = "SELECT u FROM UsoModulo u WHERE u.usoModuloPK.funcionario = :funcionario")
    , @NamedQuery(name = "UsoModulo.findByModulo", query = "SELECT u FROM UsoModulo u WHERE u.usoModuloPK.modulo = :modulo")
    , @NamedQuery(name = "UsoModulo.findByContadorUso", query = "SELECT u FROM UsoModulo u WHERE u.contadorUso = :contadorUso")})
public class UsoModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsoModuloPK usoModuloPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "contador_uso")
    private int contadorUso;
    @JoinColumn(name = "funcionario", referencedColumnName = "idfuncionario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Funcionario funcionario1;
    @JoinColumn(name = "modulo", referencedColumnName = "idmodulo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Modulo modulo1;

    public UsoModulo() {
    }

    public UsoModulo(UsoModuloPK usoModuloPK) {
        this.usoModuloPK = usoModuloPK;
    }

    public UsoModulo(UsoModuloPK usoModuloPK, int contadorUso) {
        this.usoModuloPK = usoModuloPK;
        this.contadorUso = contadorUso;
    }

    public UsoModulo(int funcionario, int modulo) {
        this.usoModuloPK = new UsoModuloPK(funcionario, modulo);
    }

    public UsoModuloPK getUsoModuloPK() {
        return usoModuloPK;
    }

    public void setUsoModuloPK(UsoModuloPK usoModuloPK) {
        this.usoModuloPK = usoModuloPK;
    }

    public int getContadorUso() {
        return contadorUso;
    }

    public void setContadorUso(int contadorUso) {
        this.contadorUso = contadorUso;
    }

    public Funcionario getFuncionario1() {
        return funcionario1;
    }

    public void setFuncionario1(Funcionario funcionario1) {
        this.funcionario1 = funcionario1;
    }

    public Modulo getModulo1() {
        return modulo1;
    }

    public void setModulo1(Modulo modulo1) {
        this.modulo1 = modulo1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usoModuloPK != null ? usoModuloPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoModulo)) {
            return false;
        }
        UsoModulo other = (UsoModulo) object;
        if ((this.usoModuloPK == null && other.usoModuloPK != null) || (this.usoModuloPK != null && !this.usoModuloPK.equals(other.usoModuloPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.UsoModulo[ usoModuloPK=" + usoModuloPK + " ]";
    }
    
}
