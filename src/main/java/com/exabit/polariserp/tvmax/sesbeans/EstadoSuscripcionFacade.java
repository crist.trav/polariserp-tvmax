package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class EstadoSuscripcionFacade extends AbstractFacade<EstadoSuscripcion> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoSuscripcionFacade() {
        super(EstadoSuscripcion.class);
    }
   
}
