package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "factura")
@NamedQueries({
    @NamedQuery(name = "Factura.findAll", query = "SELECT f FROM Factura f")
    , @NamedQuery(name = "Factura.findByIdfactura", query = "SELECT f FROM Factura f WHERE f.idfactura = :idfactura")
    , @NamedQuery(name = "Factura.findByAnulado", query = "SELECT f FROM Factura f WHERE f.anulado = :anulado")
    , @NamedQuery(name = "Factura.findByCancelado", query = "SELECT f FROM Factura f WHERE f.cancelado = :cancelado")
    , @NamedQuery(name = "Factura.findByContado", query = "SELECT f FROM Factura f WHERE f.contado = :contado")
    , @NamedQuery(name = "Factura.findByFecha", query = "SELECT f FROM Factura f WHERE f.fecha = :fecha")
    , @NamedQuery(name = "Factura.findByIva10", query = "SELECT f FROM Factura f WHERE f.iva10 = :iva10")
    , @NamedQuery(name = "Factura.findByIva5", query = "SELECT f FROM Factura f WHERE f.iva5 = :iva5")
    , @NamedQuery(name = "Factura.findByNroFactura", query = "SELECT f FROM Factura f WHERE f.nroFactura = :nroFactura")
    , @NamedQuery(name = "Factura.findByTotal", query = "SELECT f FROM Factura f WHERE f.total = :total")
    , @NamedQuery(name = "Factura.findByFechaHoraRegistro", query = "SELECT f FROM Factura f WHERE f.fechaHoraRegistro = :fechaHoraRegistro")
    , @NamedQuery(name = "Factura.findByFechaHoraAnulacion", query = "SELECT f FROM Factura f WHERE f.fechaHoraAnulacion = :fechaHoraAnulacion")
    , @NamedQuery(name = "Factura.findByObservacion", query = "SELECT f FROM Factura f WHERE f.observacion = :observacion")})
public class Factura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idfactura", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idfactura", table = "idsequences", allocationSize = 1, initialValue = 98188)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idfactura")
    private Integer idfactura;
    @Column(name = "anulado")
    private Boolean anulado;
    @Column(name = "cancelado")
    private Boolean cancelado;
    @Column(name = "contado")
    private Boolean contado;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "iva10")
    private Integer iva10;
    @Column(name = "iva5")
    private Integer iva5;
    @Column(name = "nro_factura")
    private Integer nroFactura;
    @Column(name = "total")
    private Integer total;
    @Column(name = "fecha_hora_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraRegistro;
    @Column(name = "fecha_hora_anulacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraAnulacion;
    @Size(max = 250)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "cliente", referencedColumnName = "idcliente")
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "talonario_factura", referencedColumnName = "idtalonario")
    @ManyToOne
    private TalonarioFactura talonarioFactura;
    @JoinColumn(name = "funcionario_registro", referencedColumnName = "idfuncionario")
    @ManyToOne
    private Funcionario funcionarioRegistro;
    @OneToMany(mappedBy = "factura")
    private List<AsignacionFactura> asignacionFacturaList;
    @OneToMany(mappedBy = "factura", cascade = CascadeType.ALL)
    private List<DetalleFactura> detalleFacturaList;

    public Factura() {
    }

    public Factura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    public Integer getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public Boolean getContado() {
        return contado;
    }

    public void setContado(Boolean contado) {
        this.contado = contado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getIva10() {
        return iva10;
    }

    public void setIva10(Integer iva10) {
        this.iva10 = iva10;
    }

    public Integer getIva5() {
        return iva5;
    }

    public void setIva5(Integer iva5) {
        this.iva5 = iva5;
    }

    public Integer getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(Integer nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Date getFechaHoraAnulacion() {
        return fechaHoraAnulacion;
    }

    public void setFechaHoraAnulacion(Date fechaHoraAnulacion) {
        this.fechaHoraAnulacion = fechaHoraAnulacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TalonarioFactura getTalonarioFactura() {
        return talonarioFactura;
    }

    public void setTalonarioFactura(TalonarioFactura talonarioFactura) {
        this.talonarioFactura = talonarioFactura;
    }

    public Funcionario getFuncionarioRegistro() {
        return funcionarioRegistro;
    }

    public void setFuncionarioRegistro(Funcionario funcionarioRegistro) {
        this.funcionarioRegistro = funcionarioRegistro;
    }

    public List<AsignacionFactura> getAsignacionFacturaList() {
        return asignacionFacturaList;
    }

    public void setAsignacionFacturaList(List<AsignacionFactura> asignacionFacturaList) {
        this.asignacionFacturaList = asignacionFacturaList;
    }

    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfactura != null ? idfactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Factura)) {
            return false;
        }
        Factura other = (Factura) object;
        if ((this.idfactura == null && other.idfactura != null) || (this.idfactura != null && !this.idfactura.equals(other.idfactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Factura[ idfactura=" + idfactura + " ]";
    }
    
}
