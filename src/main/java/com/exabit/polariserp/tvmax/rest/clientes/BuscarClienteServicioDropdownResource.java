/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.clientes;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.google.gson.Gson;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cristhian-kn
 */
@Path("buscarClienteServicioDropdown")
public class BuscarClienteServicioDropdownResource {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of BuscarClienteDropdownResource
     */
    public BuscarClienteServicioDropdownResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.polariserp.web.rest.clientes.BuscarClienteDropdownResource
     *
     * @param q
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText(@QueryParam("q") String q) {
        TypedQuery<Suscripcion> queryfiltro = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=true AND (LOWER(s.cliente.nombres) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx) ORDER BY s.idsuscripcion DESC", Suscripcion.class);

        TypedQuery<Suscripcion> querytodos = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=true ORDER BY s.idsuscripcion DESC", Suscripcion.class);
        queryfiltro.setParameter("tx", "%" + q.toLowerCase() + "%");
        Map<Integer, Cliente> hmClientes = new HashMap<>();
        if (q.isEmpty()) {
            System.out.println("todos: " + q);
            List<Suscripcion> lsts = querytodos.setMaxResults(100).getResultList();
            for (Suscripcion s : lsts) {
                hmClientes.put(s.getCliente().getIdcliente(), s.getCliente());
            }
        } else {
            System.out.println("filtrar: " + q);
            List<Suscripcion> lsts = queryfiltro.setMaxResults(100).getResultList();
            for (Suscripcion s : lsts) {
                hmClientes.put(s.getCliente().getIdcliente(), s.getCliente());
            }
        }

        RespuestaBusquedaCliente r = new RespuestaBusquedaCliente();
        r.setSuccess(true);
        DecimalFormat df = new DecimalFormat("###,###,###,###");
        for (Cliente c : hmClientes.values()) {
            ClienteBasicDropdown clib = new ClienteBasicDropdown();
            String tmpName = c.getIdcliente() + "-" + c.getRazonSocial();
            if (c.getCi() != null) {
                tmpName = tmpName + "(" + df.format(c.getCi()) + ")";
            }
            clib.setName(tmpName);
            clib.setValue(c.getIdcliente());
            r.getResults().add(clib);
        }
        System.out.println("hay " + r.getResults().size() + " resultados");
        Gson gson = new Gson();
        return gson.toJson(r);
    }

    /**
     * PUT method for updating or creating an instance of
     * BuscarClienteDropdownResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
