package com.exabit.polariserp.tvmax.facturas.util;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author traver
 */
public class SuscripcionGenerar {

    private Suscripcion suscripcion;
    private List<GrupoCuotaGenerar> lstGruposCuotas = new ArrayList<>();

    public List<GrupoCuotaGenerar> getLstGruposCuotas() {
        return lstGruposCuotas;
    }

    public void setLstGruposCuotas(List<GrupoCuotaGenerar> lstGruposCuotas) {
        this.lstGruposCuotas = lstGruposCuotas;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }

    public int getMontoTotalGenerar() {
        int total = 0;
        for (GrupoCuotaGenerar gc : this.lstGruposCuotas) {
            if (gc.isIncluir()) {
                for (Cuota c : gc.getLstCuotas()) {
                    total = total + (c.getMontoCuota() - c.getMontoEntrega());
                }
            }
        }
        return total;
    }

    public int getTotalCuotasIncluidas() {
        int total = 0;
        for (GrupoCuotaGenerar gc : this.lstGruposCuotas) {
            if(gc.isIncluir()){
                total++;
            }
            //Esta seccion no suma otros conceptos
//            for (Cuota cuo : gc.getLstCuotas()) {
//                if (cuo.getServicio() != null) {
//                    if (cuo.getServicio().getIdservicio().equals(cuo.getSuscripcion().getServicio().getIdservicio())) {
//                        if (gc.isIncluir()) {
//                            total++;
//                        }
//                    }
//                } else {
//                    if (gc.isIncluir()) {
//                        total++;
//                    }
//                }
//            }
        }
        return total;
    }

    public int getTotalCuotasExcluidas() {
        int total = 0;
        for (GrupoCuotaGenerar gc : this.lstGruposCuotas) {
            if(!gc.isIncluir()){
                total++;
            }
            //Esta seccion no suma otros conceptos
//            for (Cuota cuo : gc.getLstCuotas()) {
//                if (cuo.getServicio() != null) {
//                    if (cuo.getServicio().getIdservicio().equals(cuo.getSuscripcion().getServicio().getIdservicio())) {
//                        if (!gc.isIncluir()) {
//                            total++;
//                        }
//                    }
//                } else {
//                    if (!gc.isIncluir()) {
//                        total++;
//                    }
//                }
//            }
        }
        return total;
    }

    public boolean existenCuotasSecundarias() {
        boolean existe = false;
        for (GrupoCuotaGenerar gcg : this.lstGruposCuotas) {
            for (Cuota c : gcg.getLstCuotas()) {
                if (c.getServicio() != null) {
                    if (!c.getServicio().getIdservicio().equals(this.suscripcion.getServicio().getIdservicio())) {
                        existe = true;
                        break;
                    }
                }
            }
        }
        return existe;
    }
   
}
