/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Zona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class ZonaFacade extends AbstractFacade<Zona> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ZonaFacade() {
        super(Zona.class);
    }
    
    public List<Zona> buscarZona(String texto){
        TypedQuery<Zona> q=this.em.createQuery("SELECT z FROM Zona z WHERE LOWER(z.nombre) LIKE :tx OR SQL('CAST(? AS CHAR(11))', z.idzona) LIKE :tx", Zona.class);
        q.setParameter("tx", "%"+texto.toLowerCase()+"%");
        return q.getResultList();
    }
    
}
