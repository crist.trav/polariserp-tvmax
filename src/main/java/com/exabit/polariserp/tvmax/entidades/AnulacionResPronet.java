package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "anulacion_res_pronet")
@NamedQueries({
    @NamedQuery(name = "AnulacionResPronet.findAll", query = "SELECT a FROM AnulacionResPronet a")
    , @NamedQuery(name = "AnulacionResPronet.findByIdanulacionRes", query = "SELECT a FROM AnulacionResPronet a WHERE a.idanulacionRes = :idanulacionRes")
    , @NamedQuery(name = "AnulacionResPronet.findByCodServicio", query = "SELECT a FROM AnulacionResPronet a WHERE a.codServicio = :codServicio")
    , @NamedQuery(name = "AnulacionResPronet.findByTipoTrx", query = "SELECT a FROM AnulacionResPronet a WHERE a.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "AnulacionResPronet.findByCodRetorno", query = "SELECT a FROM AnulacionResPronet a WHERE a.codRetorno = :codRetorno")
    , @NamedQuery(name = "AnulacionResPronet.findByDesRetorno", query = "SELECT a FROM AnulacionResPronet a WHERE a.desRetorno = :desRetorno")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnulacionResPronet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idanulacionrespronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idanulacionrespronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idanulacion_res")
    @JsonIgnore
    private Integer idanulacionRes;
    @Size(max = 5)
    @Column(name = "codServicio")
    private String codServicio;
    @Column(name = "tipoTrx")
    private Integer tipoTrx;
    @Size(max = 3)
    @Column(name = "codRetorno")
    private String codRetorno;
    @Size(max = 30)
    @Column(name = "desRetorno")
    private String desRetorno;
    @JoinColumn(name = "anulacion_req_pronet", referencedColumnName = "idanulacion_req")
    @ManyToOne(optional = false)
    @JsonIgnore
    private AnulacionReqPronet anulacionReqPronet;

    public AnulacionResPronet() {
    }

    public AnulacionResPronet(Integer idanulacionRes) {
        this.idanulacionRes = idanulacionRes;
    }

    public Integer getIdanulacionRes() {
        return idanulacionRes;
    }

    public void setIdanulacionRes(Integer idanulacionRes) {
        this.idanulacionRes = idanulacionRes;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
    }

    public String getDesRetorno() {
        return desRetorno;
    }

    public void setDesRetorno(String desRetorno) {
        this.desRetorno = desRetorno;
    }

    public AnulacionReqPronet getAnulacionReqPronet() {
        return anulacionReqPronet;
    }

    public void setAnulacionReqPronet(AnulacionReqPronet anulacionReqPronet) {
        this.anulacionReqPronet = anulacionReqPronet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idanulacionRes != null ? idanulacionRes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnulacionResPronet)) {
            return false;
        }
        AnulacionResPronet other = (AnulacionResPronet) object;
        if ((this.idanulacionRes == null && other.idanulacionRes != null) || (this.idanulacionRes != null && !this.idanulacionRes.equals(other.idanulacionRes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.AnulacionResPronet[ idanulacionRes=" + idanulacionRes + " ]";
    }
    
}
