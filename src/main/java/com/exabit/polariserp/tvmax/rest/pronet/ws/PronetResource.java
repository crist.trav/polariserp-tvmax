package com.exabit.polariserp.tvmax.rest.pronet.ws;

import com.exabit.polariserp.tvmax.entidades.AnulacionReqPronet;
import com.exabit.polariserp.tvmax.entidades.AnulacionResPronet;
import com.exabit.polariserp.tvmax.entidades.ConsultaReqPronet;
import com.exabit.polariserp.tvmax.entidades.ConsultaResPronet;
import com.exabit.polariserp.tvmax.entidades.PagoReqPronet;
import com.exabit.polariserp.tvmax.entidades.PagoResPronet;
import com.exabit.polariserp.tvmax.sesbeans.PronetSesBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author traver
 */
@Path("pronet")
public class PronetResource {

    private static final Logger LOG = Logger.getLogger(PronetResource.class.getName());

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    @Context
    private UriInfo context;

    PronetSesBean pronetSesBean = lookupPronetSesBeanBean();

    /**
     * Creates a new instance of PronetResource
     */
    public PronetResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.polariserp.web.rest.pronet.ws.PronetResource
     *
     * @param consultaJson
     * @return an instance of java.lang.String
     */
    @POST
    @Path("/consulta")
    @Consumes("application/json; charset=utf-8")
    @Produces("application/json; charset=utf-8")
    public Response consultar(String consultaJson) {
        try {
            ObjectMapper om = new ObjectMapper();
            ConsultaReqPronet pedido;

            pedido = om.readValue(consultaJson, ConsultaReqPronet.class);

            Integer ciuser = -1;
            Integer cicli = -1;
            try {
                ciuser = Integer.parseInt(pedido.getUsuario().replace(".", "").replace(",", ""));
            } catch (NumberFormatException ex) {
                System.out.println("Number format exception ci usuario");
                LOG.log(Level.SEVERE, "Error al convertir ci usuario", ex);
            }

            try {
                cicli = Integer.parseInt(pedido.getNroDocumento());
            } catch (NumberFormatException ex) {
                {
                    System.out.println("Number format exception ci cliente");
                    LOG.log(Level.SEVERE, "Error al convertir nro documento clieente", ex);
                }

            }
            if (ciuser != -1) {
                if (cicli != -1) {
                    if (this.pronetSesBean.validarPassword(ciuser, pedido.getPassword())) {
                        this.pronetSesBean.guardarConsultaReq(pedido);
                        String jsonrespuesta = om.writeValueAsString(this.pronetSesBean.getDeudaCliente(cicli, pedido));
                        return Response.status(Response.Status.OK).entity(jsonrespuesta).build();
                    } else {
                        ConsultaResPronet cr = new ConsultaResPronet();
                        cr.setCodRetorno("999");
                        cr.setDesRetorno("ERROR DE USR O CONTR");
                        cr.setTipoTrx(5);
                        return Response.status(Response.Status.OK).entity(om.writeValueAsString(cr)).build();
                    }
                } else {
                    ConsultaResPronet cr = new ConsultaResPronet();
                    cr.setCodRetorno("999");
                    cr.setDesRetorno("DOCUMENTO INVALIDO");
                    cr.setTipoTrx(5);
                    return Response.status(Response.Status.OK).entity(cr).build();
                }
            } else {
                ConsultaResPronet cr = new ConsultaResPronet();
                cr.setCodRetorno("999");
                cr.setDesRetorno("ERROR DE USR O CONTR");
                cr.setTipoTrx(5);
                return Response.status(Response.Status.OK).entity(cr).build();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al deserializar/deserializar JSON", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).header("Content-Type", "text/plain; charset=utf-8").build();
        }
    }

    @POST
    @Path("/pago")
    @Consumes("application/json; charset=utf-8")
    @Produces("application/json; charset=utf-8")
    public Response pagar(String pagoJson) {
        try {
            ObjectMapper om = new ObjectMapper();
            System.out.println(pagoJson);
            PagoReqPronet pagoReq = om.readValue(pagoJson, PagoReqPronet.class);
            Integer ciuser = -1;
            try {
                ciuser = Integer.parseInt(pagoReq.getUsuario());
            } catch (NumberFormatException ex) {
                LOG.log(Level.SEVERE, "Error al convertir string a int (documento usuario)", ex);
            }

            if (this.pronetSesBean.validarPassword(ciuser, pagoReq.getPassword())) {
                PagoResPronet pagoResponse = this.pronetSesBean.pagar(pagoReq);
                String jsonrespuesta = om.writeValueAsString(pagoResponse);
                return Response.status(Response.Status.OK).entity(jsonrespuesta).build();
            } else {
                PagoResPronet pagoResponse = new PagoResPronet();
                pagoResponse.setCodRetorno("999");
                pagoResponse.setDesRetorno("ERROR DE USUARIO O CONTRASEÑA");
                return Response.status(Response.Status.OK).entity(om.writeValueAsString(pagoResponse)).build();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al serializar/deserializar JSON", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).header("Content-Type", "text/plain; charset=utf-8").build();
        }
    }

    @POST
    @Path("/anulacion")
    @Produces("application/json; charset=utf-8")
    @Consumes("application/json; charset=utf-8")
    public Response anular(String anulacionJson) {
        try {
            ObjectMapper om = new ObjectMapper();
            AnulacionReqPronet anulReq = om.readValue(anulacionJson, AnulacionReqPronet.class);
            Integer ciuser = -1;
            try {
                ciuser = Integer.parseInt(anulReq.getUsuario());
            } catch (NumberFormatException ex) {
                LOG.log(Level.SEVERE, "Error al convertir string a integer usuario", ex);
            }
            if (ciuser != -1) {
                if (this.pronetSesBean.validarPassword(ciuser, anulReq.getPassword())) {
                    AnulacionResPronet anulRes = this.pronetSesBean.anular(anulReq);
                    return Response.status(Response.Status.OK).entity(om.writeValueAsString(anulRes)).build();
                } else {
                    AnulacionResPronet anulRes = new AnulacionResPronet();
                    anulRes.setCodRetorno("999");
                    anulRes.setDesRetorno("ERROR DE USR O CONTR");
                    return Response.status(Response.Status.OK).entity(om.writeValueAsString(anulRes)).build();
                }
            } else {
                AnulacionResPronet anulRes = new AnulacionResPronet();
                anulRes.setCodRetorno("999");
                anulRes.setDesRetorno("ERROR DE USR O CONTR");
                return Response.status(Response.Status.OK).entity(om.writeValueAsString(anulRes)).build();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al serializar/deserializar json", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).header("Content-Type", "text/plain; charset=utf-8").build();
        }
    }

    private PronetSesBean lookupPronetSesBeanBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (PronetSesBean) c.lookup("java:module/PronetSesBean");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
