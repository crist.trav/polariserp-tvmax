/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.service;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 */
public class ResultBusquedaSusc {
    
    private Integer cantidad;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    private List<SuscripcionBasic> lstSusc = new LinkedList<>();

    public List<SuscripcionBasic> getLstSusc() {
        return lstSusc;
    }

    public void setLstSusc(List<SuscripcionBasic> lstSusc) {
        this.lstSusc = lstSusc;
    }

    
}
