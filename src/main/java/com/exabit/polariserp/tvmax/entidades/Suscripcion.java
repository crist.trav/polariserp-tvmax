/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "suscripcion")
@NamedQueries({
    @NamedQuery(name = "Suscripcion.findAll", query = "SELECT s FROM Suscripcion s")
    , @NamedQuery(name = "Suscripcion.findByIdsuscripcion", query = "SELECT s FROM Suscripcion s WHERE s.idsuscripcion = :idsuscripcion")
    , @NamedQuery(name = "Suscripcion.findByCantidadTvs", query = "SELECT s FROM Suscripcion s WHERE s.cantidadTvs = :cantidadTvs")
    , @NamedQuery(name = "Suscripcion.findByDiaVencimientoMes", query = "SELECT s FROM Suscripcion s WHERE s.diaVencimientoMes = :diaVencimientoMes")
    , @NamedQuery(name = "Suscripcion.findByDireccion", query = "SELECT s FROM Suscripcion s WHERE s.direccion = :direccion")
    , @NamedQuery(name = "Suscripcion.findByFechaCambioEstado", query = "SELECT s FROM Suscripcion s WHERE s.fechaCambioEstado = :fechaCambioEstado")
    , @NamedQuery(name = "Suscripcion.findByFechaSuscripcion", query = "SELECT s FROM Suscripcion s WHERE s.fechaSuscripcion = :fechaSuscripcion")
    , @NamedQuery(name = "Suscripcion.findByNroMedidorElectrico", query = "SELECT s FROM Suscripcion s WHERE s.nroMedidorElectrico = :nroMedidorElectrico")
    , @NamedQuery(name = "Suscripcion.findByPrecio", query = "SELECT s FROM Suscripcion s WHERE s.precio = :precio")})
public class Suscripcion implements Serializable {

    @OneToMany(mappedBy = "suscripcion")
    private List<Cobro> cobroList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "suscripcion")
    private List<SuscripcionesExcluidasSorteo> suscripcionesExcluidasSorteoList;

    @OneToMany(mappedBy = "suscripcion")
    private List<DetalleFactura> detalleFacturaList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idsuscripcion", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idsuscripcion", table = "idsequences", allocationSize = 1, initialValue = 101126)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsuscripcion")
    private Integer idsuscripcion;
    @Column(name = "cantidad_tvs")
    private Integer cantidadTvs;
    @Column(name = "dia_vencimiento_mes")
    private Short diaVencimientoMes;
    @Size(max = 255)
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "fecha_cambio_estado")
    @Temporal(TemporalType.DATE)
    private Date fechaCambioEstado;
    @Column(name = "fecha_suscripcion")
    @Temporal(TemporalType.DATE)
    private Date fechaSuscripcion;
    @Size(max = 255)
    @Column(name = "nro_medidor_electrico")
    private String nroMedidorElectrico;
    @Column(name = "precio")
    private Integer precio;
    @ManyToMany(mappedBy = "suscripcionList")
    private List<Sorteo> sorteoList;
    @OneToMany(mappedBy = "suscripcion")
    private List<Cuota> cuotaList;
    @OneToMany(mappedBy = "ganador")
    private List<Premio> premioList;
    @JoinColumn(name = "barrio", referencedColumnName = "idbarrio")
    @ManyToOne
    private Barrio barrio;
    @JoinColumn(name = "cliente", referencedColumnName = "idcliente")
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(name = "cobrador", referencedColumnName = "idfuncionario")
    @ManyToOne
    private Funcionario cobrador;
    @JoinColumn(name = "estado", referencedColumnName = "idestado")
    @ManyToOne
    private EstadoSuscripcion estado;
    @JoinColumn(name = "servicio", referencedColumnName = "idservicio")
    @ManyToOne
    private Servicio servicio;
    @JoinColumn(name = "tipo_suscripcion", referencedColumnName = "idtipo_suscripcion")
    @ManyToOne
    private TipoSuscripcion tipoSuscripcion;
    @JoinColumn(name = "tipo_vivienda", referencedColumnName = "idtipo_vivienda")
    @ManyToOne
    private TipoVivienda tipoVivienda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "suscripcion")
    private List<HistorialSuscripcion> historialSuscripcionList;
    @OneToMany(mappedBy = "suscripcion")
    private List<SolicitudCambioEstado> solicitudCambioEstadoList;

    public Suscripcion() {
    }

    public Suscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }

    public Integer getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }

    public Integer getCantidadTvs() {
        return cantidadTvs;
    }

    public void setCantidadTvs(Integer cantidadTvs) {
        this.cantidadTvs = cantidadTvs;
    }

    public Short getDiaVencimientoMes() {
        return diaVencimientoMes;
    }

    public void setDiaVencimientoMes(Short diaVencimientoMes) {
        this.diaVencimientoMes = diaVencimientoMes;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaCambioEstado() {
        return fechaCambioEstado;
    }

    public void setFechaCambioEstado(Date fechaCambioEstado) {
        this.fechaCambioEstado = fechaCambioEstado;
    }

    public Date getFechaSuscripcion() {
        return fechaSuscripcion;
    }

    public void setFechaSuscripcion(Date fechaSuscripcion) {
        this.fechaSuscripcion = fechaSuscripcion;
    }

    public String getNroMedidorElectrico() {
        return nroMedidorElectrico;
    }

    public void setNroMedidorElectrico(String nroMedidorElectrico) {
        this.nroMedidorElectrico = nroMedidorElectrico;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public List<Sorteo> getSorteoList() {
        return sorteoList;
    }

    public void setSorteoList(List<Sorteo> sorteoList) {
        this.sorteoList = sorteoList;
    }

    public List<Cuota> getCuotaList() {
        return cuotaList;
    }

    public void setCuotaList(List<Cuota> cuotaList) {
        this.cuotaList = cuotaList;
    }

    public List<Premio> getPremioList() {
        return premioList;
    }

    public void setPremioList(List<Premio> premioList) {
        this.premioList = premioList;
    }

    public Barrio getBarrio() {
        return barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionario getCobrador() {
        return cobrador;
    }

    public void setCobrador(Funcionario cobrador) {
        this.cobrador = cobrador;
    }

    public EstadoSuscripcion getEstado() {
        return estado;
    }

    public void setEstado(EstadoSuscripcion estado) {
        this.estado = estado;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public TipoSuscripcion getTipoSuscripcion() {
        return tipoSuscripcion;
    }

    public void setTipoSuscripcion(TipoSuscripcion tipoSuscripcion) {
        this.tipoSuscripcion = tipoSuscripcion;
    }

    public TipoVivienda getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(TipoVivienda tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public List<HistorialSuscripcion> getHistorialSuscripcionList() {
        return historialSuscripcionList;
    }

    public void setHistorialSuscripcionList(List<HistorialSuscripcion> historialSuscripcionList) {
        this.historialSuscripcionList = historialSuscripcionList;
    }

    public List<SolicitudCambioEstado> getSolicitudCambioEstadoList() {
        return solicitudCambioEstadoList;
    }

    public void setSolicitudCambioEstadoList(List<SolicitudCambioEstado> solicitudCambioEstadoList) {
        this.solicitudCambioEstadoList = solicitudCambioEstadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsuscripcion != null ? idsuscripcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suscripcion)) {
            return false;
        }
        Suscripcion other = (Suscripcion) object;
        if ((this.idsuscripcion == null && other.idsuscripcion != null) || (this.idsuscripcion != null && !this.idsuscripcion.equals(other.idsuscripcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Suscripcion[ idsuscripcion=" + idsuscripcion + " ]";
    }

    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    public List<SuscripcionesExcluidasSorteo> getSuscripcionesExcluidasSorteoList() {
        return suscripcionesExcluidasSorteoList;
    }

    public void setSuscripcionesExcluidasSorteoList(List<SuscripcionesExcluidasSorteo> suscripcionesExcluidasSorteoList) {
        this.suscripcionesExcluidasSorteoList = suscripcionesExcluidasSorteoList;
    }

    public List<Cobro> getCobroList() {
        return cobroList;
    }

    public void setCobroList(List<Cobro> cobroList) {
        this.cobroList = cobroList;
    }
    
}
