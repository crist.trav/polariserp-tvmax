package com.exabit.polariserp.tvmax.rest.pronet;

/**
 *
 * @author traver
 */
public class ConsultaRespuestaDetalle {
    
    private Integer nroOperacion;
    private String desOperacion;    
    private Integer nroCuota;
    private String capital;
    private String interes;
    private String mora;
    private String punitorio;
    private String gastos;
    private String iva10;
    private String iva5;
    private String totalCuota;
    private String moneda;
    private String fechaVencimiento;

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fecha) {
        this.fechaVencimiento = fecha;
    }


    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }


    public String getTotalCuota() {
        return totalCuota;
    }

    public void setTotalCuota(String totalCuota) {
        this.totalCuota = totalCuota;
    }


    public String getIva5() {
        return iva5;
    }

    public void setIva5(String iva5) {
        this.iva5 = iva5;
    }
    
    public String getIva10() {
        return iva10;
    }

    public void setIva10(String iva10) {
        this.iva10 = iva10;
    }


    public String getGastos() {
        return gastos;
    }

    public void setGastos(String gastos) {
        this.gastos = gastos;
    }


    public String getPunitorio() {
        return punitorio;
    }

    public void setPunitorio(String punitorio) {
        this.punitorio = punitorio;
    }


    public String getMora() {
        return mora;
    }

    public void setMora(String mora) {
        this.mora = mora;
    }


    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }


    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }


    public Integer getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(int nroCuota) {
        this.nroCuota = nroCuota;
    }
    
    public Integer getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(int nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public String getDesOperacion() {
        return desOperacion;
    }

    public void setDesOperacion(String desOperacion) {
        this.desOperacion = desOperacion;
    }

}
