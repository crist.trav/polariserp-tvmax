/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "cobro")
@NamedQueries({
    @NamedQuery(name = "Cobro.findAll", query = "SELECT c FROM Cobro c")
    , @NamedQuery(name = "Cobro.findByIdcobro", query = "SELECT c FROM Cobro c WHERE c.idcobro = :idcobro")
    , @NamedQuery(name = "Cobro.findByAnulado", query = "SELECT c FROM Cobro c WHERE c.anulado = :anulado")
    , @NamedQuery(name = "Cobro.findByCodTransBanco", query = "SELECT c FROM Cobro c WHERE c.codTransBanco = :codTransBanco")
    , @NamedQuery(name = "Cobro.findByFecha", query = "SELECT c FROM Cobro c WHERE c.fecha = :fecha")
    , @NamedQuery(name = "Cobro.findByFechaHoraRegistro", query = "SELECT c FROM Cobro c WHERE c.fechaHoraRegistro = :fechaHoraRegistro")
    , @NamedQuery(name = "Cobro.findByMonto", query = "SELECT c FROM Cobro c WHERE c.monto = :monto")
    , @NamedQuery(name = "Cobro.findByPorceComisionAplicado", query = "SELECT c FROM Cobro c WHERE c.porceComisionAplicado = :porceComisionAplicado")})
public class Cobro implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idcobro", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idcobro", table = "idsequences", allocationSize = 1, initialValue = 78154)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcobro")
    private Integer idcobro;
    @Column(name = "anulado")
    private Boolean anulado;
    @Size(max = 255)
    @Column(name = "cod_trans_banco")
    private String codTransBanco;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "fecha_hora_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraRegistro;
    @Column(name = "monto")
    private Integer monto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "porce_comision_aplicado")
    private Float porceComisionAplicado;
//    @JoinTable(name = "cobro_cuota", joinColumns = {
//        @JoinColumn(name = "cobro", referencedColumnName = "idcobro")}, inverseJoinColumns = {
//        @JoinColumn(name = "cuota", referencedColumnName = "idcuota")})
//    @ManyToMany
//    private List<Cuota> cuotaList;
//    @JoinColumn(name = "cobrador", referencedColumnName = "idfuncionario")
    @JoinColumn(name = "cobrador", referencedColumnName = "idfuncionario")
    @ManyToOne
    private Funcionario cobrador;
    @JoinColumn(name = "concepto", referencedColumnName = "idconcepto")
    @ManyToOne
    private Concepto concepto;
    @JoinColumn(name = "detalle_factura", referencedColumnName = "iddetalle_factura")
    @ManyToOne
    private DetalleFactura detalleFactura;
    @JoinColumn(name = "medio_pago", referencedColumnName = "idmedio_pago")
    @ManyToOne
    private MedioPago medioPago;
    @JoinColumn(name = "funcionario_registro", referencedColumnName = "idfuncionario")
    @ManyToOne
    private Funcionario funcionarioRegistro;
    @JoinColumn(name = "servicio", referencedColumnName = "idservicio")
    @ManyToOne
    private Servicio servicio;
    @JoinTable(name = "cobros_pago_req_pronet", joinColumns = {
        @JoinColumn(name = "cobro_idcobro", referencedColumnName = "idcobro")}, inverseJoinColumns = {
        @JoinColumn(name = "pago_req_pronet_idpago_req", referencedColumnName = "idpago_req")})
    @ManyToMany
    private List<PagoReqPronet> pagoReqPronetList;
    @JoinColumn(name = "cuota", referencedColumnName = "idcuota")
    @ManyToOne
    private Cuota cuota;
    @JoinColumn(name = "suscripcion", referencedColumnName = "idsuscripcion")
    @ManyToOne
    private Suscripcion suscripcion;

    public Cobro() {
    }

    public Cobro(Integer idcobro) {
        this.idcobro = idcobro;
    }

    public Integer getIdcobro() {
        return idcobro;
    }

    public void setIdcobro(Integer idcobro) {
        this.idcobro = idcobro;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    public String getCodTransBanco() {
        return codTransBanco;
    }

    public void setCodTransBanco(String codTransBanco) {
        this.codTransBanco = codTransBanco;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Float getPorceComisionAplicado() {
        return porceComisionAplicado;
    }

    public void setPorceComisionAplicado(Float porceComisionAplicado) {
        this.porceComisionAplicado = porceComisionAplicado;
    }

//    public List<Cuota> getCuotaList() {
//        return cuotaList;
//    }
//
//    public void setCuotaList(List<Cuota> cuotaList) {
//        this.cuotaList = cuotaList;
//    }

    public Funcionario getCobrador() {
        return cobrador;
    }

    public void setCobrador(Funcionario cobrador) {
        this.cobrador = cobrador;
    }

    public Concepto getConcepto() {
        return concepto;
    }

    public void setConcepto(Concepto concepto) {
        this.concepto = concepto;
    }

    public DetalleFactura getDetalleFactura() {
        return detalleFactura;
    }

    public void setDetalleFactura(DetalleFactura detalleFactura) {
        this.detalleFactura = detalleFactura;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    public Funcionario getFuncionarioRegistro() {
        return funcionarioRegistro;
    }

    public void setFuncionarioRegistro(Funcionario funcionarioRegistro) {
        this.funcionarioRegistro = funcionarioRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcobro != null ? idcobro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobro)) {
            return false;
        }
        Cobro other = (Cobro) object;
        if ((this.idcobro == null && other.idcobro != null) || (this.idcobro != null && !this.idcobro.equals(other.idcobro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Cobro[ idcobro=" + idcobro + " ]";
    }

    public List<PagoReqPronet> getPagoReqPronetList() {
        return pagoReqPronetList;
    }

    public void setPagoReqPronetList(List<PagoReqPronet> pagoReqPronetList) {
        this.pagoReqPronetList = pagoReqPronetList;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Cuota getCuota() {
        return cuota;
    }

    public void setCuota(Cuota cuota) {
        this.cuota = cuota;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }
}
