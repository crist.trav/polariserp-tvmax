/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.servicio;

import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "estadoSusManBean")
@RequestScoped
public class EstadoSusManBean {

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;
    
    
    
    /**
     * Creates a new instance of EstadoSusManBean
     */
    public EstadoSusManBean() {
    }
    
    public List<EstadoSuscripcion> getEstadosSuscripcion(){
        return this.estadoSuscripcionFacade.findAll();
    }
    
}
