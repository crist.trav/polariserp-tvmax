/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.suscripciones;

import com.exabit.polariserp.tvmax.entidades.CampoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.HistorialSuscripcion;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.reportes.UtilReportesSesBean;
import com.exabit.polariserp.tvmax.sesbeans.CampoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.HistorialSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.SolicitudCambioEstadoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "detalleSuscripcionManBean")
@ViewScoped
public class DetalleSuscripcionManBean implements Serializable {

    @EJB
    private HistorialSuscripcionFacade historialSuscripcionFacade;

    @EJB
    private CampoSuscripcionFacade campoSuscripcionFacade;

    @EJB
    private UtilReportesSesBean utilReportesSesBean;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;

    @EJB
    private CuotaFacade cuotaFacade;

    private Integer idCampoSuscFiltro;

    public Integer getIdCampoSuscFiltro() {
        return idCampoSuscFiltro;
    }

    public void setIdCampoSuscFiltro(Integer idCampoSuscFiltro) {
        this.idCampoSuscFiltro = idCampoSuscFiltro;
    }

    private final List<HistorialSuscripcion> lstHistorialSuscripcion = new LinkedList<>();

    public List<HistorialSuscripcion> getLstHistorialSuscripcion() {
        return lstHistorialSuscripcion;
    }

    private String fechaVtoEdit;

    public String getFechaVtoEdit() {
        return fechaVtoEdit;
    }

    public void setFechaVtoEdit(String fechaVtoEdit) {
        this.fechaVtoEdit = fechaVtoEdit;
    }

    private Cuota cuotaEditVar = new Cuota();

    public Cuota getCuotaEditVar() {
        return cuotaEditVar;
    }

    public void setCuotaEditVar(Cuota cuotaEditVar) {
        this.cuotaEditVar = cuotaEditVar;
    }

    private Integer idCuotaEliminar;

    public Integer getIdCuotaEliminar() {
        return idCuotaEliminar;
    }

    public void setIdCuotaEliminar(Integer idCuotaEliminar) {
        this.idCuotaEliminar = idCuotaEliminar;
    }

//        private Integer idCuotaEditar;
//
//    public Integer getIdCuotaEditar() {
//        return idCuotaEditar;
//    }
//
//    public void setIdCuotaEditar(Integer idCuotaEditar) {
//        this.idCuotaEditar = idCuotaEditar;
//    }
    private SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");

    @PostConstruct
    private void postConstruct() {
        
    }

    private List<SolicitudCambioEstado> lstSolicitudCE = new LinkedList<>();

    public List<SolicitudCambioEstado> getLstSolicitudCE() {
        return lstSolicitudCE;
    }

    public void setLstSolicitudCE(List<SolicitudCambioEstado> lstSolicitudCE) {
        this.lstSolicitudCE = lstSolicitudCE;
    }

    @EJB
    private SuscripcionFacade suscripcionFacade;

    private Suscripcion suscripcionVar;

    public Suscripcion getSuscripcionVar() {
        return suscripcionVar;
    }

    public void setSuscripcionVar(Suscripcion suscripcionVar) {
        this.suscripcionVar = suscripcionVar;
    }

    private Integer idSusc = -1;

    public Integer getIdSusc() {
        return idSusc;
    }

    public void setIdSusc(Integer idSusc) {
        this.idSusc = idSusc;
    }

    private List<Cuota> lstCuotas;

    public List<Cuota> getLstCuotas() {
        return lstCuotas;
    }

    public void setLstCuotas(List<Cuota> lstCuotas) {
        this.lstCuotas = lstCuotas;
    }

    public DetalleSuscripcionManBean() {
        this.lstCuotas = new LinkedList<>();
    }

    public void verDetalleSuscripcion() {
//        this.iniciarConversacion();
//        this.setIdSusc(id);
        this.suscripcionVar = this.suscripcionFacade.find(this.idSusc);
//        System.out.println("Id recibido: "+id);
        this.lstCuotas.addAll(this.cuotaFacade.getCuotaOrdenVencimiento(suscripcionVar));
        this.lstSolicitudCE.addAll(this.solicitudCambioEstadoFacade.getSolicitudesOrdenFecha(suscripcionVar));
//        this.finalizarConversacion();
//        return "/servicios/detallesuscripcion.xhtml?faces-redirect=true&ids="+id;
        this.filtrarHistorialSuscripcion();
    }
    
    public void filtrarHistorialSuscripcion(){
        
        this.lstHistorialSuscripcion.clear();
        if(this.idCampoSuscFiltro==null){
        this.lstHistorialSuscripcion.addAll(this.historialSuscripcionFacade.getHistorialSuscripcionListOrdenado(this.suscripcionVar));
        }else{
            CampoSuscripcion campos=this.campoSuscripcionFacade.find(this.idCampoSuscFiltro);
            this.lstHistorialSuscripcion.addAll(this.historialSuscripcionFacade.getHistorialSuscripcionListOrdenado(suscripcionVar, campos));
        }
    }

    public void verificarValor() {
        System.out.println("id susc " + this.idSusc);
    }

    public void eliminarCuota() {
        System.out.println("Eliminar cuota " + this.idCuotaEliminar);
        this.cuotaFacade.remove(this.cuotaFacade.find(this.idCuotaEliminar));
        this.lstCuotas.clear();
        this.lstCuotas.addAll(this.cuotaFacade.getCuotaOrdenVencimiento(suscripcionVar));
    }

    public void seleccionCuotaEditar(Integer idcuota) {

        this.cuotaEditVar = this.cuotaFacade.find(idcuota);
        this.fechaVtoEdit = this.sdf.format(this.cuotaEditVar.getFechaVencimiento());

    }

    public void guardarCuota() {
        System.out.println("Guardar cuota");

        if (this.cuotaEditVar.getMontoCuota().equals(0)) {
            this.cuotaEditVar.setMontoEntrega(0);
            this.cuotaEditVar.setCancelado(true);
        } else {
            this.cuotaEditVar.setCancelado(false);
        }
        this.cuotaFacade.edit(cuotaEditVar);
        this.lstCuotas.clear();
        this.lstCuotas.addAll(this.cuotaFacade.getCuotaOrdenVencimiento(suscripcionVar));
    }

    public void generarReportePDF() {
        this.utilReportesSesBean.generarReporteDetalleSuscripcion(idSusc);
    }

    public List<CampoSuscripcion> getCamposSuscripcionList() {
        return this.campoSuscripcionFacade.getCamposSuscripcionOrdenado();
    }
}
