/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.ParametroSistema;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class ParametroSistemaFacade extends AbstractFacade<ParametroSistema> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ParametroSistemaFacade() {
        super(ParametroSistema.class);
    }
    
    public String getParametro(int clave){
        TypedQuery<ParametroSistema> q=this.em.createQuery("SELECT p FROM ParametroSistema p WHERE p.clave=:clave", ParametroSistema.class);
        q.setParameter("clave", clave);
        List<ParametroSistema> lstpar=q.getResultList();
        if(lstpar.isEmpty()){
            return null;
        }else{
            return lstpar.get(0).getValor();
        }
    }
    
    public void setParametro(Integer clave, String valor){
        ParametroSistema p=(this.em.find(ParametroSistema.class, clave));
        if(p!=null){
            p.setValor(valor);
            this.edit(p);
        }else{
            ParametroSistema np=new ParametroSistema();
            np.setClave(clave);
            np.setValor(valor);
            this.create(np);
        }
    }
    
}
