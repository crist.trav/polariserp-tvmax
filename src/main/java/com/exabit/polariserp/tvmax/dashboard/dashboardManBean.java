package com.exabit.polariserp.tvmax.dashboard;

import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.UsoModuloFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author traver
 */
@Named(value = "dashboardManBean")
@ViewScoped
public class dashboardManBean implements Serializable {

    @EJB
    private UsoModuloFacade usoModuloFacade;

    @Inject
    private SessionManBean sessionManBean;

    /**
     * Creates a new instance of dashboardManBean
     */
    public dashboardManBean() {

    }

    public List<Modulo> getModulosUsoFrecuente(int idfuncionario) {
        return this.usoModuloFacade.getModulosMasUsadosAutorizados(idfuncionario, 6);
    }

}
