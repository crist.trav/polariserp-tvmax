/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "servicio")
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s")
    , @NamedQuery(name = "Servicio.findByIdservicio", query = "SELECT s FROM Servicio s WHERE s.idservicio = :idservicio")
    , @NamedQuery(name = "Servicio.findByNombre", query = "SELECT s FROM Servicio s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Servicio.findByPorcentajeIva", query = "SELECT s FROM Servicio s WHERE s.porcentajeIva = :porcentajeIva")
    , @NamedQuery(name = "Servicio.findByPrecio", query = "SELECT s FROM Servicio s WHERE s.precio = :precio")})
public class Servicio implements Serializable {

    

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idservicio", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idservicio", table = "idsequences", allocationSize = 1, initialValue = 6)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idservicio")
    private Integer idservicio;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "porcentaje_iva")
    private Short porcentajeIva;
    @Column(name = "precio")
    private Integer precio;
    @OneToMany(mappedBy = "servicio")
    private List<Suscripcion> suscripcionList;
    @JoinColumn(name = "concepto_cobro", referencedColumnName = "idconcepto")
    @ManyToOne
    private Concepto conceptoCobro;
    @OneToMany(mappedBy = "servicio")
    private List<DetalleFactura> detalleFacturaList;
    @Column(name = "suscribible")
    private Boolean suscribible;
    @OneToMany(mappedBy = "servicio")
    private List<Cuota> cuotaList;
    @OneToMany(mappedBy = "servicio")
    private List<Cobro> cobroList;

    public Servicio() {
    }

    public Servicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public Integer getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Short getPorcentajeIva() {
        return porcentajeIva;
    }

    public void setPorcentajeIva(Short porcentajeIva) {
        this.porcentajeIva = porcentajeIva;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    public Concepto getConceptoCobro() {
        return conceptoCobro;
    }

    public void setConceptoCobro(Concepto conceptoCobro) {
        this.conceptoCobro = conceptoCobro;
    }

    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idservicio != null ? idservicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.idservicio == null && other.idservicio != null) || (this.idservicio != null && !this.idservicio.equals(other.idservicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Servicio[ idservicio=" + idservicio + " ]";
    }

    public Boolean getSuscribible() {
        return suscribible;
    }

    public void setSuscribible(Boolean suscribible) {
        this.suscribible = suscribible;
    }

    public List<Cuota> getCuotaList() {
        return cuotaList;
    }

    public void setCuotaList(List<Cuota> cuotaList) {
        this.cuotaList = cuotaList;
    }

    public List<Cobro> getCobroList() {
        return cobroList;
    }

    public void setCobroList(List<Cobro> cobroList) {
        this.cobroList = cobroList;
    }
    
}
