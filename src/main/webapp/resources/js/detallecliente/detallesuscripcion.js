var dateFormatter = {
    date: function (date, settings) {
        if (!date)
            return '';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + '/' + month + '/' + year;
    }
};

$(document).ready(function () {
    $("#tabDatosSuscripcion").tab();
    $("#tabCuotasSuscripcion").tab();
    $("#tabOtrasCuotas").tab();
    
});

function ajaxGuardarSuscripcion(data) {
    var status = data.status;
    switch (status) {
        case 'begin':
        {
            $("#btnGuardarSuscripcion").addClass("loading");
            $("#btnGuardarSuscripcion").addClass("disabled");
            break;
        }
        case 'success':
        {
            $("#btnGuardarSuscripcion").removeClass("loading");
            $("#btnGuardarSuscripcion").removeClass("disabled");

            setTipoMsgSusc($("#tipoMsgSusc").val());

            $("#msgDetalleSusc").removeClass("hidden");
            $("#msgDetalleSusc").show();
            var idsusc = $("#in-idsuscripcion").val();
            if (idsusc !== "") {
                $("#suscripcionActual").empty();
                $("#suscripcionActual").append(idsusc);
            }
            break;
        }
    }
}

function mostrarMsgSusc(tipo, cab, msg) {
    setTipoMsgSusc(tipo);

    $("#cabeceraMsgSusc").empty();
    $("#cabeceraMsgSusc").append(cab);

    $("#detalleMsgSusc").empty();
    $("#detalleMsgSusc").append(msg);

    $("#msgDetalleSusc").removeClass("hidden");
    $("#msgDetalleSusc").show();
}

function setTipoMsgSusc(tipo) {
    $("#msgDetalleSusc").removeClass("info");
    $("#msgDetalleSusc").removeClass("warning");
    $("#msgDetalleSusc").removeClass("error");
    $("#msgDetalleSusc").addClass(tipo);
}

function validarSuscripcion() {
    $("#in-idclientesusc").val($("#idcliente").html());
    var validado = true;
    if ($("#in-idservicio").val() === "") {
        $("#fieldServicio").addClass("error");
        validado = false;
    } else {
        $("#fieldServicio").removeClass("error");
    }
    if ($("#in-montocuota").val() === "") {
        $("#fieldMontoCuota").addClass("error");
        validado = false;
    } else {
        $("#fieldMontoCuota").removeClass("error");
    }
    if ($("#in-idtiposusc").val() === "") {
        $("#fieldTipoSuscripcion").addClass("error");
        validado = false;
    } else {
        $("#fieldTipoSuscripcion").removeClass("error");
    }
    if ($("#in-idbarrio").val() === "") {
        validado = false;
        $("#fieldBarrio").addClass("error");
    } else {
        $("#fieldBarrio").removeClass("error");
    }
    if ($("#in-tipoviv").val() === "") {
        validado = false;
        $("#fieldTipoVivienda").addClass("error");
    } else {
        $("#fieldTipoVivienda").removeClass("error");
    }
    
    if($("#in-fecha-suscripcion").val()===""){
        validado=false;
        $("#fieldFechaSuscripcion").addClass("error");
    }else{
        $("#fieldFechaSuscripcion").removeClass("error");
    }
//    if ($("#in-idcobrador").val() === "") {
//        validado = false;
//        $("#fieldCobrador").addClass("error");
//    } else {
//        $("#fieldCobrador").removeClass("error");
//    }
    if (!validado) {
        mostrarMsgSusc("error", "Error de validación", "Complete los campos obligatorios");
    }
    return validado;
}

function mostrarConfirmacionEliminarSusc(idsusceliminar) {
    $("#txfIdSuscEliminar").val(idsusceliminar);
    $("#modalEliminarSusc").modal("show");
    $("#spnIdSuscElim").empty();
    $("#spnIdSuscElim").append(idsusceliminar);
}
;

function mostrarConfirmacionEliminarCuo(idcuotaeliminar) {
    console.log("id cuota eliminar: " + idcuotaeliminar);
    $("#txfIdCuotaEliminar").val(idcuotaeliminar);
    $("#modalEliminarCuota").modal("show");
}

function mostrarCuotasAnio(anio) {
    console.log("mostrar anio: " + anio);
    var mostrar = true;
    if ($("#icono-fila-grupo-" + anio).hasClass("down")) {
        $("#icono-fila-grupo-" + anio).removeClass("down");
        $("#icono-fila-grupo-" + anio).addClass("right");
        mostrar = false;
    } else {
        $("#icono-fila-grupo-" + anio).removeClass("right");
        $("#icono-fila-grupo-" + anio).addClass("down");
        mostrar = true;
    }
    $("[id^='fila-" + anio + "']").each(function (index, value) {
        console.log("filaoculta");
        if (mostrar) {
            $(this).show();
        } else {
            $(this).hide();
        }

    });
}

function ajaxCargarCuotaModificar(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#modalEditarCuota").modal("show");
            $('#calVencimientoCuotaEditar').calendar({
                type: "date",
                monthFirst: false,
                formatter: dateFormatter
            });
            $('#modalEditarCuota .message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
            break;
        }
    }
}

function validarEdicionCuota() {
    var validado = true;
    if ($("#in-montocuotaeditar").val() === "") {
        validado = false;
        $("#fieldMontoCuotaEditar").addClass("error");
    } else {
        $("#fieldMontoCuotaEditar").removeClass("error");
    }
    if ($("#in-fechavenceditar").val() === "") {
        validado = false;
        $("#fieldVencimientoEditar").addClass("error");
    } else {
        $("#fieldVencimientoEditar").removeClass("error");
    }
    if (!validado) {
        if ($("#msgEditarCuota").hasClass("hidden")) {
            $("#msgEditarCuota").transition("fade");
        }
    }
    return validado;
}

function ajaxEditarCuota(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#modalEditarCuota").modal("hide");
            setTipoMsgSusc($("#tipoMsgSusc").val());
            if ($("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            break;
        }
    }
}