const textoCalendar = {
    days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    months: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
    monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
    today: 'Hoy',
    now: 'Ahora',
    am: 'AM',
    pm: 'PM'
};

const formatterCalendar = {
    date: function (date, settings) {
        if (!date)
            return '';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        return day + '/' + month + '/' + year;
    }
};

$(document).ready(function () {
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    inicializarCalendars();
    $("#in-busqueda").val("");

    limpiarSeleccion();
});

function inicializarCalendars() {
    $("#fecha-inicio").calendar({
        type: 'date',
        text: textoCalendar,
        formatter: formatterCalendar,
        endCalendar: $("#fecha-fin")
    });

    $("#fecha-fin").calendar({
        type: 'date',
        text: textoCalendar,
        formatter: formatterCalendar,
        startCalendar: $("#fecha-inicio")
    });
}

function limpiarSeleccion() {
    $("[id^='rw']").removeClass("active");
    $("[id^='chk-sl']").prop("checked", false);
}

function selecUnaFila(id) {
    limpiarSeleccion();
    $("#rw-" + id).addClass("active");
    $("#chk-sl-" + id).prop("checked", true);
    $("#in-idtalonariosel").val(id);
    $("#cml-seleccionartalonario").click();
    evaluarMostrarBtnElim();
}

function agregarSeleccion(id) {
    if ($("#rw-" + id).hasClass("active")) {
        $("#rw-" + id).removeClass("active");
        $("#chk-sl-" + id).prop("checked", false);
    } else {
        $("#rw-" + id).addClass("active");
        $("#chk-sl-" + id).prop("checked", true);
    }
    evaluarMostrarBtnElim();
}

function generarDatosSeleccion() {
    var idSeleccionados = [];
    $("tbody tr [id^='col-idtalonario']").each(function (index, value) {
        var id = parseInt($(this).html());
        if ($('#chk-sl-' + id).prop('checked')) {
            idSeleccionados.push(id);
        }
    });
    var jsonselect = JSON.stringify(idSeleccionados);
    $('#selbar').val(jsonselect);
}
function setTipoMsg(tipo) {
    $("#msg-status").removeClass("positive");
    $("#msg-status").removeClass("negative");
    $("#msg-status").removeClass("info");
    $("#msg-status").removeClass("warning");
    $("#msg-status").removeClass("error");
    $("#msg-status").addClass(tipo);
}

function mostrarMsg(tipo, cabecera, detalle) {
    setTipoMsg(tipo);
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#detalleMsg").empty();
    $("#detalleMsg").append(detalle);

    if ($("#msg-status").hasClass("hidden")) {
        $("#msg-status").transition("fade");
    }
}


function ajaxGuardarTalonario(data) {
    var s = data.status;
    switch (s) {
        case "begin":
        {
            $("#btnGuardar").addClass("loading");
            $("#btnGuardar").addClass("disabled");
            break;
        }
        case "success":
        {
            $("#btnGuardar").removeClass("loading");
            $("#btnGuardar").removeClass("disabled");
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg-status").hasClass("hidden")) {
                $("#msg-status").transition("fade");
            }
            break;
        }
    }
}

function evaluarMostrarBtnElim() {
    if ($("[id^='chk-sl']:checked").length !== 0) {
        $("#btn-eliminar").show();
    } else {
        $("#btn-eliminar").hide();
    }
}

function mostrarModalConfElim() {
    generarDatosSeleccion();
    $("#dlg-conf-elim").modal("show");
}

function validar() {
    var validado = true;
    if ($("#in-notimbrado").val() === "") {
        validado = false;
        $("#fieldTimbrado").addClass("error");
    } else {
        $("#fieldTimbrado").removeClass("error");
    }

    if ($("#in-codestablecimiento").val() === "") {
        validado = false;
        $("#fieldCodEstablecimiento").addClass("error");
    } else {
        $("#fieldCodEstablecimiento").removeClass("error");
    }

    if ($("#in-fecha-inicio").val() === "") {
        validado = false;
        $("#fieldFechaInicioVigencia").addClass("error");
    } else {
        $("#fieldFechaInicioVigencia").removeClass("error");
    }

    if ($("#in-fecha-fin").val() === "") {
        validado = false;
        $("#fieldFechaFinVigencia").addClass("error");
    } else {
        $("#fieldFechaFinVigencia").removeClass("error");
    }

    if ($("#in-noinicio").val() === "") {
        validado = false;
        $("#fieldNoInicio").addClass("error");
    } else {
        $("#fieldNoInicio").removeClass("error");
    }

    if ($("#in-nofin").val() === "") {
        validado = false;
        $("#fieldNoFin").addClass("error");
    } else {
        $("#fieldNoFin").removeClass("error");
    }

    if ($("#in-noactual").val() === "") {
        validado = false;
        $("#fieldNoActual").addClass("error");
    } else {
        $("#fieldNoActual").removeClass("error");
    }

    if ($("#in-idterminal").val() === "") {
        validado = false;
        $("#fieldTerminalImpresion").addClass("error");
    } else {
        $("#fieldTerminalImpresion").removeClass("error");
    }

    if ($("#in-formatoimpresion").val() === "") {
        validado = false;
        $("#fieldFormatoImpresion").addClass("error");
    } else {
        $("#fieldFormatoImpresion").removeClass("error");
    }

    if (!validado) {
        mostrarMsg("negative", "Error de validación", "Complete los campos obligatorios");
    } else {
        if ($("#in-noinicio").val() > $("#in-nofin").val()) {
            validado = false;
            $("#fieldNoInicio").addClass("error");
            $("#fieldNoFin").addClass("error");
            mostrarMsg("negative", "Error de validación", "Rango invertido");
        } else {
            $("#fieldNoInicio").removeClass("error");
            $("#fieldNoFin").removeClass("error");

            if ($("#in-noactual").val() < $("#in-noinicio").val() || $("#in-noactual").val() > $("#in-nofin").val()) {
                validado = false;
                $("#fieldNoActual").addClass("error");
                mostrarMsg("negative", "Error de validación", "Número actual fuera del rango del talonario");
            } else {
                $("#fieldNoActual").removeClass("error");
            }
        }

    }
    return validado;
}

function ajaxBusquedaTalonario(data) {
    var s = data.status;
    switch (s) {
        case "begin":
        {
            $("#icon-busqueda").removeClass("search");
            $("#icon-busqueda").addClass("spinner");
            break;
        }
        case "success":
        {
            $("#icon-busqueda").addClass("search");
            $("#icon-busqueda").removeClass("spinner");
            break;
        }
    }
}

function ajaxSeleccionarTalonario(data) {
    var s = data.status;
    switch (s) {
        case "success":
        {
            $("#cmb-terminal").dropdown();
            $("#cmb-formato").dropdown();
            break;
        }
    }
}

function ajaxEliminarTalonarios(data) {
    var s = data.status;
    switch (s) {
        case "success":
        {
            setTipoMsg($("#tipoMsg").val());
            inicializarCalendars();
            if ($("#msg-status").hasClass("hidden")) {
                $("#msg-status").transition("fade");
            }
            break;
        }
    }
}

function ajaxLimpiarFormulario(data) {
    var s = data.status;
    switch (s) {
        case "success":
        {
            limpiarSeleccion();
            inicializarCalendars();
            $("#cmb-terminal").dropdown();
            $("#cmb-formato").dropdown();
            if (!$("#msg-status").hasClass("hidden")) {
                $("#msg-status").transition("fade");
            }
            evaluarMostrarBtnElim();
            $("#in-notimbrado").focus();
            break;
        }
    }
}

var timeOut = 0;
$('#in-busqueda').on('input', function () {
    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        $('#cml-buscar').click();
    }, 500);
});
