package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "pago_res_pronet")
@NamedQueries({
    @NamedQuery(name = "PagoResPronet.findAll", query = "SELECT p FROM PagoResPronet p")
    , @NamedQuery(name = "PagoResPronet.findByIdpagoRes", query = "SELECT p FROM PagoResPronet p WHERE p.idpagoRes = :idpagoRes")
    , @NamedQuery(name = "PagoResPronet.findByCodServicio", query = "SELECT p FROM PagoResPronet p WHERE p.codServicio = :codServicio")
    , @NamedQuery(name = "PagoResPronet.findByTipoTrx", query = "SELECT p FROM PagoResPronet p WHERE p.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "PagoResPronet.findByCodRetorno", query = "SELECT p FROM PagoResPronet p WHERE p.codRetorno = :codRetorno")
    , @NamedQuery(name = "PagoResPronet.findByDesRetorno", query = "SELECT p FROM PagoResPronet p WHERE p.desRetorno = :desRetorno")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PagoResPronet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idpagorespronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idpagorespronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idpago_res")
    @JsonIgnore
    private Integer idpagoRes;
    @Size(max = 5)
    @Column(name = "codServicio")
    private String codServicio;
    @Column(name = "tipoTrx")
    private Integer tipoTrx;
    @Size(max = 3)
    @Column(name = "codRetorno")
    private String codRetorno;
    @Size(max = 30)
    @Column(name = "desRetorno")
    private String desRetorno;
    @JoinColumn(name = "pago_req_pronet", referencedColumnName = "idpago_req")
    @ManyToOne(optional = false)
    @JsonIgnore
    private PagoReqPronet pagoReqPronet;

    public PagoResPronet() {
    }

    public PagoResPronet(Integer idpagoRes) {
        this.idpagoRes = idpagoRes;
    }

    public Integer getIdpagoRes() {
        return idpagoRes;
    }

    public void setIdpagoRes(Integer idpagoRes) {
        this.idpagoRes = idpagoRes;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
    }

    public String getDesRetorno() {
        return desRetorno;
    }

    public void setDesRetorno(String desRetorno) {
        this.desRetorno = desRetorno;
    }

    public PagoReqPronet getPagoReqPronet() {
        return pagoReqPronet;
    }

    public void setPagoReqPronet(PagoReqPronet pagoReqPronet) {
        this.pagoReqPronet = pagoReqPronet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpagoRes != null ? idpagoRes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagoResPronet)) {
            return false;
        }
        PagoResPronet other = (PagoResPronet) object;
        if ((this.idpagoRes == null && other.idpagoRes != null) || (this.idpagoRes != null && !this.idpagoRes.equals(other.idpagoRes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.PagoResPronet[ idpagoRes=" + idpagoRes + " ]";
    }
    
}
