package com.exabit.polariserp.tvmax.facturas.util;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cuota;;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author traver
 */
public class ClienteGenerar {

    private Cliente cliente;
    private List<SuscripcionGenerar> lstSuscripciones = new ArrayList<>();
    private int maxDetallesPorFactura = 4;

    public ClienteGenerar(int maxDetallesPorFactura) {
        this.maxDetallesPorFactura = maxDetallesPorFactura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<SuscripcionGenerar> getLstSuscripciones() {
        return lstSuscripciones;
    }

    public void setLstSuscripciones(List<SuscripcionGenerar> lstSuscripciones) {
        this.lstSuscripciones = lstSuscripciones;
    }

    public int getTotalCuotasIncluidas() {
        int total = 0;
        for (SuscripcionGenerar sg : this.lstSuscripciones) {
            total = total + sg.getTotalCuotasIncluidas();
        }
        return total;
    }

    public int getTotalCuotasExcluidas() {
        int total = 0;
        for (SuscripcionGenerar sg : this.lstSuscripciones) {
            total = total + sg.getTotalCuotasExcluidas();
        }
        return total;
    }

    public boolean existenCuotasSecundarias() {
        boolean existe = false;
        for (SuscripcionGenerar sg : this.lstSuscripciones) {
            if (sg.existenCuotasSecundarias()) {
                existe = true;
            }
        }
        return existe;
    }

    public List<List<SuscripcionGenerar>> getGruposSuscPorUbicacion(List<SuscripcionGenerar> lsts) {
        Map<String, List<SuscripcionGenerar>> maps = new HashMap<>();
        for (SuscripcionGenerar s : lsts) {
            String dir = s.getSuscripcion().getBarrio().getIdbarrio() + s.getSuscripcion().getDireccion().trim().replace(" ", "");
            if (maps.get(dir) == null) {
                maps.put(dir, new ArrayList<>());
            }
            maps.get(dir).add(s);
//            }
        }
        List<List<SuscripcionGenerar>> lstgs = new ArrayList<>();
        lstgs.addAll(maps.values());
        return lstgs;
    }

    public List<List<Cuota>> getGruposCuotasGenerar() {
        int cgrupos = 0;
        List<List<Cuota>> lstfacturasgeneral = new ArrayList<>();
        List<List<SuscripcionGenerar>> lstg = this.getGruposSuscPorUbicacion(this.getLstSuscripciones());
        for (List<SuscripcionGenerar> l : lstg) {
            List<List<Cuota>> lst = new ArrayList<>();
            List<List<Cuota>> lstRezagados = new ArrayList<>();//Grupos de cuotas que se dejan para agregar al final por sobrepasar la cantidad maxima de items por factura;
            for (SuscripcionGenerar sg : l) {
                for (GrupoCuotaGenerar gcg : sg.getLstGruposCuotas()) {
                    if (gcg.isIncluir()) {
                        if (lst.size() > sg.getLstGruposCuotas().indexOf(gcg)) {
                            int cantExistenteGrupo = lst.get(sg.getLstGruposCuotas().indexOf(gcg)).size();
                            int cantAgregar = gcg.getLstCuotas().size();
                            if ((cantExistenteGrupo + cantAgregar) <= maxDetallesPorFactura) {
                                lst.get(sg.getLstGruposCuotas().indexOf(gcg)).addAll(gcg.getLstCuotas());
                            } else {
                                lstRezagados.add(gcg.getLstCuotas());
                            }
                        } else {
                            List<Cuota> lstc = new ArrayList<>();
                            lstc.addAll(gcg.getLstCuotas());
                            lst.add(lstc);
                        }
                    }
                }
            }
            cgrupos++;
            lstfacturasgeneral.addAll(lst);
            lstfacturasgeneral.addAll(lstRezagados);
        }

        for (List<Cuota> l : lstfacturasgeneral) {
            System.out.println("######Factura " + lstfacturasgeneral.indexOf(l) + "#########");
            for (Cuota c : l) {
                System.out.println(c.getSuscripcion().getIdsuscripcion() + "-" + (c.getServicio() != null ? c.getServicio().getNombre() : c.getSuscripcion().getServicio().getNombre()) + "-" + c.getMontoCuota() + "->" + c.getMesCuota() + "/" + c.getAnioCuota());
            }
        }
        return lstfacturasgeneral;
    }

}
