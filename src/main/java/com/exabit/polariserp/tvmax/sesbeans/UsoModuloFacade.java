package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.entidades.UsoModulo;
import com.exabit.polariserp.tvmax.entidades.UsoModuloPK;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author traver
 */
@Stateless
public class UsoModuloFacade extends AbstractFacade<UsoModulo> {

    @EJB
    private ModuloFacade moduloFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsoModuloFacade() {
        super(UsoModulo.class);
    }

    public void incrementarContadorUso(Funcionario f, Modulo m) {
        this.incrementarContadorUso(f.getIdfuncionario(), m.getIdmodulo());
    }

    public void incrementarContadorUso(int idfuncionario, int idmodulo) {
        UsoModuloPK umpk = new UsoModuloPK(idfuncionario, idmodulo);
        UsoModulo um = this.find(umpk);
        if (um == null) {
            um = new UsoModulo();
            um.setUsoModuloPK(umpk);
        }
        if (!this.getUsoModulosMasUsados(idfuncionario, idmodulo).isEmpty()) {
            UsoModulo mu = this.getUsoModulosMasUsados(idfuncionario, idmodulo).get(0);
            if (!mu.equals(um)) {
                um.setContadorUso(um.getContadorUso() + 1);
                this.edit(um);
            }
        } else {
            um.setContadorUso(um.getContadorUso() + 1);
            this.edit(um);
        }
    }

    public List<Modulo> getModulosMasUsados(int idfuncionario, int cantidadModulos) {
        TypedQuery<UsoModulo> q = this.em.createQuery("SELECT um FROM UsoModulo um WHERE um.usoModuloPK.funcionario=:fun ORDER BY um.contadorUso DESC", UsoModulo.class);
        q.setMaxResults(cantidadModulos);
        q.setParameter("fun", idfuncionario);
        List<Modulo> lstMod = new LinkedList<>();
        List<UsoModulo> lstUsoM = q.getResultList();
        for (UsoModulo um : lstUsoM) {
            lstMod.add(this.moduloFacade.find(um.getUsoModuloPK().getModulo()));
        }
        return lstMod;
    }

    public List<Modulo> getModulosMasUsadosAutorizados(int idfuncionario, int cantidadModulos) {
        List<Modulo> lstum = this.getModulosMasUsados(idfuncionario, cantidadModulos);
        List<Modulo> lstModulosAuth = new LinkedList<>();
        for (Modulo m : lstum) {
            if (this.moduloFacade.isFuncionarioAutorizado(this.funcionarioFacade.find(idfuncionario), m.getIdmodulo())) {
                lstModulosAuth.add(m);
            }
        }
        return lstModulosAuth;
    }

    public List<UsoModulo> getUsoModulosMasUsados(int idfuncionario, int cantidadModulos) {
        TypedQuery<UsoModulo> q = this.em.createQuery("SELECT um FROM UsoModulo um WHERE um.usoModuloPK.funcionario=:fun ORDER BY um.contadorUso DESC", UsoModulo.class);
        q.setMaxResults(cantidadModulos);
        q.setParameter("fun", idfuncionario);
        return q.getResultList();
    }

}
