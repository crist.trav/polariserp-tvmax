/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "parametro_sistema")
@NamedQueries({
    @NamedQuery(name = "ParametroSistema.findAll", query = "SELECT p FROM ParametroSistema p")
    , @NamedQuery(name = "ParametroSistema.findByClave", query = "SELECT p FROM ParametroSistema p WHERE p.clave = :clave")
    , @NamedQuery(name = "ParametroSistema.findByValor", query = "SELECT p FROM ParametroSistema p WHERE p.valor = :valor")})
public class ParametroSistema implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "clave")
    private Integer clave;
    @Size(max = 255)
    @Column(name = "valor")
    private String valor;

    public ParametroSistema() {
    }

    public ParametroSistema(Integer clave) {
        this.clave = clave;
    }

    public Integer getClave() {
        return clave;
    }

    public void setClave(Integer clave) {
        this.clave = clave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParametroSistema)) {
            return false;
        }
        ParametroSistema other = (ParametroSistema) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.ParametroSistema[ clave=" + clave + " ]";
    }
    
}
