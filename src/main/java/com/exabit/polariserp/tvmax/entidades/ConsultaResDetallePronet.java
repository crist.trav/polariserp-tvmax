/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "consulta_res_detalle_pronet")
@NamedQueries({
    @NamedQuery(name = "ConsultaResDetallePronet.findAll", query = "SELECT c FROM ConsultaResDetallePronet c")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByNroOperacion", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.nroOperacion = :nroOperacion")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByDesOperacion", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.desOperacion = :desOperacion")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByNroCuota", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.nroCuota = :nroCuota")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByCapital", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.capital = :capital")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByInteres", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.interes = :interes")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByMora", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.mora = :mora")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByPunitorio", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.punitorio = :punitorio")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByGastos", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.gastos = :gastos")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByIva10", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.iva10 = :iva10")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByIva5", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.iva5 = :iva5")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByTotalDetalle", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.totalDetalle = :totalDetalle")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByMoneda", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.moneda = :moneda")
    , @NamedQuery(name = "ConsultaResDetallePronet.findByFechaVencimiento", query = "SELECT c FROM ConsultaResDetallePronet c WHERE c.fechaVencimiento = :fechaVencimiento")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsultaResDetallePronet implements Serializable {

    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idconsultaresdetallepronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idconsultaresdetallepronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "nroOperacion")
    private Integer nroOperacion;
    @Size(max = 100)
    @Column(name = "desOperacion")
    private String desOperacion;
    @Column(name = "nroCuota")
    private Integer nroCuota;
    @Size(max = 15)
    @Column(name = "capital")
    private String capital;
    @Size(max = 15)
    @Column(name = "interes")
    private String interes;
    @Size(max = 15)
    @Column(name = "mora")
    private String mora;
    @Size(max = 15)
    @Column(name = "punitorio")
    private String punitorio;
    @Size(max = 15)
    @Column(name = "gastos")
    private String gastos;
    @Size(max = 15)
    @Column(name = "iva10")
    private String iva10;
    @Size(max = 15)
    @Column(name = "iva5")
    private String iva5;
    @Size(max = 15)
    @Column(name = "totalDetalle")
    private String totalDetalle;
    @Size(max = 1)
    @Column(name = "moneda")
    private String moneda;
    @Size(max = 8)
    @Column(name = "fechaVencimiento")
    private String fechaVencimiento;
    @Size(max = 150)
    @Column(name = "direccion_domicilio")
    private String direccionDomicilio;
    @Size(max = 30)
    @Column(name = "codigo_suscripcion")
    private String codigoSuscripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "consultaResDetallePronet")
    @JsonIgnore
    private List<PagoReqPronet> pagoReqPronetList;
    @JoinColumn(name = "consulta_res_pronet", referencedColumnName = "idconsulta_res")
    @ManyToOne(optional = false)
    @JsonIgnore
    private ConsultaResPronet consultaResPronet;
    @JoinTable(name = "cuotas_res_detalle_pronet", joinColumns = {
        @JoinColumn(name = "consulta_res_detalle_pronet_nroOperacion", referencedColumnName = "nroOperacion")}, inverseJoinColumns = {
        @JoinColumn(name = "cuota_idcuota", referencedColumnName = "idcuota")})
    @ManyToMany
    @JsonIgnore
    private List<Cuota> cuotaList;

    public ConsultaResDetallePronet() {
    }

    public ConsultaResDetallePronet(Integer nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public Integer getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(Integer nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public String getDesOperacion() {
        return desOperacion;
    }

    public void setDesOperacion(String desOperacion) {
        this.desOperacion = desOperacion;
    }

    public Integer getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(Integer nroCuota) {
        this.nroCuota = nroCuota;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }

    public String getMora() {
        return mora;
    }

    public void setMora(String mora) {
        this.mora = mora;
    }

    public String getPunitorio() {
        return punitorio;
    }

    public void setPunitorio(String punitorio) {
        this.punitorio = punitorio;
    }

    public String getGastos() {
        return gastos;
    }

    public void setGastos(String gastos) {
        this.gastos = gastos;
    }

    public String getIva10() {
        return iva10;
    }

    public void setIva10(String iva10) {
        this.iva10 = iva10;
    }

    public String getIva5() {
        return iva5;
    }

    public void setIva5(String iva5) {
        this.iva5 = iva5;
    }

    public String getTotalDetalle() {
        return totalDetalle;
    }

    public void setTotalDetalle(String totalDetalle) {
        this.totalDetalle = totalDetalle;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public List<PagoReqPronet> getPagoReqPronetList() {
        return pagoReqPronetList;
    }

    public void setPagoReqPronetList(List<PagoReqPronet> pagoReqPronetList) {
        this.pagoReqPronetList = pagoReqPronetList;
    }

    public ConsultaResPronet getConsultaResPronet() {
        return consultaResPronet;
    }

    public void setConsultaResPronet(ConsultaResPronet consultaResPronet) {
        this.consultaResPronet = consultaResPronet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nroOperacion != null ? nroOperacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultaResDetallePronet)) {
            return false;
        }
        ConsultaResDetallePronet other = (ConsultaResDetallePronet) object;
        if ((this.nroOperacion == null && other.nroOperacion != null) || (this.nroOperacion != null && !this.nroOperacion.equals(other.nroOperacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.ConsultaResDetallePronet[ nroOperacion=" + nroOperacion + " ]";
    }

    public List<Cuota> getCuotaList() {
        return cuotaList;
    }

    public void setCuotaList(List<Cuota> cuotaList) {
        this.cuotaList = cuotaList;
    }

    public String getDireccionDomicilio() {
        return direccionDomicilio;
    }

    public void setDireccionDomicilio(String direccionDomicilio) {
        this.direccionDomicilio = direccionDomicilio;
    }

    public String getCodigoSuscripcion() {
        return codigoSuscripcion;
    }

    public void setCodigoSuscripcion(String codigoSuscripcion) {
        this.codigoSuscripcion = codigoSuscripcion;
    }
    
}
