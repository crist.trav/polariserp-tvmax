package com.exabit.polariserp.tvmax.facturas.util;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author traver
 */
public class GrupoCuotaGenerar {

    private Date vencimiento;
    private List<Cuota> lstCuotas = new ArrayList<>();
    private boolean incluir = false;

    public Date getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(Date vencimiento) {
        this.vencimiento = vencimiento;
    }

    public List<Cuota> getLstCuotas() {
        return lstCuotas;
    }

    public void setLstCuotas(List<Cuota> lstCuotas) {
        this.lstCuotas = lstCuotas;
    }

    public boolean isIncluir() {
        return incluir;
    }

    public void setIncluir(boolean incluir) {
        this.incluir = incluir;
    }
    
    public int getSubtotal(){
        int subtotal=0;
        for(Cuota c:this.lstCuotas){
            subtotal=subtotal+(c.getMontoCuota()-c.getMontoEntrega());
        }
        return subtotal;
    }

}
