$(document).ready(function () {
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    $("#inputAgregarFactura").keypress(function (e) {
        if (e.which === 13) {
            $("#cmlAgregarFactura").click();
            return false;
        }
    });

    $("#inputAgregarFactura").focus();

    $('#calFechaCobro').calendar({
        type: "date",
        monthFirst: false,
        formatter: {
            date: function (date, settings) {
                if (!date)
                    return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        }
    });
});

function setTipoMsg(tipo) {
    $("#msg").removeClass("error");
    $("#msg").removeClass("info");
    $("#msg").removeClass("warning");
    $("#msg").addClass(tipo);
}

function mostrarMsg(tipo, cabecera, cuerpo) {
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);

    setTipoMsg(tipo);
    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function ajaxAgregarFactura(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsg($("#tipoMsg").val());
            if ($("#tipoMsg").val() === "error" || $("#tipoMsg").val() === "warning") {
                if ($("#msg").hasClass("hidden")) {
                    $("#msg").transition("fade");
                }
            } else {
                if (!$("#msg").hasClass("hidden")) {
                    $("#msg").transition("fade");
                }
            }
            $("#in-nrofactagregar").val("");
            break;
        }
    }
}

function ajaxGuardarCobros(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            setTipoMsg($("#tipoMsg").val());
            $("#in-nrofactagregar").val("");
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $('#calFechaCobro').calendar({
                type: "date",
                monthFirst: false,
                formatter: {
                    date: function (date, settings) {
                        if (!date)
                            return '';
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        return day + '/' + month + '/' + year;
                    }
                }
            });
            break;
        }
    }
}

function validar() {
    var validado = true;
    if ($("#in-fechacob").val() === "") {
        validado = false;
        $("#inputFechaCobro").addClass("error");
    } else {
        $("#inputFechaCobro").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
    } else {
        if (!$("#msg").hasClass("hidden")) {
            $("#msg").transition("fade");
        }
    }
    return validado;
}

function ajaxLimpiar(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $('#calFechaCobro').calendar({
                type: "date",
                monthFirst: false,
                formatter: {
                    date: function (date, settings) {
                        if (!date)
                            return '';
                        var day = date.getDate();
                        var month = date.getMonth() + 1;
                        var year = date.getFullYear();
                        return day + '/' + month + '/' + year;
                    }
                }
            });
            break;
        }
    }
}