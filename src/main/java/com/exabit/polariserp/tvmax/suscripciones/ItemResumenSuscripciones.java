/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.suscripciones;

/**
 *
 * @author cristhian-kn
 */
public class ItemResumenSuscripciones {
    
    private Integer nroCuotas;

    public Integer getNroCuotas() {
        return nroCuotas;
    }

    public void setNroCuotas(Integer nroCuotas) {
        this.nroCuotas = nroCuotas;
    }

    private Integer cantSuscripciones;

    public Integer getCantSuscripciones() {
        return cantSuscripciones;
    }

    public void setCantSuscripciones(Integer cantSuscripciones) {
        this.cantSuscripciones = cantSuscripciones;
    }
    
    private Integer monto;

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
