/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Embeddable
public class UsoModuloPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "funcionario")
    private int funcionario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modulo")
    private int modulo;

    public UsoModuloPK() {
    }

    public UsoModuloPK(int funcionario, int modulo) {
        this.funcionario = funcionario;
        this.modulo = modulo;
    }

    public int getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(int funcionario) {
        this.funcionario = funcionario;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) funcionario;
        hash += (int) modulo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoModuloPK)) {
            return false;
        }
        UsoModuloPK other = (UsoModuloPK) object;
        if (this.funcionario != other.funcionario) {
            return false;
        }
        if (this.modulo != other.modulo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.UsoModuloPK[ funcionario=" + funcionario + ", modulo=" + modulo + " ]";
    }
    
}
