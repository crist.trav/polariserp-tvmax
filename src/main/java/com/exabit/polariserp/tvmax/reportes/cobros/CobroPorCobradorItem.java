package com.exabit.polariserp.tvmax.reportes.cobros;

import java.util.Date;

/**
 *
 * @author cristhian-kn
 */
public class CobroPorCobradorItem {
    
    private Integer idsuscripcion;
    private Date fechaven;
    private String cliente;
    private Date fechapago;
    private String factura;
    private Integer montocuota;
    private Integer montopagado;
    private String cobrador;
    private String concepto;
    private String ruc;

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    
    public Integer getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Date getFechaven() {
        return fechaven;
    }

    public void setFechaven(Date fechaven) {
        this.fechaven = fechaven;
    }
    
    public Date getFechapago() {
        return fechapago;
    }

    public void setFechapago(Date fechapago) {
        this.fechapago = fechapago;
    }
    
    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public Integer getMontocuota() {
        return montocuota;
    }

    public void setMontocuota(Integer montocuota) {
        this.montocuota = montocuota;
    }

    public Integer getMontopagado() {
        return montopagado;
    }

    public void setMontopagado(Integer montopagado) {
        this.montopagado = montopagado;
    }

    public String getCobrador() {
        return cobrador;
    }

    public void setCobrador(String cobrador) {
        this.cobrador = cobrador;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    
}
