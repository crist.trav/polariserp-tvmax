package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.PagoReqPronet;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.TerminalImpresionFacade;
import com.exabit.polariserp.tvmax.sesbeans.CobroFacade;
import com.exabit.polariserp.tvmax.util.UtilConversion;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author traver
 */
@Named(value = "newBillManBean")
@SessionScoped
public class NewBillManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(NewBillManBean.class.getName());

    @Inject
    private SessionManBean sesBean;
    
    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private CobroFacade cobroFacade;
    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;
    @EJB
    private TerminalImpresionFacade terminalImpresionFacade;
    @EJB
    private CuotaFacade cuotaFacade;
    @EJB
    private SuscripcionFacade suscripcionFacade;
    @EJB
    private ClienteFacade clienteFacade;
    @EJB
    private ServicioFacade servicioFacade;
    @EJB
    private FacturaFacade facturaFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private Integer idCliente;
    private String strFechaFactura;
    private SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    private Integer idSuscripcionOtrosCobros;
    private List<DetalleFactura> lstDetalleFactura = new ArrayList<>();
    private Integer idTerminalAsignada;
    private Integer idTalonarioSeleccionado;
    private Integer nroFacturaActual;
    private String tipoMsg = "info";
    private String cabeceraMsg = "x";
    private String cuerpoMsg = "y";
    private boolean facturaNueva = true;
    private Factura factura;
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public boolean isFacturaNueva() {
        return facturaNueva;
    }

    public void setFacturaNueva(boolean facturaNueva) {
        this.facturaNueva = facturaNueva;
    }

    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public Integer getNroFacturaActual() {
        return nroFacturaActual;
    }

    public void setNroFacturaActual(Integer nroFacturaActual) {
        this.nroFacturaActual = nroFacturaActual;
    }

    public Integer getIdTalonarioSeleccionado() {
        return idTalonarioSeleccionado;
    }

    public void setIdTalonarioSeleccionado(Integer idTalonarioSeleccionado) {
        this.idTalonarioSeleccionado = idTalonarioSeleccionado;
    }

    public Integer getIdTerminalAsignada() {
        return idTerminalAsignada;
    }

    public void setIdTerminalAsignada(Integer idTerminalAsignada) {
        this.idTerminalAsignada = idTerminalAsignada;
    }

    public List<DetalleFactura> getLstDetalleFactura() {
        return lstDetalleFactura;
    }

    public Integer getIdSuscripcionOtrosCobros() {
        return idSuscripcionOtrosCobros;
    }

    public void setIdSuscripcionOtrosCobros(Integer idSuscripcionOtrosCobros) {
        this.idSuscripcionOtrosCobros = idSuscripcionOtrosCobros;
    }

    public String getStrFechaFactura() {
        return strFechaFactura;
    }

    public void setStrFechaFactura(String strFechaFactura) {
        this.strFechaFactura = strFechaFactura;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
    //</editor-fold>

    /**
     * Creates a new instance of NewBillManBean
     */
    public NewBillManBean() {
        this.strFechaFactura = this.sdf.format(new Date());
    }

    public void seleccionarCliente() {
        System.out.println("Seleccion cliente " + this.idCliente);
        this.lstDetalleFactura.clear();
    }

    public List<Suscripcion> getSuscripcionesConectadas() {
        System.out.println("Id cliente: " + this.idCliente);
        if (this.idCliente != null) {
            return this.suscripcionFacade.getSuscripcionesConServicio(this.clienteFacade.find(this.idCliente));
        } else {
            return new ArrayList<>();
        }
    }

    public List<Suscripcion> getSuscripciones() {
        System.out.println("Id cliente: " + this.idCliente);
        if (this.idCliente != null) {
            return this.suscripcionFacade.getSuscripcionesOrdenDESCid(this.clienteFacade.find(this.idCliente));
        } else {
            return new ArrayList<>();
        }
    }

    public List<Suscripcion> getSuscripcionesConServicio() {
        System.out.println("Get suscripciones con servicio new bill: " + this.idCliente);
        if (this.idCliente != null) {
            return this.suscripcionFacade.getSuscripcionesConServicio(this.clienteFacade.find(this.idCliente));
        } else {
            return new ArrayList<>();
        }
    }

    public List<Servicio> getServiciosNoSuscribibles() {
        return this.servicioFacade.getServiciosNoSuscribibles();
    }

    public void agregarCuota(Integer idcuota) {
        Cuota cuota = this.cuotaFacade.find(idcuota);
        DetalleFactura df = new DetalleFactura();
        df.setCuota(cuota);
        df.setCantidad(1);
//        df.setMonto(cuota.getMontoCuota() - cuota.getMontoEntrega());
        df.setMonto(cuota.getMontoCuota());
        if (cuota.getServicio() != null) {
            df.setServicio(cuota.getServicio());
        } else {
            df.setServicio(cuota.getSuscripcion().getServicio());
        }
        df.setSuscripcion(cuota.getSuscripcion());
        df.setSubtotal(df.getCantidad() * df.getMonto());
        df.setIva10(0);
        df.setIva5(0);
        double impuesto = df.getServicio().getPorcentajeIva().doubleValue() / 100.0;
        double factorMulti = 1.0 + impuesto;
        int iva = (int) Math.round((df.getMonto() * impuesto) / factorMulti);
        if (df.getServicio().getPorcentajeIva().equals((short) 10)) {
            df.setIva10(iva);
        } else if (df.getServicio().getPorcentajeIva().equals((short) 5)) {
            df.setIva5(iva);
        }
        UtilConversion utilc = new UtilConversion();
        df.setDescripcion("(" + df.getSuscripcion().getIdsuscripcion() + ") " + df.getSuscripcion().getServicio().getNombre() + " |");
        if (!df.getServicio().getIdservicio().equals(df.getSuscripcion().getServicio().getIdservicio())) {
            df.setDescripcion(df.getDescripcion() + " " + df.getServicio().getNombre() + " cuota " + df.getCuota().getNroCuota() + "/" + df.getCuota().getHistorialGeneracion().getCantidadCuotas());
        } else {
            df.setDescripcion(df.getDescripcion() + " cuota " + utilc.getNombreMesCorto((int) df.getCuota().getMesCuota()) + "/" + df.getCuota().getAnioCuota());
        }
        df.setDescripcion(df.getDescripcion().toUpperCase());
        this.lstDetalleFactura.add(df);
    }

    public List<Cuota> getCuotasPendientes(Integer idsusc) {
        if (idsusc != null) {
            List<Cuota> lstPrevia = new ArrayList<>();
            List<Cuota> lstFinal = new ArrayList<>();
            lstPrevia.addAll(this.cuotaFacade.getCuotasPendYPagadosPronetPrinc(this.suscripcionFacade.find(idsusc)));
//            lstPrevia.addAll(this.cuotaFacade.getCuotasPendientesPrincASCFechaVenc(this.suscripcionFacade.find(idsusc)));
            lstPrevia.addAll(this.cuotaFacade.getCuotasPendSecundYPagSinFactASCFechaVenc(this.suscripcionFacade.find(idsusc)));
//            lstPrevia.addAll(this.cuotaFacade.getCuotasPendientesSecundASCFechaVenc(this.suscripcionFacade.find(idsusc)));
            for (Cuota c : lstPrevia) {
                boolean existe = false;
                for (DetalleFactura df : this.lstDetalleFactura) {
                    if (df.getCuota() != null) {
                        if (df.getCuota().getIdcuota().equals(c.getIdcuota())) {
                            existe = true;
                        }
                    }
                }
                if (!existe) {
                    lstFinal.add(c);
                }
            }
            return lstFinal;
        } else {
            return new ArrayList<>();
        }
    }

    public boolean yaEnDetalle(Integer idcuota) {
        boolean ya = false;
        for (DetalleFactura df : this.lstDetalleFactura) {
            if (df.getCuota().getIdcuota().equals(idcuota)) {
                ya = true;
                break;
            }
        }
        return ya;
    }

    public void quitarDetalleFactura(int posicion) {
        System.out.println("Quitar:" + posicion);
        this.lstDetalleFactura.remove(posicion);
    }

    public int getTotalFactura() {
        int total = 0;
        for (DetalleFactura df : this.lstDetalleFactura) {
            total = total + df.getSubtotal();
        }
        return total;
    }

    public void agregarOtrosCobros(Integer idservicio) {
        System.out.println("Ide servicio: " + idservicio + " " + this.idSuscripcionOtrosCobros);
        Servicio serv = this.servicioFacade.find(idservicio);
        Suscripcion susc = this.suscripcionFacade.find(this.idSuscripcionOtrosCobros);
        boolean existe = false;
        for (DetalleFactura detf : this.lstDetalleFactura) {
            if (detf.getCuota() == null && detf.getServicio().getIdservicio().equals(idservicio) && detf.getSuscripcion().getIdsuscripcion().equals(this.idSuscripcionOtrosCobros)) {
                detf.setCantidad(detf.getCantidad() + 1);
                detf.setSubtotal(detf.getMonto() * detf.getCantidad());
                detf.setIva10(0);
                detf.setIva5(0);
                double impuesto = detf.getServicio().getPorcentajeIva().doubleValue() / 100.0;
                double factorMulti = 1.0 + impuesto;
                int iva = (int) Math.round((detf.getSubtotal() * impuesto) / factorMulti);
                if (detf.getServicio().getPorcentajeIva().equals((short) 10)) {
                    detf.setIva10(iva);
                } else if (detf.getServicio().getPorcentajeIva().equals((short) 5)) {
                    detf.setIva5(iva);
                }
                existe = true;
                break;
            }
        }
        if (!existe) {
            DetalleFactura df = new DetalleFactura();
            df.setMonto(serv.getPrecio());
            df.setSuscripcion(susc);

            df.setServicio(serv);
            df.setCantidad(1);
            df.setDescripcion(serv.getNombre());
            df.setSubtotal(df.getCantidad() * df.getMonto());
            df.setIva10(0);
            df.setIva5(0);
            double impuesto = df.getServicio().getPorcentajeIva().doubleValue() / 100.0;
            double factorMulti = 1.0 + impuesto;
            int iva = (int) Math.round((df.getSubtotal() * impuesto) / factorMulti);
            if (df.getServicio().getPorcentajeIva().equals((short) 10)) {
                df.setIva10(iva);
            } else if (df.getServicio().getPorcentajeIva().equals((short) 5)) {
                df.setIva5(iva);
            }
            df.setDescripcion("(" + df.getSuscripcion().getIdsuscripcion() + ") " + df.getSuscripcion().getServicio().getNombre() + " | " + df.getServicio().getNombre());
            df.setDescripcion(df.getDescripcion().toUpperCase());
            this.lstDetalleFactura.add(df);
        }
    }

    public void actualizarTalonario() {
        System.out.println("Terminal: " + this.idTerminalAsignada);
        if (this.idTerminalAsignada != null) {
            for (TalonarioFactura tal : this.talonarioFacturaFacade.getTalonariosDisponibles()) {
                if (tal.getTerminalImpresion() != null) {
                    if (tal.getTerminalImpresion().getIdterminalImpresion().equals(this.idTerminalAsignada)) {
                        this.idTalonarioSeleccionado = tal.getIdtalonario();
                        this.nroFacturaActual = tal.getNroActual();
                        break;
                    }
                }
            }
        }
    }

    public List<TalonarioFactura> getLstTalonariosActivos() {
        return this.talonarioFacturaFacade.getTalonariosDisponibles();
    }

    public void guardarFactura() {
        if (this.lstDetalleFactura.isEmpty()) {
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error de validación";
            this.cuerpoMsg = "Ningún detalle";
        } else {
            try {
                Factura f = new Factura();
                f.setAnulado(false);
                f.setCancelado(true);
                f.setCliente(this.clienteFacade.find(this.idCliente));
                f.setContado(true);
                f.setFecha(this.sdf.parse(this.strFechaFactura));
                f.setFechaHoraRegistro(new Date());
                f.setTotal(0);
                f.setIva10(0);
                f.setIva5(0);
                for (DetalleFactura df : this.lstDetalleFactura) {
                    f.setIva10(f.getIva10() + df.getIva10());
                    f.setIva5(f.getIva5() + df.getIva5());
                    f.setTotal(f.getTotal() + df.getSubtotal());
                    df.setFactura(f);
                }
                f.setNroFactura(this.nroFacturaActual);
                f.setTalonarioFactura(this.talonarioFacturaFacade.find(this.idTalonarioSeleccionado));
                f.setDetalleFacturaList(this.lstDetalleFactura);
                f.setFuncionarioRegistro(this.sesBean.getFuncionarioVar());
                this.factura = this.facturaFacade.registrarFactura(f);
                this.facturaNueva = false;
                this.tipoMsg = "info";
                this.cabeceraMsg = "Éxito";
                this.cuerpoMsg = "Factura guardada correctamente.";
            } catch (Exception ex) {
                this.tipoMsg = "error";
                this.cabeceraMsg = "Error al guardar la factura";
                this.cuerpoMsg = ex.getMessage();
                LOG.log(Level.SEVERE, "Error al guardar factura", ex);
            }
        }
        System.out.println("Guardar factura");
    }

    public void actualizarNumeracionTalonario() {
        if (this.idTalonarioSeleccionado != null) {
            TalonarioFactura tf = this.talonarioFacturaFacade.find(this.idTalonarioSeleccionado);
            this.nroFacturaActual = tf.getNroActual();
        }
    }

    public void nuevaFactura() {
        this.facturaNueva = true;
        this.lstDetalleFactura.clear();
        this.idCliente = null;
        if (this.idTalonarioSeleccionado != null) {
            TalonarioFactura tf = this.talonarioFacturaFacade.find(this.idTalonarioSeleccionado);
            this.nroFacturaActual = tf.getNroActual();
        }
        this.factura = null;
        this.idSuscripcionOtrosCobros = null;
    }

    public void generarPDF() {
        List<Factura> lstf = new ArrayList<>();
        lstf.add(this.factura);
        this.generarPDFPendSeleccionado(lstf);
    }

    public void generarPDFPendSeleccionado(List<Factura> lstFacturas) {
        if (!lstFacturas.isEmpty()) {
            List<JasperPrint> lstJP = UtilReportes.generarJPListFacturas(lstFacturas);
            try {
                HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.addHeader("content-disposition", "inline; filename=facturas.pdf");
                ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
                JRPdfExporter exporter = new JRPdfExporter();
                SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
                configExport.setCreatingBatchModeBookmarks(true);
                exporter.setConfiguration(configExport);
                exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
                SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
                exporter.setExporterOutput(out);
                exporter.exportReport();
            } catch (IOException | JRException ex) {
                LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
            }
        } else {
            System.out.println("Lista de facturas vacia");
        }
    }

    public boolean existeFacturaPendiente(Integer idCuota) {
        return this.facturaFacade.existeFacturaPendiente(idCuota);
    }

    public String getNroFacturaPendiente(Integer idCuota) {
        return this.facturaFacade.getNroFacturaPendiente(idCuota);
    }

    public String getDatosPago(Integer idcuota) {
        System.out.println("Idcuota datospago: " + idcuota);
        String datospago = "";
        Cuota cuo = this.cuotaFacade.find(idcuota);
        Cobro cob=this.cobroFacade.getCobroPorCuota(cuo, false);
        PagoReqPronet pagoPronet = null;
        if(cob!=null){
            if(!cob.getPagoReqPronetList().isEmpty()){
                pagoPronet=cob.getPagoReqPronetList().get(0);
            }
        }
        if (pagoPronet != null) {
            datospago = "AQUIPAGO - Cod. Transacción: " + pagoPronet.getCodTransaccion() + " | Nro. Operación: " + pagoPronet.getConsultaResDetallePronet().getNroOperacion();
        }
        return datospago;
    }
}
