package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "anulacion_req_pronet")
@NamedQueries({
    @NamedQuery(name = "AnulacionReqPronet.findAll", query = "SELECT a FROM AnulacionReqPronet a")
    , @NamedQuery(name = "AnulacionReqPronet.findByIdanulacionReq", query = "SELECT a FROM AnulacionReqPronet a WHERE a.idanulacionReq = :idanulacionReq")
    , @NamedQuery(name = "AnulacionReqPronet.findByCodServicio", query = "SELECT a FROM AnulacionReqPronet a WHERE a.codServicio = :codServicio")
    , @NamedQuery(name = "AnulacionReqPronet.findByTipoTrx", query = "SELECT a FROM AnulacionReqPronet a WHERE a.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "AnulacionReqPronet.findByUsuario", query = "SELECT a FROM AnulacionReqPronet a WHERE a.usuario = :usuario")
    , @NamedQuery(name = "AnulacionReqPronet.findByPassword", query = "SELECT a FROM AnulacionReqPronet a WHERE a.password = :password")
    , @NamedQuery(name = "AnulacionReqPronet.findByNroOperacion", query = "SELECT a FROM AnulacionReqPronet a WHERE a.nroOperacion = :nroOperacion")
    , @NamedQuery(name = "AnulacionReqPronet.findByNroCuota", query = "SELECT a FROM AnulacionReqPronet a WHERE a.nroCuota = :nroCuota")
    , @NamedQuery(name = "AnulacionReqPronet.findByImporte", query = "SELECT a FROM AnulacionReqPronet a WHERE a.importe = :importe")
    , @NamedQuery(name = "AnulacionReqPronet.findByMoneda", query = "SELECT a FROM AnulacionReqPronet a WHERE a.moneda = :moneda")
    , @NamedQuery(name = "AnulacionReqPronet.findByCodTransaccion", query = "SELECT a FROM AnulacionReqPronet a WHERE a.codTransaccion = :codTransaccion")
    , @NamedQuery(name = "AnulacionReqPronet.findByCodTransaccionAnular", query = "SELECT a FROM AnulacionReqPronet a WHERE a.codTransaccionAnular = :codTransaccionAnular")})
@JsonInclude(Include.NON_NULL)
public class AnulacionReqPronet implements Serializable {

    @Column(name = "fechaHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idanulacionreqpronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idanulacionreqpronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idanulacion_req")
    @JsonIgnore
    private Integer idanulacionReq;
    @Size(max = 5)
    @Column(name = "codServicio")
    private String codServicio;
    @Column(name = "tipoTrx")
    private Integer tipoTrx;
    @Size(max = 20)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 70)
    @Column(name = "password")
    private String password;
    @Column(name = "nroOperacion")
    private Integer nroOperacion;
    @Column(name = "nroCuota")
    private Integer nroCuota;
    @Size(max = 15)
    @Column(name = "importe")
    private String importe;
    @Size(max = 1)
    @Column(name = "moneda")
    private String moneda;
    @Column(name = "codTransaccion")
    private Long codTransaccion;
    @Column(name = "codTransaccionAnular")
    private Long codTransaccionAnular;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anulacionReqPronet")
    @JsonIgnore
    private List<AnulacionResPronet> anulacionResPronetList;
    @JoinColumn(name = "pago_req_pronet", referencedColumnName = "idpago_req")
    @ManyToOne(optional = false)
    @JsonIgnore
    private PagoReqPronet pagoReqPronet;

    public AnulacionReqPronet() {
    }

    public AnulacionReqPronet(Integer idanulacionReq) {
        this.idanulacionReq = idanulacionReq;
    }

    public Integer getIdanulacionReq() {
        return idanulacionReq;
    }

    public void setIdanulacionReq(Integer idanulacionReq) {
        this.idanulacionReq = idanulacionReq;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(Integer nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public Integer getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(Integer nroCuota) {
        this.nroCuota = nroCuota;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }


    public List<AnulacionResPronet> getAnulacionResPronetList() {
        return anulacionResPronetList;
    }

    public void setAnulacionResPronetList(List<AnulacionResPronet> anulacionResPronetList) {
        this.anulacionResPronetList = anulacionResPronetList;
    }

    public PagoReqPronet getPagoReqPronet() {
        return pagoReqPronet;
    }

    public void setPagoReqPronet(PagoReqPronet pagoReqPronet) {
        this.pagoReqPronet = pagoReqPronet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idanulacionReq != null ? idanulacionReq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnulacionReqPronet)) {
            return false;
        }
        AnulacionReqPronet other = (AnulacionReqPronet) object;
        if ((this.idanulacionReq == null && other.idanulacionReq != null) || (this.idanulacionReq != null && !this.idanulacionReq.equals(other.idanulacionReq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.AnulacionReqPronet[ idanulacionReq=" + idanulacionReq + " ]";
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Long getCodTransaccion() {
        return codTransaccion;
    }

    public void setCodTransaccion(Long codTransaccion) {
        this.codTransaccion = codTransaccion;
    }

    public Long getCodTransaccionAnular() {
        return codTransaccionAnular;
    }

    public void setCodTransaccionAnular(Long codTransaccionAnular) {
        this.codTransaccionAnular = codTransaccionAnular;
    }
    
}
