package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.AsignacionFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "generarFacturasManBean")
@SessionScoped
public class GenerarFacturasManBean implements Serializable {

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @Inject
    private SessionManBean sesManBean;
    
    @EJB
    private AsignacionFacturaFacade asignacionFacturaFacade;

    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    private String agregarPor = "suscripcion";

    public String getAgregarPor() {
        return agregarPor;
    }

    public void setAgregarPor(String agregarPor) {
        this.agregarPor = agregarPor;
    }

    private Integer idCobradorSeleccionado;

    public Integer getIdCobradorSeleccionado() {
        return idCobradorSeleccionado;
    }

    public void setIdCobradorSeleccionado(Integer idCobradorSeleccionado) {
        this.idCobradorSeleccionado = idCobradorSeleccionado;
    }

    private Integer idTalonarioSeleccionado;

    public Integer getIdTalonarioSeleccionado() {
        return idTalonarioSeleccionado;
    }

    public void setIdTalonarioSeleccionado(Integer idTalonarioSeleccionado) {
        this.idTalonarioSeleccionado = idTalonarioSeleccionado;
    }

    private boolean yaGenerados = false;

    public boolean isYaGenerados() {
        return yaGenerados;
    }

    public void setYaGenerados(boolean yaGenerados) {
        this.yaGenerados = yaGenerados;
    }

    private List<Factura> lstFacturasGeneradas = new LinkedList<>();

    public List<Factura> getLstFacturasGeneradas() {
        return lstFacturasGeneradas;
    }

    public void setLstFacturasGeneradas(List<Factura> lstFacturasGeneradas) {
        this.lstFacturasGeneradas = lstFacturasGeneradas;
    }

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private final Map<Integer, List<Cuota>> hmCuotasGenerar = new HashMap<>();
    private final Map<Integer, List<Cuota>> hmCuotasExcluidas = new HashMap<>();

    private Integer cantCuotasIncluir = 2;

    public Integer getCantCuotasIncluir() {
        return cantCuotasIncluir;
    }

    public void setCantCuotasIncluir(Integer cantCuotasIncluir) {
        this.cantCuotasIncluir = cantCuotasIncluir;
    }

    @EJB
    private FacturaFacade facturaFacade;

    private static final Logger LOG = Logger.getLogger(GenerarFacturasManBean.class.getName());

    @EJB
    private CuotaFacade cuotaFacade;

    private Integer idCliSeleccionado;

    public Integer getIdCliSeleccionado() {
        return idCliSeleccionado;
    }

    public void setIdCliSeleccionado(Integer idCliSeleccionado) {
        this.idCliSeleccionado = idCliSeleccionado;
    }

    private Suscripcion susSeleccionada;

    public Suscripcion getSusSeleccionada() {
        return susSeleccionada;
    }

    public void setSusSeleccionada(Suscripcion susSeleccionada) {
        this.susSeleccionada = susSeleccionada;
    }

    @EJB
    private ClienteFacade clienteFacade;

//    private Map<Integer, Suscripcion> hmSuscripcionesGenerar = new HashMap<>();
//
//    public Map<Integer, Suscripcion> getHmSuscripcionesGenerar() {
//        return hmSuscripcionesGenerar;
//    }
//
//    public void setHmSuscripcionesGenerar(Map<Integer, Suscripcion> hmSuscripcionesGenerar) {
//        this.hmSuscripcionesGenerar = hmSuscripcionesGenerar;
//    }
    private List<Suscripcion> lstSucripcionesGenerar = new LinkedList<>();

    public List<Suscripcion> getLstSucripcionesGenerar() {
        return lstSucripcionesGenerar;
    }

    public void setLstSucripcionesGenerar(List<Suscripcion> lstSucripcionesGenerar) {
        this.lstSucripcionesGenerar = lstSucripcionesGenerar;
    }

    private Integer idSuscripcionSeleccionada;

    public Integer getIdSuscripcionSeleccionada() {
        return idSuscripcionSeleccionada;
    }

    public void setIdSuscripcionSeleccionada(Integer idSuscripcionSeleccionada) {
        this.idSuscripcionSeleccionada = idSuscripcionSeleccionada;
    }

    private String fechaFact;

    public String getFechaFact() {
        return fechaFact;
    }

    public void setFechaFact(String fechaFact) {
        this.fechaFact = fechaFact;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");

    /**
     * Creates a new instance of GenerarFacturasManBean
     */
    public GenerarFacturasManBean() {
        this.fechaFact = sdf.format(new Date());
    }

    @PostConstruct
    private void postConstruct() {

        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();
        if (requestCookieMap.get("polariserp-idterminal") != null) {
            Cookie cid = (Cookie) requestCookieMap.get("polariserp-idterminal");
            TalonarioFactura t = this.talonarioFacturaFacade.getTalonarioPorIdTerminal(Integer.parseInt(cid.getValue()));
            if (t != null) {
                this.idTalonarioSeleccionado = t.getIdtalonario();
            } else {
                this.idTalonarioSeleccionado = talonarioFacturaFacade.getTalonarioActivo().getIdtalonario();
            }
        } else {
            System.out.println("Galleta null en nueva factura :(");
        }
    }

    private void agregarPorSuscripcion(Suscripcion s) {
            boolean existe = false;
            for (Suscripcion su : this.lstSucripcionesGenerar) {
                if (su.getIdsuscripcion().equals(s.getIdsuscripcion())) {
                    existe = true;
                }
            }
            if (!existe) {
                this.lstSucripcionesGenerar.add(0, s);
                List<Cuota> lstc = this.cuotaFacade.getCuotasPendientesASCFechaVenc(s);
                for (Cuota c : lstc) {
                    Factura fc = this.facturaFacade.getFacturaPendientePorCuota(c.getIdcuota());
                    if (fc != null) {
                        if (this.hmCuotasExcluidas.get(s.getIdsuscripcion()) == null) {
                            this.hmCuotasExcluidas.put(s.getIdsuscripcion(), new ArrayList<Cuota>());
                        }
                        this.hmCuotasExcluidas.get(s.getIdsuscripcion()).add(c);
                    } else {
                        if (this.hmCuotasGenerar.get(s.getIdsuscripcion()) == null) {
                            this.hmCuotasGenerar.put(s.getIdsuscripcion(), new ArrayList<Cuota>());
                            this.hmCuotasGenerar.get(s.getIdsuscripcion()).add(c);
                        } else {
                            if (this.hmCuotasGenerar.get(s.getIdsuscripcion()).size() < this.cantCuotasIncluir) {
                                this.hmCuotasGenerar.get(s.getIdsuscripcion()).add(c);
                            } else {
                                if (this.hmCuotasExcluidas.get(s.getIdsuscripcion()) == null) {
                                    this.hmCuotasExcluidas.put(s.getIdsuscripcion(), new ArrayList<Cuota>());
                                }
                                this.hmCuotasExcluidas.get(s.getIdsuscripcion()).add(c);
                            }
                        }

                    }
                }
            }
    }

    public void agregarSuscripcion() {
        if(this.agregarPor.equals("suscripcion")){
            if (this.idSuscripcionSeleccionada != null) {
                Suscripcion s = this.suscripcionFacade.find(this.idSuscripcionSeleccionada);
                this.agregarPorSuscripcion(s);
            }
            
        }else{
            if(this.idCobradorSeleccionado!=null){
                Funcionario cobrador=this.funcionarioFacade.find(this.idCobradorSeleccionado);
                List<Suscripcion> lstSuscAgregar=this.suscripcionFacade.getSuscripcionesActivasPorCobrador(cobrador);
                System.out.println("Suscripciones a agregar: "+lstSuscAgregar.size());
                for(Suscripcion s:lstSuscAgregar){
                    this.agregarPorSuscripcion(s);
                }
            }
        }
    }

    public void seleccionarSuscripcion() {

        if (this.idCliSeleccionado != null) {
            this.susSeleccionada = this.suscripcionFacade.find(this.idCliSeleccionado);

        } else {
            this.susSeleccionada = null;
        }
    }

    public List<Cuota> getCuotasPendientes() {

        List<Cuota> lstc = new LinkedList<>();
        if (this.susSeleccionada != null && this.fechaFact != null) {
            try {
                Calendar f = new GregorianCalendar();
                f.setTime(this.sdf.parse(this.fechaFact));

//                lstc.addAll(this.cuotaFacade.getCuotasPendientes(this.susSeleccionada, f.get(Calendar.YEAR), f.get(Calendar.MONTH) + 1));
                lstc.addAll(this.cuotaFacade.getCuotasPendientes(this.susSeleccionada));
            } catch (ParseException ex) {
                LOG.log(Level.SEVERE, "Error al convertir fecha de string", ex);
            }
        } else {
            System.out.println("cli o fecha null" + this.susSeleccionada + ", " + this.fechaFact);
        }
        System.out.println("cantidad de cuotas pendientes " + lstc.size());

        return lstc;
    }

    public void quitarSuscripcionLista(Integer idcli) {
//        this.hmSuscripcionesGenerar.remove(idcli);
//        this.lstSucripcionesGenerar.clear();
//        this.lstSucripcionesGenerar.addAll(this.hmSuscripcionesGenerar.values());
        this.hmCuotasExcluidas.remove(idcli);
        this.hmCuotasGenerar.remove(idcli);
        this.susSeleccionada = null;
        this.idCliSeleccionado = null;
        for (Suscripcion s : this.lstSucripcionesGenerar) {
            if (s.getIdsuscripcion().equals(idcli)) {
                this.lstSucripcionesGenerar.remove(s);
                break;
            }
        }

    }

    public List<Factura> getFacturasPendientes() {
        List<Factura> lstf = new LinkedList<>();
        if (this.susSeleccionada != null && this.fechaFact != null) {
            try {
                Calendar f = new GregorianCalendar();
                f.setTime(this.sdf.parse(this.fechaFact));

                lstf.addAll(this.facturaFacade.getFacturasPendientes(this.susSeleccionada, f.get(Calendar.YEAR), f.get(Calendar.MONTH) + 1));
            } catch (ParseException ex) {
                LOG.log(Level.SEVERE, "Error al convertir fecha de string", ex);
            }
        } else {
            System.out.println("cli o fecha null" + this.susSeleccionada + ", " + this.fechaFact);
        }
        System.out.println("cantidad de cuotas pendientes " + lstf.size());
        return lstf;
    }

    public void generarFacturasDlgCli() {
        Integer totalgen = 0;
        try {
//            if (this.cliSeleccionado != null) {
            Date f = this.sdf.parse(fechaFact);
            System.out.println("Cantidad de suscripciones a generarL " + this.lstSucripcionesGenerar.size());
            for (Suscripcion c : this.lstSucripcionesGenerar) {
                if (this.hmCuotasGenerar.get(c.getIdsuscripcion()) != null) {
                    Map<String, Object> hmResumen = this.facturaFacade.generarFacturasCuotas(this.talonarioFacturaFacade.find(this.idTalonarioSeleccionado), f, this.hmCuotasGenerar.get(c.getIdsuscripcion()), this.sesManBean.getFuncionarioVar());
                    this.lstFacturasGeneradas.addAll((Collection<? extends Factura>) hmResumen.get("lstfacturasgen"));
                    totalgen = totalgen + (Integer) hmResumen.get("cantgen");
                    this.msg = "Generadas exitosamente " + totalgen + " facturas";
                    this.yaGenerados = true;
                } else {
                    System.out.println("Lista cuotas null");
                }
            }
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "error al convertir a date de string", ex);
        }
        System.out.println("Total generado: " + totalgen);
    }

    public boolean isFacturasGeneradasEmpty() {
        return this.lstFacturasGeneradas.isEmpty();
    }

    public void generarPDFPendSeleccionado() {
//        String rutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
        HashMap<Integer, String> hmCobrador = new HashMap<>();
        for (Factura f : this.lstFacturasGeneradas) {
            Funcionario c = this.asignacionFacturaFacade.getCobradorAsinado(f);
            String cobra = c.getNombres();
            if (c.getApellidos() != null) {
                cobra = cobra + " " + c.getApellidos();
            }
            hmCobrador.put(f.getIdfactura(), cobra);
        }
        List<JasperPrint> lstJP = UtilReportes.generarJPListFacturas(this.lstFacturasGeneradas, hmCobrador);
        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=facturas.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
        }
    }

    public void reiniciarCampos() {
        System.out.println("Reiniciar campos");
        this.idCliSeleccionado = null;
        this.susSeleccionada = null;
        this.lstSucripcionesGenerar.clear();
        this.lstFacturasGeneradas.clear();
        this.yaGenerados = false;
        this.msg = "";
        this.hmCuotasGenerar.clear();
        this.hmCuotasExcluidas.clear();
    }

    public int getTotalCuotas() {

        int tc = 0;
        for (Suscripcion s : this.lstSucripcionesGenerar) {
            List<Cuota> lstc = this.hmCuotasGenerar.get(s.getIdsuscripcion());
            if (lstc != null) {
                tc = tc + lstc.size();
            }
        }
        return tc;
//        try {
//            Calendar f = new GregorianCalendar();
//            f.setTime(this.sdf.parse(this.fechaFact));
//            for (Suscripcion s : this.lstSucripcionesGenerar) {
//                List<Cuota> lstCuoPend = this.cuotaFacade.getCuotasPendientes(s, f.get(Calendar.YEAR), f.get(Calendar.MONTH) + 1);
//                tc = tc + lstCuoPend.size();
//            }
//        } catch (ParseException ex) {
//            LOG.log(Level.SEVERE, "Error al ", ex);
//        }
//        System.out.println("total cuotas llamado: " + tc);
//        return tc;
    }

    public int getTotalSuscripciones() {
        System.out.println("Total suscripciones llamado: " + this.lstSucripcionesGenerar.size());
        return this.lstSucripcionesGenerar.size();
    }

    public List<TalonarioFactura> getTalonariosDisponibles() {
        return this.talonarioFacturaFacade.getTalonariosDisponibles();
    }

    private Integer idCuotaEliminar;

    public Integer getIdCuotaEliminar() {
        return idCuotaEliminar;
    }

    public void setIdCuotaEliminar(Integer idCuotaEliminar) {
        this.idCuotaEliminar = idCuotaEliminar;
    }

    public void eliminarCuota() {
        Cuota c = this.cuotaFacade.find(this.idCuotaEliminar);
        this.cuotaFacade.remove(c);
    }

    public List<Cuota> getLstCuotasGenerar() {
        if (this.susSeleccionada != null) {
            List<Cuota> lstc = this.hmCuotasGenerar.get(this.susSeleccionada.getIdsuscripcion());
            if (lstc != null) {
                return lstc;
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    public Integer getCantidadCuotasGenerar(Integer idsuscripcion) {
        List<Cuota> lstcuo = this.hmCuotasGenerar.get(idsuscripcion);
        if (lstcuo == null) {
            return 0;
        } else {
            return lstcuo.size();
        }
    }

    public Integer getCantidadCuotasPendientes(Integer idsuscripcion) {
        return this.cuotaFacade.getCuotasPendientesPrincipal(this.suscripcionFacade.find(idsuscripcion)).size();
    }

    public List<Cuota> getLstCuotasExcluidas() {
        if (this.susSeleccionada != null) {
            List<Cuota> lstc = this.hmCuotasExcluidas.get(this.susSeleccionada.getIdsuscripcion());
            if (lstc != null) {
                return lstc;
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    public String getNroFacturaPendiente(Integer idf) {
        return this.facturaFacade.getNroFacturaPendiente(idf);
    }

    public boolean existeFacturaPendiente(Integer idcuota) {
        return this.facturaFacade.existeFacturaPendiente(idcuota);
    }

    public void excluirCuota(Integer idsus, Integer idcuo) {
        List<Cuota> lstc = this.hmCuotasGenerar.get(idsus);
        for (Cuota c : lstc) {
            if (c.getIdcuota().equals(idcuo)) {
                if (hmCuotasExcluidas.get(idsus) == null) {
                    this.hmCuotasExcluidas.put(idsus, new LinkedList<Cuota>());
                }
                this.hmCuotasExcluidas.get(idsus).add(c);
                this.hmCuotasGenerar.get(idsus).remove(c);
                break;
            }
        }
    }

    public void incluirCuota(Integer idsus, Integer idcuo) {
        List<Cuota> lstc = this.hmCuotasExcluidas.get(idsus);
        for (Cuota c : lstc) {
            if (c.getIdcuota().equals(idcuo)) {
                this.hmCuotasGenerar.get(idsus).add(c);
                this.hmCuotasExcluidas.get(idsus).remove(c);
                break;
            }
        }
    }

}
