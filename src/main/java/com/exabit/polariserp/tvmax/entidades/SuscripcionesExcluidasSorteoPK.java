/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Embeddable
public class SuscripcionesExcluidasSorteoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "suscripcion_idsuscripcion")
    private int suscripcionIdsuscripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sorteo_idsorteo")
    private int sorteoIdsorteo;

    public SuscripcionesExcluidasSorteoPK() {
    }

    public SuscripcionesExcluidasSorteoPK(int suscripcionIdsuscripcion, int sorteoIdsorteo) {
        this.suscripcionIdsuscripcion = suscripcionIdsuscripcion;
        this.sorteoIdsorteo = sorteoIdsorteo;
    }

    public int getSuscripcionIdsuscripcion() {
        return suscripcionIdsuscripcion;
    }

    public void setSuscripcionIdsuscripcion(int suscripcionIdsuscripcion) {
        this.suscripcionIdsuscripcion = suscripcionIdsuscripcion;
    }

    public int getSorteoIdsorteo() {
        return sorteoIdsorteo;
    }

    public void setSorteoIdsorteo(int sorteoIdsorteo) {
        this.sorteoIdsorteo = sorteoIdsorteo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) suscripcionIdsuscripcion;
        hash += (int) sorteoIdsorteo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuscripcionesExcluidasSorteoPK)) {
            return false;
        }
        SuscripcionesExcluidasSorteoPK other = (SuscripcionesExcluidasSorteoPK) object;
        if (this.suscripcionIdsuscripcion != other.suscripcionIdsuscripcion) {
            return false;
        }
        if (this.sorteoIdsorteo != other.sorteoIdsorteo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.SuscripcionesExcluidasSorteoPK[ suscripcionIdsuscripcion=" + suscripcionIdsuscripcion + ", sorteoIdsorteo=" + sorteoIdsorteo + " ]";
    }
    
}
