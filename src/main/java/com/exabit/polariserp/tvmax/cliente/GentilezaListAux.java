/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.cliente;

/**
 *
 * @author frioss
 */
public class GentilezaListAux {

    public GentilezaListAux(Integer id, String razonSocial, String fechaSuscripcion, String direcciona, String barrio, boolean estado, String telefono) {
        this.id = id;
        this.razonSocial = razonSocial;
//        this.ci = ci;
        this.fechaSuscripcion = fechaSuscripcion;
        this.direcciona = direcciona;
        this.barrio = barrio;
        this.estado = estado;
        this.telefono = telefono;
    }

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    private String razonSocial;

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    private Integer ci;

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    private String fechaSuscripcion;

    public String getFechaSuscripcion() {
        return fechaSuscripcion;
    }

    public void setFechaSuscripcion(String fechaSuscripcion) {
        this.fechaSuscripcion = fechaSuscripcion;
    }

    private String direcciona;

    public String getDirecciona() {
        return direcciona;
    }

    public void setDirecciona(String direcciona) {
        this.direcciona = direcciona;
    }

    private String barrio;

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    private boolean estado;

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    private String telefono;

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
