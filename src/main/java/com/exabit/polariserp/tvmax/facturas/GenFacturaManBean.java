package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.facturas.util.ClienteGenerar;
import com.exabit.polariserp.tvmax.facturas.util.GrupoCuotaGenerar;
import com.exabit.polariserp.tvmax.facturas.util.SuscripcionGenerar;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author traver
 */
@Named(value = "genFacturaManBean")
@SessionScoped
public class GenFacturaManBean implements Serializable {
    
    private static final Logger LOG = Logger.getLogger(GenFacturaManBean.class.getName());

    @Inject
    private SessionManBean sesManBean;

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;
    @EJB
    private ClienteFacade clienteFacade;
    @EJB
    private SuscripcionFacade suscripcionFacade;
    @EJB
    private CuotaFacade cuotaFacade;
    @EJB
    private FacturaFacade facturaFacade;
    @EJB
    private FuncionarioFacade funcionarioFacade;
    //</editor-fold>

    private SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    private String strFechaFactura;
    private Integer cantCuotasDef;
    private Integer idTalonarioSeleccionado;
    private Integer idClienteAgregar;
    private final List<ClienteGenerar> lstClientesGenerar = new LinkedList<>();
    private boolean agrePorCliente = true;
    private Integer idCobradorAgregar;
    private Integer idClienteVer;
    private Integer idSuscripcionVer;
    private String cabeceraMsg = "Lorem ipsum";
    private String tipoMsg = "info";
    private String cuerpoMsg = "Dolor sit amet, consectetur adipiscing elit.";
    private final List<Factura> lstFacturasGeneradas = new ArrayList<>();
    private Integer maxItemsPorFactura = 4;

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public Integer getMaxItemsPorFactura() {
        return maxItemsPorFactura;
    }

    public void setMaxItemsPorFactura(Integer maxItemsPorFactura) {
        this.maxItemsPorFactura = maxItemsPorFactura;
    }
    
    public List<Factura> getLstFacturasGeneradas() {
        return lstFacturasGeneradas;
    }

    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public Integer getIdSuscripcionVer() {
        return idSuscripcionVer;
    }

    public void setIdSuscripcionVer(Integer idSuscripcionVer) {
        this.idSuscripcionVer = idSuscripcionVer;
    }

    public Integer getIdClienteVer() {
        return idClienteVer;
    }

    public void setIdClienteVer(Integer idClienteVer) {
        this.idClienteVer = idClienteVer;
    }

    public Integer getIdCobradorAgregar() {
        return idCobradorAgregar;
    }

    public void setIdCobradorAgregar(Integer idCobradorAgregar) {
        this.idCobradorAgregar = idCobradorAgregar;
    }

    public boolean isAgrePorCliente() {
        return agrePorCliente;
    }

    public void setAgrePorCliente(boolean agrePorCliente) {
        this.agrePorCliente = agrePorCliente;
    }

    public List<ClienteGenerar> getLstClientesGenerar() {
        return lstClientesGenerar;
    }

//    public void setLstClientesGenerar(List<ClienteGenerar> lstClientesGenerar) {
//        this.lstClientesGenerar = lstClientesGenerar;
//    }

    public Integer getIdClienteAgregar() {
        return idClienteAgregar;
    }

    public void setIdClienteAgregar(Integer idClienteAgregar) {
        this.idClienteAgregar = idClienteAgregar;
    }

    public Integer getIdTalonarioSeleccionado() {
        return idTalonarioSeleccionado;
    }

    public void setIdTalonarioSeleccionado(Integer idTalonarioSeleccionado) {
        this.idTalonarioSeleccionado = idTalonarioSeleccionado;
    }

    public String getStrFechaFactura() {
        return strFechaFactura;
    }

    public void setStrFechaFactura(String strFechaFactura) {
        this.strFechaFactura = strFechaFactura;
    }

    public Integer getCantCuotasDef() {
        return cantCuotasDef;
    }

    public void setCantCuotasDef(Integer cantCuotasDef) {
        this.cantCuotasDef = cantCuotasDef;
    }
    //</editor-fold>

    /**
     * Creates a new instance of GenFacturaManBean
     */
    public GenFacturaManBean() {
        this.cantCuotasDef = 2;
        this.strFechaFactura = this.sdf.format(new Date());
    }

    public List<TalonarioFactura> getTalonariosDisponibles() {
        return this.talonarioFacturaFacade.getTalonariosDisponibles();
    }

    public void agregarCliCobra() {
        System.out.println("fecha: " + this.strFechaFactura + " nrocuo" + this.cantCuotasDef + " idtalon" + this.idTalonarioSeleccionado + " idcli" + this.idClienteAgregar + " agregar cli " + this.agrePorCliente + " idcobra" + this.idCobradorAgregar);
        if (this.agrePorCliente) {
            if (this.idClienteAgregar != null) {
                this.agregarCliente(this.idClienteAgregar);
            }
        } else {
            if (this.idCobradorAgregar != null) {
                Funcionario cob=this.funcionarioFacade.find(this.idCobradorAgregar);
                for(Cliente cli:this.clienteFacade.getClientesPorCobrador(cob)){
                    this.agregarCliente(cli.getIdcliente());
                }
            }
        }
    }

    public void agregarCliente(Integer idcliente) {
        boolean existe = false;
        for (ClienteGenerar cligen : this.lstClientesGenerar) {
            if (cligen.getCliente().getIdcliente().equals(idcliente)) {
                existe = true;
                break;
            }
        }
        if (!existe) {
            ClienteGenerar cg = new ClienteGenerar(this.maxItemsPorFactura);
            cg.setCliente(this.clienteFacade.find(idcliente));
            List<Suscripcion> lstsusc = this.suscripcionFacade.getSuscripcionesConServicio(cg.getCliente());
            for (Suscripcion s : lstsusc) {
                SuscripcionGenerar sg = new SuscripcionGenerar();
                sg.setSuscripcion(s);
                cg.getLstSuscripciones().add(sg);
                List<Cuota> lstCuotasPrinc = this.cuotaFacade.getCuotasPendientesASCFechaVenc(s);
                int cInc = 0;
                for (Cuota c : lstCuotasPrinc) {
                    GrupoCuotaGenerar gcg = new GrupoCuotaGenerar();
                    gcg.setVencimiento(c.getFechaVencimiento());
                    gcg.getLstCuotas().add(c);

                    Calendar calMesLimite = new GregorianCalendar();
                    calMesLimite.set(Calendar.DAY_OF_MONTH, 1);
                    calMesLimite.add(Calendar.MONTH, 1);
                    calMesLimite.add(Calendar.DAY_OF_MONTH, -1);

                    Calendar cVenc = new GregorianCalendar();
                    cVenc.setTime(c.getFechaVencimiento());
                    if(this.cantCuotasDef==null){
                        this.cantCuotasDef=2;
                    }
                    if (cInc < this.cantCuotasDef && cVenc.before(calMesLimite)) {
                        gcg.setIncluir(true);
                    } else {
                        gcg.setIncluir(false);
                    }
                    sg.getLstGruposCuotas().add(gcg);
                    cInc++;
                }

                List<Cuota> lstCuotasSec = this.cuotaFacade.getCuotasPendientesSecundASCFechaVenc(s);
                System.out.println("Total cuotas secundarias: " + lstCuotasSec.size());
                List<Servicio> lstServiciosSec = new ArrayList<>();
                for (Cuota c : lstCuotasSec) {
                    boolean servEnc = false;
                    for (Servicio ser : lstServiciosSec) {
                        if (c.getServicio().getIdservicio().equals(ser.getIdservicio())) {
                            servEnc = true;
                        }
                    }
                    if (!servEnc) {
                        System.out.println("Servicio no encontrado, agregando " + c.getServicio().getNombre());
                        lstServiciosSec.add(c.getServicio());
                    }
                }
                System.out.println("Otros servicios: " + lstServiciosSec.size());
                for (Servicio ser : lstServiciosSec) {
                    int cantidadGruposActual = sg.getLstGruposCuotas().size();
                    System.out.println("Cantidad de grupos actual: " + cantidadGruposActual);
                    int contadorGrupos = 0;
                    List<Cuota> lstCuoServ = this.cuotaFacade.getCuotasPendientesSecundASCFechaVenc(s, ser);
                    System.out.println("Cantidad cuotas servicio: " + ser.getNombre() + " " + lstCuoServ.size());
                    for (Cuota c : lstCuoServ) {
                        if (contadorGrupos < cantidadGruposActual) {
                            sg.getLstGruposCuotas().get(contadorGrupos).getLstCuotas().add(c);
                        } else {
                            GrupoCuotaGenerar grcg = new GrupoCuotaGenerar();

                            grcg.setVencimiento(c.getFechaVencimiento());
                            grcg.getLstCuotas().add(c);

                            Calendar calMesLimite = new GregorianCalendar();
                            calMesLimite.set(Calendar.DAY_OF_MONTH, 1);
                            calMesLimite.add(Calendar.MONTH, 1);
                            calMesLimite.add(Calendar.DAY_OF_MONTH, -1);

                            Calendar cVenc = new GregorianCalendar();
                            cVenc.setTime(c.getFechaVencimiento());

                            if (contadorGrupos < this.cantCuotasDef && cVenc.before(calMesLimite)) {
                                grcg.setIncluir(true);
                            } else {
                                grcg.setIncluir(false);
                            }
                            sg.getLstGruposCuotas().add(grcg);
                        }
                        contadorGrupos++;
                    }
                }
            }
            this.lstClientesGenerar.add(0, cg);
        }
    }

    public List<SuscripcionGenerar> getLstSuscripcionesCliente() {
        List<SuscripcionGenerar> lstsg = new ArrayList<>();
        if (this.idClienteVer != null) {
            for (ClienteGenerar cg : this.lstClientesGenerar) {
                if (cg.getCliente().getIdcliente().equals(this.idClienteVer)) {
                    lstsg.addAll(cg.getLstSuscripciones());
                    break;
                }
            }
        }
        return lstsg;
    }

    public List<GrupoCuotaGenerar> getListCuotasGenerar() {
        List<GrupoCuotaGenerar> lstcuo = new ArrayList<>();
        if (this.idSuscripcionVer != null) {
            for (ClienteGenerar cg : this.lstClientesGenerar) {
                for (SuscripcionGenerar sg : cg.getLstSuscripciones()) {
                    if (sg.getSuscripcion().getIdsuscripcion().equals(this.idSuscripcionVer)) {
                        lstcuo.addAll(sg.getLstGruposCuotas());
                        break;
                    }
                }
            }
        }
        return lstcuo;
    }

    public void quitarCliente(Integer idcliente) {
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            if (cg.getCliente().getIdcliente().equals(idcliente)) {
                this.lstClientesGenerar.remove(cg);
                break;
            }
        }
    }

    public void limpiarTablaClientes() {
        this.lstClientesGenerar.clear();
    }

    public int getTotalCobrarCliente(Integer idcliente) {
        int total = 0;
        ClienteGenerar cg = null;
        for (ClienteGenerar clg : this.lstClientesGenerar) {
            if (clg.getCliente().getIdcliente().equals(idcliente)) {
                cg = clg;
                break;
            }
        }
        if (cg != null) {
            for (SuscripcionGenerar sg : cg.getLstSuscripciones()) {
                for (GrupoCuotaGenerar gcg : sg.getLstGruposCuotas()) {
                    if (gcg.isIncluir()) {
                        total = total + gcg.getSubtotal();
                    }
                }
            }
        }
        return total;
    }

    public void incluirExcluirGrupo(int posgrupo) {
        System.out.println("Posicion cuota cambiar: " + posgrupo);
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            if (cg.getCliente().getIdcliente().equals(this.idClienteVer)) {
                for (SuscripcionGenerar sg : cg.getLstSuscripciones()) {
                    if (sg.getSuscripcion().getIdsuscripcion().equals(this.idSuscripcionVer)) {
                        for (GrupoCuotaGenerar cug : sg.getLstGruposCuotas()) {
                            if (sg.getLstGruposCuotas().indexOf(cug) == posgrupo) {
                                cug.setIncluir(!cug.isIncluir());
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean existenCuotasSecundarias(Integer idcliente) {
        boolean existe = false;
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            if (cg.getCliente().getIdcliente().equals(idcliente)) {
                existe = cg.existenCuotasSecundarias();
                break;
            }
        }
        return existe;
    }

    public int getTotalSuscripiones() {
        int total = 0;
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            total = total + cg.getLstSuscripciones().size();
        }
        return total;
    }

    public int getTotalFacturas() {
        int total = 0;
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            total = total + cg.getGruposCuotasGenerar().size();
        }
        return total;
    }

    public void generarFacturas() {
        System.out.println("A generar se ha dicho");
        List<List<Cuota>> lstGruposCuotasGenerar = new ArrayList<>();
        for (ClienteGenerar cg : this.lstClientesGenerar) {
            lstGruposCuotasGenerar.addAll(cg.getGruposCuotasGenerar());
        }
        System.out.println("Lista grupos cuotas generar: " + lstGruposCuotasGenerar.size());
        Date fechaFactura = new Date();
        try {
            fechaFactura = this.sdf.parse(this.strFechaFactura);
        } catch (Exception ex) {
            System.out.println("Error al convertir str a Date: " + ex.getMessage());
        }
        try {
            this.lstFacturasGeneradas.addAll(this.facturaFacade.generarFacturas(lstGruposCuotasGenerar, fechaFactura, this.talonarioFacturaFacade.find(this.idTalonarioSeleccionado), this.sesManBean.getFuncionarioVar()));
            System.out.println("Cuotas generadas: " + this.lstFacturasGeneradas.size());
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            DecimalFormat df=new DecimalFormat("#,###,###,###");
            this.cuerpoMsg = "Se generaron "+df.format(this.lstFacturasGeneradas.size())+" facturas.";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al generar facturas", ex);
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error";
            this.cuerpoMsg = "Error al generar facturas: " + ex.getMessage();
        }
    }

    public void vistaPreviaImpresion() {
        this.generarPDFPendSeleccionado(lstFacturasGeneradas);
    }

    public void generarPDFPendSeleccionado(List<Factura> lstFacturas) {
        if (!lstFacturas.isEmpty()) {
            List<JasperPrint> lstJP = UtilReportes.generarJPListFacturas(lstFacturas);
            try {
                HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                httpServletResponse.addHeader("content-disposition", "inline; filename=facturas.pdf");
                ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
                JRPdfExporter exporter = new JRPdfExporter();
                SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
                configExport.setCreatingBatchModeBookmarks(true);
                exporter.setConfiguration(configExport);
                exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
                SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
                exporter.setExporterOutput(out);
                exporter.exportReport();
            } catch (IOException | JRException ex) {
                LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
            }
        } else {
            System.out.println("Lista de facturas vacia");
        }
    }

    public void limpiar() {
        this.lstClientesGenerar.clear();
        this.lstFacturasGeneradas.clear();
        this.idClienteAgregar = null;
        this.cantCuotasDef = 2;
        this.strFechaFactura = this.sdf.format(new Date());
        this.idCobradorAgregar = null;
    }

}
