/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "medio_pago")
@NamedQueries({
    @NamedQuery(name = "MedioPago.findAll", query = "SELECT m FROM MedioPago m")
    , @NamedQuery(name = "MedioPago.findByIdmedioPago", query = "SELECT m FROM MedioPago m WHERE m.idmedioPago = :idmedioPago")
    , @NamedQuery(name = "MedioPago.findByNombre", query = "SELECT m FROM MedioPago m WHERE m.nombre = :nombre")})
public class MedioPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmedio_pago")
    private Integer idmedioPago;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "medioPago")
    private List<Cobro> cobroList;

    public MedioPago() {
    }

    public MedioPago(Integer idmedioPago) {
        this.idmedioPago = idmedioPago;
    }

    public Integer getIdmedioPago() {
        return idmedioPago;
    }

    public void setIdmedioPago(Integer idmedioPago) {
        this.idmedioPago = idmedioPago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Cobro> getCobroList() {
        return cobroList;
    }

    public void setCobroList(List<Cobro> cobroList) {
        this.cobroList = cobroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmedioPago != null ? idmedioPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedioPago)) {
            return false;
        }
        MedioPago other = (MedioPago) object;
        if ((this.idmedioPago == null && other.idmedioPago != null) || (this.idmedioPago != null && !this.idmedioPago.equals(other.idmedioPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.MedioPago[ idmedioPago=" + idmedioPago + " ]";
    }
    
}
