/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "historial_generacion_cuotas")
@NamedQueries({
    @NamedQuery(name = "HistorialGeneracionCuotas.findAll", query = "SELECT h FROM HistorialGeneracionCuotas h")
    , @NamedQuery(name = "HistorialGeneracionCuotas.findByIdhistorialGeneracionCuotas", query = "SELECT h FROM HistorialGeneracionCuotas h WHERE h.idhistorialGeneracionCuotas = :idhistorialGeneracionCuotas")
    , @NamedQuery(name = "HistorialGeneracionCuotas.findByCantidadCuotas", query = "SELECT h FROM HistorialGeneracionCuotas h WHERE h.cantidadCuotas = :cantidadCuotas")
    , @NamedQuery(name = "HistorialGeneracionCuotas.findByFechaHora", query = "SELECT h FROM HistorialGeneracionCuotas h WHERE h.fechaHora = :fechaHora")})
public class HistorialGeneracionCuotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idhistorialgeneracioncuotas", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idhistorialgeneracioncuotas", table = "idsequences", allocationSize = 1, initialValue = 1937)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idhistorial_generacion_cuotas")
    private Integer idhistorialGeneracionCuotas;
    @Column(name = "cantidad_cuotas")
    private Integer cantidadCuotas;
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(mappedBy = "historialGeneracion")
    private List<Cuota> cuotaList;

    public HistorialGeneracionCuotas() {
    }

    public HistorialGeneracionCuotas(Integer idhistorialGeneracionCuotas) {
        this.idhistorialGeneracionCuotas = idhistorialGeneracionCuotas;
    }

    public Integer getIdhistorialGeneracionCuotas() {
        return idhistorialGeneracionCuotas;
    }

    public void setIdhistorialGeneracionCuotas(Integer idhistorialGeneracionCuotas) {
        this.idhistorialGeneracionCuotas = idhistorialGeneracionCuotas;
    }

    public Integer getCantidadCuotas() {
        return cantidadCuotas;
    }

    public void setCantidadCuotas(Integer cantidadCuotas) {
        this.cantidadCuotas = cantidadCuotas;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public List<Cuota> getCuotaList() {
        return cuotaList;
    }

    public void setCuotaList(List<Cuota> cuotaList) {
        this.cuotaList = cuotaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idhistorialGeneracionCuotas != null ? idhistorialGeneracionCuotas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialGeneracionCuotas)) {
            return false;
        }
        HistorialGeneracionCuotas other = (HistorialGeneracionCuotas) object;
        if ((this.idhistorialGeneracionCuotas == null && other.idhistorialGeneracionCuotas != null) || (this.idhistorialGeneracionCuotas != null && !this.idhistorialGeneracionCuotas.equals(other.idhistorialGeneracionCuotas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.HistorialGeneracionCuotas[ idhistorialGeneracionCuotas=" + idhistorialGeneracionCuotas + " ]";
    }
    
}
