/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.conceptomovimiento;

import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.sesbeans.ConceptoFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "conceptoMovimientoManBean")
@ViewScoped
public class ConceptoMovimientoManBean implements Serializable {

    @EJB
    private ConceptoFacade conceptoFacade;
    private Concepto conceptoVar = new Concepto();

    public Concepto getConceptoVar() {
        return conceptoVar;
    }

    public void setConceptoVar(Concepto conceptoVar) {
        this.conceptoVar = conceptoVar;
    }
    

    /**
     * Creates a new instance of ConceptoMovimientoManBean
     */
    public ConceptoMovimientoManBean() {
    }
    
    
    public List<Concepto> getLstConceptos(){
        return this.conceptoFacade.findAll();
    }
    
    public void registrarConcepto(){
        conceptoFacade.create(conceptoVar);
    }
}
