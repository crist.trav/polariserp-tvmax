/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.parametrossistema;

import com.exabit.polariserp.tvmax.entidades.ParametroSistema;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author frioss
 */
@Named(value = "ajustesManBean")
@ViewScoped
public class AjustesManBean implements Serializable {
    
    private Future futuro;
    
    @Resource
    ManagedScheduledExecutorService executor;
    
    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;
    
    private final List<ParametroSistema> listaParametroOfSistema=new ArrayList<>();
    private String msg;


//    private ParametroSistema parSisRutaReportes = new ParametroSistema(EntidadesEstaticas.PAR_RUTA_REPORTES);
    private ParametroSistema parServicioGenCuotaActivo=new ParametroSistema(EntidadesEstaticas.PAR_SERVICIO_GENERACION_CUOTA_ACTIVO);
    private ParametroSistema parServicioAnulFacturaActivo=new ParametroSistema(EntidadesEstaticas.PAR_SERVICIO_ANULACION_FACTURA_ACTIVO);

    public ParametroSistema getParServicioGenCuotaActivo() {
        return parServicioGenCuotaActivo;
    }

    public void setParServicioGenCuotaActivo(ParametroSistema parServicioGenCuotaActivo) {
        this.parServicioGenCuotaActivo = parServicioGenCuotaActivo;
    }
    

//    public ParametroSistema getParSisRutaReportes() {
//        return parSisRutaReportes;
//    }
//
//    public void setParSisRutaReportes(ParametroSistema parSisRutaReportes) {
//        this.parSisRutaReportes = parSisRutaReportes;
//    }
    
        private boolean servicioGenActivo;

    public boolean isServicioGenActivo() {
        return servicioGenActivo;
    }

    public void setServicioGenActivo(boolean servicioGenActivo) {
        this.servicioGenActivo = servicioGenActivo;
    }

    private boolean servicioAnulFactActivo;

    public boolean isServicioAnulFactActivo() {
        return servicioAnulFactActivo;
    }

    public void setServicioAnulFactActivo(boolean servicioAnulFactActivo) {
        this.servicioAnulFactActivo = servicioAnulFactActivo;
    }
    
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public AjustesManBean() {
//        this.listaParametroOfSistema.add(parSisRutaReportes);
        this.listaParametroOfSistema.add(parServicioGenCuotaActivo);
        this.listaParametroOfSistema.add(parServicioAnulFacturaActivo);
    }
    
    @PostConstruct
    private void postConstruct(){
        for(ParametroSistema ps:this.listaParametroOfSistema){
            String valor=this.parametroSistemaFacade.getParametro(ps.getClave());
            if(ps.getClave().equals(EntidadesEstaticas.PAR_SERVICIO_GENERACION_CUOTA_ACTIVO)){
                this.servicioGenActivo=Boolean.parseBoolean(valor);
                System.out.println("Servicio generacion cuota activo: "+this.servicioGenActivo);
            }
            if(ps.getClave().equals(EntidadesEstaticas.PAR_SERVICIO_ANULACION_FACTURA_ACTIVO)){
                this.servicioAnulFactActivo=Boolean.parseBoolean(valor);
                System.out.println("Servicio anulacion facturas activo: "+this.servicioGenActivo);
            }
            if(valor!=null){
                ps.setValor(valor);
            }
        }
    }
    
    public void registrarPrametrodelSistema(){
        for(ParametroSistema p:this.listaParametroOfSistema){
            if(p.getClave().equals(EntidadesEstaticas.PAR_SERVICIO_GENERACION_CUOTA_ACTIVO)){
                p.setValor(this.servicioGenActivo+"");
            }
            if(p.getClave().equals(EntidadesEstaticas.PAR_SERVICIO_ANULACION_FACTURA_ACTIVO)){
                p.setValor(this.servicioAnulFactActivo+"");
            }
            ParametroSistema pf=this.parametroSistemaFacade.find(p.getClave());
            if(pf==null){
                this.parametroSistemaFacade.create(p);
            }else{
                pf.setValor(p.getValor());
                System.out.println("id: "+p.getClave()+" valor dep: "+p.getValor());
                this.parametroSistemaFacade.edit(pf);
            }
        }
        msg = "Parametros del sistema actualizados correctamente";
    }
    
}
