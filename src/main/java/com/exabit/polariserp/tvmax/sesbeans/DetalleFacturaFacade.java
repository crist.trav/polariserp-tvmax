/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class DetalleFacturaFacade extends AbstractFacade<DetalleFactura> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleFacturaFacade() {
        super(DetalleFactura.class);
    }
    
    
    public DetalleFactura getDetallePorCuota(Cuota c){
        TypedQuery<DetalleFactura> q=this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota=:cuota AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cuota", c);
        List<DetalleFactura> lstdf=q.getResultList();
        if(!lstdf.isEmpty()){
            return lstdf.get(0);
        }else{
            return null;
        }        
    }
    public boolean existeDetallePorCuotaPagadosDistinto(Cuota c, DetalleFactura detf){
        TypedQuery<DetalleFactura> q=this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df!=:deta AND df.cuota=:cuota AND df.factura.cancelado=true AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cuota", c);
        q.setParameter("deta", detf);
        List<DetalleFactura> lstdf=q.getResultList();
        return !lstdf.isEmpty();
    }
    
    
    public List<DetalleFactura> getDetallesPorFactura(Factura f){
        TypedQuery<DetalleFactura> q=this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.factura=:fact", DetalleFactura.class);
        q.setParameter("fact", f);
        List<DetalleFactura> lstDf=q.getResultList();
        return lstDf;
    }
    

}
