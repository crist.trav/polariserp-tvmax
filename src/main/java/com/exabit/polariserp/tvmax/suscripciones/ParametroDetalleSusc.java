/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.suscripciones;

import javax.inject.Named;
import javax.enterprise.context.ConversationScoped;
import java.io.Serializable;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "parametroDetalleSusc")
@ConversationScoped
public class ParametroDetalleSusc implements Serializable {
    
    private Integer idsuscripcion;

    public Integer getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }


    /**
     * Creates a new instance of parametroDetalleSusc
     */
    public ParametroDetalleSusc() {
    }
    
}
