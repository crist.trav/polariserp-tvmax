package com.exabit.polariserp.tvmax.facturas;

import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.reportes.facturas.FacturaReporteItem;
import com.exabit.polariserp.tvmax.sesbeans.AsignacionFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.DetalleFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "facturasManBean")
@ViewScoped
public class FacturasManBean implements Serializable {

    @EJB
    private AsignacionFacturaFacade asignacionFacturaFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;

    private static final Logger LOG = Logger.getLogger(FacturasManBean.class.getName());

    private String fechaFiltroDesde;

    public String getFechaFiltroDesde() {
        return fechaFiltroDesde;
    }

    public void setFechaFiltroDesde(String fechaFiltroDesde) {
        this.fechaFiltroDesde = fechaFiltroDesde;
    }

    private String fechaFiltroHasta;

    public String getFechaFiltroHasta() {
        return fechaFiltroHasta;
    }

    public void setFechaFiltroHasta(String fechaFiltroHasta) {
        this.fechaFiltroHasta = fechaFiltroHasta;
    }

    private Integer idClienteFiltro;

    public Integer getIdClienteFiltro() {
        return idClienteFiltro;
    }

    public void setIdClienteFiltro(Integer idClienteFiltro) {
        this.idClienteFiltro = idClienteFiltro;
    }

    private boolean mostrarPendientes = true;

    public boolean isMostrarPendientes() {
        return mostrarPendientes;
    }

    public void setMostrarPendientes(boolean mostrarPendientes) {
        this.mostrarPendientes = mostrarPendientes;
    }

    private boolean mostrarAnulados;

    public boolean isMostrarAnulados() {
        return mostrarAnulados;
    }

    public void setMostrarAnulados(boolean mostrarAnulados) {
        this.mostrarAnulados = mostrarAnulados;
    }

    private boolean mostrarPagados;

    public boolean isMostrarPagados() {
        return mostrarPagados;
    }

    public void setMostrarPagados(boolean mostrarPagados) {
        this.mostrarPagados = mostrarPagados;
    }

    private List<Factura> lstFacturasImprimir = new LinkedList<>();

    public List<Factura> getLstFacturasImprimir() {
        return lstFacturasImprimir;
    }

    public void setLstFacturasImprimir(List<Factura> lstFacturasImprimir) {
        this.lstFacturasImprimir = lstFacturasImprimir;
    }

    private Factura facturaSeleccionada;

    public Factura getFacturaSeleccionada() {
        return facturaSeleccionada;
    }

    public void setFacturaSeleccionada(Factura facturaSeleccionada) {
        this.facturaSeleccionada = facturaSeleccionada;
    }

    private String textoBusqueda = "";

    public String getTextoBusqueda() {
        return textoBusqueda;
    }

    public void setTextoBusqueda(String textoBusqueda) {
        this.textoBusqueda = textoBusqueda;
    }

    @EJB
    private DetalleFacturaFacade detalleFacturaFacade;

    private String jsonFacturasSeleccionadas;

    public String getJsonFacturasSeleccionadas() {
        return jsonFacturasSeleccionadas;
    }

    public void setJsonFacturasSeleccionadas(String jsonFacturasSeleccionadas) {
        this.jsonFacturasSeleccionadas = jsonFacturasSeleccionadas;
    }

    private List<Factura> lstFacturasSeleccionadas = new LinkedList<>();

    public List<Factura> getLstFacturasSeleccionadas() {
        return lstFacturasSeleccionadas;
    }

    public void setLstFacturasSeleccionadas(List<Factura> lstFacturasSeleccionadas) {
        this.lstFacturasSeleccionadas = lstFacturasSeleccionadas;
    }

    @EJB
    private CuotaFacade cuotaFacade;

    private List<String> lstMensajesGen = new LinkedList<>();

    public List<String> getLstMensajesGen() {
        return lstMensajesGen;
    }

    public void setLstMensajesGen(List<String> lstMensajesGen) {
        this.lstMensajesGen = lstMensajesGen;
    }

    private int tipoMsgGenerar = 1;

    public int getTipoMsgGenerar() {
        return tipoMsgGenerar;
    }

    public void setTipoMsgGenerar(int tipoMsgGenerar) {
        this.tipoMsgGenerar = tipoMsgGenerar;
    }

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private FacturaFacade facturaFacade;

    private String msgGenFactura;

    public String getMsgGenFactura() {
        return msgGenFactura;
    }

    public void setMsgGenFactura(String msgGenFactura) {
        this.msgGenFactura = msgGenFactura;
    }

    private Integer idCobradorGen;

    public Integer getIdCobradorGen() {
        return idCobradorGen;
    }

    public void setIdCobradorGen(Integer idCobradorGen) {
        this.idCobradorGen = idCobradorGen;
    }

    private Integer mesGen;

    public Integer getMesGen() {
        return mesGen;
    }

    public void setMesGen(Integer mesGen) {
        this.mesGen = mesGen;
    }

    private Integer anioGen;

    public Integer getAnioGen() {
        return anioGen;
    }

    public void setAnioGen(Integer anioGen) {
        this.anioGen = anioGen;
    }

    private List<DetalleFactura> lstDetalleFactSelec = new LinkedList<>();

    public List<DetalleFactura> getLstDetalleFactSelec() {
        return lstDetalleFactSelec;
    }

    public void setLstDetalleFactSelec(List<DetalleFactura> lstDetalleFactSelec) {
        this.lstDetalleFactSelec = lstDetalleFactSelec;
    }

    private List<Factura> lstFacturas = new LinkedList<>();

    public List<Factura> getLstFacturas() {
        return lstFacturas;
    }

    public void setLstFacturas(List<Factura> lstFacturas) {
        this.lstFacturas = lstFacturas;
    }

    SimpleDateFormat sdfcal = new SimpleDateFormat("MMMM d, yyyy");

    private Paginador paginador;

    public Paginador getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador paginador) {
        this.paginador = paginador;
    }

    /**
     * Creates a new instance of FacturasManBean
     */
    public FacturasManBean() {
        Calendar c = new GregorianCalendar();
        this.mesGen = c.get(Calendar.MONTH) + 1;
        this.anioGen = c.get(Calendar.YEAR);

        Calendar ahora = new GregorianCalendar();
        Calendar desde = new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        Calendar hasta = new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        hasta.add(Calendar.MONTH, 1);
        hasta.add(Calendar.DAY_OF_MONTH, -1);

        this.fechaFiltroDesde = sdfcal.format(new Date(desde.getTimeInMillis()));
        this.fechaFiltroHasta = sdfcal.format(new Date(hasta.getTimeInMillis()));
        this.paginador = new Paginador(this.lstFacturas);
    }

    @PostConstruct
    private void postConstruct() {
        try {
            this.paginador.agregarItemsLista(this.facturaFacade.getFacturasFiltro(idClienteFiltro, mostrarPendientes, mostrarPagados, mostrarAnulados, this.sdfcal.parse(this.fechaFiltroDesde), this.sdfcal.parse(this.fechaFiltroHasta), ""));
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al realizar obtener las facturas en postconstruct", ex);
        }
    }

    public List<Funcionario> getLstCobrador() {
        return this.funcionarioFacade.getCobradores();
    }

//    public void generarFacturas() {
//        this.lstMensajesGen.clear();
//        this.lstFacturasImprimir.clear();
//        if (this.cuotaFacade.existenCuotasPeriodo(mesGen, anioGen)) {
//            Map<String, Object> hmResumen = this.facturaFacade.generarFacturasCuotas(mesGen, anioGen, this.funcionarioFacade.find(this.idCobradorGen));
//            this.lstFacturasImprimir.addAll((List<Factura>) hmResumen.get("lstfacturasgen"));
//            this.msgGenFactura = "Generado exitosamente";
//            this.lstMensajesGen.add("Facturas Generadas: " + hmResumen.get("cantgen"));
//            this.lstMensajesGen.add("Facturas Anuladas: " + hmResumen.get("cantanul"));
//            this.tipoMsgGenerar = 1;
//            this.lstFacturas.clear();
//            try{
//            Date desde = this.sdfcal.parse(this.fechaFiltroDesde);
//            Date hasta = this.sdfcal.parse(this.fechaFiltroHasta);
//            this.lstFacturas.addAll(this.facturaFacade.getFacturasFiltro(idClienteFiltro, mostrarPendientes, mostrarPagados, mostrarAnulados, desde, hasta));
//            }catch(ParseException ex){
//                System.out.println("Error al convetir fecha: "+ex);
//            }
//        } else {
//            this.msgGenFactura = "No existen cuotas registradas en el periodo seleccionado.";
//            this.lstMensajesGen.add("Facturas Generadas: 0");
//            this.lstMensajesGen.add("Facturas Anuladas: 0");
//            this.tipoMsgGenerar = 3;
//        }
//    }
    public void seleccionFactura() {
        System.out.println(this.jsonFacturasSeleccionadas + "JSON seleccion factura: ");
        this.lstDetalleFactSelec.clear();
        this.lstFacturasSeleccionadas.clear();
        if (this.jsonFacturasSeleccionadas != null) {
            Gson gson = new Gson();
            Integer[] arrayIdFactura = gson.fromJson(this.jsonFacturasSeleccionadas, Integer[].class);
            for (Integer idf : arrayIdFactura) {
                this.lstFacturasSeleccionadas.add(this.facturaFacade.find(idf));
            }
        }
        if (this.lstFacturasSeleccionadas.size() == 1) {
            System.out.println("Se carga detalles");
            this.facturaSeleccionada = this.lstFacturasSeleccionadas.get(0);
            this.lstDetalleFactSelec.addAll(this.detalleFacturaFacade.getDetallesPorFactura(this.lstFacturasSeleccionadas.get(0)));
        } else {
            this.facturaSeleccionada = null;
        }
    }

    public void anularFacturas() {
        this.facturaFacade.anularFacturas(this.lstFacturasSeleccionadas, "");
        this.lstFacturasSeleccionadas.clear();
        this.facturaSeleccionada = null;
        this.filtrarFacturas();
    }

    public void generarPDFImpreSeleccionados() {
        List<Factura> lstfa = new LinkedList<>();
        if (this.jsonFacturasSeleccionadas != null) {
            Gson gson = new Gson();
            Integer[] lstIdsFact = gson.fromJson(this.jsonFacturasSeleccionadas, Integer[].class);
            for (Integer idf : lstIdsFact) {
                lstfa.add(this.facturaFacade.find(idf));
            }
        }
        this.generarPDF(lstfa);
    }

    private void generarPDF(List<Factura> lstF) {
//        String rutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
        HashMap<Integer, String> hmCobrador = new HashMap<>();
        for (Factura f : lstF) {
            Funcionario c = this.asignacionFacturaFacade.getCobradorAsinado(f);
            if (c == null) {
                for (DetalleFactura df : f.getDetalleFacturaList()) {
                    if (df.getCuota() != null) {
                        c = df.getCuota().getSuscripcion().getCobrador();
                    }
                }
            }
            if (c != null) {
                String cobra = c.getNombres();
                if (c.getApellidos() != null) {
                    cobra = cobra + " " + c.getApellidos();
                }
                hmCobrador.put(f.getIdfactura(), cobra);
            } else {
                hmCobrador.put(f.getIdfactura(), "");
            }
        }
        List<JasperPrint> lstJasperPrint = UtilReportes.generarJPListFacturas(lstF, hmCobrador);

        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=report.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJasperPrint));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
        }
    }

    public void generarPDFImpresionDlgGenerar() {
        this.generarPDF(this.lstFacturasImprimir);
    }

    public void filtrarFacturas() {
        System.out.println("Filtrar facturas: " + this.mostrarPendientes + ", " + this.mostrarPagados + ", " + this.mostrarAnulados);
        System.out.println("Id cli: " + this.idClienteFiltro);
        System.out.println("desde: " + this.fechaFiltroDesde);
        System.out.println("hasta: " + this.fechaFiltroHasta);
        this.paginador.limpiarLista();
        try {
            Date desde = this.sdfcal.parse(this.fechaFiltroDesde);
            Date hasta = this.sdfcal.parse(this.fechaFiltroHasta);
            this.paginador.agregarItemsLista(this.facturaFacade.getFacturasFiltro(idClienteFiltro, mostrarPendientes, mostrarPagados, mostrarAnulados, desde, hasta, this.textoBusqueda));
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir a fecha desde formato", ex);
        }
    }

    public boolean existeTalonarioActivo() {
        TalonarioFactura t = this.talonarioFacturaFacade.getTalonarioActivo();
        if (t != null) {
            return true;
        } else {
            return false;
        }
    }

    public void generarPDFResumenFacturas() {
        String rutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
        Map<String, Object> parametros = new HashMap<>();
        List<FacturaReporteItem> lstItems = new ArrayList<>();
        for (Factura f : (List<Factura>) this.paginador.getListaCompleta()) {
            lstItems.add(new FacturaReporteItem(f));
        }

        JRDataSource datos = new JRBeanCollectionDataSource(lstItems);
        JasperReport jr = null;
        JasperPrint jpr = null;
        try {
            parametros.put("fdesde", this.sdfcal.parse(this.fechaFiltroDesde));
            parametros.put("fhasta", this.sdfcal.parse(this.fechaFiltroHasta));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a date", ex);
        }

        try {
            jr = (JasperReport) JRLoader.loadObject(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/reportes/rpResumenFacturas.jasper"));
            jpr = JasperFillManager.fillReport(jr, parametros, datos);
        } catch (JRException ex) {
            LOG.log(Level.SEVERE, "Error al cargar reporte de resumen de facturas", ex);
        }
        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=resumenfacturas.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            List<JasperPrint> lstJasperPrint = new ArrayList<>();
            lstJasperPrint.add(jpr);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJasperPrint));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
        }

    }

}
