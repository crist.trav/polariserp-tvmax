package com.exabit.polariserp.tvmax.reportes.suscripciones;

import java.util.Date;

/**
 *
 * @author cristhian-kn
 */
public class SuscripcionCantidadCuotasItem {

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private String servicio;
    private Integer idsuscripcion;
    private String cliente;
    private String direccion;
    private Date fechaultimopago;
    private Date fechaingreso;
    private Integer monto;
    private Integer cantcuotas;
    private Date fechavenc;
    private Integer montocuota;
    private String telefono;
    private String ruc;

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    
    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public Integer getIdsuscripcion() {
        return idsuscripcion;
    }

    public void setIdsuscripcion(Integer idsuscripcion) {
        this.idsuscripcion = idsuscripcion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaultimopago() {
        return fechaultimopago;
    }

    public void setFechaultimopago(Date ultimoPago) {
        this.fechaultimopago = ultimoPago;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getCantcuotas() {
        return cantcuotas;
    }

    public void setCantcuotas(Integer cantcuotas) {
        this.cantcuotas = cantcuotas;
    }

    public Date getFechavenc() {
        return fechavenc;
    }

    public void setFechavenc(Date fechavenc) {
        this.fechavenc = fechavenc;
    }

    public Integer getMontocuota() {
        return montocuota;
    }

    public void setMontocuota(Integer montocuota) {
        this.montocuota = montocuota;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    //</editor-fold>

}
