package com.exabit.polariserp.tvmax.cliente;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author traver
 */
@Named(value = "detalleClienteManBean")
@ViewScoped
public class DetalleClienteManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(DetalleClienteManBean.class.getName());

    @EJB
    private ClienteFacade clienteFacade;
    @EJB
    private SuscripcionFacade suscripcionFacade;
    @EJB
    private BarrioFacade barrioFacade;
    @EJB
    private FuncionarioFacade funcionarioFacade;

    private String cabeceraMsgCli;
    private String cuerpoMsgCli;
    private String nombreCli;
    private String razonSocialCli;
    private String apellidoCli;
    private String telefonoCli;
    private Integer dvrucCli;
    private Integer ciCli;
    private String direccionFacturacionCli;
    private Integer idBarrioFacturacionCli;
    private Integer idCobradorCli;
    private String tipoMsg = "info";

   //<editor-fold desc="Getters y Setters" defaultstate="collapsed">
    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }
    public Integer getIdCobradorCli() {
        return idCobradorCli;
    }

    public void setIdCobradorCli(Integer idCobradorCli) {
        this.idCobradorCli = idCobradorCli;
    }
    
    public Integer getIdBarrioFacturacionCli() {
        return idBarrioFacturacionCli;
    }

    public void setIdBarrioFacturacionCli(Integer idBarrioFacturacionCli) {
        this.idBarrioFacturacionCli = idBarrioFacturacionCli;
    }

    public String getDireccionFacturacionCli() {
        return direccionFacturacionCli;
    }

    public void setDireccionFacturacionCli(String direccionFacturacionCli) {
        this.direccionFacturacionCli = direccionFacturacionCli;
    }


    public Integer getCiCli() {
        return ciCli;
    }

    public void setCiCli(Integer ciCli) {
        this.ciCli = ciCli;
    }

    public Integer getDvrucCli() {
        return dvrucCli;
    }

    public void setDvrucCli(Integer dvrucCli) {
        this.dvrucCli = dvrucCli;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getApellidoCli() {
        return apellidoCli;
    }

    public void setApellidoCli(String apellidoCli) {
        this.apellidoCli = apellidoCli;
    }

    public String getRazonSocialCli() {
        return razonSocialCli;
    }

    public void setRazonSocialCli(String razonSocialCli) {
        this.razonSocialCli = razonSocialCli;
    }

    public String getTelefonoCli() {
        return telefonoCli;
    }

    public void setTelefonoCli(String telefonoCli) {
        this.telefonoCli = telefonoCli;
    }

    public String getCabeceraMsgCli() {
        return cabeceraMsgCli;
    }

    public void setCabeceraMsgCli(String cabeceraMsgCli) {
        this.cabeceraMsgCli = cabeceraMsgCli;
    }

    public String getCuerpoMsgCli() {
        return cuerpoMsgCli;
    }

    public void setCuerpoMsgCli(String cuerpoMsgCli) {
        this.cuerpoMsgCli = cuerpoMsgCli;
    }
    //</editor-fold>

    /**
     * Creates a new instance of DetalleClienteManBean
     */
    public DetalleClienteManBean() {
    }

    private Integer idcliente;

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public void cargarDatosCliente() {
        System.out.println("idcliente mem"+this.idcliente);
        if (this.idcliente != null) {
            Cliente c = this.clienteFacade.find(this.idcliente);
            this.nombreCli = c.getNombres();
            this.apellidoCli = c.getApellidos();
            this.razonSocialCli = c.getRazonSocial();
            this.telefonoCli = c.getTelefono1();
            this.ciCli = c.getCi();
            if (c.getDvRuc() != null) {
                this.dvrucCli = (int) c.getDvRuc();
            }
            this.direccionFacturacionCli=c.getDireccion();
            if(c.getBarrio()!=null){
                this.idBarrioFacturacionCli=c.getBarrio().getIdbarrio();
            }
            if(c.getCobrador()!=null){
                this.idCobradorCli=c.getCobrador().getIdfuncionario();
            }
        }else{
            this.limpiarCampos();
        }
    }

    public List<Suscripcion> getSuscripcionesCli() {
        if (this.idcliente != null) {
            Cliente c = this.clienteFacade.find(this.idcliente);
            if (c != null) {
                return this.suscripcionFacade.getSuscripciones(c);
            } else {
                return new LinkedList<>();
            }
        } else {
            return new LinkedList<>();
        }
    }

    public void guardarCliente() {
        try {
            System.out.println("Guardar cliente");
            Cliente cli;
            if (this.idcliente != null) {
                cli = this.clienteFacade.find(this.idcliente);
            } else {
                cli = new Cliente();
            }
            cli.setNombres(this.nombreCli);
            cli.setApellidos(this.apellidoCli);
            cli.setRazonSocial(this.razonSocialCli);
            cli.setCi(this.ciCli);
            System.out.println("idbarrio cli guardar: "+this.idBarrioFacturacionCli);
            cli.setDireccion(this.direccionFacturacionCli);
            cli.setCobrador(this.funcionarioFacade.find(this.idCobradorCli));
            cli.setBarrio(this.barrioFacade.find(this.idBarrioFacturacionCli));
            if (this.dvrucCli != null) {
                cli.setDvRuc(this.dvrucCli.shortValue());
            }
            cli.setTelefono1(this.telefonoCli);
            if (this.idcliente != null) {
                for(Suscripcion sus:cli.getSuscripcionList()){
                    sus.setCobrador(cli.getCobrador());
                    this.suscripcionFacade.edit(sus);
                }
                this.clienteFacade.edit(cli);
            }else{
                cli.setCategoria(EntidadesEstaticas.CATEGORIA_CLIENTE_SIN_CATEGORIA);
                this.clienteFacade.create(cli);
                this.idcliente=cli.getIdcliente();
                System.out.println("Id cliente generado: "+this.idcliente);
            }
            this.tipoMsg = "info";
            this.cabeceraMsgCli = "Exito";
            this.cuerpoMsgCli = "Guardado correctamente";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al guardar cliente", ex);
            this.tipoMsg = "error";
            this.cabeceraMsgCli = "Error";
            this.cuerpoMsgCli = "Algo salió mal: " + ex.getMessage();
        }
    }

    public void limpiarCampos() {
        this.idcliente = null;
        this.apellidoCli = null;
        this.dvrucCli = null;
        this.nombreCli = null;
        this.razonSocialCli = null;
        this.telefonoCli = null;
        this.ciCli = null;
        this.idBarrioFacturacionCli=null;
        this.direccionFacturacionCli=null;
        this.idCobradorCli=null;
    }
    
    public List<Funcionario> getLstCobradores(){
        return this.funcionarioFacade.getCobradoresActivos();
    }
    
    
}
