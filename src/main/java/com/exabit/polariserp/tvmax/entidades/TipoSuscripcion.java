/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "tipo_suscripcion")
@NamedQueries({
    @NamedQuery(name = "TipoSuscripcion.findAll", query = "SELECT t FROM TipoSuscripcion t")
    , @NamedQuery(name = "TipoSuscripcion.findByIdtipoSuscripcion", query = "SELECT t FROM TipoSuscripcion t WHERE t.idtipoSuscripcion = :idtipoSuscripcion")
    , @NamedQuery(name = "TipoSuscripcion.findByNombre", query = "SELECT t FROM TipoSuscripcion t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TipoSuscripcion.findByPorcentajeDescuento", query = "SELECT t FROM TipoSuscripcion t WHERE t.porcentajeDescuento = :porcentajeDescuento")})
public class TipoSuscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idtiposuscripcion", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idtiposuscripcion", table = "idsequences", allocationSize = 1, initialValue = 3)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipo_suscripcion")
    private Integer idtipoSuscripcion;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "porcentaje_descuento")
    private Float porcentajeDescuento;
    @OneToMany(mappedBy = "tipoSuscripcion")
    private List<Suscripcion> suscripcionList;

    public TipoSuscripcion() {
    }

    public TipoSuscripcion(Integer idtipoSuscripcion) {
        this.idtipoSuscripcion = idtipoSuscripcion;
    }

    public Integer getIdtipoSuscripcion() {
        return idtipoSuscripcion;
    }

    public void setIdtipoSuscripcion(Integer idtipoSuscripcion) {
        this.idtipoSuscripcion = idtipoSuscripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(Float porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoSuscripcion != null ? idtipoSuscripcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoSuscripcion)) {
            return false;
        }
        TipoSuscripcion other = (TipoSuscripcion) object;
        if ((this.idtipoSuscripcion == null && other.idtipoSuscripcion != null) || (this.idtipoSuscripcion != null && !this.idtipoSuscripcion.equals(other.idtipoSuscripcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.TipoSuscripcion[ idtipoSuscripcion=" + idtipoSuscripcion + " ]";
    }
    
}
