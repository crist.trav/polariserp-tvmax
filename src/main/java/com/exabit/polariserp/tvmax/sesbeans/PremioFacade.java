/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Premio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author gemacron
 */
@Stateless
public class PremioFacade extends AbstractFacade<Premio> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PremioFacade() {
        super(Premio.class);
    }
    
        public List<Premio> getPremioPorSorteoOrdenado(Integer idSorteo){
        TypedQuery<Premio> q=getEntityManager().createQuery("SELECT p FROM Premio p WHERE p.sorteoIdsorteo.idsorteo=:idsorteo ORDER BY p.numeroPremio ASC", Premio.class);
        q.setParameter("idsorteo", idSorteo);
        return q.getResultList();
    }
    
}
