/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "consulta_res_pronet")
@NamedQueries({
    @NamedQuery(name = "ConsultaResPronet.findAll", query = "SELECT c FROM ConsultaResPronet c")
    , @NamedQuery(name = "ConsultaResPronet.findByIdconsultaRes", query = "SELECT c FROM ConsultaResPronet c WHERE c.idconsultaRes = :idconsultaRes")
    , @NamedQuery(name = "ConsultaResPronet.findByCodServicio", query = "SELECT c FROM ConsultaResPronet c WHERE c.codServicio = :codServicio")
    , @NamedQuery(name = "ConsultaResPronet.findByTipoTrx", query = "SELECT c FROM ConsultaResPronet c WHERE c.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "ConsultaResPronet.findByCodRetorno", query = "SELECT c FROM ConsultaResPronet c WHERE c.codRetorno = :codRetorno")
    , @NamedQuery(name = "ConsultaResPronet.findByDesRetorno", query = "SELECT c FROM ConsultaResPronet c WHERE c.desRetorno = :desRetorno")
    , @NamedQuery(name = "ConsultaResPronet.findByNombreApellido", query = "SELECT c FROM ConsultaResPronet c WHERE c.nombreApellido = :nombreApellido")
    , @NamedQuery(name = "ConsultaResPronet.findByCantFilas", query = "SELECT c FROM ConsultaResPronet c WHERE c.cantFilas = :cantFilas")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsultaResPronet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idconsultarespronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idconsultarespronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idconsulta_res")
    @JsonIgnore
    private Integer idconsultaRes;
    @Size(max = 5)
    @Column(name = "codServicio")
    private String codServicio;
    @Column(name = "tipoTrx")
    private Integer tipoTrx;
    @Size(max = 3)
    @Column(name = "codRetorno")
    private String codRetorno;
    @Size(max = 30)
    @Column(name = "desRetorno")
    private String desRetorno;
    @Size(max = 60)
    @Column(name = "nombreApellido")
    private String nombreApellido;
    @Column(name = "cantFilas")
    private Integer cantFilas;
    @JoinColumn(name = "consulta_req_pronet", referencedColumnName = "idconsulta_req")
    @ManyToOne(optional = false)
    @JsonIgnore
    private ConsultaReqPronet consultaReqPronet;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "consultaResPronet")
    private List<ConsultaResDetallePronet> detalles;

    public ConsultaResPronet() {
    }

    public ConsultaResPronet(Integer idconsultaRes) {
        this.idconsultaRes = idconsultaRes;
    }

    public Integer getIdconsultaRes() {
        return idconsultaRes;
    }

    public void setIdconsultaRes(Integer idconsultaRes) {
        this.idconsultaRes = idconsultaRes;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
    }

    public String getDesRetorno() {
        return desRetorno;
    }

    public void setDesRetorno(String desRetorno) {
        this.desRetorno = desRetorno;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public Integer getCantFilas() {
        return cantFilas;
    }

    public void setCantFilas(Integer cantFilas) {
        this.cantFilas = cantFilas;
    }

    public ConsultaReqPronet getConsultaReqPronet() {
        return consultaReqPronet;
    }

    public void setConsultaReqPronet(ConsultaReqPronet consultaReqPronet) {
        this.consultaReqPronet = consultaReqPronet;
    }

    public List<ConsultaResDetallePronet> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<ConsultaResDetallePronet> consultaResDetallePronetList) {
        this.detalles = consultaResDetallePronetList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconsultaRes != null ? idconsultaRes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConsultaResPronet)) {
            return false;
        }
        ConsultaResPronet other = (ConsultaResPronet) object;
        if ((this.idconsultaRes == null && other.idconsultaRes != null) || (this.idconsultaRes != null && !this.idconsultaRes.equals(other.idconsultaRes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.ConsultaResPronet[ idconsultaRes=" + idconsultaRes + " ]";
    }
    
}
