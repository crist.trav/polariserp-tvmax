/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.funcionarios;

import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.rest.DropdownItem;
import com.exabit.polariserp.tvmax.rest.RespuestaDropdown;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.google.gson.Gson;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cristhian-kn
 */
@Path("buscarcobradordropdown")
public class BuscarcobradordropdownResource {

    @Context
    private UriInfo context;
    
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    /**
     * Creates a new instance of BuscarcobradordropdownResource
     */
    public BuscarcobradordropdownResource() {
        
    }

    /**
     * Retrieves representation of an instance of com.polariserp.web.rest.funcionarios.BuscarcobradordropdownResource
     * @param q
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText(@QueryParam("q") String q) {
        System.out.println("Busqueda de cobrador: "+q);
        String consulta="SELECT f FROM Funcionario f WHERE f.cargo=:cargo";
//        TypedQuery<Funcionario> query =this.em.createQuery("SELECT f FROM Funcionario f WHERE f.cargo=:cargo AND (f.nombres LIKE :tx OR f.apellidos LIKE :tx OR SQL('CAST(? AS CHAR(11))', f.idfuncionario))", Funcionario.class);
        TypedQuery<Funcionario> query =this.em.createQuery(consulta, Funcionario.class);
        if(!q.isEmpty()){
            System.out.println("consulta no vacia");
//            consulta=consulta+" AND (LOWER(f.nombres) LIKE :tx OR LOWER(f.apellidos) LIKE :tx OR SQL('CAST(? AS CHAR(11))', f.idfuncionario)) LIKE :tx";
            consulta=consulta+" AND (LOWER(f.nombres) LIKE :tx OR LOWER(f.apellidos) LIKE :tx OR SQL('CAST(? AS CHAR(11))', f.idfuncionario) LIKE :tx)";
            query=this.em.createQuery(consulta, Funcionario.class);
            query.setParameter("tx", "%"+q.toLowerCase()+"%");
        }
        query.setParameter("cargo", EntidadesEstaticas.CARGO_COBRADOR);
        List<Funcionario> lstFuncionarios=query.getResultList();
        RespuestaDropdown r=new RespuestaDropdown();
        for(Funcionario f:lstFuncionarios){
            DropdownItem di=new DropdownItem();
            di.setName(f.getIdfuncionario()+"-"+f.getNombres()+(f.getActivo()?" (Activo)":" (Inactivo)"));
            if(f.getApellidos()!=null){
                di.setName(di.getName()+" "+f.getApellidos());
            }
            di.setValue(f.getIdfuncionario());
            r.getResults().add(di);
        }
        Gson gson=new Gson();
        return gson.toJson(r);
    }

    /**
     * PUT method for updating or creating an instance of BuscarcobradordropdownResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
