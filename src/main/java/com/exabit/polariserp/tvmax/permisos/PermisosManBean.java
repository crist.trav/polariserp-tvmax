/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.permisos;

import com.exabit.polariserp.tvmax.entidades.Cargo;
import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.funcionalidad.AuxFuncionalidad;
import com.exabit.polariserp.tvmax.sesbeans.CargoFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionalidadFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ModuloFacade;
import com.google.gson.Gson;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author frioss
 */
@Named(value = "permisoManBean")
@RequestScoped
public class PermisosManBean {

    @EJB
    private CargoFacade cargoFacade;

    @EJB
    private ModuloFacade moduloFacade;
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    @EJB
    private FuncionalidadFacade funcionalidadFacade;
    @EJB
    private FuncionarioFacade funcionarioFacade;
    private Cargo cargoVar = new Cargo();
    private Integer idFuncionalidadTmp;
    private Integer idFuncionarioTmp;
    private Funcionario funcionarioVar = new Funcionario();
    private Funcionalidad funcionalidadVar = new Funcionalidad();
    private Modulo moduloVar = new Modulo();
    private Integer idModuloTmp;
    private String seleccionIDJSON;
    private String seleccionRemoverJSON;
    private List<Funcionalidad> listFuncionalidad = new LinkedList<>();
    private List<Funcionalidad> listFuncionalidadPermiso = new LinkedList<>();
    private List<Funcionalidad> listFuncionalidadTMP = new LinkedList<>();
    private List<AuxFuncionalidad> lstAux = new LinkedList();
    private boolean permiso;
    private Integer var1;

    public Cargo getCargoVar() {
        return cargoVar;
    }

    public void setCargoVar(Cargo cargoVar) {
        this.cargoVar = cargoVar;
    }

    public Modulo getModuloVar() {
        return moduloVar;
    }

    public void setModuloVar(Modulo moduloVar) {
        this.moduloVar = moduloVar;
    }

    public String getSeleccionRemoverJSON() {
        return seleccionRemoverJSON;
    }

    public Integer getVar1() {
        return var1;
    }

    public void setVar1(Integer var1) {
        this.var1 = var1;
    }

    public void setSeleccionRemoverJSON(String seleccionRemoverJSON) {
        this.seleccionRemoverJSON = seleccionRemoverJSON;
    }

    public boolean isPermiso() {
        return permiso;
    }

    public void setPermiso(boolean permiso) {
        this.permiso = permiso;
    }

    public List<AuxFuncionalidad> getLstAux() {
        return lstAux;
    }

    public void setLstAux(List<AuxFuncionalidad> lstAux) {
        this.lstAux = lstAux;
    }

    public List<Funcionalidad> getListFuncionalidadTMP() {
        return listFuncionalidadTMP;
    }

    public void setListFuncionalidadTMP(List<Funcionalidad> listFuncionalidadTMP) {
        this.listFuncionalidadTMP = listFuncionalidadTMP;
    }

    public List<Funcionalidad> getListFuncionalidadPermiso() {
        return listFuncionalidadPermiso;
    }

    public void setListFuncionalidadPermiso(List<Funcionalidad> listFuncionalidadPermiso) {
        this.listFuncionalidadPermiso = listFuncionalidadPermiso;
    }
    private List<Funcionario> listaFuncionario;

    public List<Funcionario> getListaFuncionario() {
        return listaFuncionario;
    }

    public void setListaFuncionario(List<Funcionario> listaFuncionario) {
        this.listaFuncionario = listaFuncionario;
    }

    public List<Funcionalidad> getListFuncionalidad() {
        return listFuncionalidad;
    }

    public void setListFuncionalidad(List<Funcionalidad> listFuncionalidad) {
        this.listFuncionalidad = listFuncionalidad;
    }

    public String getSeleccionIDJSON() {
        return seleccionIDJSON;
    }

    public void setSeleccionIDJSON(String seleccionIDJSON) {
        this.seleccionIDJSON = seleccionIDJSON;
    }

    public Integer getIdModuloTmp() {
        return idModuloTmp;
    }

    public void setIdModuloTmp(Integer idModuloTmp) {
        this.idModuloTmp = idModuloTmp;
    }

    public Funcionario getFuncionarioVar() {
        return funcionarioVar;
    }

    public void setFuncionarioVar(Funcionario funcionarioVar) {
        this.funcionarioVar = funcionarioVar;
    }

    public Funcionalidad getFuncionalidadVar() {
        return funcionalidadVar;
    }

    public void setFuncionalidadVar(Funcionalidad funcionalidadVar) {
        this.funcionalidadVar = funcionalidadVar;
    }

    public Integer getIdFuncionalidadTmp() {
        return idFuncionalidadTmp;
    }

    public void setIdFuncionalidadTmp(Integer idFuncionalidadTmp) {
        this.idFuncionalidadTmp = idFuncionalidadTmp;
    }

    public Integer getIdFuncionarioTmp() {
        return idFuncionarioTmp;
    }

    public void setIdFuncionarioTmp(Integer idFuncionarioTmp) {
        this.idFuncionarioTmp = idFuncionarioTmp;
    }

    public PermisosManBean() {
    }

    public List<Funcionario> getFuncionarios() {
        return funcionarioFacade.findAll();
    }

    public List<Modulo> getModulo() {
        return moduloFacade.findAll();
    }

    public List<Funcionalidad> getPermisoFuncionario(Integer idfuncionario) {
        funcionarioVar = funcionarioFacade.find(idfuncionario);
        if (funcionarioVar != null) {
            for (Funcionalidad funcionalidad : funcionarioVar.getFuncionalidadList()) {
                if (funcionalidad != null) {
                    listFuncionalidadPermiso.add(funcionalidad);
                    funcionarioVar.setFuncionalidadList(listFuncionalidadPermiso);
                    return listFuncionalidadPermiso;
                }
            }
        } else {
            System.out.println("Funcionario: NO ENCONTRADO");
        }
        return null;
    }

    public void administrarRegistro() {
        getFuncionalidadFromModulo(moduloVar.getIdmodulo());
        System.out.println("Seleccion: " + seleccionIDJSON + ", idpersonal:" + funcionarioVar.getIdfuncionario());
        Funcionalidad funciona;
        Funcionario funcionario = funcionarioFacade.find(funcionarioVar.getIdfuncionario());
        Gson gson = new Gson();
        Integer[] idbusquedas = gson.fromJson(this.seleccionIDJSON, Integer[].class);
        for (Integer idbusqueda : idbusquedas) {
            System.out.println("lista idbusqueda: " + idbusqueda);
            for (AuxFuncionalidad auxFuncionalidad : lstAux) {
                if (Objects.equals(auxFuncionalidad.getIdFuncionalidad(), idbusqueda)) {
                    if (auxFuncionalidad.isEstado() == false) {
                        System.out.println("estado: " + auxFuncionalidad.isEstado() + ", idbusqueda: " + idbusqueda);
                        funciona = funcionalidadFacade.find(idbusqueda);
                        funciona.getFuncionarioList().add(funcionario);
                        funcionalidadFacade.edit(funciona);
//                    getFuncionalidadFromModulo(moduloVar.getIdmodulo());
                    } else {
                        System.out.println("estado==" + auxFuncionalidad.isEstado());
                    }
                }
            }
            getFuncionalidadFromModulo(moduloVar.getIdmodulo());
        }
        for (AuxFuncionalidad auxFuncionalidad : lstAux) {
            System.out.println("lista auxiliar: " + auxFuncionalidad.isEstado() + ", id" + auxFuncionalidad.getIdFuncionalidad());
        }

    }

    public void administrarEliminarPermiso() {
        getFuncionalidadFromModulo(moduloVar.getIdmodulo());
        try {
            System.out.println("Remover: " + seleccionRemoverJSON + ", idpersonal:" + funcionarioVar.getIdfuncionario());
            Funcionalidad funcionalidad;
            Funcionario funcionario = funcionarioFacade.find(funcionarioVar.getIdfuncionario());
            Gson gson = new Gson();
            Integer[] idRemmovers = gson.fromJson(this.seleccionRemoverJSON, Integer[].class);
            for (Integer idRemmover : idRemmovers) {
                funcionalidad = funcionalidadFacade.find(idRemmover);
                funcionalidad.getFuncionarioList().remove(funcionario);
                funcionalidadFacade.edit(funcionalidad);
                System.out.println("idModulo tmp:" + idModuloTmp);
                getFuncionalidadFromModulo(moduloVar.getIdmodulo());
            }
        } catch (Exception e) {
            System.out.println("Error..");
            throw e;
        }

    }

    public List<Funcionalidad> listaFuncionalidadPorPersonal() {
        listFuncionalidadPermiso.clear();
        Integer idper = funcionarioVar.getIdfuncionario();
        Funcionario funcionariov = funcionarioFacade.find(funcionarioVar.getIdfuncionario());
        try {
            Query q = em.createQuery("SELECT pf FROM Funcionalidad pf JOIN pf.funcionarioList fp WHERE fp.idfuncionario=:idper");
            q.setParameter("idper", idper);
            listFuncionalidadPermiso.addAll(q.getResultList());
        } catch (Exception e) {
            System.out.println("error en la consulta");
            throw e;
        }
        return listFuncionalidadPermiso;
    }

    public List<AuxFuncionalidad> getFuncionalidadFromModulo(Integer idmodulo) {
        lstAux.clear();
        listFuncionalidadTMP.clear();
        listFuncionalidad.clear();
        listFuncionalidadTMP = listaFuncionalidadPorPersonal();
        Modulo mod = moduloFacade.find(idmodulo);
        try {
            Query q = em.createQuery("SELECT f FROM Funcionalidad f WHERE f.modulo=:mod");
            q.setParameter("mod", mod);
            listFuncionalidad = q.getResultList();
            if (listFuncionalidad != null) {
                for (Funcionalidad funcionalidad : listFuncionalidad) {
                    Integer idfuncionalidad = funcionalidad.getIdfuncionalidad();
                    String nombre = funcionalidad.getNombre();
                    boolean estado = false;
                    AuxFuncionalidad lstAuxFuncionalidad = new AuxFuncionalidad(nombre, idfuncionalidad, estado);
                    lstAux.add(lstAuxFuncionalidad);
                }
                for (AuxFuncionalidad auxFuncionalidad : lstAux) {
                    for (Funcionalidad funcional : listFuncionalidadPermiso) {
                        if (Objects.equals(funcional.getIdfuncionalidad(), auxFuncionalidad.getIdFuncionalidad())) {
                            auxFuncionalidad.setEstado(true);
                        }
                    }

                }
                return null;
            }

        } catch (Exception e) {
            throw e;
        }

        return null;
    }

}
