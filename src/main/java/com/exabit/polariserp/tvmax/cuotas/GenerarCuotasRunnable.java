package com.exabit.polariserp.tvmax.cuotas;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.HistorialGeneracionCuotas;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.HistorialGeneracionCuotasFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author cristhian-kn
 */
public class GenerarCuotasRunnable implements Callable {

    private static final Logger LOG = Logger.getLogger(GenerarCuotasRunnable.class.getName());
    Context ctx = this.obtenerContextoInicial();
    ParametroSistemaFacade parametroSistemaFacade = this.lookupParametroSistemaFacade();
    UserTransaction utx = obtenerUserTransaction();
    EntityManager em = this.obtenerEntityManager();
    GenerarCuotasSesBeanSingleton generarCuotasSesBeanSingleton = lookupGenerarCuotasSesBeanSingleton();    
    CuotaFacade cuotaFacade = this.lookupCuotaFacade();
    private final HistorialGeneracionCuotasFacade hgcFacade = this.lookupHistorialGeneracionCuotasFacade();

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private List<Suscripcion> lstSusc;
    private HistorialGeneracionCuotas hgc;
    private boolean inactivainactauto = false;
    private final List<Cuota> lstCuotasGeneradas = new LinkedList<>();
    private Date fdesde;
    private Date fhasta;
    private boolean generarsolorecientes;
    private Integer totalSuscripciones = 0;
    private Integer totalSuscProcesadas = 0;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public boolean isGenerarsolorecientes() {
        return generarsolorecientes;
    }

    public void setGenerarsolorecientes(boolean generarsolorecientes) {
        this.generarsolorecientes = generarsolorecientes;
    }

    public Date getFdesde() {
        return fdesde;
    }

    public void setFdesde(Date fdesde) {
        this.fdesde = fdesde;
    }

    public Date getFhasta() {
        return fhasta;
    }

    public void setFhasta(Date fhasta) {
        this.fhasta = fhasta;
    }

    public List<Suscripcion> getLstSusc() {
        return lstSusc;
    }

    public void setLstSusc(List<Suscripcion> lstSusc) {
        this.lstSusc = lstSusc;
    }

    public Integer getTotalSuscripciones() {
        return totalSuscripciones;
    }

    public void setTotalSuscripciones(Integer totalSuscripciones) {
        this.totalSuscripciones = totalSuscripciones;
    }

    public Integer getTotalSuscProcesadas() {
        return totalSuscProcesadas;
    }

    public void setTotalSuscProcesadas(Integer totalSuscProcesadas) {
        this.totalSuscProcesadas = totalSuscProcesadas;
    }

    public Integer getPorcentajeProcesado() {
        return (this.totalSuscProcesadas * 100) / this.totalSuscripciones;
    }
    //</editor-fold>

    public GenerarCuotasRunnable(Date fdesde, Date fhasta, List<Suscripcion> lstSusc, boolean inactauto, boolean generardesdereciente) {
        System.out.println("Rango Inicio " + this.sdf.format(fdesde) + "-" + this.sdf.format(fhasta));
        this.fdesde = fdesde;
        this.fhasta = fhasta;
        this.lstSusc = lstSusc;
        this.totalSuscripciones = lstSusc.size();
        this.inactivainactauto = inactauto;
        this.generarsolorecientes = generardesdereciente;
    }

//    @Override
//    public void run() {
//        this.generarCuotasSesBeanSingleton.reiniciarEstado();
//        this.generarCuotasSesBeanSingleton.setGenerando(true);
//        for (Suscripcion s : this.lstSusc) {
//            System.out.println("Creando cuotas para suscripcion: "+s.getIdsuscripcion());
//            Integer cant = this.generarCuotas(s, fdesde, fhasta, this.inactivainactauto);
//            this.generarCuotasSesBeanSingleton.incrementarCuotasGeneradas(cant);
//            this.totalSuscProcesadas++;
//            this.generarCuotasSesBeanSingleton.enviarPorcentajeProgreso(this.getPorcentajeProcesado());
//        }
//        System.out.println("Fin generacion de cuotas");
//        this.generarCuotasSesBeanSingleton.setGenerando(false);
//        this.generarCuotasSesBeanSingleton.enviarPorcentajeProgreso(100);
//    }
    public Integer generarCuotas(Suscripcion s, Date desde, Date hasta, boolean inactauto) {
        Calendar calDesde = new GregorianCalendar();
        calDesde.setTime(desde);
        Calendar calHasta = new GregorianCalendar();
        calHasta.setTime(hasta);
        int cantgenerado = 0;
        System.out.println("Generando Rango " + this.sdf.format(desde) + "-" + this.sdf.format(hasta));
        System.out.println("cal compare: " + calDesde.compareTo(calHasta));
        while (calDesde.compareTo(calHasta) <= 0) {
            int mes = calDesde.get(Calendar.MONTH) + 1;
            int anio = calDesde.get(Calendar.YEAR);
            System.out.println("Se genera para " + mes + " " + anio);
            cantgenerado = cantgenerado + this.generarCuotas(s, mes, anio, inactauto);
            calDesde.add(Calendar.MONTH, 1);
        }
        return cantgenerado;
    }

    public Integer generarCuotas(Suscripcion s, Integer mes, Integer anio, boolean inactauto) {
//        Calendar calsus = new GregorianCalendar();
//        calsus.setTime(s.getFechaSuscripcion());
//
//        int messus = calsus.get(Calendar.MONTH) + 1;
//        int aniosus = calsus.get(Calendar.YEAR);
//        Calendar calgeneracion = new GregorianCalendar(anio, mes, 1);
//
//        System.out.println("Mes suscripcion: " + messus + " anio susc: " + aniosus);
        Integer cantCuotas = 0;
//        if (calsus.before(calgeneracion)) {
        if (inactauto) {
            TypedQuery<Cuota> qc = this.em.createQuery("SELECT c FROM Cuota c WHERE c.cancelado=false AND c.suscripcion=:s", Cuota.class);
            qc.setParameter("s", s);
            List<Cuota> lstCuo = qc.getResultList();
            if (lstCuo.size() >= 4) {
                SolicitudCambioEstado sce = new SolicitudCambioEstado();
                sce.setConcretado(true);
                sce.setEstadoAnterior(s.getEstado());
                sce.setEstadoNuevo(EntidadesEstaticas.ESTADO_RECONECTADO);
                sce.setFechaConcrecion(new Date());
                sce.setFechaSolicitud(new Date());
                sce.setObservacion("Desactivación automática. Más de 4 cuotas sin pago.");
                sce.setSuscripcion(s);
                s.setEstado(EntidadesEstaticas.ESTADO_RECONECTADO);
                this.em.persist(sce);
                this.em.merge(s);
            } else {
                cantCuotas = cantCuotas + this.generarCuotas(s, mes, anio);
            }
        } else {
            System.out.println("se genera cuotas por el false");
            cantCuotas = cantCuotas + this.generarCuotas(s, mes, anio);
        }
//        } else {
//            System.out.println("fecha de suscripcion posterior al mes: " + mes + ", anio: " + anio + ", Suscripcion: " + s.getIdsuscripcion());
//        }
        System.out.println("Cantidad cuotas a retornar por metodo principal: " + cantCuotas);
        return cantCuotas;
    }

    private Integer generarCuotas(Suscripcion s, Integer mes, Integer anio) {
        System.out.println("genrerar cuota final boss");
        Calendar calsus = new GregorianCalendar();
        calsus.setTime(s.getFechaSuscripcion());

        int messus = calsus.get(Calendar.MONTH) + 1;
        int aniosus = calsus.get(Calendar.YEAR);
        Calendar calgeneracion = new GregorianCalendar(anio, mes - 1, 1);
        Calendar ahora = new GregorianCalendar();
        if (ahora.get(Calendar.YEAR) == anio && ahora.get(Calendar.MONTH) == (mes - 1)) {
            calgeneracion.set(Calendar.DAY_OF_MONTH, ahora.get(Calendar.DAY_OF_MONTH));
        }
        System.out.println("Mes suscripcion: " + messus + " anio susc: " + aniosus);
        boolean comparacionCuotaReciente = true;
        if (this.generarsolorecientes) {
            Date fecUltCuota = this.cuotaFacade.getFechaCuotaRecientePrincipal(s);
            if (fecUltCuota != null) {
                System.out.println("Cuota mas reciente: " + this.sdf.format(fecUltCuota));
                Calendar calCuoRec = new GregorianCalendar();
                calCuoRec.setTime(fecUltCuota);
                calCuoRec.set(Calendar.DAY_OF_MONTH, 1);
                calsus = new GregorianCalendar(calCuoRec.get(Calendar.YEAR), calCuoRec.get(Calendar.MONTH), 1);
            }
            System.out.println("calgeneracion: " + this.sdf.format(calgeneracion.getTime()));
            comparacionCuotaReciente = calgeneracion.compareTo(calsus) >= 0;
        }

        Integer cantCuotas = 0;
        if (comparacionCuotaReciente) {
            try {
                this.utx.begin();
                if (!this.cuotaFacade.existenCuotasPeriodo(s, mes, anio)) {
                    double descuento = s.getTipoSuscripcion().getPorcentajeDescuento();
                    if (mes == messus && anio == aniosus) {
//                        if (diaExoneracion != 0 && calsus.get(Calendar.DAY_OF_MONTH) >= diaExoneracion) {
//                            Cuota cuo = new Cuota();
//                            cuo.setSuscripcion(s);
//                            cuo.setCancelado(true);
//                            Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
//                            cuo.setFechaVencimiento(calvenc.getTime());
//                            cuo.setMesCuota(mes.shortValue());
//                            cuo.setAnioCuota(anio.shortValue());
//                            cuo.setFechaPago(calvenc.getTime());
//                            cuo.setMontoCuota(0);
//                            cuo.setMontoEntrega(0);
//                            this.em.persist(cuo);
//                            cantCuotas = cantCuotas + 1;
//                        } else {
                        if (descuento < 100) {
                            Cuota cuo = new Cuota();
                            cuo.setHistorialGeneracion(hgc);
                            cuo.setSuscripcion(s);
                            cuo.setServicio(s.getServicio());
                            cuo.setCancelado(false);
                            Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                            cuo.setFechaVencimiento(new Date(calvenc.getTimeInMillis()));
                            cuo.setMesCuota(mes.shortValue());
                            cuo.setAnioCuota(anio.shortValue());
                            cuo.setNroCuota(0);
                            double precionormal = s.getPrecio();
//                                if (diaCuotaParcial != 0 && calsus.get(Calendar.DAY_OF_MONTH) >= diaCuotaParcial) {
//                                    Calendar calfinmes = new GregorianCalendar(calsus.get(Calendar.YEAR), calsus.get(Calendar.MONTH), 1);
//                                    calfinmes.add(Calendar.DAY_OF_MONTH, -1);
//                                    int cantidadDiasParcial = calfinmes.get(Calendar.DAY_OF_MONTH) - calsus.get(Calendar.DAY_OF_MONTH);
//                                    int montoPorDia = s.getPrecio() / calfinmes.get(Calendar.DAY_OF_MONTH);
//                                    precionormal = montoPorDia * cantidadDiasParcial;
//                                }

                            double montoDescuento = (precionormal * descuento) / (double) 100;
                            Integer montoFinalCuota = (int) Math.round(precionormal - montoDescuento);

                            cuo.setMontoCuota(montoFinalCuota);

                            if (descuento == (double) 100) {
                                cuo.setCancelado(true);
                            }
                            cuo.setMontoEntrega(0);
                            this.em.persist(cuo);
                            this.lstCuotasGeneradas.add(cuo);
                            cantCuotas = cantCuotas + 1;
                        } else {
                            Cuota cuo = new Cuota();
                            cuo.setHistorialGeneracion(hgc);
                            cuo.setSuscripcion(s);
                            cuo.setServicio(s.getServicio());
                            cuo.setCancelado(true);
                            Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                            cuo.setFechaVencimiento(calvenc.getTime());
                            cuo.setMesCuota(mes.shortValue());
                            cuo.setAnioCuota(anio.shortValue());
                            cuo.setFechaPago(calvenc.getTime());
                            cuo.setMontoCuota(0);
                            cuo.setMontoEntrega(0);
                            cuo.setNroCuota(0);
                            this.em.persist(cuo);
                            this.lstCuotasGeneradas.add(cuo);
                            cantCuotas = cantCuotas + 1;
                        }
//                        }
                    } else {
                        if (descuento < 100) {
                            Cuota cuo = new Cuota();
                            cuo.setHistorialGeneracion(hgc);
                            cuo.setSuscripcion(s);
                            cuo.setServicio(s.getServicio());
                            cuo.setCancelado(false);
                            Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                            cuo.setFechaVencimiento(new Date(calvenc.getTimeInMillis()));
                            cuo.setMesCuota(mes.shortValue());
                            cuo.setAnioCuota(anio.shortValue());
                            cuo.setNroCuota(0);
                            double precionormal = s.getPrecio();
//                            if (diaCuotaParcial != 0 && calsus.get(Calendar.DAY_OF_MONTH) >= diaCuotaParcial) {
//                                Calendar calfinmes = new GregorianCalendar(calsus.get(Calendar.YEAR), calsus.get(Calendar.MONTH), 1);
//                                calfinmes.add(Calendar.DAY_OF_MONTH, -1);
//                                int cantidadDiasParcial = calfinmes.get(Calendar.DAY_OF_MONTH) - calsus.get(Calendar.DAY_OF_MONTH);
//                                int montoPorDia = s.getPrecio() / calfinmes.get(Calendar.DAY_OF_MONTH);
//                                precionormal = montoPorDia * cantidadDiasParcial;
//                            }
                            double montoDescuento = (precionormal * descuento) / (double) 100;
                            Integer montoFinalCuota = (int) Math.round(precionormal - montoDescuento);
                            cuo.setMontoCuota(montoFinalCuota);

                            if (descuento == (double) 100) {
                                cuo.setCancelado(true);
                            }
                            cuo.setMontoEntrega(0);
                            this.em.persist(cuo);
                            this.lstCuotasGeneradas.add(cuo);
                            cantCuotas = cantCuotas + 1;
                        } else {
                            Cuota cuo = new Cuota();
                            cuo.setHistorialGeneracion(hgc);
                            cuo.setSuscripcion(s);
                            cuo.setServicio(s.getServicio());
                            cuo.setCancelado(true);
                            Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                            cuo.setFechaVencimiento(calvenc.getTime());
                            cuo.setMesCuota(mes.shortValue());
                            cuo.setAnioCuota(anio.shortValue());
                            cuo.setFechaPago(calvenc.getTime());
                            cuo.setMontoCuota(0);
                            cuo.setMontoEntrega(0);
                            cuo.setNroCuota(0);
                            this.em.persist(cuo);
                            this.lstCuotasGeneradas.add(cuo);
                            cantCuotas = cantCuotas + 1;
                        }
                    }
                } else {
                    System.out.println("Existen cuotas en periodo " + mes + " " + anio);
                }
                this.utx.commit();
            } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                LOG.log(Level.SEVERE, "Error al procesar transaccion, en generacion de cuotas", ex);
            }
        } else {
            System.out.println("fecha de suscripcion posterior al mes: " + mes + ", anio: " + anio + ", Suscripcion: " + s.getIdsuscripcion());
        }
        System.out.println("Cantidad cuotas a retornar por metodo principal: " + cantCuotas);
        return cantCuotas;
    }

    private Context obtenerContextoInicial() {
        try {
            return new InitialContext();
        } catch (NamingException ex) {
            Logger.getLogger(GenerarCuotasRunnable.class.getName()).log(Level.SEVERE, "Error al obtener contexto inicial", ex);
            throw new RuntimeException(ex);
        }
    }

    private GenerarCuotasSesBeanSingleton lookupGenerarCuotasSesBeanSingleton() {
        try {
            return (GenerarCuotasSesBeanSingleton) ctx.lookup("java:module/GenerarCuotasSesBeanSingleton");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private ParametroSistemaFacade lookupParametroSistemaFacade() {
        try {
            return (ParametroSistemaFacade) ctx.lookup("java:module/ParametroSistemaFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private HistorialGeneracionCuotasFacade lookupHistorialGeneracionCuotasFacade() {
        try {
            return (HistorialGeneracionCuotasFacade) ctx.lookup("java:module/HistorialGeneracionCuotasFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Error al obtener instancia de bean HistorialGeneracionCuotas", ne);
            throw new RuntimeException(ne);
        }
    }

    private CuotaFacade lookupCuotaFacade() {
        try {

            return (CuotaFacade) ctx.lookup("java:module/CuotaFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    public UserTransaction obtenerUserTransaction() {
        try {
            return (UserTransaction) ctx.lookup("java:comp/env/UserTransaction");
        } catch (NamingException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

    public EntityManager obtenerEntityManager() {

        try {
            return (EntityManager) ctx.lookup("java:comp/env/persistence/PolarisERP-PU");
        } catch (NamingException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer call() throws Exception {
        if (!this.generarCuotasSesBeanSingleton.isGenerando()) {
            this.generarCuotasSesBeanSingleton.reiniciarEstado();
            this.generarCuotasSesBeanSingleton.setGenerando(true);
            int totalcuotas = 0;
            hgc = new HistorialGeneracionCuotas();
            this.lstCuotasGeneradas.clear();
            hgc.setFechaHora(new Date());
            this.hgcFacade.create(hgc);
            for (Suscripcion s : this.lstSusc) {
                System.out.println("Creando cuotas para suscripcion: " + s.getIdsuscripcion());
//                List<Cuota> lstcuotasgen=this.cuotaFacade.generarCuotas(s, fdesde, fhasta, this.inactivainactauto);
//                this.lstCuotasGeneradas.addAll(lstcuotasgen);
//                Integer cant=lstcuotasgen.size();
                Integer cant = this.generarCuotas(s, fdesde, fhasta, this.inactivainactauto);
                totalcuotas = totalcuotas + cant;
                this.generarCuotasSesBeanSingleton.incrementarCuotasGeneradas(cant);
                this.totalSuscProcesadas++;
                this.generarCuotasSesBeanSingleton.enviarPorcentajeProgreso(this.getPorcentajeProcesado());
            }
            this.hgc.setCantidadCuotas(this.lstCuotasGeneradas.size());
            this.hgcFacade.edit(hgc);
            System.out.println("Fin generacion de cuotas");
            this.generarCuotasSesBeanSingleton.setGenerando(false);
            this.generarCuotasSesBeanSingleton.enviarPorcentajeProgreso(100);
            return totalcuotas;
        } else {
            System.out.println("Ya se estan generando las cuotas");
            return 0;
        }
    }
}
