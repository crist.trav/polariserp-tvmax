/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "solicitud_cambio_estado")
@NamedQueries({
    @NamedQuery(name = "SolicitudCambioEstado.findAll", query = "SELECT s FROM SolicitudCambioEstado s")
    , @NamedQuery(name = "SolicitudCambioEstado.findByIdsolicitud", query = "SELECT s FROM SolicitudCambioEstado s WHERE s.idsolicitud = :idsolicitud")
    , @NamedQuery(name = "SolicitudCambioEstado.findByConcretado", query = "SELECT s FROM SolicitudCambioEstado s WHERE s.concretado = :concretado")
    , @NamedQuery(name = "SolicitudCambioEstado.findByFechaConcrecion", query = "SELECT s FROM SolicitudCambioEstado s WHERE s.fechaConcrecion = :fechaConcrecion")
    , @NamedQuery(name = "SolicitudCambioEstado.findByFechaSolicitud", query = "SELECT s FROM SolicitudCambioEstado s WHERE s.fechaSolicitud = :fechaSolicitud")
    , @NamedQuery(name = "SolicitudCambioEstado.findByNroDocumento", query = "SELECT s FROM SolicitudCambioEstado s WHERE s.nroDocumento = :nroDocumento")})
public class SolicitudCambioEstado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idsolicitud", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idsolicitud", table = "idsequences", allocationSize = 1, initialValue = 13804)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsolicitud")
    private Integer idsolicitud;
    @Column(name = "concretado")
    private Boolean concretado;
    @Column(name = "fecha_concrecion")
    @Temporal(TemporalType.DATE)
    private Date fechaConcrecion;
    @Column(name = "fecha_solicitud")
    @Temporal(TemporalType.DATE)
    private Date fechaSolicitud;
    @Size(max = 255)
    @Column(name = "nro_documento")
    private String nroDocumento;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "observacion")
    private String observacion;
    @JoinColumn(name = "estado_anterior", referencedColumnName = "idestado")
    @ManyToOne
    private EstadoSuscripcion estadoAnterior;
    @JoinColumn(name = "estado_nuevo", referencedColumnName = "idestado")
    @ManyToOne
    private EstadoSuscripcion estadoNuevo;
    @JoinColumn(name = "suscripcion", referencedColumnName = "idsuscripcion")
    @ManyToOne
    private Suscripcion suscripcion;

    public SolicitudCambioEstado() {
    }

    public SolicitudCambioEstado(Integer idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Integer getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Integer idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Boolean getConcretado() {
        return concretado;
    }

    public void setConcretado(Boolean concretado) {
        this.concretado = concretado;
    }

    public Date getFechaConcrecion() {
        return fechaConcrecion;
    }

    public void setFechaConcrecion(Date fechaConcrecion) {
        this.fechaConcrecion = fechaConcrecion;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public EstadoSuscripcion getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(EstadoSuscripcion estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public EstadoSuscripcion getEstadoNuevo() {
        return estadoNuevo;
    }

    public void setEstadoNuevo(EstadoSuscripcion estadoNuevo) {
        this.estadoNuevo = estadoNuevo;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsolicitud != null ? idsolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudCambioEstado)) {
            return false;
        }
        SolicitudCambioEstado other = (SolicitudCambioEstado) object;
        if ((this.idsolicitud == null && other.idsolicitud != null) || (this.idsolicitud != null && !this.idsolicitud.equals(other.idsolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado[ idsolicitud=" + idsolicitud + " ]";
    }
    
}
