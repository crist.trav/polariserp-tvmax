/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.premios;

import com.exabit.polariserp.tvmax.entidades.Premio;
import com.exabit.polariserp.tvmax.entidades.Sorteo;
import com.exabit.polariserp.tvmax.sesbeans.PremioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SorteoFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author frioss
 */
@Named(value="premioManBean")
@RequestScoped
public class PremioSessionManBean implements Serializable{

    @EJB
    private SorteoFacade sorteoFacade;
    @EJB
    private PremioFacade premioFacade;
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    private Premio premioVar = new  Premio();
    private String msg = "";
    private Integer idSorteoTmp;
    private Integer idSorteoPremio;
    private Integer idVarSorteoPremio;
    private String seleccionarIdPremioJSON;
    private Sorteo sorteoVar = new Sorteo();
    private Sorteo sorteoVarP = new Sorteo();
    private List<Premio> listPremio = new LinkedList();
    private List<Premio> listAux = new LinkedList();

    public List<Premio> getListAux() {
        return listAux;
    }

    public Integer getIdVarSorteoPremio() {
        return idVarSorteoPremio;
    }

    public void setIdVarSorteoPremio(Integer idVarSorteoPremio) {
        this.idVarSorteoPremio = idVarSorteoPremio;
    }

    public Sorteo getSorteoVarP() {
        return sorteoVarP;
    }

    public void setSorteoVarP(Sorteo sorteoVarP) {
        this.sorteoVarP = sorteoVarP;
    }

    public void setListAux(List<Premio> listAux) {
        this.listAux = listAux;
    }
    
    public List<Premio> getListPremio() {
        System.out.println("recorriendo getPremioList---");
        System.out.println("idBusqueda: " + idVarSorteoPremio);
        listPremio.clear();
        if (idVarSorteoPremio != null) {
            Sorteo sorteo = sorteoFacade.find(idVarSorteoPremio);
            if (sorteo != null) {
                TypedQuery<Premio> query = this.em.createQuery("SELECT p FROM Premio p WHERE p.sorteoIdsorteo=:sorteo", Premio.class);
                query.setParameter("sorteo", sorteo);
                for (Premio pr : query.getResultList()) {
                    if (pr != null) {
                        System.out.println("pr: " + pr.getDescripcion());
                        listPremio.add(pr);
//                    for (Premio prem : listPremio) {
//                        listAux.add(prem);
//                        System.out.println("prem: "+prem.getIdpremios()+", "+prem.getNumeroPremio());
//                    }
                        sorteo.setPremioList(listPremio);

                    }
                }
            } else {
                return null;
            }
        }else{
            System.out.println("valor nulo");
            return null;
        }
        return listPremio;
    }

    public void setListPremio(List<Premio> listPremio) {
        this.listPremio = listPremio;
    }
    
    
    public Sorteo getSorteoVar() {
        return sorteoVar;
    }

    public Integer getIdSorteoPremio() {
        return idSorteoPremio;
    }

    public void setIdSorteoPremio(Integer idSorteoPremio) {
        this.idSorteoPremio = idSorteoPremio;
    }

    public void setSorteoVar(Sorteo sorteoVar) {
        this.sorteoVar = sorteoVar;
    }
    
    public String getSeleccionarIdPremioJSON() {
        return seleccionarIdPremioJSON;
    }

    public void setSeleccionarIdPremioJSON(String seleccionarIdPremioJSON) {
        this.seleccionarIdPremioJSON = seleccionarIdPremioJSON;
    }

    public Integer getIdSorteoTmp() {
        return idSorteoTmp;
    }

    public void setIdSorteoTmp(Integer idSorteoTmp) {
        this.idSorteoTmp = idSorteoTmp;
    }
    
    public Premio getPremioVar() {
        return premioVar;
    }

    public void setPremioVar(Premio premioVar) {
        this.premioVar = premioVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    
    public PremioSessionManBean(){
        
    }
    
    public List<Premio>getPremios(){
        return premioFacade.findAll();
    }
    
//    public List<Premio> getListPorSorteo(Integer i) {
//        System.out.println("recorriendo getPremioList---");
//        System.out.println("idBusqueda: "+idSorteoTmp);
//        listPremio.clear();
//        sorteoVar = sorteoFacade.find(i);
//        if (sorteoVar!=null) {
//            for (Premio pr : sorteoVar.getPremioList()) {
//                if (pr!=null) {
//                    System.out.println("pr: "+pr.getDescripcion());
//                    listPremio.add(pr);
////                    for (Premio prem : listPremio) {
////                        listAux.add(prem);
////                        System.out.println("prem: "+prem.getIdpremios()+", "+prem.getNumeroPremio());
////                    }
//                    sorteoVar.setPremioList(listPremio);
//                    
//                }
//            }
//        }
//        return null;
//    }
    
    public void registrarPremio() {
        idVarSorteoPremio = idSorteoTmp;
        System.out.println("idsorteo: "+idSorteoTmp);
        if (idSorteoTmp != null) {
            Sorteo so = sorteoFacade.find(this.idSorteoTmp);
            premioVar.setSorteoIdsorteo(so);
            premioVar.setFechaHora(new Date());
            this.premioFacade.create(premioVar);
            this.msg = "Reistrado correctamente [" + premioVar.getNumeroPremio() + "]-[" + premioVar.getDescripcion() + "]";
        } else {
            this.msg = "MANTENGA LA CALMA!! Primero tienes que cargar un sorteo, luego presionar sobre el boton azul que se encuentra a su izquierda!";
        }
    }
    
    public void modificarPremio(){
        idVarSorteoPremio = idSorteoTmp;
        Sorteo so = sorteoFacade.find(this.idSorteoTmp);
        premioVar.setSorteoIdsorteo(so);
        premioVar.setFechaHora(new Date());
        this.premioFacade.edit(premioVar);
        this.msg = "Se ha modificado correctamente: ["+premioVar.getNumeroPremio()+"]-["+premioVar.getDescripcion()+"]";
//        this.getListPremio();
//        listPremio.addAll(getListPorSorteo(so.getIdsorteo()));
    }
    
    public void eliminarPremio(){
        idVarSorteoPremio = idSorteoPremio;
        System.out.println("ideliminar: "+idSorteoPremio);
        Premio pr = this.premioFacade.find(idSorteoPremio);
        if (pr!=null) {
            this.premioFacade.remove(pr);
//            this.getListPremio();
        }else{
            System.out.println("Error al obtener premio: "+idSorteoPremio);
        }
//        Gson gson = new Gson();
//        Integer [] idEliminar = gson.fromJson(this.seleccionarIdPremioJSON, Integer[].class);
//        for (Integer integer : idEliminar) {
            
//        }
    }
    
    public List<Premio> listapremiosSorteo(){
        Sorteo i = sorteoFacade.find(sorteoVar.getIdsorteo());
        System.out.println("Sorteo: "+i.getDescripcion());
        return null;
    }
}
