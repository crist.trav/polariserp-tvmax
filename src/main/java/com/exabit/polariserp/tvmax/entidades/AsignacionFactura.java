package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "asignacion_factura")
@NamedQueries({
    @NamedQuery(name = "AsignacionFactura.findAll", query = "SELECT a FROM AsignacionFactura a")
    , @NamedQuery(name = "AsignacionFactura.findByIdasignacionFactura", query = "SELECT a FROM AsignacionFactura a WHERE a.idasignacionFactura = :idasignacionFactura")
    , @NamedQuery(name = "AsignacionFactura.findByFechaHoraRegistro", query = "SELECT a FROM AsignacionFactura a WHERE a.fechaHoraRegistro = :fechaHoraRegistro")})
public class AsignacionFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idasignacionfactura", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idasignacionfactura", table = "idsequences", allocationSize = 1, initialValue = 53003)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idasignacion_factura")
    private Integer idasignacionFactura;
    @Column(name = "fecha_hora_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraRegistro;
    @JoinColumn(name = "cobrador", referencedColumnName = "idfuncionario")
    @ManyToOne
    private Funcionario cobrador;
    @JoinColumn(name = "factura", referencedColumnName = "idfactura")
    @ManyToOne
    private Factura factura;

    public AsignacionFactura() {
    }

    public AsignacionFactura(Integer idasignacionFactura) {
        this.idasignacionFactura = idasignacionFactura;
    }

    public Integer getIdasignacionFactura() {
        return idasignacionFactura;
    }

    public void setIdasignacionFactura(Integer idasignacionFactura) {
        this.idasignacionFactura = idasignacionFactura;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public Funcionario getCobrador() {
        return cobrador;
    }

    public void setCobrador(Funcionario cobrador) {
        this.cobrador = cobrador;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idasignacionFactura != null ? idasignacionFactura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignacionFactura)) {
            return false;
        }
        AsignacionFactura other = (AsignacionFactura) object;
        if ((this.idasignacionFactura == null && other.idasignacionFactura != null) || (this.idasignacionFactura != null && !this.idasignacionFactura.equals(other.idasignacionFactura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.AsignacionFactura[ idasignacionFactura=" + idasignacionFactura + " ]";
    }
    
}
