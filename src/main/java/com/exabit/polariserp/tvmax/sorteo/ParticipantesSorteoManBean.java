/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package com.exabit.polariserp.tvmax.sorteo;

import com.exabit.polariserp.tvmax.entidades.Sorteo;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.SorteoFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author traver
 */
@Named(value = "participantesSorteoManBean")
@ViewScoped
public class ParticipantesSorteoManBean implements Serializable {

    @EJB
    private SorteoFacade sorteoFacade;

    @EJB
    private CuotaFacade cuotaFacade;
    
    
    private Integer totalExcluidos = 0;
    private List<Suscripcion> lstParticipantes = new ArrayList<>();
    private Integer idsorteo;
    

    public Integer getTotalExcluidos() {
        return totalExcluidos;
    }

    public void setTotalExcluidos(Integer totalExcluidos) {
        this.totalExcluidos = totalExcluidos;
    }

    public List<Suscripcion> getLstParticipantes() {
        return lstParticipantes;
    }

    public void setLstParticipantes(List<Suscripcion> lstParticipantes) {
        this.lstParticipantes = lstParticipantes;
    }

    public Integer getIdsorteo() {
        return idsorteo;
    }

    public void setIdsorteo(Integer idsorteo) {
        this.idsorteo = idsorteo;
    }

    /**
     * Creates a new instance of ParticipantesSorteoManBean
     */
    public ParticipantesSorteoManBean() {
    }

    public void cargarParticipantes() {
        Sorteo sor = this.sorteoFacade.find(this.idsorteo);
        List<Suscripcion> lstSusExcluidas = sor.getSuscripcionList();
        System.out.println("No participantes: "+lstSusExcluidas.size());
        List<Suscripcion> lstSuscAlDia = this.cuotaFacade.getSuscHabilitadasSorteo(2022, 12);
        
        for(Suscripcion susAldia : lstSuscAlDia){
            boolean seExcluye = false;
            for(Suscripcion susExc : lstSusExcluidas){
                if(susAldia.getIdsuscripcion().equals(susExc.getIdsuscripcion())){
                    seExcluye = true;
                    this.totalExcluidos++;
                    break;
                }
            }
            if(!seExcluye){
                this.lstParticipantes.add(susAldia);
            }
        }
        System.out.println("Se cargan los participantes del sorteo " + this.idsorteo);
    }
    
    public String formatearFecha(Date d){
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(d);
    }

}
