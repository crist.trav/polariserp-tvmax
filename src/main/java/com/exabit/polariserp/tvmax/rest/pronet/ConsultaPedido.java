package com.exabit.polariserp.tvmax.rest.pronet;

/**
 *
 * @author traver
 */
public class ConsultaPedido {
    
    private String codServicio;
    private Integer tipoTrx;
    private String usuario;
    private String password;
    private String nroDocumento;
    private String moneda;

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }
    
    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public Integer getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(Integer tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

}
