/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sorteo;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.Premio;
import com.exabit.polariserp.tvmax.entidades.Sorteo;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.PremioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SorteoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author gemacron
 */
@Named(value = "sorteopremioManBean")
@ViewScoped
public class sorteopremioManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(sorteopremioManBean.class.getName());
    
    private boolean yaExcluido = false;

    @EJB
    private SorteoFacade sorteofacade;

    @EJB
    private PremioFacade premiofacade;

    @EJB
    private SuscripcionFacade suscripcionfacade;

    @EJB
    private CuotaFacade cuotafacade;

    private String msgNroCi;
    private Integer idSorteoSeleccionado;
    private Integer idPremioSeleccionado;
    private List<Premio> lstPremios = new LinkedList<>();
    private String msgGanador;
    private List<Premio> lstpremio = new ArrayList<>();
    private List<Suscripcion> suschabilitadas = new ArrayList<>();
    private List<Cuota> cuotas = new ArrayList<>();
    private List<Premio> lstganadores = new ArrayList<>();
    private Integer idGanador;
    private Premio premioganadores;

    public List<Suscripcion> getSuschabilitadas() {
        return suschabilitadas;
    }

    public void setSuschabilitadas(List<Suscripcion> suschabilitadas) {
        this.suschabilitadas = suschabilitadas;
    }

    public String getMsgNroCi() {
        return msgNroCi;
    }

    public void setMsgNroCi(String msgNroCi) {
        this.msgNroCi = msgNroCi;
    }

    public Integer getIdSorteoSeleccionado() {
        return idSorteoSeleccionado;
    }

    public void setIdSorteoSeleccionado(Integer idSorteoSeleccionado) {
        this.idSorteoSeleccionado = idSorteoSeleccionado;
    }

    public Integer getIdPremioSeleccionado() {
        return idPremioSeleccionado;
    }

    public void setIdPremioSeleccionado(Integer idPremioSeleccionado) {
        this.idPremioSeleccionado = idPremioSeleccionado;
    }

    public List<Premio> getLstPremios() {
        return lstPremios;
    }

    public void setLstPremios(List<Premio> lstPremios) {
        this.lstPremios = lstPremios;
    }

    public String getMsgGanador() {
        return msgGanador;
    }

    public void setMsgGanador(String msgGanador) {
        this.msgGanador = msgGanador;
    }

    public List<Premio> getLstpremio() {
        return lstpremio;
    }

    public void setLstpremio(List<Premio> lstpremio) {
        this.lstpremio = lstpremio;
    }

    public List<Premio> getLstganadores() {
        return lstganadores;
    }

    public void setLstganadores(List<Premio> lstganadores) {
        this.lstganadores = lstganadores;
    }

    public sorteopremioManBean() {
        this.premioganadores = new Premio();

    }

    public Premio getPremioganadores() {
        return premioganadores;
    }

    public void setPremioganadores(Premio premioganadores) {
        this.premioganadores = premioganadores;
    }

    public Integer getIdGanador() {
        return idGanador;
    }

    public void setIdGanador(Integer idGanador) {
        this.idGanador = idGanador;
    }

    public List<Premio> listadepremiados() {
        return premiofacade.findAll();
    }

    public List<Sorteo> listadeSorteo() {
        return sorteofacade.findAll();
    }

    public void actualizarListaHabilitados() {
        this.suschabilitadas.clear();
        Sorteo sortactual = null;

        if (this.idSorteoSeleccionado != null) {
            sortactual = this.sorteofacade.find(this.idSorteoSeleccionado);
        } 
        
        if (sortactual != null) {
           
            List<Suscripcion> lstExcluidos = sortactual.getSuscripcionList();
            List<Suscripcion> lstHablitadosTmp = cuotafacade.getSuscHabilitadasSorteo(2022, 12);
            for(Suscripcion sh : lstHablitadosTmp){
                boolean seExcluye = false;
                for(Suscripcion se : lstExcluidos){
                    if(sh.getIdsuscripcion().equals(se.getIdsuscripcion())){
                        seExcluye = true;
                        break;
                    }
                }
                if(!seExcluye){
                    this.suschabilitadas.add(sh);
                }
            }
            List<Premio> lstp = this.premiofacade.getPremioPorSorteoOrdenado(sortactual.getIdsorteo());
            for (Premio p : lstp) {
                if (p.getGanador() != null) {
                    for(Suscripcion s : this.suschabilitadas){
                        if(s.getIdsuscripcion().equals(p.getGanador().getIdsuscripcion())){
                            this.suschabilitadas.remove(s);
                            break;
                        }
                    }
                }
            }
        }
        System.out.println("Total de habilitados > "+this.suschabilitadas.size());
        FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("inJsonParticipantes");
    }
    

    public String formateoNumero(Integer i) {
        DecimalFormat formateo = new DecimalFormat("000000");
        String nuevonumero = formateo.format(i);
        return nuevonumero;
    }

    public String conversorStringJson() {
        List<String> premiados = new ArrayList<>();
        for (Suscripcion s : this.suschabilitadas) {
            premiados.add(formateoNumero(s.getIdsuscripcion()));
        }
        System.out.println("A sortear >"+premiados.size());
        String json = new Gson().toJson(premiados);
        return json;
    }

    public void registrarGanador() {
        Premio p = this.premiofacade.find(this.idPremioSeleccionado);
        Suscripcion sGanador = this.suscripcionfacade.find(this.idGanador);
        p.setGanador(sGanador);
        p.setFechaHora(new Date());
        this.premiofacade.edit(p);
        if (sGanador.getCliente().getNombres() != null && !sGanador.getCliente().getNombres().isEmpty() && sGanador.getCliente().getApellidos() != null && !sGanador.getCliente().getApellidos().isEmpty()) {
            this.msgGanador = "¡Felicidades " + sGanador.getCliente().getNombres() + " " + sGanador.getCliente().getApellidos() + "! ";
        } else {
            this.msgGanador = "¡Felicidades " + sGanador.getCliente().getRazonSocial() + "!";
        }
        if (sGanador.getCliente().getCi() != null) {
            this.msgNroCi = "Documento: " + this.enmascararCi(sGanador.getCliente().getCi());
        } else {
            this.msgNroCi = "";
        }
    }

    public void insertarGanador() {
        System.out.println("premio:" + this.premioIDsuscripcionJson);
        System.out.println("sorteo:" + this.sorteoIDsuscripcionJson);
        String strpremio = premioIDsuscripcionJson.replaceAll("\"", "");
        String strsorteo = sorteoIDsuscripcionJson.replaceAll("\"", "");
        int idsusc = Integer.parseInt(strsorteo);
        int idpremio = Integer.parseInt(strpremio);
        Suscripcion susc = suscripcionfacade.find(idsusc);
        Premio premios = premiofacade.find(idpremio);

        premios.setGanador(susc);

        premiofacade.edit(premios);
    }
    private String sorteoIDsuscripcionJson;

    public String getSorteoIDsuscripcionJson() {
        return sorteoIDsuscripcionJson;
    }

    public void setSorteoIDsuscripcionJson(String sorteoIDsuscripcionJson) {
        this.sorteoIDsuscripcionJson = sorteoIDsuscripcionJson;
    }

    private String premioIDsuscripcionJson;

    public String getPremioIDsuscripcionJson() {
        return premioIDsuscripcionJson;
    }

    public void setPremioIDsuscripcionJson(String premioIDsuscripcionJson) {
        this.premioIDsuscripcionJson = premioIDsuscripcionJson;
    }
    private String idtempsorteo;

    public String getIdtempsorteo() {
        return idtempsorteo;
    }

    public void setIdtempsorteo(String idtempsorteo) {
        this.idtempsorteo = idtempsorteo;
    }

    public List<Premio> listapremioxsorteo() {
        System.out.println("idsorteotmp:" + idtempsorteo);
        List<Premio> premios = new ArrayList<>();
        if (idtempsorteo != null) {
            String idstrsorteo = idtempsorteo.replaceAll("\"", "");
            int idorteoint = Integer.parseInt(idstrsorteo);
            Sorteo s = sorteofacade.find(idorteoint);
            System.out.println("idsorteotmp2:" + idtempsorteo);
            premios = s.getPremioList();
        }
        return premios;
    }

    public void seleccionarSorteo() {
        this.lstPremios.clear();
        if (this.idSorteoSeleccionado != null) {
            System.out.println("sorteo seleccionado: " + this.idSorteoSeleccionado);
            this.lstPremios.addAll(this.premiofacade.getPremioPorSorteoOrdenado(this.idSorteoSeleccionado));
        } else {
            System.out.println("sorteo seleccionado null");
        }
        this.actualizarListaHabilitados();
    }

    public String enmascararCi(Integer ci) {
        if (ci != null) {
            String strCi = ci.toString();
            int tamanioMascara = strCi.length() - 3;
            String mascara = "";
            for (int i = 0; i < tamanioMascara; i++) {
                mascara = mascara + "X";
            }
            return mascara + strCi.substring(tamanioMascara, strCi.length());
        } else {
            return "";
        }
    }

    public void seleccionarPremio() {
        if (this.idPremioSeleccionado != null) {
            Premio p = this.premiofacade.find(this.idPremioSeleccionado);
            if (p.getGanador() != null) {
                this.idGanador = p.getGanador().getIdsuscripcion();
            } else {
                this.idGanador = null;
            }

        } else {
            this.idGanador = null;
        }
        this.actualizarListaHabilitados();
    }

}
