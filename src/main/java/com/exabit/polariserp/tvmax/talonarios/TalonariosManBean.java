package com.exabit.polariserp.tvmax.talonarios;

import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.entidades.TerminalImpresion;
import com.exabit.polariserp.tvmax.sesbeans.TalonarioFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.TerminalImpresionFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "talonariosManBean")
@ViewScoped
public class TalonariosManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(TalonariosManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private TerminalImpresionFacade terminalImpresionFacade;
    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private String selElimJSON;
    private String strFechaFin;
    private String strFechaInicio;
    private Integer idTerminalSelec;

    private String tipoMsg = "info";
    private String detalleMsg = "Y";
    private String cabeceraMsg = "X";

    private Integer timbrado;
    private String codEstablecimiento;
    private Integer nroInicio;
    private Integer nroFin;
    private Integer idTerminalImpresion;
    private String formatoImpresion;
    private boolean talonarioActivo = false;
    private Integer idTalonario;
    private Integer nroActual;
    private String busquedaTalonario = "";

    private SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getBusquedaTalonario() {
        return busquedaTalonario;
    }

    public void setBusquedaTalonario(String busquedaTalonario) {
        this.busquedaTalonario = busquedaTalonario;
    }

    public Integer getNroActual() {
        return nroActual;
    }

    public void setNroActual(Integer nroActual) {
        this.nroActual = nroActual;
    }

    public Integer getIdTalonario() {
        return idTalonario;
    }

    public void setIdTalonario(Integer idTalonario) {
        this.idTalonario = idTalonario;
    }

    public boolean getTalonarioActivo() {
        return talonarioActivo;
    }

    public void setTalonarioActivo(boolean talonarioActivo) {
        this.talonarioActivo = talonarioActivo;
    }

    public String getFormatoImpresion() {
        return formatoImpresion;
    }

    public void setFormatoImpresion(String formatoImpresion) {
        this.formatoImpresion = formatoImpresion;
    }

    public Integer getIdTerminalImpresion() {
        return idTerminalImpresion;
    }

    public void setIdTerminalImpresion(Integer idTerminalImpresion) {
        this.idTerminalImpresion = idTerminalImpresion;
    }

    public Integer getNroFin() {
        return nroFin;
    }

    public void setNroFin(Integer nroFin) {
        this.nroFin = nroFin;
    }

    public Integer getNroInicio() {
        return nroInicio;
    }

    public void setNroInicio(Integer nroInicio) {
        this.nroInicio = nroInicio;
    }

    public String getCodEstablecimiento() {
        return codEstablecimiento;
    }

    public void setCodEstablecimiento(String codEstablecimiento) {
        this.codEstablecimiento = codEstablecimiento;
    }

    public Integer getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public String getDetalleMsg() {
        return detalleMsg;
    }

    public void setDetalleMsg(String detalleMsg) {
        this.detalleMsg = detalleMsg;
    }

    public String getSelElimJSON() {
        return selElimJSON;
    }

    public void setSelElimJSON(String selElimJSON) {
        this.selElimJSON = selElimJSON;
    }

    public String getStrFechaFin() {
        return strFechaFin;
    }

    public void setStrFechaFin(String strFechaFin) {
        this.strFechaFin = strFechaFin;
    }

    public String getStrFechaInicio() {
        return strFechaInicio;
    }

    public void setStrFechaInicio(String strFechaInicio) {
        this.strFechaInicio = strFechaInicio;
    }

    public Integer getIdTerminalSelec() {
        return idTerminalSelec;
    }

    public void setIdTerminalSelec(Integer idTerminalSelec) {
        this.idTerminalSelec = idTerminalSelec;
    }
    //</editor-fold>

    public List<TalonarioFactura> getLstTalonarios() {
        if (this.busquedaTalonario.isEmpty()) {
            System.out.println("Lista completa talonario");
            return this.talonarioFacturaFacade.findAll();
        } else {
            System.out.println("busqueda talonario");
            return this.talonarioFacturaFacade.buscarTalonario(this.busquedaTalonario);
        }
    }

    public void eliminarTalonarios() {
        try {
            Gson gson = new Gson();
            Integer[] lstIdElim = gson.fromJson(this.selElimJSON, Integer[].class);
            for (Integer idtal : lstIdElim) {
                TalonarioFactura t = this.talonarioFacturaFacade.find(idtal);
                this.talonarioFacturaFacade.remove(t);
            }
            this.tipoMsg="info";
            this.cabeceraMsg="Éxito";
            this.detalleMsg="Talonarios eliminados correctamente";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al eliminar", ex);
            this.tipoMsg = "negative";
            this.cabeceraMsg = "Error al eliminar";
            this.detalleMsg = ex.getMessage();
        }
    }

    public List<TerminalImpresion> getLstTerminalesImpresion() {
        return this.terminalImpresionFacade.findAll();
    }

    private TalonarioFactura toNew(TalonarioFactura tf) {
        TalonarioFactura tal = new TalonarioFactura();
        tal.setActivo(tf.getActivo());
        tal.setCodEstablecimiento(tf.getCodEstablecimiento());
        tal.setFacturaList(tf.getFacturaList());
        tal.setFinVigencia(tf.getFinVigencia());
        tal.setIdtalonario(tf.getIdtalonario());
        tal.setInicioVigencia(tf.getInicioVigencia());
        tal.setNombreFormato(tf.getNombreFormato());
        tal.setNroActual(tf.getNroActual());
        tal.setNroFin(tf.getNroFin());
        tal.setNroInicio(tf.getNroInicio());
        tal.setTerminalImpresion(tf.getTerminalImpresion());
        tal.setTimbrado(tf.getTimbrado());
        return tal;
    }

    public void guardarTalonario() {

        try {
            TalonarioFactura tf = new TalonarioFactura();
            tf.setIdtalonario(this.idTalonario);
            tf.setTimbrado(this.timbrado);
            tf.setCodEstablecimiento(this.codEstablecimiento);
            tf.setInicioVigencia(sdf.parse(this.strFechaInicio));
            tf.setFinVigencia(sdf.parse(this.strFechaFin));
            tf.setNroInicio(this.nroInicio);
            tf.setNroFin(this.nroFin);
            tf.setNroActual(this.nroActual);
            tf.setTerminalImpresion(this.terminalImpresionFacade.find(this.idTerminalImpresion));
            tf.setNombreFormato(this.formatoImpresion);
            tf.setActivo(this.talonarioActivo);
            System.out.println("talonarioactivo" + this.talonarioActivo);
            if (tf.getIdtalonario() == null) {
                this.talonarioFacturaFacade.create(tf);
                this.idTalonario = tf.getIdtalonario();
            } else {
                this.talonarioFacturaFacade.edit(tf);
            }

            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.detalleMsg = "Talonario guardado";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al guardar talonario", ex);
            this.tipoMsg = "negative";
            this.cabeceraMsg = "Error al guardar";
            this.detalleMsg = ex.getMessage();
        }
    }

    private void cargarDatosTalonario(Integer id) {
        TalonarioFactura tf = this.talonarioFacturaFacade.find(id);
        if (tf != null) {
            this.timbrado = tf.getTimbrado();
            this.codEstablecimiento = tf.getCodEstablecimiento();
            this.strFechaInicio = this.sdf.format(tf.getInicioVigencia());
            this.strFechaFin = this.sdf.format(tf.getFinVigencia());
            this.nroInicio = tf.getNroInicio();
            this.nroFin = tf.getNroFin();
            this.nroActual = tf.getNroActual();
            this.idTerminalImpresion = tf.getTerminalImpresion().getIdterminalImpresion();
            this.formatoImpresion = tf.getNombreFormato();
            this.talonarioActivo = tf.getActivo();
        } else {
            this.limpiarDatosTalonario();
        }
    }

    public void limpiarDatosTalonario() {
        this.idTalonario = null;
        this.timbrado = null;
        this.codEstablecimiento = null;
        this.strFechaInicio = null;
        this.strFechaFin = null;
        this.nroInicio = null;
        this.nroFin = null;
        this.nroActual = null;
        this.idTerminalImpresion = null;
        this.formatoImpresion = null;
        this.talonarioActivo = false;
    }

    public void seleccionarTalonario() {
        if (this.idTalonario != null) {
            this.cargarDatosTalonario(this.idTalonario);
        } else {
            this.limpiarDatosTalonario();
        }
        System.out.println("Seleccionar talonario: " + this.idTalonario);
    }
}
