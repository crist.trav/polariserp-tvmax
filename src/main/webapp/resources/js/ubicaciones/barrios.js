$(document).ready(function () {
    limpiarSeleccion();
    limpiarCampos();
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    initCmbCantReg();
    $("#cmbCantReg").dropdown("set selected", 20);
});

function ajaxBusqueda(data) {
    if (data.status === 'begin') {
        $('#dim-tabla').dimmer('show');
        $('#icono-busqueda').removeClass('search');
        $('#icono-busqueda').addClass('spinner');
        $('#icono-busqueda').addClass('loading');
    } else {
        $('#dim-tabla').dimmer('hide');
        $('#icono-busqueda').addClass('search');
        $('#icono-busqueda').removeClass('spinner');
        $('#icono-busqueda').removeClass('loading');
    }
}
var timeOut = 0;
$('#txf-buscar').on('input', function () {
    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        $('#cl-buscar').click();
        modoNuevo();
    }, 500);
});


function limpiarCampos() {
    $("#in-idbarrio").val("");
    $("#in-nombrebarrio").val("");
    $("#cmbZona").dropdown("clear");
}

function limpiarSeleccion() {
    $("[id^='fila-']").removeClass("active");
    $("[id^='chk-']").prop("checked", false);
}

function seleccionUnica(idbarrio) {
    limpiarSeleccion();
    $("#in-idbarrio").val(idbarrio);
    $("#chk-" + idbarrio).prop("checked", true);
    $("#fila-" + idbarrio).addClass("active");
    $("#cmlCargarDatosBarrio").click();
    evaluarMostrarEliminar();
}

function setTipoMsg(tipo) {
    $("#msg").removeClass("info");
    $("#msg").removeClass("error");
    $("#msg").removeClass("warning");
    $("#msg").removeClass("negative");
    $("#msg").removeClass("positive");
    $("#msg").addClass(tipo);
}

function mostrarMsg(tipo, cabecera, cuerpo) {
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);

    setTipoMsg(tipo);
    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function agregarSeleccion(idbarrio) {
    $("#chk-" + idbarrio).prop("checked", !$("#chk-" + idbarrio).prop("checked"));
    if ($("#chk-" + idbarrio).prop("checked")) {
        $("#fila-" + idbarrio).addClass("active");
    } else {
        $("#fila-" + idbarrio).removeClass("active");

    }
    evaluarMostrarEliminar();
}

function ajaxCargarDatosBarrio(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbZona").dropdown();
            initCmbCantReg();
            break;
        }
    }
}

function ajaxGuardarBarrio(data) {
    var status = data.status;
    switch (status) {
        case "begin":
        {
            $("#cmlGuardarBarrio").addClass("disabled");
            $("#cmlGuardarBarrio").addClass("loading");
            break;
        }
        case "success":
        {
            initCmbCantReg();
            $("#cmlGuardarBarrio").removeClass("disabled");
            $("#cmlGuardarBarrio").removeClass("loading");
            setTipoMsg($("#tipoMsg").val());
            if ($("#in-idbarrio").val() !== "") {
                seleccionUnica($("#in-idbarrio").val());
            }
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function validar() {
    var validado = true;
    if ($("#in-nombrebarrio").val() === "") {
        validado = false;
        $("#fieldNombreBarrio").addClass("error");
    } else {
        $("#fieldNombreBarrio").removeClass("error");
    }
    if ($("#in-idzona").val() === "") {
        validado = false;
        $("#fieldZonaBarrio").addClass("error");
    } else {
        $("#fieldZonaBarrio").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
    }
    return validado;
}

function ajaxNuevoBarrio(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            limpiarSeleccion();
            $("#cmbZona").dropdown();
            if (!$("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function mostrarDialogoConfElim() {
    var ids = new Array;
    $("#elementos-tabla tbody tr.active [id^='col-idbarrio-']").each(function (index, value) {
        ids.push(parseInt($(this).html()));
    });
    $("#selbar").val(JSON.stringify(ids));
    $("#modalConfElim").modal("show");
}

function evaluarMostrarEliminar() {
    if ($("[id^='chk-']:checked").length !== 0) {
        $("#btnEliminar").show();
    } else {
        $("#btnEliminar").hide();
    }
}

function ajaxEliminarBarrios(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            
            limpiarCampos();
            $("#modalConfElim").modal("hide");
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            initCmbCantReg();
            break;
        }
    }
}

function seleccionarTodo() {
    $("[id^='fila-']").addClass("active");
    $("[id^='chk-']").prop("checked", true);
}

function seleccionMainCheck() {
    if ($("#chk-selec-todo").prop("checked")) {
        seleccionarTodo();
    } else {
        limpiarSeleccion();
    }
}

function initCmbCantReg(){
    $("#cmbCantReg").dropdown({
       onChange: function(){
           $("#cml-cambio-cantreg").click();
       } 
    });
}

function ajaxPaginador(data){
    var status=data.status;
    switch(status){
        case "success":{
                initCmbCantReg();
                break;
        }
    }
}