package com.exabit.polariserp.tvmax.util;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author traver
 */
@Named(value = "navegacionManBean")
@RequestScoped
public class NavegacionManBean {
    
//    private final Map<String, String> mapaDirecciones=new HashMap<>();
    private final String indexUrl = "/faces/index.xhtml";
    private final String loginUrl = "/faces/login.xhtml";
    private final String restPath= "/api";

    /**
     * Creates a new instance of NavegacionManBean
     */
    public NavegacionManBean() {
//        this.mapaDirecciones.put("barrios", "/"+this.nombreApp+"/faces/ubicaciones/barrios.xhtml");
//        this.mapaDirecciones.put("funcionario", "/"+this.nombreApp+"/faces/funcionario/funcionario.xhtml");
//        this.mapaDirecciones.put("cargo", "/"+this.nombreApp+"/faces/funcionario/cargo.xhtml");
//        this.mapaDirecciones.put("categoriaC", "/"+this.nombreApp+"/faces/cliente/categoria_Cliente.xhtml");
//        this.mapaDirecciones.put("cliente", "/"+this.nombreApp+"/faces/cliente/cliente.xhtml");
//        this.mapaDirecciones.put("servicios", "/"+this.nombreApp+"/faces/servicios/servicios.xhtml");
//        this.mapaDirecciones.put("zonas", "/"+this.nombreApp+"/faces/ubicaciones/zonas.xhtml");
//        this.mapaDirecciones.put("suscripciones", "/"+this.nombreApp+"/faces/servicios/suscripciones.xhtml");
//        this.mapaDirecciones.put("modulo", "/"+this.nombreApp+"/faces/modulos/modulos.xhtml");
//        this.mapaDirecciones.put("fun", "/"+this.nombreApp+"/faces/modulos/funcionalidad.xhtml");
//        this.mapaDirecciones.put("permiso", "/"+this.nombreApp+"/faces/modulos/permisos.xhtml");
//        this.mapaDirecciones.put("cartera-de-clientes", "/"+this.nombreApp+"/faces/funcionario-cliente/funcionario-cliente.xhtml");
//        this.mapaDirecciones.put("registrocobros", "/"+this.nombreApp+"/faces/cobros/registrocobros.xhtml");
//        this.mapaDirecciones.put("talonarios", "/"+this.nombreApp+"/faces/facturas/talonarios.xhtml");
//        this.mapaDirecciones.put("facturas", "/"+this.nombreApp+"/faces/facturas/facturas.xhtml");
//        this.mapaDirecciones.put("cuotasfacturas", "/"+this.nombreApp+"/faces/servicios/cuotas-facturas.xhtml");
//        this.mapaDirecciones.put("asignacionfacturas", "/"+this.nombreApp+"/faces/facturas/asignacioncobradores.xhtml");
//        this.mapaDirecciones.put("inicio", "/"+this.nombreApp);
//        this.mapaDirecciones.put("cobros", "/"+this.nombreApp+"/faces/cobros/cobros.xhtml");
//        this.mapaDirecciones.put("detallesuscripcion", "/"+this.nombreApp+"/faces/servicios/detallesuscripcion.xhtml");
//        this.mapaDirecciones.put("solicitudcambioestado", "/"+this.nombreApp+"/faces/servicios/estados-suscripciones.xhtml");
//        this.mapaDirecciones.put("ajustes", "/"+this.nombreApp+"/faces/ajustes/ajustes.xhtml");
//        this.mapaDirecciones.put("importar", "/"+this.nombreApp+"/faces/ajustes/importar.xhtml");
//        this.mapaDirecciones.put("sorteo", "/"+this.nombreApp+"/faces/sorteo/sorteo.xhtml");
//        this.mapaDirecciones.put("premio", "/"+this.nombreApp+"/faces/premio/pemio.xhtml");
//        this.mapaDirecciones.put("excluido", "/"+this.nombreApp+"/faces/premio/excluidos.xhtml");
    }
    
    public String getIndexUrl(){
        String fullUrl=this.getRootPath()+this.indexUrl;
        System.out.println("index url"+fullUrl);
        return fullUrl;
    }
    
    public String getLoginUrl(){
        String fullUrl=this.getRootPath()+this.loginUrl;
        System.out.println("index url"+fullUrl);
        return fullUrl;
    }
    
    public String getRootPath(){
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
    }
    
    public String getRestApiPath(){
        String fullPath=this.getRootPath()+this.restPath;
        return fullPath;
    }
    
//    public String getDireccionNav(String modulo){
//        return this.mapaDirecciones.get(modulo);
//    }
}
