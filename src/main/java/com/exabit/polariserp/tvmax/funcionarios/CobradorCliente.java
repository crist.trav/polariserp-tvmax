/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.funcionarios;

import java.util.Date;

/**
 *
 * @author gemacron
 */
public class CobradorCliente {
    private Integer suscripcion;
    private String cliente;
    private String direccion;
    private Date ultimopago;
    private Date fechadevencimiento;
    private Integer cuota;
    private Integer meses;
    private Integer deuda;

    public Integer getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Integer suscripcion) {
        this.suscripcion = suscripcion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getUltimopago() {
        return ultimopago;
    }

    public void setUltimopago(Date ultimopago) {
        this.ultimopago = ultimopago;
    }

    public Date getFechadevencimiento() {
        return fechadevencimiento;
    }

    public void setFechadevencimiento(Date fechadevencimiento) {
        this.fechadevencimiento = fechadevencimiento;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Integer getMeses() {
        return meses;
    }

    public void setMeses(Integer meses) {
        this.meses = meses;
    }

    public Integer getDeuda() {
        return deuda;
    }

    public void setDeuda(Integer deuda) {
        this.deuda = deuda;
    }
    
    
}
