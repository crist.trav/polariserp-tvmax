package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.HistorialGeneracionCuotas;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class CuotaFacade extends AbstractFacade<Cuota> {

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CuotaFacade() {
        super(Cuota.class);
    }

    public List<Cuota> getCuotaOrdenVencimiento(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=true AND c.suscripcion=:susc ORDER BY c.fechaVencimiento DESC", Cuota.class);
//        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.suscripcion=:susc ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotaOrdenVencimiento(Suscripcion s, Short anio) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=true AND c.anioCuota=:anio AND c.suscripcion=:susc ORDER BY c.fechaVencimiento DESC", Cuota.class);
//        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.anioCuota=:anio AND c.suscripcion=:susc ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("susc", s);
        q.setParameter("anio", anio);
        return q.getResultList();
    }

    public List<Cuota> getUltimaFechadePago(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.suscripcion=:sus ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("sus", s);
        List<Cuota> c = q.getResultList();
        return c;
    }

    public List<Cuota> getCuotasPendientesPrincipal(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientesPrincASCFechaVenc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendYPagadosPronetPrinc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=true AND c.suscripcion=:susc ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        List<Cuota> lstc = q.getResultList();
        List<Cuota> lstcretorno = new ArrayList<>();
        for (Cuota c : lstc) {
            this.em.refresh(c);
            if (!c.getCancelado()) {
                lstcretorno.add(c);
            } else {
                if (c.getDetalleFacturaList().isEmpty()) {
                    TypedQuery<Cobro> qc = this.em.createQuery("SELECT co FROM Cobro co WHERE co.cuota=:cuota AND co.anulado=false", Cobro.class);
                    qc.setParameter("cuota", c);
                    List<Cobro> lstco = qc.getResultList();
                    if (!lstco.isEmpty()) {
                        lstcretorno.add(c);
                    }
                }
            }
        }
        return lstcretorno;
    }

    public List<Cuota> getCuotasPendientesASCFechaVenc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=true AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientesSecundDESCFechaVenc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=false AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientesSecundASCFechaVenc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=false AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendSecundYPagSinFactASCFechaVenc(Suscripcion s) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=false AND c.suscripcion=:susc ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        List<Cuota> lstc = q.getResultList();
        List<Cuota> lstcretorno = new ArrayList<>();
        for (Cuota c : lstc) {
            this.em.refresh(c);
            if (!c.getCancelado()) {
                lstcretorno.add(c);
            } else {
                if (c.getDetalleFacturaList().isEmpty()) {
                    System.out.println("Cuota cancelada " + c.getIdcuota() + " sin detalle factura ");
                    lstcretorno.add(c);
                } else {
                    boolean existeSinAnular = false;
                    for (DetalleFactura df : c.getDetalleFacturaList()) {
                        System.out.println("cuota " + c.getIdcuota() + " detallefactura: " + df.getIddetalleFactura());
                        if (!df.getFactura().getAnulado() && df.getFactura().getCancelado()) {
                            System.out.println("Existe cancelado sin anular " + df.getFactura().getIdfactura());
                            existeSinAnular = true;
                            break;
                        }
                    }
                    System.out.println("Existe sin anular: " + existeSinAnular);
                    if (!existeSinAnular) {
                        lstcretorno.add(c);
                    }
                }
            }
        }
        return lstcretorno;
    }

    public List<Cuota> getCuotasPendientesSecundASCFechaVenc(Suscripcion s, Servicio serv) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=false AND c.servicio=:serv AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Cuota.class);
        q.setParameter("susc", s);
        q.setParameter("serv", serv);
        return q.getResultList();
    }

    public List<Cuota> getCuotaPendientes(Suscripcion s, Date desde, Date hasta) {
        TypedQuery q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=true AND c.fechaVencimiento>=:desde AND c.fechaVencimiento<=:hasta AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("susc", s);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientesOtrosServicios(Suscripcion s) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:s AND c.servicio.suscribible=false AND c.cancelado=false", Cuota.class);
        q.setParameter("s", s);
        return q.getResultList();
    }
    public List<Cuota> getCuotasPendientesSecHastaFecha(Suscripcion s, Date hasta) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:s AND c.servicio.suscribible=false AND c.cancelado=false AND c.fechaVencimiento<=:hasta", Cuota.class);
        q.setParameter("s", s);
        q.setParameter("hasta", hasta);
        return q.getResultList();
    }

    public boolean existenCuotasPeriodo(Integer mes, Integer anio) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.mesCuota=:mes AND c.anioCuota=:anio", Cuota.class);
        q.setParameter("mes", mes.shortValue());
        q.setParameter("anio", anio.shortValue());
        List<Cuota> lstCuotas = q.getResultList();
        for (Cuota c : lstCuotas) {
            System.out.println(c.getIdcuota() + "-" + c.getAnioCuota() + " " + c.getMesCuota());
        }
        System.out.println("Cantidad cuotas: " + lstCuotas.size());
        return !lstCuotas.isEmpty();
    }

    public boolean existenCuotasPeriodo(Suscripcion s, Integer mes, Integer anio) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE (c.servicio=c.suscripcion.servicio OR c.servicio IS NULL) AND c.suscripcion=:sus AND c.mesCuota=:mes AND c.anioCuota=:anio", Cuota.class);
        q.setParameter("mes", mes.shortValue());
        q.setParameter("anio", anio.shortValue());
        q.setParameter("sus", s);
        List<Cuota> lstCuotas = q.getResultList();
        for (Cuota c : lstCuotas) {
            System.out.println(c.getIdcuota() + "-" + c.getAnioCuota() + " " + c.getMesCuota());
        }
        System.out.println("Cantidad cuotas: " + lstCuotas.size());
        return !lstCuotas.isEmpty();
    }

    public List<Cuota> getCuotasPendientes(Cliente c, Integer anioInicio, Integer mesInicio) {
        System.out.println("get cuotas pendiente: " + c + " " + anioInicio + " " + mesInicio);
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion.cliente=:cli AND c.cancelado=false AND c.anioCuota<=:anio AND c.mesCuota<=:mes", Cuota.class);
//        TypedQuery<Cuota> q=this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion.cliente=:cli", Cuota.class);
        q.setParameter("cli", c);
        q.setParameter("anio", anioInicio);
        q.setParameter("mes", mesInicio);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientes(Suscripcion s, Integer anioInicio, Integer mesInicio) {
        System.out.println("get cuotas pendiente: " + s + " " + anioInicio + " " + mesInicio);
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.cancelado=false AND c.anioCuota<=:anio AND c.mesCuota<=:mes", Cuota.class);
//        TypedQuery<Cuota> q=this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion.cliente=:cli", Cuota.class);
        q.setParameter("sus", s);
        q.setParameter("anio", anioInicio);
        q.setParameter("mes", mesInicio);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientes(Suscripcion s) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("sus", s);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientes(Suscripcion s, Short anio, Integer mes) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.anioCuota=:anio AND c.mesCuota=:mes AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("sus", s);
        q.setParameter("anio", anio);
        q.setParameter("mes", mes);
        return q.getResultList();
    }

    public List<Cuota> getCuotasPendientes(Cliente c) {
        System.out.println(c);
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.cancelado=false AND c.suscripcion.cliente=:cli ORDER BY c.fechaVencimiento DESC", Cuota.class);
        q.setParameter("cli", c);
        return q.getResultList();
    }

    public int getCantidadCuotasPendientes(Suscripcion s) {
        return this.getCuotasPendientesPrincipal(s).size();
    }

    public int getCantidadCuotasPendientes(Suscripcion s, Date desde, Date hasta) {
        return this.getCuotaPendientes(s, desde, hasta).size();
    }

    public int getMontoDeuda(Suscripcion s) {
        List<Cuota> lstc = this.getCuotasPendientesPrincipal(s);
        int total = 0;
        for (Cuota c : lstc) {
            if (c.getMontoEntrega() != null) {
                total = total + (c.getMontoCuota() - c.getMontoEntrega());
            } else {
                total = total + c.getMontoCuota();
            }
        }
        return total;
    }

    public int getMontoDeudaOtrosServicios(Suscripcion s) {
        List<Cuota> lstc = this.getCuotasPendientesOtrosServicios(s);
        int total = 0;
        for (Cuota c : lstc) {
            total = total + (c.getMontoCuota() - c.getMontoEntrega());
        }
        return total;
    }

    public int getTotalDeuda(Suscripcion s, Date desde, Date hasta) {
        List<Cuota> lstc = this.getCuotaPendientes(s, desde, hasta);
        int total = 0;
        for (Cuota c : lstc) {
            if (c.getMontoEntrega() != null) {
//                if(c.getMontoCuota()!=null){
                total = total + (c.getMontoCuota() - c.getMontoEntrega());
//                }
            } else {
                total = total + c.getMontoCuota();
            }
        }
        return total;
    }
    public int getDeudaSuscripcion(Suscripcion s, Date desde, Date hasta) {
        List<Cuota> lstc = this.getCuotaPendientes(s, desde, hasta);
        int total = 0;
        for (Cuota c : lstc) {
            if (c.getMontoEntrega() != null) {
//                if(c.getMontoCuota()!=null){
                total = total + (c.getMontoCuota() - c.getMontoEntrega());
//                }
            } else {
                total = total + c.getMontoCuota();
            }
        }
        return total;
    }

    public Date getFechaUltimoPago(Suscripcion s) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaPago FROM Cuota c WHERE c.servicio.suscribible=true AND c.suscripcion=:sus AND c.fechaPago IS NOT NULL order by c.fechaPago DESC", Date.class);
        q.setParameter("sus", s);
        List<Date> lstf = q.getResultList();
        if (lstf.isEmpty()) {
            return null;
        } else {
            return lstf.get(0);
        }
    }
    public Date getFechaUltimoPagoOtrosServ(Suscripcion s) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaPago FROM Cuota c WHERE c.servicio.suscribible=false AND c.suscripcion=:sus AND c.fechaPago IS NOT NULL order by c.fechaPago DESC", Date.class);
        q.setParameter("sus", s);
        List<Date> lstf = q.getResultList();
        if (lstf.isEmpty()) {
            return null;
        } else {
            return lstf.get(0);
        }
    }

    public Date getFechaCuotaPendienteAntigua(Suscripcion s) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaVencimiento FROM Cuota c WHERE c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Date.class);
        q.setParameter("susc", s);
        List<Date> lstd = q.getResultList();
        if (lstd.isEmpty()) {
            return null;
        } else {
            return lstd.get(0);
        }
    }
    public Date getFechaCuotaPendienteAntiguaOtrosServ(Suscripcion s, Date hasta) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaVencimiento FROM Cuota c WHERE c.fechaVencimiento <= :hasta AND c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento ASC", Date.class);
        q.setParameter("susc", s);
        q.setParameter("hasta", hasta);
        List<Date> lstd = q.getResultList();
        if (lstd.isEmpty()) {
            return null;
        } else {
            return lstd.get(0);
        }
    }

    public Integer getUltimoPrecioCuotaPend(Suscripcion s) {
        TypedQuery<Integer> q = this.em.createQuery("SELECT c.montoCuota FROM Cuota c WHERE c.suscripcion=:susc AND c.cancelado=false ORDER BY c.fechaVencimiento DESC", Integer.class);
        q.setParameter("susc", s);
        List<Integer> lstm = q.getResultList();
        if (lstm.isEmpty()) {
            return null;
        } else {
            return lstm.get(0);
        }
    }

    public void exonerarCuota(Suscripcion s, int anio, int mes) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.anioCuota=:anio AND c.mesCuota=:mes", Cuota.class);
        q.setParameter("anio", anio);
        q.setParameter("mes", mes);
        q.setParameter("sus", s);
        List<Cuota> lstc = q.getResultList();
        if (lstc.isEmpty()) {
            Cuota c = new Cuota();
            c.setSuscripcion(s);
            c.setMontoCuota(0);
            c.setMontoEntrega(0);
            c.setCancelado(true);
            Calendar calven = new GregorianCalendar(anio, mes - 1, 1);
            c.setFechaVencimiento(calven.getTime());
            c.setAnioCuota((short) anio);
            c.setMesCuota((short) mes);
            this.create(c);
        }
    }

    //DEBE COMPROBAR SOLO LAS CUOTAS PRINCIPALES DEL LA SUSCRIPCION, EXCLUIR OTRAS CUOTAS
    public Date getFechaCuotaReciente(Suscripcion s) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaVencimiento FROM Cuota c WHERE c.suscripcion=:susc ORDER BY c.fechaVencimiento DESC", Date.class);
        q.setParameter("susc", s);
        List<Date> lstf = q.getResultList();

        if (!lstf.isEmpty()) {
            return lstf.get(0);
        } else {
            return null;
        }
    }

    public Date getFechaCuotaRecientePrincipal(Suscripcion s) {
        TypedQuery<Date> q = this.em.createQuery("SELECT c.fechaVencimiento FROM Cuota c WHERE c.suscripcion=:susc AND c.suscripcion.servicio=c.servicio ORDER BY c.fechaVencimiento DESC", Date.class);
        q.setParameter("susc", s);
        List<Date> lstf = q.getResultList();

        if (!lstf.isEmpty()) {
            return lstf.get(0);
        } else {
            return null;
        }
    }

    public List<Cuota> getCuotasPendientesAnteriores(Cuota c) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.fechaVencimiento<:fc AND c.cancelado=false AND c.suscripcion=:susc", Cuota.class);
        q.setParameter("fc", c.getFechaVencimiento());
        q.setParameter("susc", c.getSuscripcion());
        return q.getResultList();
    }

    public List<Suscripcion> getSuscHabilitadasSorteo(Integer anio, Integer mes) {
        Calendar fantiguedad = new GregorianCalendar(anio, mes - 1, 1);
        fantiguedad.add(Calendar.MONTH, -6);
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio = true AND s.fechaSuscripcion <= :fanti AND s.tipoSuscripcion = :tiposus AND EXISTS (SELECT c FROM Cuota c WHERE c.cancelado=true AND c.anioCuota=:anio AND c.mesCuota=:mes AND c.montoCuota!=0 AND c.suscripcion = s) ORDER BY s.cliente.razonSocial, s.fechaSuscripcion ASC", Suscripcion.class);
        //TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.cancelado=true AND c.anioCuota=:anio AND c.mesCuota=:mes AND c.montoCuota!=0", Cuota.class);
        qs.setParameter("anio", anio);
        qs.setParameter("mes", mes);
        qs.setParameter("fanti", fantiguedad.getTime());
        qs.setParameter("tiposus", this.em.find(TipoSuscripcion.class, 1));
        List<Suscripcion> suscripciones = new ArrayList<>();
        List<Suscripcion> lstSustmp = qs.getResultList();
        
        for(Suscripcion shab:lstSustmp){
            boolean yaEstaCliente = false;
            for(Suscripcion s: suscripciones){
                if(s.getCliente().getIdcliente().equals(shab.getCliente().getIdcliente())){
                    yaEstaCliente = true;
                    break;
                }
            }
            if(!yaEstaCliente){
                suscripciones.add(shab);
            }
        }
        
        System.out.println("Cantidad de suscripciones: "+suscripciones.size());
        return suscripciones;
    }

    public void generarCuotasOtrosServicios(Suscripcion susc, Servicio serv, Date primerVenc, Integer montocuo, Integer cantCuo) {
        Calendar cpv = new GregorianCalendar();
        if (primerVenc != null) {
            cpv.setTime(primerVenc);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Primer venc otras cuotas: " + sdf.format(cpv.getTime()));
        HistorialGeneracionCuotas hg = new HistorialGeneracionCuotas();
        hg.setFechaHora(new Date());
        hg.setCuotaList(new ArrayList<>());
        for (int i = 1; i <= cantCuo; i++) {
            Cuota c = new Cuota();
            c.setAnioCuota((short) cpv.get(Calendar.YEAR));
            c.setMesCuota((short) (cpv.get(Calendar.MONTH) + 1));
            c.setFechaVencimiento(cpv.getTime());
            c.setSuscripcion(susc);
            c.setServicio(serv);
            c.setCancelado(false);
            c.setMontoCuota(montocuo);
            c.setMontoEntrega(0);
            c.setNroCuota(i);

            this.em.persist(c);
            hg.getCuotaList().add(c);
            cpv.add(Calendar.MONTH, 1);
        }
        hg.setCantidadCuotas(hg.getCuotaList().size());
        this.em.persist(hg);
        for (Cuota cu : hg.getCuotaList()) {
            cu.setHistorialGeneracion(hg);
            this.em.merge(cu);
        }
    }

    public List<Cuota> getCuotasOtrosServicios(Suscripcion s) {
        TypedQuery<Cuota> q = getEntityManager().createQuery("SELECT c FROM Cuota c WHERE c.servicio.suscribible=false AND c.suscripcion=:susc ORDER BY c.nroCuota, c.servicio DESC", Cuota.class);
        q.setParameter("susc", s);
        return q.getResultList();
    }

//    public Integer generarCuotas(List<Suscripcion> lstSusc, Date fdesde, Date fhasta, boolean inactivainactauto, boolean generarsolorecientes) {        
//        List<Cuota> lstCuotasGeneradas = new LinkedList<>();
//        HistorialGeneracionCuotas hgc = new HistorialGeneracionCuotas();
//        hgc.setFechaHora(new Date());
//        this.hgcFacade.create(hgc);
//        for (Suscripcion s : lstSusc) {
//            System.out.println("Creando cuotas para suscripcion: " + s.getIdsuscripcion());
//            lstCuotasGeneradas.addAll(this.generarCuotas(s, fdesde, fhasta, inactivainactauto));
//        }
//        hgc.setCantidadCuotas(this.lstCuotasGeneradas.size());
//        this.hgcFacade.edit(hgc);
//        System.out.println("Fin generacion de cuotas");
//        this.generarCuotasSesBeanSingleton.setGenerando(false);
//        this.generarCuotasSesBeanSingleton.enviarPorcentajeProgreso(100);
//        return totalcuotas;
//    }
    public List<Cuota> generarCuotas(Suscripcion s, Date desde, Date hasta, Integer montocuota) {
        List<Cuota> lstCuotasGeneradas = new LinkedList<>();
        Calendar calDesde = new GregorianCalendar();
        calDesde.setTime(desde);
        Calendar calHasta = new GregorianCalendar();
        calHasta.setTime(hasta);
        System.out.println("cal compare: " + calDesde.compareTo(calHasta));
        while (calDesde.compareTo(calHasta) <= 0) {
            int mes = calDesde.get(Calendar.MONTH) + 1;
            int anio = calDesde.get(Calendar.YEAR);
            System.out.println("Se genera para " + mes + " " + anio);
            lstCuotasGeneradas.addAll(this.generarCuotas(s, mes, anio, montocuota, false));
            calDesde.add(Calendar.MONTH, 1);
        }
        return lstCuotasGeneradas;
    }

    public List<Cuota> generarCuotas(Suscripcion s, Integer mes, Integer anio, Integer montocuota, boolean inactauto, boolean generarsolorecientes) {
        if (inactauto) {
            TypedQuery<Cuota> qc = this.em.createQuery("SELECT c FROM Cuota c WHERE c.cancelado=false AND c.suscripcion=:s", Cuota.class);
            qc.setParameter("s", s);
            List<Cuota> lstCuo = qc.getResultList();
            if (lstCuo.size() >= 4) {
                SolicitudCambioEstado sce = new SolicitudCambioEstado();
                sce.setConcretado(true);
                sce.setEstadoAnterior(s.getEstado());
                sce.setEstadoNuevo(EntidadesEstaticas.ESTADO_RECONECTADO);
                sce.setFechaConcrecion(new Date());
                sce.setFechaSolicitud(new Date());
                sce.setObservacion("Desactivación automática. Más de 4 cuotas sin pago.");
                sce.setSuscripcion(s);
                s.setEstado(EntidadesEstaticas.ESTADO_RECONECTADO);
                this.em.persist(sce);
                this.em.merge(s);
                return new LinkedList<>();
            } else {
                return this.generarCuotas(s, mes, anio, montocuota, generarsolorecientes);
            }
        } else {
            System.out.println("se genera cuotas por el false");
            return this.generarCuotas(s, mes, anio, montocuota, generarsolorecientes);
        }
    }

    private List<Cuota> generarCuotas(Suscripcion s, Integer mes, Integer anio, Integer montocuota, boolean generarsolorecientes) {
        List<Cuota> lstCuotasGeneradas = new LinkedList<>();
        System.out.println("genrerar cuota final boss");
        Calendar calsus = new GregorianCalendar();
        calsus.setTime(s.getFechaSuscripcion());

        int messus = calsus.get(Calendar.MONTH) + 1;
        int aniosus = calsus.get(Calendar.YEAR);
        Calendar calgeneracion = new GregorianCalendar(anio, mes - 1, 1);
        Calendar ahora = new GregorianCalendar();
        if (ahora.get(Calendar.YEAR) == anio && ahora.get(Calendar.MONTH) == (mes - 1)) {
            calgeneracion.set(Calendar.DAY_OF_MONTH, ahora.get(Calendar.DAY_OF_MONTH));
        }
        System.out.println("Mes suscripcion: " + messus + " anio susc: " + aniosus);
        boolean comparacionCuotaReciente = true;
        if (generarsolorecientes) {
            Date fecUltCuota = this.getFechaCuotaReciente(s);
            if (fecUltCuota != null) {
                Calendar calCuoRec = new GregorianCalendar();
                calCuoRec.setTime(fecUltCuota);
                calCuoRec.set(Calendar.DAY_OF_MONTH, 1);
                calsus = new GregorianCalendar(calCuoRec.get(Calendar.YEAR), calCuoRec.get(Calendar.MONTH), 1);
            }
            comparacionCuotaReciente = calgeneracion.compareTo(calsus) >= 0;
        }

        Integer cantCuotas = 0;
        if (comparacionCuotaReciente) {
            if (!this.existenCuotasPeriodo(s, mes, anio)) {
                double descuento = s.getTipoSuscripcion().getPorcentajeDescuento();
                if (mes == messus && anio == aniosus) {
                    if (descuento < 100) {
                        Cuota cuo = new Cuota();
                        cuo.setSuscripcion(s);
                        cuo.setServicio(s.getServicio());
                        cuo.setCancelado(false);
                        Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                        cuo.setFechaVencimiento(new Date(calvenc.getTimeInMillis()));
                        cuo.setMesCuota(mes.shortValue());
                        cuo.setAnioCuota(anio.shortValue());
                        cuo.setNroCuota(0);
                        double precionormal = 0.0;
                        if (montocuota == null) {
                            precionormal = s.getPrecio();
                        } else {
                            precionormal = montocuota;
                        }
                        double montoDescuento = (precionormal * descuento) / (double) 100;
                        Integer montoFinalCuota = (int) Math.round(precionormal - montoDescuento);

                        cuo.setMontoCuota(montoFinalCuota);

                        if (descuento == (double) 100) {
                            cuo.setCancelado(true);
                        }
                        cuo.setMontoEntrega(0);
                        this.em.persist(cuo);
                        lstCuotasGeneradas.add(cuo);
                        cantCuotas = cantCuotas + 1;
                    } else {
                        Cuota cuo = new Cuota();
                        cuo.setSuscripcion(s);
                        cuo.setServicio(s.getServicio());
                        cuo.setCancelado(true);
                        Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                        cuo.setFechaVencimiento(calvenc.getTime());
                        cuo.setMesCuota(mes.shortValue());
                        cuo.setAnioCuota(anio.shortValue());
                        cuo.setFechaPago(calvenc.getTime());
                        cuo.setMontoCuota(0);
                        cuo.setMontoEntrega(0);
                        cuo.setNroCuota(0);
                        this.em.persist(cuo);
                        lstCuotasGeneradas.add(cuo);
                        cantCuotas = cantCuotas + 1;
                    }
                } else {
                    if (descuento < 100) {
                        Cuota cuo = new Cuota();
                        cuo.setSuscripcion(s);
                        cuo.setServicio(s.getServicio());
                        cuo.setCancelado(false);
                        Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                        cuo.setFechaVencimiento(new Date(calvenc.getTimeInMillis()));
                        cuo.setMesCuota(mes.shortValue());
                        cuo.setAnioCuota(anio.shortValue());
                        cuo.setNroCuota(0);
                        double precionormal = 0.0;
                        if (montocuota == null) {
                            precionormal = s.getPrecio();
                        } else {
                            precionormal = montocuota;
                        }
                        double montoDescuento = (precionormal * descuento) / (double) 100;
                        Integer montoFinalCuota = (int) Math.round(precionormal - montoDescuento);
                        cuo.setMontoCuota(montoFinalCuota);

                        if (descuento == (double) 100) {
                            cuo.setCancelado(true);
                        }
                        cuo.setMontoEntrega(0);
                        this.em.persist(cuo);
                        lstCuotasGeneradas.add(cuo);
                        cantCuotas = cantCuotas + 1;
                    } else {
                        Cuota cuo = new Cuota();
                        cuo.setSuscripcion(s);
                        cuo.setServicio(s.getServicio());
                        cuo.setCancelado(true);
                        Calendar calvenc = new GregorianCalendar(anio, mes - 1, 1);
                        cuo.setFechaVencimiento(calvenc.getTime());
                        cuo.setMesCuota(mes.shortValue());
                        cuo.setAnioCuota(anio.shortValue());
                        cuo.setFechaPago(calvenc.getTime());
                        cuo.setMontoCuota(0);
                        cuo.setMontoEntrega(0);
                        cuo.setNroCuota(0);
                        this.em.persist(cuo);
                        lstCuotasGeneradas.add(cuo);
                        cantCuotas = cantCuotas + 1;
                    }
                }
            } else {
                System.out.println("Existen cuotas en periodo " + mes + " " + anio);
            }
        } else {
            System.out.println("fecha de suscripcion posterior al mes: " + mes + ", anio: " + anio + ", Suscripcion: " + s.getIdsuscripcion());
        }
        System.out.println("Cantidad cuotas a retornar por metodo principal: " + cantCuotas);
        return lstCuotasGeneradas;
    }

    public Cuota getCuotaPendienteMasAntigua(Suscripcion s, Date desde, Date hasta) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT f FROM Cuota f WHERE f.suscripcion=:s AND f.cancelado=false AND f.fechaVencimiento>=:desde ANd f.fechaVencimiento<=:hasta ORDER BY f.fechaVencimiento ASC", Cuota.class);
        q.setParameter("s", s);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        List<Cuota> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Cuota getCuotaPendienteMasAntigua(Suscripcion s) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT f FROM Cuota f WHERE f.suscripcion=:s AND f.cancelado=false ORDER BY f.fechaVencimiento ASC", Cuota.class);
        q.setParameter("s", s);
        List<Cuota> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Cuota getCuotaPendienteMasReciente(Suscripcion s, Date desde, Date hasta) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT f FROM Cuota f WHERE f.suscripcion=:s AND f.cancelado=false AND f.fechaVencimiento>=:desde ANd f.fechaVencimiento<=:hasta ORDER BY f.fechaVencimiento DESC", Cuota.class);
        q.setParameter("s", s);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        List<Cuota> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Cuota getCuotaPendienteMasReciente(Suscripcion s) {
        TypedQuery<Cuota> q = this.em.createQuery("SELECT f FROM Cuota f WHERE f.suscripcion=:s AND f.cancelado=false ORDER BY f.fechaVencimiento DESC", Cuota.class);
        q.setParameter("s", s);
        List<Cuota> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Integer getCantMesesConCuotaPend(Suscripcion s, Date desde, Date hasta) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Cuota cAnt = this.getCuotaPendienteMasAntigua(s, desde, hasta);
        Cuota cRec = this.getCuotaPendienteMasReciente(s, desde, hasta);
        if (cAnt != null && cRec != null) {
            String servdesde = cAnt.getServicio() != null ? cAnt.getServicio().getNombre() : "TV Cable";
            String servhasta = cRec.getServicio() != null ? cRec.getServicio().getNombre() : "TV Cable";
            System.out.println("Mas antiguo: " + servdesde + " " + sdf.format(cAnt.getFechaVencimiento()));
            System.out.println("Mas Reciente: " + servhasta + " " + sdf.format(cRec.getFechaVencimiento()));
            Calendar cdesde = new GregorianCalendar();
            cdesde.setTime(cAnt.getFechaVencimiento());
            cdesde.set(Calendar.DAY_OF_MONTH, 1);
            Calendar chasta = new GregorianCalendar();
            chasta.setTime(cRec.getFechaVencimiento());
            chasta.set(Calendar.DAY_OF_MONTH, 1);
            chasta.add(Calendar.MONTH, 1);
            chasta.add(Calendar.DAY_OF_MONTH, -1);
            int cantmeses = 0;
            while (cdesde.before(chasta)) {
                cantmeses++;
                cdesde.add(Calendar.MONTH, 1);
                System.out.println("Suma: " + sdf.format(cdesde.getTime()));
            }
            return cantmeses;
        } else {
            return 0;
        }
    }

    public int getMontoDeudaTodasCuotas(Suscripcion s, Date desde, Date hasta) {
        int deuda = 0;
        TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:s AND c.cancelado=false AND c.fechaVencimiento>=:desde AND c.fechaVencimiento<=:hasta", Cuota.class);
        q.setParameter("s", s);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        for (Cuota cuo : q.getResultList()) {
            deuda = deuda + cuo.getMontoCuota() - cuo.getMontoEntrega();
        }
        return deuda;
    }

    public List<Cuota> getCuotasPorHistorialGeneracion(HistorialGeneracionCuotas hg, Suscripcion s) {
        if (hg != null) {
            TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:s AND c.historialGeneracion=:hg", Cuota.class);
            q.setParameter("hg", hg);
            q.setParameter("s", s);
            return q.getResultList();
        } else {
            TypedQuery<Cuota> q = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:s AND c.servicio!=c.suscripcion.servicio", Cuota.class);
            q.setParameter("s", s);
            return q.getResultList();
        }
    }
}
