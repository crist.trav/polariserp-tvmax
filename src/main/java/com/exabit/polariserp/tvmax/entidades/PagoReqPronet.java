package com.exabit.polariserp.tvmax.entidades;

import com.exabit.polariserp.tvmax.entidades.AnulacionReqPronet;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "pago_req_pronet")
@NamedQueries({
    @NamedQuery(name = "PagoReqPronet.findAll", query = "SELECT p FROM PagoReqPronet p")
    , @NamedQuery(name = "PagoReqPronet.findByIdpagoReq", query = "SELECT p FROM PagoReqPronet p WHERE p.idpagoReq = :idpagoReq")
    , @NamedQuery(name = "PagoReqPronet.findByCodServicio", query = "SELECT p FROM PagoReqPronet p WHERE p.codServicio = :codServicio")
    , @NamedQuery(name = "PagoReqPronet.findByTipoTrx", query = "SELECT p FROM PagoReqPronet p WHERE p.tipoTrx = :tipoTrx")
    , @NamedQuery(name = "PagoReqPronet.findByUsuario", query = "SELECT p FROM PagoReqPronet p WHERE p.usuario = :usuario")
    , @NamedQuery(name = "PagoReqPronet.findByPassword", query = "SELECT p FROM PagoReqPronet p WHERE p.password = :password")
    , @NamedQuery(name = "PagoReqPronet.findByNroCuota", query = "SELECT p FROM PagoReqPronet p WHERE p.nroCuota = :nroCuota")
    , @NamedQuery(name = "PagoReqPronet.findByImporte", query = "SELECT p FROM PagoReqPronet p WHERE p.importe = :importe")
    , @NamedQuery(name = "PagoReqPronet.findByMoneda", query = "SELECT p FROM PagoReqPronet p WHERE p.moneda = :moneda")
    , @NamedQuery(name = "PagoReqPronet.findByCodTransaccion", query = "SELECT p FROM PagoReqPronet p WHERE p.codTransaccion = :codTransaccion")
    , @NamedQuery(name = "PagoReqPronet.findByFechaHora", query = "SELECT p FROM PagoReqPronet p WHERE p.fechaHora = :fechaHora")})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PagoReqPronet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idpagoreqpronet", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idpagoreqpronet", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @Column(name = "idpago_req")
    private Integer idpagoReq;
    @Size(max = 5)
    @Expose
    @Column(name = "codServicio")
    private String codServicio;
    @Size(max = 2)
    @Column(name = "tipoTrx")
    @Expose
    private String tipoTrx;
    @Size(max = 20)
    @Column(name = "usuario")
    @Expose
    private String usuario;
    @Size(max = 70)
    @Column(name = "password")
    @Expose
    private String password;
    @Column(name = "nroCuota")
    @Expose
    private Integer nroCuota;
    @Size(max = 15)
    @Column(name = "importe")
    @Expose
    private String importe;
    @Size(max = 1)
    @Column(name = "moneda")
    @Expose
    private String moneda;
    @Column(name = "codTransaccion")
    private Long codTransaccion;
    @Column(name = "nroOperacion")
    @Expose
    private Integer nroOperacion;
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pagoReqPronet")
    private List<AnulacionReqPronet> anulacionReqPronetList;
    @JoinColumn(name = "consulta_res_detalle_pronet", referencedColumnName = "nroOperacion")
    @ManyToOne(optional = false)
    private ConsultaResDetallePronet consultaResDetallePronet;
    @ManyToMany(mappedBy = "pagoReqPronetList")
    private List<Cobro> cobroList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pagoReqPronet")
    private List<PagoResPronet> pagoResPronetList;

    public PagoReqPronet() {
    }

    public PagoReqPronet(Integer idpagoReq) {
        this.idpagoReq = idpagoReq;
    }

    public Integer getIdpagoReq() {
        return idpagoReq;
    }

    public void setIdpagoReq(Integer idpagoReq) {
        this.idpagoReq = idpagoReq;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public String getTipoTrx() {
        return tipoTrx;
    }

    public void setTipoTrx(String tipoTrx) {
        this.tipoTrx = tipoTrx;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(Integer nroCuota) {
        this.nroCuota = nroCuota;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }


    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public List<AnulacionReqPronet> getAnulacionReqPronetList() {
        return anulacionReqPronetList;
    }

    public void setAnulacionReqPronetList(List<AnulacionReqPronet> anulacionReqPronetList) {
        this.anulacionReqPronetList = anulacionReqPronetList;
    }

    public ConsultaResDetallePronet getConsultaResDetallePronet() {
        return consultaResDetallePronet;
    }

    public void setConsultaResDetallePronet(ConsultaResDetallePronet consultaResDetallePronet) {
        this.consultaResDetallePronet = consultaResDetallePronet;
    }

    public Integer getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(Integer nroOperacion) {
        this.nroOperacion = nroOperacion;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpagoReq != null ? idpagoReq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagoReqPronet)) {
            return false;
        }
        PagoReqPronet other = (PagoReqPronet) object;
        if ((this.idpagoReq == null && other.idpagoReq != null) || (this.idpagoReq != null && !this.idpagoReq.equals(other.idpagoReq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.PagoReqPronet[ idpagoReq=" + idpagoReq + " ]";
    }

    public List<PagoResPronet> getPagoResPronetList() {
        return pagoResPronetList;
    }

    public void setPagoResPronetList(List<PagoResPronet> pagoResPronetList) {
        this.pagoResPronetList = pagoResPronetList;
    }

    public List<Cobro> getCobroList() {
        return cobroList;
    }

    public void setCobroList(List<Cobro> cobroList) {
        this.cobroList = cobroList;
    }

    public Long getCodTransaccion() {
        return codTransaccion;
    }

    public void setCodTransaccion(Long codTransaccion) {
        this.codTransaccion = codTransaccion;
    }
    
}
