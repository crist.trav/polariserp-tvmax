/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.funcionalidad;

import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.sesbeans.FuncionalidadFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ModuloFacade;
import com.google.gson.Gson;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author frioss
 */
@Named(value = "funcionarioLoginManBean")
@RequestScoped
public class FuncionalidadManBean {

    @EJB
    private FuncionarioFacade funcionarioFacade;
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    private final List<Funcionalidad> lstFuncionalidades = new LinkedList<>();

    @EJB
    private ModuloFacade moduloFacade;
    @EJB
    private FuncionalidadFacade funcionalidadFacade;
    private Integer idFuncionalidadTmp;
    private String msg = "";
    private String seleccionarIDfuncionalidadJSON;
    private Funcionalidad funcionalidadVar = new Funcionalidad();

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionarIDfuncionalidadJSON() {
        return seleccionarIDfuncionalidadJSON;
    }

    public void setSeleccionarIDfuncionalidadJSON(String seleccionarIDfuncionalidadJSON) {
        this.seleccionarIDfuncionalidadJSON = seleccionarIDfuncionalidadJSON;
    }

    public Integer getIdFuncionalidadTmp() {
        return idFuncionalidadTmp;
    }

    public void setIdFuncionalidadTmp(Integer idFuncionalidadTmp) {
        this.idFuncionalidadTmp = idFuncionalidadTmp;
    }

    public Funcionalidad getFuncionalidadVar() {
        return funcionalidadVar;
    }

    public void setFuncionalidadVar(Funcionalidad funcionalidadVar) {
        this.funcionalidadVar = funcionalidadVar;
    }

    public FuncionalidadManBean() {
    }

    public List<Funcionalidad> getFuncionalidades() {
        return this.funcionalidadFacade.findAll();
    }

    public void registrarFuncionalidad() {
        Modulo m = this.moduloFacade.find(this.idFuncionalidadTmp);
        this.funcionalidadVar.setModulo(m);
        System.out.println("Registrada funcionalidad: " + funcionalidadVar.getNombre() + ", para el modulo" + m.getNombre());
        this.funcionalidadFacade.create(funcionalidadVar);
        msg = "Registrada funcionalidad: " + funcionalidadVar.getNombre() + ", para el modulo: " + m.getNombre();
    }

    public void editarFuncionalidad() {
        System.out.println("Datos recibidos: " + funcionalidadVar.getNombre() + ", " + funcionalidadVar.getIdfuncionalidad());
        Modulo m = this.moduloFacade.find(this.idFuncionalidadTmp);
        this.funcionalidadVar.setModulo(m);
        System.out.println("Editada funcionalidad: " + funcionalidadVar.getNombre() + ", para el modulo" + m.getNombre());
        this.funcionalidadFacade.edit(funcionalidadVar);
        msg = "Editada la funcionalidad: " + funcionalidadVar.getNombre() + ", del modulo: " + m.getNombre();
    }

    public void eliminarFuncionalidad() {
        Gson gson = new Gson();
        Integer[] idEs = gson.fromJson(this.seleccionarIDfuncionalidadJSON, Integer[].class);
        for (Integer idE : idEs) {
            this.funcionalidadFacade.remove(this.funcionalidadFacade.find(idE));
        }
    }
}
