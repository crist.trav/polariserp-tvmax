/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "categoria_modulo")
@NamedQueries({
    @NamedQuery(name = "CategoriaModulo.findAll", query = "SELECT c FROM CategoriaModulo c")
    , @NamedQuery(name = "CategoriaModulo.findByIdcategoriaModulo", query = "SELECT c FROM CategoriaModulo c WHERE c.idcategoriaModulo = :idcategoriaModulo")
    , @NamedQuery(name = "CategoriaModulo.findByIcono", query = "SELECT c FROM CategoriaModulo c WHERE c.icono = :icono")
    , @NamedQuery(name = "CategoriaModulo.findByNombre", query = "SELECT c FROM CategoriaModulo c WHERE c.nombre = :nombre")})
public class CategoriaModulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idcategoriamodulo", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idcategoriamodulo", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcategoria_modulo")
    private Integer idcategoriaModulo;
    @Size(max = 255)
    @Column(name = "icono")
    private String icono;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "categoriaModulo")
    private List<Modulo> moduloList;
    @Basic(optional = false)
    @NotNull
    @Column(name = "activo")
    private boolean activo;

    public CategoriaModulo() {
    }

    public CategoriaModulo(Integer idcategoriaModulo) {
        this.idcategoriaModulo = idcategoriaModulo;
    }

    public Integer getIdcategoriaModulo() {
        return idcategoriaModulo;
    }

    public void setIdcategoriaModulo(Integer idcategoriaModulo) {
        this.idcategoriaModulo = idcategoriaModulo;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Modulo> getModuloList() {
        return moduloList;
    }

    public void setModuloList(List<Modulo> moduloList) {
        this.moduloList = moduloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoriaModulo != null ? idcategoriaModulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaModulo)) {
            return false;
        }
        CategoriaModulo other = (CategoriaModulo) object;
        if ((this.idcategoriaModulo == null && other.idcategoriaModulo != null) || (this.idcategoriaModulo != null && !this.idcategoriaModulo.equals(other.idcategoriaModulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.CategoriaModulo[ idcategoriaModulo=" + idcategoriaModulo + " ]";
    }

    public boolean getActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
}
