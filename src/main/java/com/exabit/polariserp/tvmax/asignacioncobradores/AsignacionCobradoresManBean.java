/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.asignacioncobradores;

import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.sesbeans.AsignacionFacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.util.UtilReportes;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "asignacionCobradoresManBean")
@ViewScoped
public class AsignacionCobradoresManBean implements Serializable {

    @EJB
    private AsignacionFacturaFacade asignacionFacturaFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;
    

    private List<Factura> lstFacturasPendSel = new LinkedList<>();

    public List<Factura> getLstFacturasPendSel() {
        return lstFacturasPendSel;
    }

    public void setLstFacturasPendSel(List<Factura> lstFacturasPendSel) {
        this.lstFacturasPendSel = lstFacturasPendSel;
    }
    
        private List<Factura> lstFacturasCobradasSel = new ArrayList<>();

    public List<Factura> getLstFacturasCobradasSel() {
        return lstFacturasCobradasSel;
    }

    public void setLstFacturasCobradasSel(List<Factura> lstFacturasCobradasSel) {
        this.lstFacturasCobradasSel = lstFacturasCobradasSel;
    }


    private static final Logger LOG = Logger.getLogger(AsignacionCobradoresManBean.class.getName());

    private Funcionario cobradorSeleccionado;

    public Funcionario getCobradorSeleccionado() {
        return cobradorSeleccionado;
    }

    public void setCobradorSeleccionado(Funcionario cobradorSeleccionado) {
        this.cobradorSeleccionado = cobradorSeleccionado;
    }

    private List<Funcionario> lstCobradoresSeleccionados = new LinkedList<>();

    public List<Funcionario> getLstCobradoresSeleccionados() {
        return lstCobradoresSeleccionados;
    }

    public void setLstCobradoresSeleccionados(List<Funcionario> lstCobradoresSeleccionados) {
        this.lstCobradoresSeleccionados = lstCobradoresSeleccionados;
    }

    private String jsonSeleccionCobradores;

    public String getJsonSeleccionCobradores() {
        return jsonSeleccionCobradores;
    }

    public void setJsonSeleccionCobradores(String jsonSeleccionCobradores) {
        this.jsonSeleccionCobradores = jsonSeleccionCobradores;
    }

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;
    
    

    private String strDesdeAsignacionMostrar;

    public String getStrDesdeAsignacionMostrar() {
        return strDesdeAsignacionMostrar;
    }

    public void setStrDesdeAsignacionMostrar(String strDesdeAsignacionMostrar) {
        this.strDesdeAsignacionMostrar = strDesdeAsignacionMostrar;
    }
    
        private String strHastaAsignacionMostrar;

    public String getStrHastaAsignacionMostrar() {
        return strHastaAsignacionMostrar;
    }

    public void setStrHastaAsignacionMostrar(String strHastaAsignacionMostrar) {
        this.strHastaAsignacionMostrar = strHastaAsignacionMostrar;
    }


    private final SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");

    /**
     * Creates a new instance of asignacionCobradoresManBean
     */
    public AsignacionCobradoresManBean() {
        Calendar ahora=new GregorianCalendar();
        ahora.set(Calendar.DAY_OF_MONTH, 1);
        this.strDesdeAsignacionMostrar = this.sdf.format(new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1).getTime());
        Calendar finMes=new GregorianCalendar(ahora.get(Calendar.YEAR), ahora.get(Calendar.MONTH), 1);
        finMes.add(Calendar.MONTH, 1);
        finMes.add(Calendar.DAY_OF_MONTH, -1);
        this.strHastaAsignacionMostrar=this.sdf.format(finMes.getTime());
    }

    public List<Funcionario> getLstCobradores() {
        try{
            Date fdesde=this.sdf.parse(strDesdeAsignacionMostrar);
            Date fhasta=this.sdf.parse(this.strHastaAsignacionMostrar);
            return this.asignacionFacturaFacade.getCobradoresPeriodo(fdesde, fhasta);
        }catch(ParseException ex){
            System.err.println("Error al obtener fecha a partir de String: "+ex);
            return new ArrayList<>();
        }
    }

    public Integer getCantidadFacturas(Integer idcobrador, boolean pagado) {
        Funcionario c = this.funcionarioFacade.find(idcobrador);
        Integer cantidad = 0;
        try {
            Date d=this.sdf.parse(this.strDesdeAsignacionMostrar);
            Date h=this.sdf.parse(this.strHastaAsignacionMostrar);
            cantidad = this.asignacionFacturaFacade.getFacturas(d, h, c, pagado).size();
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "error al convertir de string a date", ex);
        }
        return cantidad;
    }

    public String getFacturaInferior(Integer idcobrador) {
        Funcionario c = this.funcionarioFacade.find(idcobrador);
        String infe = "(Ninguno)";
        try {
            Date mesanio = this.sdf.parse(this.strDesdeAsignacionMostrar);
            Calendar cmesanio = new GregorianCalendar();
            cmesanio.setTimeInMillis(mesanio.getTime());
            infe = this.facturaFacade.getFacturaInferior(c, cmesanio.get(Calendar.YEAR), cmesanio.get(Calendar.MONTH) + 1);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "error al convertir de string a date", ex);
        }
        return infe;
//        this.facturaFacade.(c, Integer.SIZE, Integer.BYTES)
    }

    public String getFacturaSuperior(Integer idcobrador) {
        Funcionario c = this.funcionarioFacade.find(idcobrador);
        String supe = "(Ninguno)";
        try {
            Date mesanio = this.sdf.parse(this.strDesdeAsignacionMostrar);
            Calendar cmesanio = new GregorianCalendar();
            cmesanio.setTimeInMillis(mesanio.getTime());
            supe = this.facturaFacade.getFacturaSuperior(c, cmesanio.get(Calendar.YEAR), cmesanio.get(Calendar.MONTH) + 1);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "error al convertir de string a date", ex);
        }
        return supe;
//        this.facturaFacade.(c, Integer.SIZE, Integer.BYTES)
    }

    public void seleccionarMesAnio() {
        System.out.println("Seleccion de mesanio");
    }

    public void seleccionarCobradores() {
        System.out.println("cobradores seleccionados");
        Gson gson = new Gson();
        this.cobradorSeleccionado = null;
        this.lstCobradoresSeleccionados.clear();
        this.lstFacturasPendSel.clear();
        this.lstFacturasCobradasSel.clear();
        if (this.jsonSeleccionCobradores != null) {
            System.out.println("el json: " + this.jsonSeleccionCobradores);
            Integer[] idsCobra = gson.fromJson(this.jsonSeleccionCobradores, Integer[].class);
            for (Integer idc : idsCobra) {
                this.lstCobradoresSeleccionados.add(this.funcionarioFacade.find(idc));
            }
            if (this.lstCobradoresSeleccionados.size() == 1) {
                this.cobradorSeleccionado = this.lstCobradoresSeleccionados.get(0);
                try {
                    Date fdesde = this.sdf.parse(this.strDesdeAsignacionMostrar);
                    Date fhasta = this.sdf.parse(this.strHastaAsignacionMostrar);
                    this.lstFacturasPendSel.addAll(this.asignacionFacturaFacade.getFacturas(fdesde, fhasta, this.cobradorSeleccionado, false));
                    this.lstFacturasCobradasSel.addAll(this.asignacionFacturaFacade.getFacturas(fdesde, fhasta, this.cobradorSeleccionado, true));
                } catch (ParseException ex) {
                    LOG.log(Level.SEVERE, "Error al convertir mesanio", ex);
                }
            }
        }
    }
    
    public void generarPDFPendSeleccionado(){
//        String rutar=this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_RUTA_REPORTES);
        HashMap<Integer, String> hmCobrador=new HashMap<>();
        for(Factura f:this.lstFacturasPendSel){
            Funcionario c=this.asignacionFacturaFacade.getCobradorAsinado(f);
            String cobra=c.getNombres();
            if(c.getApellidos()!=null){
                cobra=cobra+" "+c.getApellidos();
            }
            hmCobrador.put(f.getIdfactura(), cobra);
        }
        List<JasperPrint> lstJP=UtilReportes.generarJPListFacturas(this.lstFacturasPendSel, hmCobrador);
        try {
            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            httpServletResponse.addHeader("content-disposition", "inline; filename=report.pdf");
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            JRPdfExporter exporter = new JRPdfExporter();
            SimplePdfExporterConfiguration configExport = new SimplePdfExporterConfiguration();
            configExport.setCreatingBatchModeBookmarks(true);
            exporter.setConfiguration(configExport);
            exporter.setExporterInput(SimpleExporterInput.getInstance(lstJP));
            SimpleOutputStreamExporterOutput out = new SimpleOutputStreamExporterOutput(servletOutputStream);
            exporter.setExporterOutput(out);
            exporter.exportReport();
        } catch (IOException | JRException ex) {
            LOG.log(Level.SEVERE, "Error al exportar pdf facturas", ex);
        }
    }
    
    public Integer getSubtotal(Integer idcobrador,boolean pagado){
        Funcionario c = this.funcionarioFacade.find(idcobrador);
        Integer total = 0;
        try {
            Date d=this.sdf.parse(this.strDesdeAsignacionMostrar);
            Date h=this.sdf.parse(this.strHastaAsignacionMostrar);
            List<Factura> lstf=this.asignacionFacturaFacade.getFacturas(d, h, c, pagado);
            for(Factura f:lstf){
                total=total+f.getTotal();
            }
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "error al convertir de string a date", ex);
        }
        return total;
    }
    
    public Integer getTotalCantidadPendiente(){
        int total=0;
        List<Funcionario> lstc=this.getLstCobradores();
        for(Funcionario f:lstc){
            total=total+this.getCantidadFacturas(f.getIdfuncionario(), false);
        }
        return total;
    }
    
    public Integer getTotalMontoPendiente(){
        int total=0;
        List<Funcionario> lstc=this.getLstCobradores();
        for(Funcionario f:lstc){
            total=total+this.getSubtotal(f.getIdfuncionario(), false);
        }
        return total;
    }
    public Integer getTotalCantidadPagado(){
        int total=0;
        List<Funcionario> lstc=this.getLstCobradores();
        for(Funcionario f:lstc){
            total=total+this.getCantidadFacturas(f.getIdfuncionario(), true);
        }
        return total;
    }
    
    public Integer getTotalMontoPagado(){
        int total=0;
        List<Funcionario> lstc=this.getLstCobradores();
        for(Funcionario f:lstc){
            total=total+this.getSubtotal(f.getIdfuncionario(), true);
        }
        return total;
    }
}
