package com.exabit.polariserp.tvmax.servicio;

import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.sesbeans.ConceptoFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author frioss
 */
@Named(value = "servicioManBean")
@SessionScoped
public class ServicioManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(ServicioManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private ConceptoFacade conceptoFacade;

    @EJB
    private ServicioFacade servicioFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private String cuerpoMsg = "y";
    private String seleccionarIDservicioJSON;
    private Integer idConcepto;
    private Short iva;
    private String nombreServicio;
    private Integer precioServicio;
    private boolean suscribible = false;
    private Integer idServicio;
    private Integer idServicioCargar;
    private String tipoMsg = "info";
    private String cabeceraMsg = "x";
    private String busqueda = "";
    private String nombreNuevoConcepto;
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getNombreNuevoConcepto() {
        return nombreNuevoConcepto;
    }

    public void setNombreNuevoConcepto(String nombreNuevoConcepto) {
        this.nombreNuevoConcepto = nombreNuevoConcepto;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public Integer getIdServicioCargar() {
        return idServicioCargar;
    }

    public void setIdServicioCargar(Integer idServicioCargar) {
        this.idServicioCargar = idServicioCargar;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public boolean isSuscribible() {
        return suscribible;
    }

    public void setSuscribible(boolean suscribible) {
        this.suscribible = suscribible;
    }

    public Integer getPrecioServicio() {
        return precioServicio;
    }

    public void setPrecioServicio(Integer precioServicio) {
        this.precioServicio = precioServicio;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public Integer getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto) {
        this.idConcepto = idConcepto;
    }

    public Short getIva() {
        return iva;
    }

    public void setIva(Short iva) {
        this.iva = iva;
    }

    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getSeleccionarIDservicioJSON() {
        return seleccionarIDservicioJSON;
    }

    public void setSeleccionarIDservicioJSON(String seleccionarIDservicioJSON) {
        this.seleccionarIDservicioJSON = seleccionarIDservicioJSON;
    }

    public List<Servicio> getServicio() {
        if (this.busqueda.isEmpty()) {
            return servicioFacade.findAll();
        } else {
            return this.servicioFacade.buscarServicio(this.busqueda);
        }
    }
    //</editor-fold>

    public ServicioManBean() {
    }

    public void guardarServicio() {
        if (this.idServicio == null) {
            try {
                Servicio se = new Servicio();
                se.setNombre(this.nombreServicio);
                se.setPrecio(this.precioServicio);
                se.setPorcentajeIva(this.iva);
                se.setSuscribible(this.suscribible);
                Concepto co = this.conceptoFacade.find(this.idConcepto);
                se.setConceptoCobro(co);
                this.servicioFacade.create(se);
                this.idServicio = se.getIdservicio();
                this.tipoMsg = "info";
                this.cabeceraMsg = "Éxito";
                this.cuerpoMsg = "Servicio guardado correctamente";
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al registrar nuevo servicio", ex);
                this.tipoMsg = "error";
                this.cabeceraMsg = "Error al guardar servicio";
                this.cuerpoMsg = ex.getMessage();
            }
        } else {
            try {
                Servicio s = this.servicioFacade.find(this.idServicio);
                s.setNombre(this.nombreServicio);
                s.setPrecio(this.precioServicio);
                s.setPorcentajeIva(this.iva);
                s.setSuscribible(this.suscribible);
                Concepto co = this.conceptoFacade.find(this.idConcepto);
                s.setConceptoCobro(co);
                Concepto c = s.getConceptoCobro();

//                if (c != null) {
//                    c.setNombre(s.getNombre());
//                    this.conceptoFacade.edit(c);
//                } else {
//                    Concepto co = new Concepto();
//                    co.setNombre(s.getNombre());
//                    this.conceptoFacade.create(co);
//                    s.setConceptoCobro(co);
//                }
                this.servicioFacade.edit(s);
                this.tipoMsg = "info";
                this.cabeceraMsg = "Éxito";
                this.cuerpoMsg = "Servicio guardado correctamente";
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al registrar nuevo servicio", ex);
                this.tipoMsg = "error";
                this.cabeceraMsg = "Error al guardar servicio";
                this.cuerpoMsg = ex.getMessage();
            }
        }
    }

    public void cargarDatos() {
        System.out.println("Cargar datos " + this.idServicioCargar);
        if (this.idServicioCargar == null) {
            this.limpiar();
        } else {
            this.cargarDatos(this.servicioFacade.find(this.idServicioCargar));
        }
    }

    private void cargarDatos(Servicio s) {
        this.idServicio = s.getIdservicio();
        this.nombreServicio = s.getNombre();
        this.precioServicio = s.getPrecio();
        this.iva = s.getPorcentajeIva();
        this.suscribible = s.getSuscribible();
        this.idConcepto = s.getConceptoCobro().getIdconcepto();
    }

    public void limpiar() {
        this.idServicio = null;
        this.nombreServicio = null;
        this.iva = null;
        this.suscribible = false;
        this.precioServicio = null;
        this.idConcepto = null;
    }

    public void eliminarServicios() {
        try {
            System.out.println("Eliminar servicios: " + this.seleccionarIDservicioJSON);
            Gson gson = new Gson();
            Integer[] idsElimina = gson.fromJson(this.seleccionarIDservicioJSON, Integer[].class);
            for (Integer ide : idsElimina) {
                Servicio s = this.servicioFacade.find(ide);
                this.servicioFacade.remove(s);
                this.conceptoFacade.remove(s.getConceptoCobro());
            }
            this.limpiar();
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Servicios eliminados correctamente";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error eliminar servicios", ex);
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error al eliminar servicios";
            this.cuerpoMsg = ex.getMessage();
        }
    }

    public List<Concepto> getLstConceptoCobro() {
        return this.conceptoFacade.findAll();
    }

    public void registrarConcepto() {
        try {
            Concepto c = new Concepto();
            c.setNombre(this.nombreNuevoConcepto);
            this.conceptoFacade.create(c);
            this.idConcepto=c.getIdconcepto();
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Registrado concepto '" + this.nombreNuevoConcepto + "'.";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al registrar concepto", ex);
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error al registrar concepto";
            this.cuerpoMsg = ex.getMessage();
        }
    }
}
