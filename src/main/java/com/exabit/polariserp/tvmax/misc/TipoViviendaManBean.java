/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.misc;

import com.exabit.polariserp.tvmax.entidades.TipoVivienda;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "tipoViviendaManBean")
@RequestScoped
public class TipoViviendaManBean {

    @EJB
    private TipoViviendaFacade tipoViviendaFacade;

    /**
     * Creates a new instance of TipoViviendaManBean
     */
    public TipoViviendaManBean() {
    }
    
    public List<TipoVivienda> getTiposVivienda(){
        return this.tipoViviendaFacade.findAll();
    }
    
}
