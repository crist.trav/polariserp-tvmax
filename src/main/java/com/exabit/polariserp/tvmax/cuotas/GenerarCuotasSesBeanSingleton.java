/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.cuotas;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

/**
 *
 * @author cristhian-kn
 */
@Singleton
public class GenerarCuotasSesBeanSingleton {

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private CuotaFacade cuotaFacade;
    //</editor-fold>
    
    //<editor-fold desc="Properties" defaultstate="collapsed">
    private Date desdeFechaGenActual=new Date();
    private Date hastaFechaGenActual=new Date();
    private boolean generando = false;
    private Integer porcentajeEnviado = 0;
    private int ultPorcEnviado = 0;
    private Integer cantSuscripcionesProcesadas = 0;
    private Integer cantCuotasGeneradas = 0;
    //</editor-fold>
    
    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public Date getDesdeFechaGenActual() {
        return desdeFechaGenActual;
    }

    public void setDesdeFechaGenActual(Date desdeFechaGenActual) {
        this.desdeFechaGenActual = desdeFechaGenActual;
    }
    
    public Date getHastaFechaGenActual() {
        return hastaFechaGenActual;
    }

    public void setHastaFechaGenActual(Date hastaFechaGenActual) {
        this.hastaFechaGenActual = hastaFechaGenActual;
    }
    
    public boolean isGenerando() {
        return generando;
    }

    public void setGenerando(boolean generando) {
        this.generando = generando;
    }
    
    public Integer getPorcentajeEnviado() {
        return porcentajeEnviado;
    }

    public void setPorcentajeEnviado(Integer porcentajeEnviado) {
        this.porcentajeEnviado = porcentajeEnviado;
    }
    
    public int getUltPorcEnviado() {
        return ultPorcEnviado;
    }

    public void setUltPorcEnviado(int ultPorcEnviado) {
        this.ultPorcEnviado = ultPorcEnviado;
    }
    
    public Integer getCantSuscripcionesProcesadas() {
        return cantSuscripcionesProcesadas;
    }

    public void setCantSuscripcionesProcesadas(Integer cantSuscripcionesProcesadas) {
        this.cantSuscripcionesProcesadas = cantSuscripcionesProcesadas;
    }
    
    public Integer getCantCuotasGeneradas() {
        return cantCuotasGeneradas;
    }

    public void setCantCuotasGeneradas(Integer cantCuotasGeneradas) {
        this.cantCuotasGeneradas = cantCuotasGeneradas;
    }
    //</editor-fold>
    
    @Resource
    ManagedExecutorService executor;

    @Inject
    @Push
    private PushContext canalProgresoGenCuotaSus;
  
    public void enviarPorcentajeProgreso(Integer porc) {
        if (porc > this.ultPorcEnviado) {
            this.canalProgresoGenCuotaSus.send(porc);
            this.ultPorcEnviado=porc;
        }
    }

    public void incrementarCuotasGeneradas(Integer cant) {
        this.cantCuotasGeneradas = this.cantCuotasGeneradas + cant;
        System.out.println("Incrementar cantidad de cuotas: "+cant+", "+this.cantCuotasGeneradas);
    }

    public void generarCuotas(List<Suscripcion> lsts, Date fdesde, Date fhasta, boolean generarSoloRecientes) {
        this.desdeFechaGenActual=fdesde;
        this.hastaFechaGenActual=fhasta;
        GenerarCuotasRunnable gcr = new GenerarCuotasRunnable(fdesde, fhasta, lsts, false, generarSoloRecientes);
        this.executor.submit(gcr);
    }

    public void generarCuotas(Date fdesde, Date fhasta, boolean generarSoloRecientes) {
        this.generarCuotas(this.suscripcionFacade.getSuscripcionesConServicio(), fdesde, fhasta, generarSoloRecientes);
    }

    public void generarCuotas(Suscripcion s, Date fdesde, Date fhasta, boolean generarSoloRecientes) {
        List<Suscripcion> lsts = new ArrayList<>();
        lsts.add(s);
        this.generarCuotas(lsts, fdesde, fhasta, generarSoloRecientes);
    }

    public void reiniciarEstado(){
        this.generando=false;
        this.cantSuscripcionesProcesadas=0;
        this.ultPorcEnviado=0;
        this.cantCuotasGeneradas=0;
    }
    
}
