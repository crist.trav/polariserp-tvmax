/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "terminal_impresion")
@NamedQueries({
    @NamedQuery(name = "TerminalImpresion.findAll", query = "SELECT t FROM TerminalImpresion t")
    , @NamedQuery(name = "TerminalImpresion.findByIdterminalImpresion", query = "SELECT t FROM TerminalImpresion t WHERE t.idterminalImpresion = :idterminalImpresion")
    , @NamedQuery(name = "TerminalImpresion.findByDescripcion", query = "SELECT t FROM TerminalImpresion t WHERE t.descripcion = :descripcion")
    , @NamedQuery(name = "TerminalImpresion.findByFechaAsignacion", query = "SELECT t FROM TerminalImpresion t WHERE t.fechaAsignacion = :fechaAsignacion")})
public class TerminalImpresion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idterminalimpresion", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idterminalimpresion", table = "idsequences", allocationSize = 1, initialValue = 3)
    @Basic(optional = false)
    @Column(name = "idterminal_impresion")
    private Integer idterminalImpresion;
    @Size(max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha_asignacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsignacion;
    @OneToMany(mappedBy = "terminalImpresion")
    private List<TalonarioFactura> talonarioFacturaList;

    public TerminalImpresion() {
    }

    public TerminalImpresion(Integer idterminalImpresion) {
        this.idterminalImpresion = idterminalImpresion;
    }

    public Integer getIdterminalImpresion() {
        return idterminalImpresion;
    }

    public void setIdterminalImpresion(Integer idterminalImpresion) {
        this.idterminalImpresion = idterminalImpresion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public List<TalonarioFactura> getTalonarioFacturaList() {
        return talonarioFacturaList;
    }

    public void setTalonarioFacturaList(List<TalonarioFactura> talonarioFacturaList) {
        this.talonarioFacturaList = talonarioFacturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idterminalImpresion != null ? idterminalImpresion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TerminalImpresion)) {
            return false;
        }
        TerminalImpresion other = (TerminalImpresion) object;
        if ((this.idterminalImpresion == null && other.idterminalImpresion != null) || (this.idterminalImpresion != null && !this.idterminalImpresion.equals(other.idterminalImpresion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.TerminalImpresion[ idterminalImpresion=" + idterminalImpresion + " ]";
    }
    
}
