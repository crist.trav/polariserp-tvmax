/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cargo;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author gemacron
 */
@Stateless
public class FuncionarioFacade extends AbstractFacade<Funcionario> {

    @EJB
    private CargoFacade cargoFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FuncionarioFacade() {
        super(Funcionario.class);
    }

    public List<Funcionario> getCobradores(Integer f) {
        TypedQuery q = this.em.createQuery("SELECT f FROM Funcionario f WHERE f.cargo.idcargo=:f", Funcionario.class);
        q.setParameter("f", f);
        return q.getResultList();
    }

    public List<Funcionario> getCobradores() {
        TypedQuery<Funcionario> qf = this.em.createQuery("SELECT f FROM Funcionario f WHERE f.cargo=:cargo ORDER BY f.activo DESC, f.nombres ASC, f.apellidos ASC", Funcionario.class);
        qf.setParameter("cargo", EntidadesEstaticas.CARGO_COBRADOR);
        List<Funcionario> lstfun = qf.getResultList();
        lstfun.add(this.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR));
        return lstfun;
    }

    public List<Funcionario> getCobradoresActivos() {
        TypedQuery<Funcionario> qf = this.em.createQuery("SELECT f FROM Funcionario f WHERE f.activo=true AND f.cargo=:cargo ORDER BY f.nombres ASC, f.apellidos ASC", Funcionario.class);
        qf.setParameter("cargo", EntidadesEstaticas.CARGO_COBRADOR);
        List<Funcionario> lstfun = qf.getResultList();
        lstfun.add(this.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR));
        return lstfun;
    }

    public void crearAdministrador() {
        System.out.println("En el metodo para crear administrador");
        Funcionario usuAdmin = this.find(EntidadesEstaticas.ADMIN_DEL_SISTEMA.getIdfuncionario());
        if (usuAdmin == null) {
            Cargo cargo = cargoFacade.find(1000);
            if (cargo == null) {
                System.out.println("creando cargo");
                cargo = new Cargo();
                cargo.setNombre("Sin Cargo");
                cargoFacade.create(cargo);
            }
            EntidadesEstaticas.ADMIN_DEL_SISTEMA.setCargo(cargo);
            this.create(EntidadesEstaticas.ADMIN_DEL_SISTEMA);
        } else {
            usuAdmin.getFuncionalidadList().clear();
            usuAdmin.getFuncionalidadList().addAll(EntidadesEstaticas.getListaFuncionalidades());
            this.edit(usuAdmin);
            System.out.println("El usuario ya existe en la base de datos");
        }
    }

    public Funcionario getFuncionarioPorDocumento(Integer ci) {
        TypedQuery<Funcionario> q = getEntityManager().createQuery("SELECT f FROM Funcionario f WHERE f.ci=:ci", Funcionario.class);
        q.setParameter("ci", ci);
        List<Funcionario> lst = q.getResultList();
        if (!lst.isEmpty()) {
            return lst.get(0);
        } else {
            return null;
        }
    }

    public List<Funcionario> buscarFuncionarios(String busqueda) {
        TypedQuery q = this.em.createQuery("SELECT f FROM Funcionario f WHERE LOWER(f.nombres) LIKE :busq OR LOWER(f.apellidos) LIKE :busq OR SQL('CAST(? AS CHAR(11))', f.ci) LIKE :busq", Funcionario.class);
        q.setParameter("busq", "%"+busqueda.toLowerCase()+"%");
        return q.getResultList();
    }

}
