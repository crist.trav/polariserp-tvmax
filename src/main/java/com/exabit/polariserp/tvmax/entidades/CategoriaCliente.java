/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "categoria_cliente")
@NamedQueries({
    @NamedQuery(name = "CategoriaCliente.findAll", query = "SELECT c FROM CategoriaCliente c")
    , @NamedQuery(name = "CategoriaCliente.findByIdcategoria", query = "SELECT c FROM CategoriaCliente c WHERE c.idcategoria = :idcategoria")
    , @NamedQuery(name = "CategoriaCliente.findByNombre", query = "SELECT c FROM CategoriaCliente c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "CategoriaCliente.findByPorcentajeDescuento", query = "SELECT c FROM CategoriaCliente c WHERE c.porcentajeDescuento = :porcentajeDescuento")})
public class CategoriaCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idcategoriacliente", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idcategoriacliente", table = "idsequences", allocationSize = 1, initialValue = 10)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcategoria")
    private Integer idcategoria;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "porcentaje_descuento")
    private Float porcentajeDescuento;
    @OneToMany(mappedBy = "categoria")
    private List<Cliente> clienteList;

    public CategoriaCliente() {
    }

    public CategoriaCliente(Integer idcategoria) {
        this.idcategoria = idcategoria;
    }

    public Integer getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(Integer idcategoria) {
        this.idcategoria = idcategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(Float porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoria != null ? idcategoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaCliente)) {
            return false;
        }
        CategoriaCliente other = (CategoriaCliente) object;
        if ((this.idcategoria == null && other.idcategoria != null) || (this.idcategoria != null && !this.idcategoria.equals(other.idcategoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.CategoriaCliente[ idcategoria=" + idcategoria + " ]";
    }
    
}
