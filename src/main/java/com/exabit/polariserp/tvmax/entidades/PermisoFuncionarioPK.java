/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Embeddable
public class PermisoFuncionarioPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "funcionario")
    private int funcionario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "funcionalidad")
    private int funcionalidad;

    public PermisoFuncionarioPK() {
    }

    public PermisoFuncionarioPK(int funcionario, int funcionalidad) {
        this.funcionario = funcionario;
        this.funcionalidad = funcionalidad;
    }

    public int getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(int funcionario) {
        this.funcionario = funcionario;
    }

    public int getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(int funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) funcionario;
        hash += (int) funcionalidad;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermisoFuncionarioPK)) {
            return false;
        }
        PermisoFuncionarioPK other = (PermisoFuncionarioPK) object;
        if (this.funcionario != other.funcionario) {
            return false;
        }
        if (this.funcionalidad != other.funcionalidad) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.PermisoFuncionarioPK[ funcionario=" + funcionario + ", funcionalidad=" + funcionalidad + " ]";
    }
    
}
