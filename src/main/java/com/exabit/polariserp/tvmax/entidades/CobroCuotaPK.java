/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author traver
 */
@Embeddable
public class CobroCuotaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "cuota")
    private int cuota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cobro")
    private int cobro;

    public CobroCuotaPK() {
    }

    public CobroCuotaPK(int cuota, int cobro) {
        this.cuota = cuota;
        this.cobro = cobro;
    }

    public int getCuota() {
        return cuota;
    }

    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    public int getCobro() {
        return cobro;
    }

    public void setCobro(int cobro) {
        this.cobro = cobro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) cuota;
        hash += (int) cobro;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CobroCuotaPK)) {
            return false;
        }
        CobroCuotaPK other = (CobroCuotaPK) object;
        if (this.cuota != other.cuota) {
            return false;
        }
        if (this.cobro != other.cobro) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.CobroCuotaPK[ cuota=" + cuota + ", cobro=" + cobro + " ]";
    }
    
}
