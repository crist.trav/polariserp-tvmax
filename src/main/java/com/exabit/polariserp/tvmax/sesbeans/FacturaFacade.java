package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.AsignacionFactura;
import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.MedioPago;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TalonarioFactura;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.UtilConversion;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class FacturaFacade extends AbstractFacade<Factura> {

    @Inject
    private SessionManBean sessionManBean;

    @EJB
    private MedioPagoFacade medioPagoFacade;

    @EJB
    private AsignacionFacturaFacade asignacionFacturaFacade;

    @EJB
    private DetalleFacturaFacade detalleFacturaFacade;

    @EJB
    private ConceptoFacade conceptoFacade;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private CobroFacade cobroFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private TalonarioFacturaFacade talonarioFacturaFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FacturaFacade() {
        super(Factura.class);
    }

    public List<Factura> generarFacturas(List<List<Cuota>> lstgc, Date fe, TalonarioFactura tf, Funcionario fr) {
        List<Factura> lstf = new ArrayList<>();
        int contadorNroFactura = tf.getNroActual();
        for (List<Cuota> lstc : lstgc) {
            Factura f = new Factura();
            f.setTotal(0);
            f.setIva10(0);
            f.setIva5(0);
            f.setAnulado(false);
            f.setCancelado(false);
            f.setCliente(lstc.get(0).getSuscripcion().getCliente());
            f.setContado(true);
            f.setFechaHoraRegistro(new Date());
            f.setFuncionarioRegistro(fr);
            f.setTalonarioFactura(tf);
            f.setFecha(fe);
            f.setNroFactura(contadorNroFactura);
            List<DetalleFactura> lstdf = new ArrayList<>();
            for (Cuota c : lstc) {
                DetalleFactura df = new DetalleFactura();
                df.setSuscripcion(c.getSuscripcion());
                df.setFactura(f);
                df.setCantidad(1);
                df.setCuota(c);
                df.setMonto(c.getMontoCuota() - c.getMontoEntrega());
                df.setIva10(0);
                df.setIva5(0);
                if (c.getServicio() == null) {
                    df.setServicio(c.getSuscripcion().getServicio());
                } else {
                    df.setServicio(c.getServicio());
                }
                df.setSubtotal(df.getCantidad() * df.getMonto());
                double impuesto = df.getServicio().getPorcentajeIva().doubleValue() / 100.0;
                double factorMulti = 1.0 + impuesto;
                if (df.getServicio().getPorcentajeIva().equals((short) 10)) {
                    int iva = (int) Math.round((df.getMonto() * impuesto) / factorMulti);
                    df.setIva10(iva);
                    f.setIva10(f.getIva10() + iva);
                } else if (df.getServicio().getPorcentajeIva().equals((short) 5)) {
                    int iva = (int) Math.round((df.getMonto() * impuesto) / factorMulti);
                    df.setIva5(iva);
                    f.setIva5(f.getIva10() + iva);
                }
                f.setTotal(f.getTotal() + df.getSubtotal());
                UtilConversion utilc = new UtilConversion();
                df.setDescripcion("(" + df.getSuscripcion().getIdsuscripcion() + ") " + df.getSuscripcion().getServicio().getNombre() + " |");
                if (!df.getServicio().getIdservicio().equals(df.getSuscripcion().getServicio().getIdservicio())) {
                    if(df.getCuota().getNroCuota()!=null && df.getCuota().getHistorialGeneracion()!=null){
                        df.setDescripcion(df.getDescripcion() + " " + df.getServicio().getNombre() + " cuota " + df.getCuota().getNroCuota() + "/" + df.getCuota().getHistorialGeneracion().getCantidadCuotas());
                    }else{
                        System.out.println("Nro cuota o historial generacion NULL, idcuota: "+df.getCuota().getIdcuota());
                        df.setDescripcion(df.getDescripcion() + " " + df.getServicio().getNombre());
                    }
                } else {
                    df.setDescripcion(df.getDescripcion() + " cuota " + utilc.getNombreMesCorto((int) df.getCuota().getMesCuota()) + "/" + df.getCuota().getAnioCuota());
                }
                df.setDescripcion(df.getDescripcion().toUpperCase());
                lstdf.add(df);
            }
            contadorNroFactura++;
            f.setDetalleFacturaList(lstdf);
            this.em.persist(f);
            lstf.add(f);
        }
        tf.setNroActual(contadorNroFactura);
        this.em.merge(tf);
        return lstf;
    }

    public Map<String, Object> generarFacturasCuotas(TalonarioFactura tf, Date fe, Cliente cliente, Funcionario fr) {
        Map<String, Object> hmResumen = new HashMap<>();
        Integer cantFacturasGen = 0;
        Integer cantFacturasAnul = 0;
        List<Factura> lstFacturasgeneradas = new LinkedList<>();
        Calendar c = new GregorianCalendar();
        c.setTime(fe);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
        System.out.println("Generar cuota fecha: " + sdf.format(c.getTime()));
        TypedQuery<Factura> qfc = this.em.createQuery("SELECT f FROM Factura f WHERE f.fecha<=:fecha AND f.cliente=:cli AND f.cancelado=false AND f.anulado=false", Factura.class);
        qfc.setParameter("fecha", fe);
        qfc.setParameter("cli", cliente);

        this.anularFacturas(qfc.getResultList(), "");
        cantFacturasAnul = qfc.getResultList().size();
        Calendar calFechaGen = new GregorianCalendar();
        calFechaGen.setTime(fe);
        TypedQuery<Cuota> qcuogen = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion.cliente=:cli AND c.cancelado=false AND c.mesCuota<=:mes AND c.anioCuota<=:anio", Cuota.class);
        qcuogen.setParameter("cli", cliente);
        qcuogen.setParameter("anio", calFechaGen.get(Calendar.YEAR));
        qcuogen.setParameter("mes", calFechaGen.get(Calendar.MONTH) + 1);
        List<Cuota> lstCuoGen = qcuogen.getResultList();
        for (Cuota cu : lstCuoGen) {

            System.out.println("cuota de " + cu.getMesCuota() + " " + cu.getAnioCuota());
            Factura f = this.generarFactura(tf, cu, fe, fr);
            lstFacturasgeneradas.add(f);
            cantFacturasGen = cantFacturasGen + 1;
        }
        hmResumen.put("cantgen", cantFacturasGen);
        hmResumen.put("cantanul", cantFacturasAnul);
        hmResumen.put("lstfacturasgen", lstFacturasgeneradas);
        return hmResumen;
    }

    public Map<String, Object> generarFacturasSuscripcion(TalonarioFactura tf, Date fe, Suscripcion s, Funcionario fr) {
        Map<String, Object> hmResumen = new HashMap<>();
        Integer cantFacturasGen = 0;
        Integer cantFacturasAnul = 0;
        List<Factura> lstFacturasgeneradas = new LinkedList<>();
        Calendar c = new GregorianCalendar();
        c.setTime(fe);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
        System.out.println("Generar cuota fecha: " + sdf.format(c.getTime()));

        TypedQuery<DetalleFactura> qdf = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.factura.fecha<=:fecha AND df.cuota.suscripcion=:sus AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        qdf.setParameter("sus", s);
        qdf.setParameter("fecha", fe);
//        TypedQuery<Factura> qfc = this.em.createQuery("SELECT f FROM Factura f WHERE f.fecha<=:fecha AND f.cliente=:cli AND f.cancelado=false AND f.anulado=false", Factura.class);
//        qfc.setParameter("fecha", fe);
//        qfc.setParameter("cli", cliente);
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        List<DetalleFactura> lstdetfanular = qdf.getResultList();
        for (DetalleFactura df : lstdetfanular) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        List<Factura> lstFacturasAnular = new ArrayList<>();
        lstFacturasAnular.addAll(hmFacturas.values());
        this.anularFacturas(lstFacturasAnular, "");
        cantFacturasAnul = lstFacturasAnular.size();
        Calendar calFechaGen = new GregorianCalendar();
        calFechaGen.setTime(fe);
        TypedQuery<Cuota> qcuogen = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.cancelado=false AND c.mesCuota<=:mes AND c.anioCuota<=:anio", Cuota.class);
        qcuogen.setParameter("sus", s);
        qcuogen.setParameter("anio", calFechaGen.get(Calendar.YEAR));
        qcuogen.setParameter("mes", calFechaGen.get(Calendar.MONTH) + 1);
        List<Cuota> lstCuoGen = qcuogen.getResultList();
        for (Cuota cu : lstCuoGen) {

            System.out.println("cuota de " + cu.getMesCuota() + " " + cu.getAnioCuota());
            Factura f = this.generarFactura(tf, cu, fe, fr);
            lstFacturasgeneradas.add(f);
            cantFacturasGen = cantFacturasGen + 1;
        }
        hmResumen.put("cantgen", cantFacturasGen);
        hmResumen.put("cantanul", cantFacturasAnul);
        hmResumen.put("lstfacturasgen", lstFacturasgeneradas);
        return hmResumen;
    }

    public Map<String, Object> generarFacturasCuotas(TalonarioFactura tf, Date fe, List<Cuota> lstCuoGen, Funcionario fr) {
        Map<String, Object> hmResumen = new HashMap<>();
        Integer cantFacturasGen = 0;
        Integer cantFacturasAnul = 0;
        List<Factura> lstFacturasgeneradas = new LinkedList<>();
        Calendar c = new GregorianCalendar();
        c.setTime(fe);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
        System.out.println("Generar cuota fecha: " + sdf.format(c.getTime()));
        Calendar calFechaGen = new GregorianCalendar();
        calFechaGen.setTime(fe);
        for (Cuota cu : lstCuoGen) {
            System.out.println("cuota de " + cu.getMesCuota() + " " + cu.getAnioCuota());
            Factura f = this.generarFactura(tf, cu, fe, fr);
            lstFacturasgeneradas.add(f);
            cantFacturasGen = cantFacturasGen + 1;
        }
        hmResumen.put("cantgen", cantFacturasGen);
        hmResumen.put("cantanul", cantFacturasAnul);
        hmResumen.put("lstfacturasgen", lstFacturasgeneradas);
        return hmResumen;
    }

    public Factura generarFactura(Cuota c) {
        TalonarioFactura talonActual = this.talonarioFacturaFacade.getTalonarioActivo();
        Factura nf = new Factura();
        nf.setAnulado(false);
        nf.setCancelado(false);
        nf.setCliente(c.getSuscripcion().getCliente());
        nf.setContado(true);
        nf.setFecha(new Date());
        nf.setIva5(0);
        nf.setTalonarioFactura(talonActual);
        nf.setNroFactura(talonActual.getNroActual());
        nf.setDetalleFacturaList(new LinkedList<DetalleFactura>());
        DetalleFactura detf = new DetalleFactura();
        detf.setFactura(nf);
        detf.setCantidad(1);
        detf.setServicio(this.servicioFacade.find(1));
        detf.setCuota(c);
        detf.setDescripcion("Cuota del mes de " + EntidadesEstaticas.getNombreMes(c.getMesCuota()) + " " + c.getAnioCuota());
        detf.setMonto(c.getMontoCuota());
        detf.setSubtotal((detf.getMonto() * detf.getCantidad()));
        nf.setTotal(detf.getSubtotal());
        if (detf.getServicio().getPorcentajeIva() == 10) {
            Integer iva10 = Math.round((detf.getSubtotal() * 10) / 110);
            nf.setIva10(iva10);
        }
        if (detf.getServicio().getPorcentajeIva() == 5) {
            Integer iva5 = Math.round((detf.getSubtotal() * 5) / 105);
            nf.setIva5(iva5);
        }
        nf.getDetalleFacturaList().add(detf);
        this.create(nf);
        talonActual.setNroActual(talonActual.getNroActual() + 1);
        this.talonarioFacturaFacade.edit(talonActual);
        return nf;
    }

    public Factura generarFactura(TalonarioFactura talonActual, Cuota c, Date fe, Funcionario fg) {
//        TalonarioFactura talonActual = this.talonarioFacturaFacade.getTalonarioActivo();
        Factura nf = new Factura();
        nf.setAnulado(false);
        nf.setCancelado(false);
        nf.setCliente(c.getSuscripcion().getCliente());
        nf.setContado(true);
        nf.setFecha(fe);
        nf.setIva5(0);
        nf.setTalonarioFactura(talonActual);
        nf.setNroFactura(talonActual.getNroActual());
        nf.setDetalleFacturaList(new LinkedList<DetalleFactura>());
        DetalleFactura detf = new DetalleFactura();
        detf.setFactura(nf);
        detf.setCantidad(1);
        detf.setServicio(this.servicioFacade.find(1));
        detf.setCuota(c);
        detf.setDescripcion("Cuota del mes de " + EntidadesEstaticas.getNombreMes(c.getMesCuota()) + " " + c.getAnioCuota());
        if (c.getMontoEntrega() != null) {
            detf.setMonto(c.getMontoCuota() - c.getMontoEntrega());
        } else {
            detf.setMonto(c.getMontoCuota());
        }
        detf.setSubtotal((detf.getMonto() * detf.getCantidad()));
        nf.setTotal(detf.getSubtotal());
        if (detf.getServicio().getPorcentajeIva() == 10) {
            Integer iva10 = Math.round((detf.getSubtotal() * 10) / 110);
            nf.setIva10(iva10);
        }
        if (detf.getServicio().getPorcentajeIva() == 5) {
            Integer iva5 = Math.round((detf.getSubtotal() * 5) / 105);
            nf.setIva5(iva5);
        }
        nf.getDetalleFacturaList().add(detf);
        nf.setFuncionarioRegistro(fg);
        nf.setFechaHoraRegistro(new Date());
        this.create(nf);
        talonActual.setNroActual(talonActual.getNroActual() + 1);
        AsignacionFactura af = new AsignacionFactura();
        af.setFechaHoraRegistro(new Date());
        af.setCobrador(c.getSuscripcion().getCobrador());
        af.setFactura(nf);
        this.asignacionFacturaFacade.create(af);
        this.talonarioFacturaFacade.edit(talonActual);
        return nf;
    }

    public List<Factura> getFacturasPorIDDesc() {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f ORDER BY f.idfactura DESC", Factura.class);
        return q.getResultList();
    }

    public List<Factura> getFacturasFiltro(Integer idCliente, boolean pendientes, boolean pagados, boolean anulados, Date desde, Date hasta, String textoBusq) {
        TypedQuery<Factura> q;
        String consulta = null;
        List<Factura> lstFacturas = new LinkedList<>();
//        TypedQuery af=this.em.createQuery("SELECT f FROM Factura f WHERE f.cliente.razonSocial LIKE :tx OR f.cliente.idcliente OR SQL('CAST(? AS CHAR(11))', f.nroFactura) LIKE :tx");
        if (pendientes || pagados) {
            if (pendientes && pagados && anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.anulado=true";

            } else if (!pendientes && pagados && anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.cancelado=true AND f.anulado=true";

            } else if (pendientes && !pagados && anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.cancelado=false AND f.anulado=true";

            } else if (pendientes && pagados && !anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.anulado=false";
            } else if (!pendientes && pagados && !anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.cancelado=true AND f.anulado=false";
            } else if (pendientes && !pagados && !anulados) {
                consulta = "SELECT f FROM Factura f WHERE f.cancelado=false AND f.anulado=false";
            }
            consulta = consulta + " AND f.fecha>=:fdesde AND f.fecha<=:fhasta";
            if (idCliente != null) {
                Cliente c = this.clienteFacade.find(idCliente);
                consulta = consulta + " AND f.cliente=:cli";
                if (!textoBusq.isEmpty()) {
                    consulta = consulta + " AND (f.cliente.razonSocial LIKE :tx OR SQL('CAST(? AS CHAR(11))',f.cliente.idcliente) LIKE :tx OR SQL('CAST(? AS CHAR(11))', f.nroFactura) LIKE :tx)";
                }
                q = this.em.createQuery(consulta, Factura.class);
                if (!textoBusq.isEmpty()) {
                    q.setParameter("tx", "%" + textoBusq.toLowerCase() + "%");
                }
                q.setParameter("cli", c);
                q.setParameter("fdesde", desde);
                q.setParameter("fhasta", hasta);

                lstFacturas.addAll(q.getResultList());
            } else {
                if (!textoBusq.isEmpty()) {
                    consulta = consulta + " AND (f.cliente.razonSocial LIKE :tx OR SQL('CAST(? AS CHAR(11))',f.cliente.idcliente) LIKE :tx OR SQL('CAST(? AS CHAR(11))', f.nroFactura) LIKE :tx)";
                }
                q = this.em.createQuery(consulta, Factura.class);
                if (!textoBusq.isEmpty()) {
                    q.setParameter("tx", "%" + textoBusq.toLowerCase() + "%");
                }
//                q = this.em.createQuery(consulta, Factura.class);
                q.setParameter("fdesde", desde);
                q.setParameter("fhasta", hasta);
                lstFacturas.addAll(q.getResultList());
            }
        }
        return lstFacturas;
    }

    public Integer getCantidadFacturas(Funcionario cobrador, Integer anio, Integer mes) {
        System.out.println("anio recibido: " + anio);
        System.out.println("mes recibido: " + mes);
        System.out.println("Cantidad facturas calculo");
        Calendar cdesde = new GregorianCalendar(anio, (mes - 1), 1);
        Calendar chasta = new GregorianCalendar(anio, (mes - 1), 1);
        System.out.println("Cantidad facturas calculo");

        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        System.out.println(cdesde.get(Calendar.MONTH));
        System.out.println(cdesde.get(Calendar.DAY_OF_MONTH));
        System.out.println(chasta.get(Calendar.MONTH));
        System.out.println(chasta.get(Calendar.DAY_OF_MONTH));
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota.suscripcion.cobrador=:cobrador AND df.factura.fecha>=:fdesde AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cobrador", cobrador);
        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        return hmFacturas.size();
    }

    public String getFacturaInferior(Funcionario cobrador, Integer anio, Integer mes) {
        Calendar cdesde = new GregorianCalendar(anio, (mes - 1), 1);
        Calendar chasta = new GregorianCalendar(anio, (mes - 1), 1);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota.suscripcion.cobrador=:cobrador AND df.factura.fecha>=:fdesde AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cobrador", cobrador);
        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        Factura fmenor = null;
        for (Factura f : hmFacturas.values()) {
            if (fmenor == null) {
                fmenor = f;
            } else {
                if (f.getNroFactura() < fmenor.getNroFactura()) {
                    fmenor = f;
                }
            }
        }
        if (fmenor == null) {
            return "(Ninguno)";
        } else {
            return fmenor.getTalonarioFactura().getCodEstablecimiento() + "-" + fmenor.getNroFactura();
        }

    }

    public String getFacturaSuperior(Funcionario cobrador, Integer anio, Integer mes) {
        Calendar cdesde = new GregorianCalendar(anio, (mes - 1), 1);
        Calendar chasta = new GregorianCalendar(anio, (mes - 1), 1);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota.suscripcion.cobrador=:cobrador AND df.factura.fecha>=:fdesde AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cobrador", cobrador);
        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        Factura fmayor = null;
        for (Factura f : hmFacturas.values()) {
            if (fmayor == null) {
                fmayor = f;
            } else {
                if (f.getNroFactura() > fmayor.getNroFactura()) {
                    fmayor = f;
                }
            }
        }
        if (fmayor == null) {
            return "(Ninguno)";
        } else {
            return fmayor.getTalonarioFactura().getCodEstablecimiento() + "-" + fmayor.getNroFactura();
        }

    }

    public List<Factura> getFacturasPendientes(Funcionario cobrador, Integer anio, Integer mes) {
        Calendar cdesde = new GregorianCalendar(anio, (mes - 1), 1);
        Calendar chasta = new GregorianCalendar(anio, (mes - 1), 1);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota.suscripcion.cobrador=:cobrador AND df.factura.fecha>=:fdesde AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false ORDER BY df.factura.nroFactura ASC", DetalleFactura.class);
        q.setParameter("cobrador", cobrador);
        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        List<Factura> lstFac = new LinkedList<>();

        lstFac.addAll(hmFacturas.values());
        List<Factura> lstFacOrde = new LinkedList<>();
        for (Factura fd : lstFac) {
            boolean hayMenor = false;
            for (Factura fo : lstFacOrde) {
                if (fd.getNroFactura() < fo.getNroFactura()) {
                    lstFacOrde.add(lstFacOrde.indexOf(fo), fd);
                    hayMenor = true;
                    break;
                }
            }
            if (!hayMenor) {
                lstFacOrde.add(fd);
            }
        }
        return lstFacOrde;
    }

    public List<Factura> getFacturasPendientes(Cliente cliente, Integer anioInicio, Integer mesInicio) {
//        Calendar cdesde=new GregorianCalendar(anioInicio, (mesInicio-1),1);
        Calendar chasta = new GregorianCalendar(anioInicio, (mesInicio - 1), 1);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.factura.cliente=:cli AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cli", cliente);
//        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        List<Factura> lstFac = new LinkedList<>();

        lstFac.addAll(hmFacturas.values());
        List<Factura> lstFacOrde = new LinkedList<>();
        for (Factura fd : lstFac) {
            boolean hayMenor = false;
            for (Factura fo : lstFacOrde) {
                if (fd.getNroFactura() < fo.getNroFactura()) {
                    lstFacOrde.add(lstFacOrde.indexOf(fo), fd);
                    hayMenor = true;
                    break;
                }
            }
            if (!hayMenor) {
                lstFacOrde.add(fd);
            }
        }
        return lstFacOrde;
    }

    public List<Factura> getFacturasPendientes(Suscripcion s, Integer anioInicio, Integer mesInicio) {
//        Calendar cdesde=new GregorianCalendar(anioInicio, (mesInicio-1),1);
        Calendar chasta = new GregorianCalendar(anioInicio, (mesInicio - 1), 1);
        chasta.add(Calendar.MONTH, 1);
        chasta.add(Calendar.DAY_OF_MONTH, -1);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota.suscripcion=:sus AND df.factura.fecha<=:fhasta AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("sus", s);
//        q.setParameter("fdesde", cdesde.getTime());
        q.setParameter("fhasta", chasta.getTime());
        List<DetalleFactura> lstDF = q.getResultList();
        Map<Integer, Factura> hmFacturas = new HashMap<>();
        for (DetalleFactura df : lstDF) {
            hmFacturas.put(df.getFactura().getIdfactura(), df.getFactura());
        }
        List<Factura> lstFac = new LinkedList<>();

        lstFac.addAll(hmFacturas.values());
        List<Factura> lstFacOrde = new LinkedList<>();
        for (Factura fd : lstFac) {
            boolean hayMenor = false;
            for (Factura fo : lstFacOrde) {
                if (fd.getNroFactura() < fo.getNroFactura()) {
                    lstFacOrde.add(lstFacOrde.indexOf(fo), fd);
                    hayMenor = true;
                    break;
                }
            }
            if (!hayMenor) {
                lstFacOrde.add(fd);
            }
        }
        return lstFacOrde;
    }

    public List<Factura> getFacturasPendientes(Date desde, Date hasta) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.anulado=false AND f.cancelado=false and f.fecha>=:fdesde AND f.fecha<=:fhasta", Factura.class);
        q.setParameter("fdesde", desde);
        q.setParameter("fhasta", hasta);
        return q.getResultList();
    }

    public List<Factura> getRangoFactura(TalonarioFactura tf, Integer nroInicio, Integer nroFin) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.talonarioFactura=:talon AND f.nroFactura>=:ninicio AND f.nroFactura<=:nfin", Factura.class);
//        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.talonarioFactura=:talon AND f.nroFactura>=:ninicio AND f.nroFactura<=:nfin AND f.cancelado=false AND f.anulado=false", Factura.class);
        q.setParameter("talon", tf);
        q.setParameter("ninicio", nroInicio);
        q.setParameter("nfin", nroFin);
        return q.getResultList();
    }

    public void registrarNuevaFacturaExistente(Factura f, Funcionario cobra, Suscripcion s, Funcionario fr) {
        f.setCancelado(true);
        f.setFuncionarioRegistro(fr);
        f.setFechaHoraRegistro(new Date());
        this.create(f);
        System.out.println("El estado de la suscripcion es: " + s.getEstado());
//        if (s.getEstado().equals(EntidadesEstaticas.ESTADO_DESCONECTADO) || s.getEstado().equals(EntidadesEstaticas.ESTADO_INACTIVO)) {
//            System.out.println("Se genera nueva solicitud");
//            SolicitudCambioEstado sce = new SolicitudCambioEstado();
//            sce.setSuscripcion(s);
//            sce.setConcretado(false);
//            sce.setEstadoAnterior(s.getEstado());
//            sce.setEstadoNuevo(this.estadoSuscripcionFacade.find(EntidadesEstaticas.ESTADO_ACTIVO.getIdestado()));
//            sce.setFechaSolicitud(new Date());
//            sce.setObservacion("Reconexion (Generado automaticamente)");
//            this.solicitudCambioEstadoFacade.create(sce);
//        } else {
//
//        }
        if (f.getNroFactura().equals(f.getTalonarioFactura().getNroActual())) {
            TalonarioFactura tf = f.getTalonarioFactura();
            tf.setNroActual(tf.getNroActual() + 1);
            this.talonarioFacturaFacade.edit(tf);
        }
        for (DetalleFactura dff : f.getDetalleFacturaList()) {
            Cobro c = new Cobro();
            c.setDetalleFactura(dff);
//            c.setCuotaList(new LinkedList<Cuota>());
            c.setAnulado(false);
            c.setCobrador(cobra);
            c.setFecha(f.getFecha());
            c.setFechaHoraRegistro(new Date());
            c.setMedioPago(this.medioPagoFacade.find(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago()));
            c.setMonto(dff.getSubtotal());
            c.setPorceComisionAplicado(0.0f);
            c.setFuncionarioRegistro(fr);
            if (dff.getCuota() != null) {
                Cuota cuo = dff.getCuota();
                c.setConcepto(this.conceptoFacade.find(EntidadesEstaticas.CONCEPTO_COBRO_CUOTA.getIdconcepto()));
                if (dff.getMonto() == 0) {
                    cuo.setMontoCuota(0);
                    cuo.setMontoEntrega(0);
                    cuo.setCancelado(true);
                    if (cuo.getIdcuota() == null) {
                        this.cuotaFacade.create(cuo);
                    } else {
                        this.cuotaFacade.edit(cuo);
                    }
                } else {
                    Integer pagoResultante = 0;
                    if (cuo.getMontoEntrega() != null) {
                        pagoResultante = pagoResultante + cuo.getMontoEntrega();
                    }
                    pagoResultante = pagoResultante + dff.getSubtotal();
                    if (pagoResultante >= cuo.getMontoCuota()) {
                        cuo.setCancelado(true);
                    }
                    cuo.setMontoEntrega(pagoResultante);
                    cuo.setFechaPago(f.getFecha());
                    if (cuo.getIdcuota() == null) {
                        this.cuotaFacade.create(cuo);
                    } else {
                        this.cuotaFacade.edit(cuo);
                    }
                }
                c.setCuota(cuo);
            } else {
                c.setConcepto(dff.getServicio().getConceptoCobro());
            }
            this.cobroFacade.edit(c);
        }

    }

    public void registrarFacturaNuevaConexion(Factura f, Funcionario cobra, Funcionario fr) {
        Suscripcion s = new Suscripcion();
        s.setCantidadTvs(1);
        Cliente cl = f.getCliente();
        this.clienteFacade.create(cl);
        s.setCliente(f.getCliente());
        s.setCobrador(this.funcionarioFacade.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR));
        s.setCuotaList(new ArrayList<Cuota>());
        s.setDiaVencimientoMes((short) 1);
        s.setEstado(EntidadesEstaticas.ESTADO_NUEVO);
        s.setFechaSuscripcion(new Date());
        s.setPrecio(servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_TVCABLE).getPrecio());
        s.setServicio(servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_TVCABLE));
        s.setTipoSuscripcion(this.tipoSuscripcionFacade.find(EntidadesEstaticas.ID_TIPO_SUSCRIPCION_NORMAL));
        s.setTipoVivienda(EntidadesEstaticas.TIPO_VIVIENDA_PROPIA);
        s.setBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO);
        this.suscripcionFacade.create(s);
        SolicitudCambioEstado sce = new SolicitudCambioEstado();
        sce.setSuscripcion(s);
        sce.setEstadoAnterior(s.getEstado());
        sce.setEstadoNuevo(EntidadesEstaticas.ESTADO_CONECTADO);
        sce.setFechaSolicitud(new Date());
        sce.setObservacion("Nueva suscripcion (Generado automáticamente)");
        sce.setConcretado(false);
        this.solicitudCambioEstadoFacade.create(sce);
        f.setCancelado(true);
        f.setContado(true);
        f.setFechaHoraRegistro(new Date());
        f.setFuncionarioRegistro(fr);
        for (DetalleFactura d : f.getDetalleFacturaList()) {
            if (d.getCuota() != null) {
                if (d.getCuota().getSuscripcion() == null) {
                    d.getCuota().setSuscripcion(s);
                    d.getCuota().setMontoCuota(d.getMonto());
                    d.getCuota().setMontoEntrega(d.getMonto());
                    if (d.getCuota().getIdcuota() == null) {
                        this.cuotaFacade.create(d.getCuota());
                    }
                }
            }
        }
        this.create(f);
        TalonarioFactura tf = f.getTalonarioFactura();
        tf.setNroActual(tf.getNroActual() + 1);
        this.talonarioFacturaFacade.edit(tf);
        for (DetalleFactura df : f.getDetalleFacturaList()) {
            if (df.getSubtotal() > 0) {
                Cobro cob = new Cobro();
                cob.setFuncionarioRegistro(fr);
                cob.setAnulado(false);
                if (df.getCuota() != null) {
                    df.getCuota().setSuscripcion(s);
                }
                cob.setFecha(new Date());
                cob.setFechaHoraRegistro(new Date());
                cob.setMedioPago(this.medioPagoFacade.find(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago()));
                cob.setMonto(df.getSubtotal());
                if (cobra.getPorcentajeComision() != null) {
                    cob.setPorceComisionAplicado(cobra.getPorcentajeComision());
                }

                cob.setCobrador(cobra);
                cob.setDetalleFactura(df);
                if (df.getCuota() == null) {
                    cob.setConcepto(this.conceptoFacade.find(EntidadesEstaticas.CONCEPTO_COBRO_CUOTA.getIdconcepto()));
                } else {
                    cob.setConcepto(df.getServicio().getConceptoCobro());
                }
                this.cobroFacade.create(cob);
            } else {
                System.out.println("Detalle factura exonerado");
            }
        }

    }

    public Factura getFacturaPendientePorNro(Integer nroFactura) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.cancelado=false AND f.anulado=false AND f.nroFactura=:no", Factura.class);
        q.setParameter("no", nroFactura);
        List<Factura> lstf = q.getResultList();
        if (!lstf.isEmpty()) {
            return lstf.get(0);
        } else {
            return null;
        }
    }

    public void registrarPagoFacturas(List<Factura> lstFact) {
        for (Factura f : lstFact) {
            f.setCancelado(true);
            this.edit(f);
            Funcionario tmpCobradorCuota = null;
            for (DetalleFactura df : f.getDetalleFacturaList()) {
                if (df.getCuota() != null) {
                    tmpCobradorCuota = df.getCuota().getSuscripcion().getCobrador();
                }
            }
            for (DetalleFactura df : f.getDetalleFacturaList()) {
                Cuota cuo = df.getCuota();
                Concepto cc;
                if (cuo != null) {
                    cuo.setCancelado(true);
                    cuo.setMontoEntrega(df.getSubtotal());
                    this.cuotaFacade.edit(cuo);
                    cc = this.conceptoFacade.find(EntidadesEstaticas.CONCEPTO_COBRO_CUOTA.getIdconcepto());
                } else {
                    cc = df.getServicio().getConceptoCobro();
                }

                Cobro cob = new Cobro();
                cob.setAnulado(false);
                if (tmpCobradorCuota != null) {
                    cob.setCobrador(tmpCobradorCuota);
                } else {
                    cob.setCobrador(this.clienteFacade.getCobrador(f.getCliente()));
                }
                cob.setConcepto(cc);
                cob.setFecha(new Date());
                cob.setFechaHoraRegistro(new Date());
                MedioPago mp = this.medioPagoFacade.find(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago());
                cob.setMedioPago(mp);
                cob.setMonto(df.getSubtotal());
                if (cob.getCobrador().getPorcentajeComision() != null) {
                    cob.setPorceComisionAplicado(cob.getCobrador().getPorcentajeComision());
                } else {
                    cob.setPorceComisionAplicado(0.0f);
                }
                cob.setDetalleFactura(df);
                cob.setFuncionarioRegistro(this.sessionManBean.getFuncionarioVar());
                this.cobroFacade.create(cob);
            }
        }
    }

    public void registrarCobroFacturas(List<Factura> lstFact, Date fechaCobro, Funcionario funcionarioRegistro) {
        for (Factura f : lstFact) {
            f.setCancelado(true);
            this.edit(f);
            for (DetalleFactura df : f.getDetalleFacturaList()) {
                Cobro cob = new Cobro();
//                cob.setCuotaList(new ArrayList<>());

                Cuota cuo = df.getCuota();
                if (cuo != null) {
                    cuo.setCancelado(true);
                    cuo.setMontoEntrega(df.getSubtotal());
                    cuo.setFechaPago(fechaCobro);
//                    cob.getCuotaList().add(cuo);
                    cob.setCuota(cuo);
                    this.cuotaFacade.edit(cuo);
                }
                cob.setSuscripcion(cuo.getSuscripcion());
                cob.setServicio(df.getServicio());
                cob.setAnulado(false);
                cob.setCobrador(f.getCliente().getCobrador());
                cob.setConcepto(df.getServicio().getConceptoCobro());
                cob.setFecha(fechaCobro);
                cob.setFechaHoraRegistro(new Date());
                MedioPago mp = this.medioPagoFacade.find(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO.getIdmedioPago());
                cob.setMedioPago(mp);
                cob.setMonto(df.getSubtotal());
                if (cob.getCobrador().getPorcentajeComision() != null) {
                    cob.setPorceComisionAplicado(cob.getCobrador().getPorcentajeComision());
                } else {
                    cob.setPorceComisionAplicado(0.0f);
                }
                cob.setDetalleFactura(df);
                cob.setFuncionarioRegistro(funcionarioRegistro);
                this.cobroFacade.create(cob);
            }
        }
    }

    public void anularFactura(Factura f, String observacion) {
        f.setAnulado(true);
        this.edit(f);
        if (f.getCancelado()) {
            for (DetalleFactura df : f.getDetalleFacturaList()) {
                Cobro coAnular = this.cobroFacade.getCobroPorDetalleFactura(df);
                if (coAnular != null) {
                    coAnular.setAnulado(true);
                    this.cobroFacade.edit(coAnular);
                } else {
                    System.out.println("Cobro encontrado en busqueda NULL");
                    for (Cobro c : df.getCobroList()) {
                        Cobro co = c;
                        co.setAnulado(true);
                        this.cobroFacade.edit(co);
                    }
                }
                if (df.getCuota() != null) {
                    if (!this.detalleFacturaFacade.existeDetallePorCuotaPagadosDistinto(df.getCuota(), df)) {
                        Cuota cu = df.getCuota();
                        cu.setCancelado(false);
                        cu.setFechaPago(null);
                        if (df.getFactura().getCancelado()) {
                            int montoEntrega = 0;
                            if (cu.getMontoEntrega() != null) {
                                montoEntrega = cu.getMontoEntrega();
                            }
                            if (montoEntrega >= df.getSubtotal()) {
                                cu.setMontoEntrega(montoEntrega - df.getSubtotal());
                            }
                        }
                        this.cuotaFacade.edit(cu);
                    }
                }
            }
        }
    }

    public void anularFacturas(List<Factura> lstf, String observacion) {
        for (Factura f : lstf) {
            this.anularFactura(f, observacion);
//            for (DetalleFactura df : f.getDetalleFacturaList()) {
//                Cuota c = df.getCuota();
//                if (c != null) {
//                    if (!this.detalleFacturaFacade.existeDetallePorCuotaPagadosDistinto(c, df)) {
//                        c.setCancelado(false);
//                        c.setFechaPago(null);
//                        if (df.getFactura().getCancelado()) {
//                            int montoEntrega = 0;
//                            if (c.getMontoEntrega() != null) {
//                                montoEntrega = c.getMontoEntrega();
//                            }
//                            if (montoEntrega >= df.getSubtotal()) {
//                                c.setMontoEntrega(montoEntrega - df.getSubtotal());
//                            }
//                        }
//                        this.cuotaFacade.edit(c);
//                    }
//                    for (Cobro cob : c.getCobroList()) {
//                        cob.setAnulado(true);
//                        this.cobroFacade.edit(cob);
//
//                    }
//                } else {
//                    for (Cobro co : df.getCobroList()) {
//                        co.setAnulado(true);
//                        this.cobroFacade.edit(co);
//                    }
//                }
//
//            }
////            List<Cobro> lstAnularCobros = this.cobroFacade.getCobrosPorFactura(f);
////            for (Cobro cob : lstAnularCobros) {
////                cob.setAnulado(true);
////                this.cobroFacade.edit(cob);
////            }
//            f.setAnulado(true);
//            f.setFechaHoraAnulacion(new Date());
//            f.setObservacion(observacion);
//            this.edit(f);
        }
    }

    public Factura getFacturaPendientePorCuota(Integer idcuota) {
        Cuota c = this.cuotaFacade.find(idcuota);
        TypedQuery<DetalleFactura> q = this.em.createQuery("SELECT df FROM DetalleFactura df WHERE df.cuota=:cuo AND df.factura.cancelado=false AND df.factura.anulado=false", DetalleFactura.class);
        q.setParameter("cuo", c);
        List<DetalleFactura> lstdf = q.getResultList();
        if (!lstdf.isEmpty()) {
            return lstdf.get(0).getFactura();
        } else {
            return null;
        }
    }

    public Factura getFacturaPendientePorCuota(Cuota c) {
        return this.getFacturaPendientePorCuota(c.getIdcuota());
    }

    public String getNroFacturaPendiente(Integer idCuota) {
        Factura f = this.getFacturaPendientePorCuota(idCuota);
        String nroFactura = "";
        if (f != null) {
            DecimalFormat df = new DecimalFormat("0000000");
            nroFactura = f.getTalonarioFactura().getCodEstablecimiento() + "-" + df.format(f.getNroFactura());
        }
        return nroFactura;
    }

    public boolean existeFacturaPendiente(Integer idcuota) {
        if (idcuota != null) {
            return !this.getNroFacturaPendiente(idcuota).isEmpty();
        } else {
            return false;
        }
    }

    public Factura getFacturaNoAnuladoPorNro(Integer nroFactura) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.anulado=false AND f.nroFactura=:no", Factura.class);
        q.setParameter("no", nroFactura);
        List<Factura> lstf = q.getResultList();
        if (!lstf.isEmpty()) {
            return lstf.get(0);
        } else {
            return null;
        }
    }

    public void anularFacturasManuales(TalonarioFactura tl, Integer nroFactura) {
        Factura fAnul = new Factura();
        fAnul.setTalonarioFactura(tl);
        fAnul.setNroFactura(nroFactura);
        fAnul.setCliente(this.clienteFacade.find(EntidadesEstaticas.CLIENTE_ANULACION_FACTURA.getIdcliente()));
        fAnul.setAnulado(true);
        fAnul.setFechaHoraRegistro(new Date());
        fAnul.setFechaHoraAnulacion(new Date());
        fAnul.setTotal(0);
        fAnul.setIva10(0);
        fAnul.setIva5(0);
        fAnul.setContado(true);
        this.create(fAnul);
    }

    public boolean existeFacturaAnulada(TalonarioFactura tal, Integer nroFactura) {
        TypedQuery<Factura> q = this.em.createQuery("SELECT f FROM Factura f WHERE f.talonarioFactura=:talon AND f.nroFactura=:nro AND f.anulado=true", Factura.class);
        q.setParameter("talon", tal);
        q.setParameter("nro", nroFactura);
        return !q.getResultList().isEmpty();
    }

    public Factura registrarFactura(Factura f) {
        if (f.getTalonarioFactura() != null) {
            TalonarioFactura tf = f.getTalonarioFactura();
            tf.setNroActual(f.getNroFactura() + 1);
            this.em.merge(tf);
            this.em.persist(f);
            for (DetalleFactura df : f.getDetalleFacturaList()) {
                boolean registrarCobro = true;
                if (df.getCuota() != null) {
                    System.out.println("Cuota detalle: "+df.getCuota().getIdcuota()+", cancelado: "+df.getCuota().getCancelado());
                    if (df.getCuota().getCancelado()) {
                        registrarCobro = false;
                        TypedQuery<Cobro> tqc=em.createQuery("SELECT c FROM Cobro c WHERE c.cuota=:cuo AND c.anulado=false", Cobro.class);
                        tqc.setParameter("cuo", df.getCuota());
                        List<Cobro> lstCob=tqc.getResultList();
                        if(!lstCob.isEmpty()){
                            Cobro c=lstCob.get(0);
                            c.setDetalleFactura(df);
                            this.em.merge(c);
                        }
                    }
                }
                System.out.println("se registra cobro: "+registrarCobro);
                if (registrarCobro) {
                    Cobro co = new Cobro();
                    
                    co.setAnulado(false);
                    co.setCobrador(f.getCliente().getCobrador());
                    co.setConcepto(df.getServicio().getConceptoCobro());
                    co.setDetalleFactura(df);
                    co.setFecha(f.getFecha());
                    co.setFechaHoraRegistro(new Date());
                    co.setFuncionarioRegistro(f.getFuncionarioRegistro());
                    co.setMedioPago(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO);
                    co.setMonto(df.getSubtotal());
                    co.setServicio(df.getServicio());
                    co.setSuscripcion(df.getSuscripcion());
                    co.setCuota(df.getCuota());
                    if (df.getCuota() != null) {
                        Cuota c = df.getCuota();
                        co.setCuota(c);
                        c.setCancelado(true);
                        c.setMontoEntrega(c.getMontoCuota());
                        c.setFechaPago(f.getFecha());
                        this.em.merge(c);
                    }
                    this.em.persist(co);
                }
            }
            return f;
        } else {
            return null;
        }
    }
}
