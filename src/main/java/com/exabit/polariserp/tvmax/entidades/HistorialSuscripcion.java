/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "historial_suscripcion")
@NamedQueries({
    @NamedQuery(name = "HistorialSuscripcion.findAll", query = "SELECT h FROM HistorialSuscripcion h")
    , @NamedQuery(name = "HistorialSuscripcion.findByIdhistorialSuscripcion", query = "SELECT h FROM HistorialSuscripcion h WHERE h.idhistorialSuscripcion = :idhistorialSuscripcion")
    , @NamedQuery(name = "HistorialSuscripcion.findByFechaHoraCambio", query = "SELECT h FROM HistorialSuscripcion h WHERE h.fechaHoraCambio = :fechaHoraCambio")
    , @NamedQuery(name = "HistorialSuscripcion.findByValorAnterior", query = "SELECT h FROM HistorialSuscripcion h WHERE h.valorAnterior = :valorAnterior")
    , @NamedQuery(name = "HistorialSuscripcion.findByValorNuevo", query = "SELECT h FROM HistorialSuscripcion h WHERE h.valorNuevo = :valorNuevo")
    , @NamedQuery(name = "HistorialSuscripcion.findByClaveRefAnterior", query = "SELECT h FROM HistorialSuscripcion h WHERE h.claveRefAnterior = :claveRefAnterior")
    , @NamedQuery(name = "HistorialSuscripcion.findByClaveRefNuevo", query = "SELECT h FROM HistorialSuscripcion h WHERE h.claveRefNuevo = :claveRefNuevo")})
public class HistorialSuscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idhistorialsuscripcion", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idhistorialsuscripcion", table = "idsequences", allocationSize = 1, initialValue = 5422)
    @Basic(optional = false)
    @Column(name = "idhistorial_suscripcion")
    private Integer idhistorialSuscripcion;
    @Column(name = "fecha_hora_cambio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraCambio;
    @Size(max = 250)
    @Column(name = "valor_anterior")
    private String valorAnterior;
    @Size(max = 250)
    @Column(name = "valor_nuevo")
    private String valorNuevo;
    @Column(name = "clave_ref_anterior")
    private Integer claveRefAnterior;
    @Column(name = "clave_ref_nuevo")
    private Integer claveRefNuevo;
    @JoinColumn(name = "campo_suscripcion", referencedColumnName = "idcampo_suscripcion")
    @ManyToOne(optional = false)
    private CampoSuscripcion campoSuscripcion;
    @JoinColumn(name = "funcionario", referencedColumnName = "idfuncionario")
    @ManyToOne(optional = false)
    private Funcionario funcionario;
    @JoinColumn(name = "suscripcion", referencedColumnName = "idsuscripcion")
    @ManyToOne(optional = false)
    private Suscripcion suscripcion;

    public HistorialSuscripcion() {
    }

    public HistorialSuscripcion(Integer idhistorialSuscripcion) {
        this.idhistorialSuscripcion = idhistorialSuscripcion;
    }

    public Integer getIdhistorialSuscripcion() {
        return idhistorialSuscripcion;
    }

    public void setIdhistorialSuscripcion(Integer idhistorialSuscripcion) {
        this.idhistorialSuscripcion = idhistorialSuscripcion;
    }

    public Date getFechaHoraCambio() {
        return fechaHoraCambio;
    }

    public void setFechaHoraCambio(Date fechaHoraCambio) {
        this.fechaHoraCambio = fechaHoraCambio;
    }

    public String getValorAnterior() {
        return valorAnterior;
    }

    public void setValorAnterior(String valorAnterior) {
        this.valorAnterior = valorAnterior;
    }

    public String getValorNuevo() {
        return valorNuevo;
    }

    public void setValorNuevo(String valorNuevo) {
        this.valorNuevo = valorNuevo;
    }

    public Integer getClaveRefAnterior() {
        return claveRefAnterior;
    }

    public void setClaveRefAnterior(Integer claveRefAnterior) {
        this.claveRefAnterior = claveRefAnterior;
    }

    public Integer getClaveRefNuevo() {
        return claveRefNuevo;
    }

    public void setClaveRefNuevo(Integer claveRefNuevo) {
        this.claveRefNuevo = claveRefNuevo;
    }

    public CampoSuscripcion getCampoSuscripcion() {
        return campoSuscripcion;
    }

    public void setCampoSuscripcion(CampoSuscripcion campoSuscripcion) {
        this.campoSuscripcion = campoSuscripcion;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idhistorialSuscripcion != null ? idhistorialSuscripcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistorialSuscripcion)) {
            return false;
        }
        HistorialSuscripcion other = (HistorialSuscripcion) object;
        if ((this.idhistorialSuscripcion == null && other.idhistorialSuscripcion != null) || (this.idhistorialSuscripcion != null && !this.idhistorialSuscripcion.equals(other.idhistorialSuscripcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.HistorialSuscripcion[ idhistorialSuscripcion=" + idhistorialSuscripcion + " ]";
    }
    
}
