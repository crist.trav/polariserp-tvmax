/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author frioss
 */
@Stateless
public class ModuloFacade extends AbstractFacade<Modulo> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ModuloFacade() {
        super(Modulo.class);
        this.lstModulosEstaticos.add(MODULO_CLIENTES);
        this.lstModulosEstaticos.add(MODULO_SERVICIOS);
        this.lstModulosEstaticos.add(MODULO__CATEGORIA_CLIENTES);
        this.lstModulosEstaticos.add(MODULO__PERMISOS);
        this.lstModulosEstaticos.add(MODULO__FUNCIONALIDAD);
        this.lstModulosEstaticos.add(MODULO___MODULO);
        this.lstModulosEstaticos.add(MODULO_SUSCRIPCIONES);
    }
    
    private final List<Modulo> lstModulosEstaticos=new LinkedList<>();
    
    public static final Modulo MODULO_CLIENTES = new Modulo();
    public static final Modulo MODULO_SERVICIOS = new Modulo();
    public static final Modulo MODULO__CATEGORIA_CLIENTES = new Modulo();
    public static final Modulo MODULO__PERMISOS = new Modulo();
    public static final Modulo MODULO__FUNCIONALIDAD = new Modulo();
    public static final Modulo MODULO___MODULO = new Modulo();
    public static final Modulo MODULO_SUSCRIPCIONES=new Modulo();
    
    static {
        MODULO_CLIENTES.setIdmodulo(1);
        MODULO_CLIENTES.setNombre("Clientes");
        MODULO__CATEGORIA_CLIENTES.setIdmodulo(2);
        MODULO__CATEGORIA_CLIENTES.setNombre("Categoria_Clientes");
        
        MODULO_SERVICIOS.setIdmodulo(3);
        MODULO_SERVICIOS.setNombre("Servicios");
        
        MODULO__FUNCIONALIDAD.setIdmodulo(4);
        MODULO__FUNCIONALIDAD.setNombre("Funcionalidad");
        
        MODULO__PERMISOS.setIdmodulo(5);
        MODULO__PERMISOS.setNombre("Permisos");
        
        MODULO___MODULO.setIdmodulo(6);
        MODULO___MODULO.setNombre("Modulos");
       
        MODULO_SUSCRIPCIONES.setIdmodulo(7);
        MODULO_SUSCRIPCIONES.setNombre("Suscripciones");
    }
    
    public void registrarModulosEstaticos(){
        for(Modulo m:this.lstModulosEstaticos){
            this.edit(m);
        } 
    }
    
    public boolean isFuncionarioAutorizado(Funcionario f, Integer idmodulo){
        TypedQuery<Funcionalidad> q = this.em.createQuery("SELECT f FROM Funcionalidad f WHERE f.modulo.idmodulo=:idmod AND :funcionario MEMBER OF f.funcionarioList", Funcionalidad.class);
        q.setParameter("funcionario", f);
        q.setParameter("idmod", idmodulo);
        return !q.getResultList().isEmpty();
    }
}
