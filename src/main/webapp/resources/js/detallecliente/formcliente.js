$(document).ready(function () {
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    var idbarrio = $("#in-idbarriocli").val();
    console.log("barrio cli: " + idbarrio);
    $("#cmbBarrioCli").dropdown("set selected", idbarrio);

    $('#searchDireccionCli').search({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/direcciones?q={query}'
        }
    });

    $("#txfApellidosCli").on("input", function () {
        cargarRazonSocial();
    });
    $("#txfNombresCli").on("input", function () {
        cargarRazonSocial();
    });
});

function validarCli() {
    var validado = true;
    var razon = $("#txfRazonSocialCli").val();
    if (razon === "") {
        $("#fldRazonsSocialCli").addClass("error");
        validado = false;
    } else {
        $("#fldRazonsSocialCli").removeClass("error");
        $("#msgDetalleCli").hide();
    }
    var idbarrio = $("#in-idbarriocli").val();
    if (idbarrio === '') {
        $("#fieldBarrioCliente").addClass("error");
        validado = false;
    } else {
        $("#fieldBarrioCliente").removeClass("error");
    }

    if ($("#in-idcobrador-cli").val() === "") {
        validado = false;
        $("#fieldCobradorCli").addClass("error");
    } else {
        $("#fieldCobradorCli").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
    }
    return validado;
}

function mostrarMsg(tipo, cab, msg) {
    setTipoMsgCli(tipo);

    $("#cabeceraMsgCli").empty();
    $("#cabeceraMsgCli").append(cab);

    $("#cuerpoMsgCli").empty();
    $("#cuerpoMsgCli").append(msg);

    $("#msgDetalleCli").removeClass("hidden");
    $("#msgDetalleCli").show();
}

function setTipoMsgCli(tipo) {
    $("#msgDetalleCli").removeClass("info");
    $("#msgDetalleCli").removeClass("warning");
    $("#msgDetalleCli").removeClass("error");
    $("#msgDetalleCli").addClass(tipo);
}

function ajaxGuardarCli(data) {
    var status = data.status;
    console.log(status);
    switch (status) {
        case 'begin':
        {
            $("#bntGuardarCli").addClass("loading");
            $("#bntGuardarCli").addClass("disabled");
            break;
        }
        case 'success':
        {
            $("#bntGuardarCli").removeClass("loading");
            $("#bntGuardarCli").removeClass("disabled");
            console.log("tipo mensaje: " + $("#tipoMsgCli").val());
            setTipoMsgCli($("#tipoMsgCli").val());
            $("#msgDetalleCli").removeClass("hidden");
            $("#msgDetalleCli").show();
            $("#btnAgregar").show();
            $("#btnAgregarCliente").show();
            var idcli = $("#idcliente").html();
            var nuevaUrl = window.location.origin + window.location.pathname + "?idc=" + idcli;
//            window.location(nuevaUrl);
            window.history.replaceState({}, "PolarisERP", nuevaUrl);
        }
        default:
        {
            $("#bntGuardarCli").removeClass("loading");
            $("#bntGuardarCli").removeClass("disabled");
            break;
        }
    }
    console.log("guardando");
}

function ajaxNuevoCliente(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#txfApellidosCli").on("input", function () {
                cargarRazonSocial();
            });
            $("#txfNombresCli").on("input", function () {
                cargarRazonSocial();
            });
            if (!$("#msgDetalleSusc").hasClass("hidden")) {
                $("#msgDetalleSusc").transition("fade");
            }
            $("#cmbCobradorCli").dropdown();
            $("#cmbBarrioCli").dropdown();
            $("#sgmDetalleSuscripcion").hide();
            $("#iconFlechaSuscripciones").hide();
            $("#suscripcionActual").hide();
            $("#btnAgregar").hide();
            $("#btnAgregarCliente").hide();
            $('#searchDireccionCli').search({
                apiSettings: {
                    url: '//' + location.host + restRootPath + '/direcciones?q={query}'
                }
            });
            var nuevaUrl = window.location.origin + window.location.pathname;
            console.log("ajax nuevo cliente: " + nuevaUrl);
//            window.location.replace(nuevaUrl);
            window.history.replaceState({}, "PolarisERP", nuevaUrl);

            break;
        }
    }
}

function cargarRazonSocial() {
    var rz = $("#txfApellidosCli").val() + " " + $("#txfNombresCli").val();
    $("#txfRazonSocialCli").val(rz.trim());
}
