/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.categoriacliente;

import com.exabit.polariserp.tvmax.entidades.CategoriaCliente;
import com.exabit.polariserp.tvmax.sesbeans.CategoriaClienteFacade;
import com.google.gson.Gson;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author frioss
 */
@Named(value="categoriaManBean")
@RequestScoped
public class CategoriaClienteManBean {

    @EJB
    private CategoriaClienteFacade categoriaClienteFacade;
    private Integer idCategoriaTmp;
    private CategoriaCliente categoriaVar=new CategoriaCliente();
    private String msg = "";
    private String seleccionarCategoriIDJSON;
    public Integer getIdCategoriaTmp() {
        return idCategoriaTmp;
    }

    public void setIdCategoriaTmp(Integer idCategoriaTmp) {
        this.idCategoriaTmp = idCategoriaTmp;
    }

    public CategoriaCliente getCategoriaVar() {
        return categoriaVar;
    }

    public void setCategoriaVar(CategoriaCliente categoriaVar) {
        this.categoriaVar = categoriaVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionarCategoriIDJSON() {
        return seleccionarCategoriIDJSON;
    }

    public void setSeleccionarCategoriIDJSON(String seleccionarCategoriIDJSON) {
        this.seleccionarCategoriIDJSON = seleccionarCategoriIDJSON;
    }
    public CategoriaClienteManBean(){
        
    }
    public List<CategoriaCliente> getCategoria(){
        return categoriaClienteFacade.findAll();
    }
    
    public void registrarCategoria(){
        if (categoriaVar.getNombre()==null) {
            this.msg="Campo categoria necesita un valor";
        }if (categoriaVar.getPorcentajeDescuento()==null) {
            categoriaVar.setPorcentajeDescuento(Float.valueOf(0));
        }else{
            categoriaClienteFacade.create(categoriaVar);
            this.msg = " Registro correcto: "+categoriaVar.getIdcategoria()+"-"+categoriaVar.getNombre()+"-"+categoriaVar.getPorcentajeDescuento();
        }
        
    }
    
    public void editarCategoria(){
        this.categoriaClienteFacade.edit(categoriaVar);
    }
    
    public void eliminarCategoria(){
//        System.out.println("idJson: "+seleccionarCategoriIDJSON);
        Gson gson = new Gson();
        Integer[] idsEliminar = gson.fromJson(this.seleccionarCategoriIDJSON, Integer[].class);
        for (Integer ide : idsEliminar) {
//            System.out.println("eliminar: "+ide);
            this.categoriaClienteFacade.remove(this.categoriaClienteFacade.find(ide));
        }
    }
}
