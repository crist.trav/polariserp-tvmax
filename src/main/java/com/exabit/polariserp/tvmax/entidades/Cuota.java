package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "cuota")
@NamedQueries({
    @NamedQuery(name = "Cuota.findAll", query = "SELECT c FROM Cuota c")
    , @NamedQuery(name = "Cuota.findByIdcuota", query = "SELECT c FROM Cuota c WHERE c.idcuota = :idcuota")
    , @NamedQuery(name = "Cuota.findByAnioCuota", query = "SELECT c FROM Cuota c WHERE c.anioCuota = :anioCuota")
    , @NamedQuery(name = "Cuota.findByCancelado", query = "SELECT c FROM Cuota c WHERE c.cancelado = :cancelado")
    , @NamedQuery(name = "Cuota.findByFechaPago", query = "SELECT c FROM Cuota c WHERE c.fechaPago = :fechaPago")
    , @NamedQuery(name = "Cuota.findByFechaVencimiento", query = "SELECT c FROM Cuota c WHERE c.fechaVencimiento = :fechaVencimiento")
    , @NamedQuery(name = "Cuota.findByMesCuota", query = "SELECT c FROM Cuota c WHERE c.mesCuota = :mesCuota")
    , @NamedQuery(name = "Cuota.findByMontoCuota", query = "SELECT c FROM Cuota c WHERE c.montoCuota = :montoCuota")
    , @NamedQuery(name = "Cuota.findByMontoEntrega", query = "SELECT c FROM Cuota c WHERE c.montoEntrega = :montoEntrega")
    , @NamedQuery(name = "Cuota.findByObservacion", query = "SELECT c FROM Cuota c WHERE c.observacion = :observacion")})
public class Cuota implements Serializable {

//    @OneToMany(mappedBy = "cuota")
//    private List<Cobro> cobroList;

    @Column(name = "monto_cuota")
    private Integer montoCuota;
    @Column(name = "monto_entrega")
    private Integer montoEntrega;

    @ManyToMany(mappedBy = "cuotaList")
    private List<ConsultaResDetallePronet> consultaResDetallePronetList;
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idcuota", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idcuota", table = "idsequences", allocationSize = 1, initialValue = 439989)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuota")
    private Integer idcuota;
    @Column(name = "anio_cuota")
    private Short anioCuota;
    @Column(name = "cancelado")
    private Boolean cancelado;
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    @Column(name = "mes_cuota")
    private Short mesCuota;
    @Size(max = 250)
    @Column(name = "observacion")
    private String observacion;
//    @ManyToMany(mappedBy = "cuotaList")
//    private List<Cobro> cobroList;
    @JoinColumn(name = "historial_generacion", referencedColumnName = "idhistorial_generacion_cuotas")
    @ManyToOne
    private HistorialGeneracionCuotas historialGeneracion;
    @JoinColumn(name = "suscripcion", referencedColumnName = "idsuscripcion")
    @ManyToOne
    private Suscripcion suscripcion;
    @OneToMany(mappedBy = "cuota")
    private List<DetalleFactura> detalleFacturaList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuota")
//    private List<ConsultaResDetallePronet> consultaResDetallePronetList;
    @Column(name = "nro_cuota")
    private Integer nroCuota;
    @JoinColumn(name = "servicio", referencedColumnName = "idservicio")
    @ManyToOne
    private Servicio servicio;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuota1")
//    private List<CobroCuota> cobroCuotaList;

    public Cuota() {
    }

    public Cuota(Integer idcuota) {
        this.idcuota = idcuota;
    }

    public Integer getIdcuota() {
        return idcuota;
    }

    public void setIdcuota(Integer idcuota) {
        this.idcuota = idcuota;
    }

    public Short getAnioCuota() {
        return anioCuota;
    }

    public void setAnioCuota(Short anioCuota) {
        this.anioCuota = anioCuota;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Short getMesCuota() {
        return mesCuota;
    }

    public void setMesCuota(Short mesCuota) {
        this.mesCuota = mesCuota;
    }

    public Integer getMontoCuota() {
        return montoCuota;
    }

    public void setMontoCuota(Integer montoCuota) {
        this.montoCuota = montoCuota;
    }

    public Integer getMontoEntrega() {
        return montoEntrega;
    }

    public void setMontoEntrega(Integer montoEntrega) {
        this.montoEntrega = montoEntrega;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

//    public List<Cobro> getCobroList() {
//        return cobroList;
//    }
//
//    public void setCobroList(List<Cobro> cobroList) {
//        this.cobroList = cobroList;
//    }

    public HistorialGeneracionCuotas getHistorialGeneracion() {
        return historialGeneracion;
    }

    public void setHistorialGeneracion(HistorialGeneracionCuotas historialGeneracion) {
        this.historialGeneracion = historialGeneracion;
    }

    public Suscripcion getSuscripcion() {
        return suscripcion;
    }

    public void setSuscripcion(Suscripcion suscripcion) {
        this.suscripcion = suscripcion;
    }

    public List<DetalleFactura> getDetalleFacturaList() {
        return detalleFacturaList;
    }

    public void setDetalleFacturaList(List<DetalleFactura> detalleFacturaList) {
        this.detalleFacturaList = detalleFacturaList;
    }

//    public List<ConsultaResDetallePronet> getConsultaResDetallePronetList() {
//        return consultaResDetallePronetList;
//    }
//
//    public void setConsultaResDetallePronetList(List<ConsultaResDetallePronet> consultaResDetallePronetList) {
//        this.consultaResDetallePronetList = consultaResDetallePronetList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcuota != null ? idcuota.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuota)) {
            return false;
        }
        Cuota other = (Cuota) object;
        if ((this.idcuota == null && other.idcuota != null) || (this.idcuota != null && !this.idcuota.equals(other.idcuota))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Cuota[ idcuota=" + idcuota + " ]";
    }

    public Integer getNroCuota() {
        return nroCuota;
    }

    public void setNroCuota(Integer nroCuota) {
        this.nroCuota = nroCuota;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public void setMontoCuota(int montoCuota) {
        this.montoCuota = montoCuota;
    }

    public void setMontoEntrega(int montoEntrega) {
        this.montoEntrega = montoEntrega;
    }

//    public List<CobroCuota> getCobroCuotaList() {
//        return cobroCuotaList;
//    }
//
//    public void setCobroCuotaList(List<CobroCuota> cobroCuotaList) {
//        this.cobroCuotaList = cobroCuotaList;
//    }

    public List<ConsultaResDetallePronet> getConsultaResDetallePronetList() {
        return consultaResDetallePronetList;
    }

    public void setConsultaResDetallePronetList(List<ConsultaResDetallePronet> consultaResDetallePronetList) {
        this.consultaResDetallePronetList = consultaResDetallePronetList;
    }

//    public List<Cobro> getCobroList() {
//        return cobroList;
//    }
//
//    public void setCobroList(List<Cobro> cobroList) {
//        this.cobroList = cobroList;
//    }
}
