/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class SolicitudCambioEstadoFacade extends AbstractFacade<SolicitudCambioEstado> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SolicitudCambioEstadoFacade() {
        super(SolicitudCambioEstado.class);
    }
    
    public List<SolicitudCambioEstado> getSolicitudesConcretadas(Suscripcion s){
        TypedQuery<SolicitudCambioEstado> q=this.em.createQuery("SELECT sc FROM SolicitudCambioEstado sc WHERE sc.suscripcion=:sus AND sc.concretado=true ORDER BY sc.fechaSolicitud DESC",SolicitudCambioEstado.class);
        q.setParameter("sus", s);
        return q.getResultList();
    }
    public List<SolicitudCambioEstado> getSolicitudesPendientes(){
        TypedQuery<SolicitudCambioEstado> q=this.em.createQuery("SELECT sc FROM SolicitudCambioEstado sc WHERE sc.concretado=false ORDER BY sc.fechaSolicitud DESC",SolicitudCambioEstado.class);
        return q.getResultList();
    }
    public List<SolicitudCambioEstado> getSolicitudesConcretadas(){
        TypedQuery<SolicitudCambioEstado> q=this.em.createQuery("SELECT sc FROM SolicitudCambioEstado sc WHERE sc.concretado=true ORDER BY sc.fechaSolicitud DESC",SolicitudCambioEstado.class);
        return q.getResultList();
    }
    
    public List<SolicitudCambioEstado> getSolicitudesOrdenFecha(Suscripcion s){
        TypedQuery<SolicitudCambioEstado> q=this.em.createQuery("SELECT sc FROM SolicitudCambioEstado sc WHERE sc.suscripcion=:sus ORDER BY sc.fechaSolicitud DESC", SolicitudCambioEstado.class);
        q.setParameter("sus", s);
        return q.getResultList();
    }
    public List<SolicitudCambioEstado> getSolicitudesOrdenFecha(){
        TypedQuery<SolicitudCambioEstado> q=this.em.createQuery("SELECT sc FROM SolicitudCambioEstado sc ORDER BY sc.fechaSolicitud DESC", SolicitudCambioEstado.class);
        return q.setMaxResults(50).getResultList();
    }
    
    
    
}
