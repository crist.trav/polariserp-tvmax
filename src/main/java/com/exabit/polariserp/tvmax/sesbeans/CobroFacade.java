/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.DetalleFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class CobroFacade extends AbstractFacade<Cobro> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CobroFacade() {
        super(Cobro.class);
    }

    public List<Cobro> getCobrosOrdenFechaDesc() {
        TypedQuery<Cobro> q = this.em.createQuery("SELECT c FROM Cobro c ORDER BY c.fecha DESC", Cobro.class);
        return q.setMaxResults(500).getResultList();
    }

    public List<Cobro> getCobrosOrdenFechaDesc(Date inicio, Date fin, String criterioBusqueda) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fi = sdf.format(inicio);
        String ff = sdf.format(fin);
        String consulta = "SELECT * FROM cobro WHERE fecha >= ? AND fecha <= ? ORDER BY fecha DESC";
        if (!criterioBusqueda.isEmpty()) {
            consulta = "SELECT * FROM cobro "
                    + "INNER JOIN suscripcion ON cobro.suscripcion = suscripcion.idsuscripcion "
                    + "INNER JOIN cliente ON suscripcion.cliente = cliente.idcliente "
                    + "LEFT JOIN detalle_factura ON detalle_factura.iddetalle_factura = cobro.detalle_factura "
                    + "LEFT JOIN factura ON factura.idfactura = detalle_factura.factura "
                    + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND "
                    + "(LOWER(cliente.razon_social) LIKE ? OR "
                    + "CAST(IFNULL(factura.nro_factura, 'sin factura') AS CHAR(11)) LIKE ?) "
                    + "ORDER BY cobro.fecha DESC";
        }
        Query q = this.em.createNativeQuery(consulta, Cobro.class);
        q.setParameter(1, fi);
        q.setParameter(2, ff);
        if (!criterioBusqueda.isEmpty()) {
            q.setParameter(3, "%" + criterioBusqueda.toLowerCase() + "%");
            q.setParameter(4, "%" + criterioBusqueda.toLowerCase() + "%");
        }
        List<Cobro> lstcob = q.getResultList();
        for (Cobro c : lstcob) {
            this.em.refresh(c);
        }
        return lstcob;
    }

    public List<Cobro> getCobrosOrdenFechaDesc(Funcionario cobra, Date inicio, Date fin, String criterioBusqueda) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fi = sdf.format(inicio);
        String ff = sdf.format(fin);
        String consulta = "SELECT cobro.idcobro FROM cobro "
                + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.cobrador = ? "
                + "ORDER BY cobro.fecha DESC";

        if (!criterioBusqueda.isEmpty()) {
            consulta = "SELECT cobro.idcobro FROM cobro "
                    + "INNER JOIN suscripcion ON cobro.suscripcion = suscripcion.idsuscripcion "
                    + "INNER JOIN cliente ON suscripcion.cliente = cliente.idcliente "
                    + "LEFT JOIN detalle_factura ON detalle_factura.iddetalle_factura = cobro.detalle_factura "
                    + "LEFT JOIN factura ON factura.idfactura = detalle_factura.factura "
                    + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.cobrador = ? AND "
                    + "(LOWER(cliente.razon_social) LIKE ? OR "
                    + "CAST(IFNULL(factura.nro_factura, 'sin factura') AS CHAR(11)) LIKE ?) "
                    + "ORDER BY cobro.fecha DESC";
        }
        Query q = this.em.createNativeQuery(consulta, Cobro.class);
        q.setParameter(1, fi);
        q.setParameter(2, ff);
        q.setParameter(3, cobra.getIdfuncionario());
        if (!criterioBusqueda.isEmpty()) {
            q.setParameter(4, "%" + criterioBusqueda.toLowerCase() + "%");
            q.setParameter(5, "%" + criterioBusqueda.toLowerCase() + "%");
        }
        List<Cobro> lstCobro = q.getResultList();
        for (Cobro c : lstCobro) {
            this.em.refresh(c);
        }
        return lstCobro;
    }
    
    public List<Cobro> getCobrosOrdenFechaDesc(Funcionario cobra, Concepto con, Date inicio, Date fin, String criterioBusqueda) {
         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fi = sdf.format(inicio);
        String ff = sdf.format(fin);
        String consulta = "SELECT cobro.idcobro FROM cobro "
                + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.cobrador = ? AND cobro.concepto = ? "
                + "ORDER BY cobro.fecha DESC";

        if (!criterioBusqueda.isEmpty()) {
            consulta = "SELECT cobro.idcobro FROM cobro "
                    + "INNER JOIN suscripcion ON cobro.suscripcion = suscripcion.idsuscripcion "
                    + "INNER JOIN cliente ON suscripcion.cliente = cliente.idcliente "
                    + "LEFT JOIN detalle_factura ON detalle_factura.iddetalle_factura = cobro.detalle_factura "
                    + "LEFT JOIN factura ON factura.idfactura = detalle_factura.factura "
                    + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.cobrador = ? AND cobro.concepto = ? AND "
                    + "(LOWER(cliente.razon_social) LIKE ? OR "
                    + "CAST(IFNULL(factura.nro_factura, 'sin factura') AS CHAR(11)) LIKE ?) "
                    + "ORDER BY cobro.fecha DESC";
        }
        Query q = this.em.createNativeQuery(consulta, Cobro.class);
        q.setParameter(1, fi);
        q.setParameter(2, ff);
        q.setParameter(3, cobra.getIdfuncionario());
        q.setParameter(4, con.getIdconcepto());
        if (!criterioBusqueda.isEmpty()) {
            q.setParameter(5, "%" + criterioBusqueda.toLowerCase() + "%");
            q.setParameter(6, "%" + criterioBusqueda.toLowerCase() + "%");
        }
        List<Cobro> lstCobro = q.getResultList();
        for (Cobro c : lstCobro) {
            this.em.refresh(c);
        }
        return lstCobro;
    }

    public List<Cobro> getCobrosOrdenFechaDesc(Concepto con, Date inicio, Date fin, String criterioBusqueda) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fi = sdf.format(inicio);
        String ff = sdf.format(fin);
        String consulta = "SELECT cobro.idcobro FROM cobro "
                + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.concepto = ? "
                + "ORDER BY cobro.fecha DESC";

        if (!criterioBusqueda.isEmpty()) {
            consulta = "SELECT cobro.idcobro FROM cobro "
                    + "INNER JOIN suscripcion ON cobro.suscripcion = suscripcion.idsuscripcion "
                    + "INNER JOIN cliente ON suscripcion.cliente = cliente.idcliente "
                    + "LEFT JOIN detalle_factura ON detalle_factura.iddetalle_factura = cobro.detalle_factura "
                    + "LEFT JOIN factura ON factura.idfactura = detalle_factura.factura "
                    + "WHERE cobro.fecha >= ? AND cobro.fecha <= ? AND cobro.concepto = ? AND "
                    + "(LOWER(cliente.razon_social) LIKE ? OR "
                    + "CAST(IFNULL(factura.nro_factura, 'sin factura') AS CHAR(11)) LIKE ?) "
                    + "ORDER BY cobro.fecha DESC";
        }
        Query q = this.em.createNativeQuery(consulta, Cobro.class);
        q.setParameter(1, fi);
        q.setParameter(2, ff);
        q.setParameter(3, con.getIdconcepto());
        if (!criterioBusqueda.isEmpty()) {
            q.setParameter(4, "%" + criterioBusqueda.toLowerCase() + "%");
            q.setParameter(5, "%" + criterioBusqueda.toLowerCase() + "%");
        }
        List<Cobro> lstCobro = q.getResultList();
        for (Cobro c : lstCobro) {
            this.em.refresh(c);
        }
        return lstCobro;
    }

    public List<Cobro> getCobrosPorFactura(Factura f) {
        TypedQuery<Cobro> q = this.em.createQuery("SELECT c from Cobro c WHERE c.detalleFactura.factura=:fact", Cobro.class);
        q.setParameter("fact", f);
        return q.getResultList();
    }

    public Cobro getCobroPorDetalleFactura(DetalleFactura df) {
        TypedQuery<Cobro> q = this.em.createQuery("SELECT c FROM Cobro c WHERE c.detalleFactura=:detf", Cobro.class);
        q.setParameter("detf", df);
        List<Cobro> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return q.getResultList().get(0);
        } else {
            return null;
        }
    }

    public Cobro getCobroPorCuota(Cuota cuota, Boolean anulado) {
        TypedQuery<Cobro> q = this.em.createQuery("SELECT c FROM Cobro c WHERE c.anulado=:anulado AND c.cuota=:cuota", Cobro.class);
        q.setParameter("anulado", anulado);
        q.setParameter("cuota", cuota);
        List<Cobro> lstc = q.getResultList();
        if (!lstc.isEmpty()) {
            return lstc.get(0);
        } else {
            return null;
        }
    }

}
