package com.exabit.polariserp.tvmax.suscripciones;

import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.reportes.UtilReportesSesBean;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "suscripcionManBean")
@ViewScoped
public class SuscripcionManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(SuscripcionManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private UtilReportesSesBean utilReportesSesBean;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private TipoViviendaFacade tipoViviendaFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private BarrioFacade barrioFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed" >
    private Integer idTipoSuscripcion;
    private List<ItemResumenSuscripciones> lstItemsResumen = new ArrayList<>();
    private Suscripcion suscSeleccionada;
    private List<Suscripcion> lstSuscSeleccionadas = new LinkedList<>();
    private Integer montoPorConexion;
    private Paginador<Suscripcion> paginador;
    private List<Suscripcion> lstSuscripciones;
    private Integer idBarrio;
    private Integer idCobrador;
    private Integer idTipoVivienda;
    private Integer idEstadoSusc;
    private Integer idCliente;
    private Suscripcion suscVar;
    private Integer idServicio;
    private String msg;
    private String strFechaDesdeCobroRes;
    private String strFechaHastaCobroRes;
    private boolean chkMostrarTodosEstados = true;
    private Integer idEstadoFiltro;
    private String seleccionJson;
    private String textBusq;
    private Integer idBarrioFiltro;
    private Integer idServicioFiltro;
    private Integer idTipoSuscFiltro;
    private Integer cantCuotasFiltro;
    private String strFechaPrimerVenc;
    private Integer montoPrimeraCuota;
    private Integer idCobradorFiltro;
    private String strDesdeFiltro;
    private String strHastaFiltro;
    private Integer cantCuotasResaltar = 4;
    private boolean filtrarTodasFechasCambioEstado = true;
    private String strDesdeTS;
    private String strHastaTS;
    private final SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
    private final SimpleDateFormat sdfMesAnio = new SimpleDateFormat("MMMM yyyy");
    private String strFechaSuscripcion;
    private List<Servicio> lstServiciosResumenCantidad = new ArrayList<>();
    //</editor-fold>
    
    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public List<Servicio> getLstServiciosResumenCantidad() {
        return lstServiciosResumenCantidad;
    }

//    public void setLstServiciosResumenCantidad(List<Servicio> lstServiciosResumenCantidad) {
//        this.lstServiciosResumenCantidad = lstServiciosResumenCantidad;
//    }
    
    public String getStrFechaSuscripcion() {
        return strFechaSuscripcion;
    }
    
    public void setStrFechaSuscripcion(String strFechaSuscripcion) {
        this.strFechaSuscripcion = strFechaSuscripcion;
    }
    
    public List<ItemResumenSuscripciones> getLstItemsResumen() {
        return lstItemsResumen;
    }

    public void setLstItemsResumen(List<ItemResumenSuscripciones> lstItemsResumen) {
        this.lstItemsResumen = lstItemsResumen;
    }

    public Suscripcion getSuscSeleccionada() {
        return suscSeleccionada;
    }

    public void setSuscSeleccionada(Suscripcion suscSeleccionada) {
        this.suscSeleccionada = suscSeleccionada;
    }

    public List<Suscripcion> getLstSuscSeleccionadas() {
        return lstSuscSeleccionadas;
    }

    public void setLstSuscSeleccionadas(List<Suscripcion> lstSuscSeleccionadas) {
        this.lstSuscSeleccionadas = lstSuscSeleccionadas;
    }

    public Integer getMontoPorConexion() {
        return montoPorConexion;
    }

    public void setMontoPorConexion(Integer montoPorConexion) {
        this.montoPorConexion = montoPorConexion;
    }

    public Paginador<Suscripcion> getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador<Suscripcion> paginador) {
        this.paginador = paginador;
    }
    
    public Integer getIdServicioFiltro() {
        return idServicioFiltro;
    }

    public void setIdServicioFiltro(Integer idServicioFiltro) {
        this.idServicioFiltro = idServicioFiltro;
    }

    public Integer getIdBarrioFiltro() {
        return idBarrioFiltro;
    }

    public void setIdBarrioFiltro(Integer idBarrioFiltro) {
        this.idBarrioFiltro = idBarrioFiltro;
    }

    public String getStrFechaDesdeCobroRes() {
        return strFechaDesdeCobroRes;
    }

    public void setStrFechaDesdeCobroRes(String strFechaDesdeCobroRes) {
        this.strFechaDesdeCobroRes = strFechaDesdeCobroRes;
    }

    public String getStrFechaHastaCobroRes() {
        return strFechaHastaCobroRes;
    }

    public void setStrFechaHastaCobroRes(String strFechaHastaCobroRes) {
        this.strFechaHastaCobroRes = strFechaHastaCobroRes;
    }

    public boolean isChkMostrarTodosEstados() {
        return chkMostrarTodosEstados;
    }

    public void setChkMostrarTodosEstados(boolean chkMostrarTodosEstados) {
        this.chkMostrarTodosEstados = chkMostrarTodosEstados;
    }

    public Integer getIdEstadoFiltro() {
        return idEstadoFiltro;
    }

    public void setIdEstadoFiltro(Integer idEstadoFiltro) {
        this.idEstadoFiltro = idEstadoFiltro;
    }

    public List<Suscripcion> getLstSuscripciones() {
        return lstSuscripciones;
    }

    public void setLstSuscripciones(List<Suscripcion> lstSuscripciones) {
        this.lstSuscripciones = lstSuscripciones;
    }

    public Integer getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Integer idBarrio) {
        this.idBarrio = idBarrio;
    }

    public Integer getIdCobrador() {
        return idCobrador;
    }

    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }

    public Integer getIdTipoVivienda() {
        return idTipoVivienda;
    }

    public void setIdTipoVivienda(Integer idTipoVivienda) {
        this.idTipoVivienda = idTipoVivienda;
    }

    public Integer getIdEstadoSusc() {
        return idEstadoSusc;
    }

    public void setIdEstadoSusc(Integer idEstadoSusc) {
        this.idEstadoSusc = idEstadoSusc;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Suscripcion getSuscVar() {
        return suscVar;
    }

    public void setSuscVar(Suscripcion suscVar) {
        this.suscVar = suscVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionJson() {
        return seleccionJson;
    }

    public void setSeleccionJson(String seleccionJson) {
        this.seleccionJson = seleccionJson;
    }

    public String getTextBusq() {
        return textBusq;
    }

    public void setTextBusq(String textBusq) {
        this.textBusq = textBusq;
    }

    public Integer getIdTipoSuscripcion() {
        return idTipoSuscripcion;
    }

    public void setIdTipoSuscripcion(Integer idTipoSuscripcion) {
        this.idTipoSuscripcion = idTipoSuscripcion;
    }

    public Integer getIdTipoSuscFiltro() {
        return idTipoSuscFiltro;
    }

    public void setIdTipoSuscFiltro(Integer idTipoSuscFiltro) {
        this.idTipoSuscFiltro = idTipoSuscFiltro;
    }

    public Integer getCantCuotasFiltro() {
        return cantCuotasFiltro;
    }

    public void setCantCuotasFiltro(Integer cantCuotasFiltro) {
        this.cantCuotasFiltro = cantCuotasFiltro;
    }

    public String getStrFechaPrimerVenc() {
        return strFechaPrimerVenc;
    }

    public void setStrFechaPrimerVenc(String strFechaPrimerVenc) {
        this.strFechaPrimerVenc = strFechaPrimerVenc;
    }


    public Integer getMontoPrimeraCuota() {
        return montoPrimeraCuota;
    }

    public void setMontoPrimeraCuota(Integer montoPrimeraCuota) {
        this.montoPrimeraCuota = montoPrimeraCuota;
    }

    public Integer getIdCobradorFiltro() {
        return idCobradorFiltro;
    }

    public void setIdCobradorFiltro(Integer idCobradorFiltro) {
        this.idCobradorFiltro = idCobradorFiltro;
    }

    public String getStrDesdeFiltro() {
        return strDesdeFiltro;
    }

    public void setStrDesdeFiltro(String strDesdeFiltro) {
        this.strDesdeFiltro = strDesdeFiltro;
    }

    public String getStrHastaFiltro() {
        return strHastaFiltro;
    }

    public void setStrHastaFiltro(String strHastaFiltro) {
        this.strHastaFiltro = strHastaFiltro;
    }

    public Integer getCantCuotasResaltar() {
        return cantCuotasResaltar;
    }

    public void setCantCuotasResaltar(Integer cantCuotasResaltar) {
        this.cantCuotasResaltar = cantCuotasResaltar;
    }

    public String getStrDesdeTS() {
        return strDesdeTS;
    }

    public void setStrDesdeTS(String strDesdeTS) {
        this.strDesdeTS = strDesdeTS;
    }

    public String getStrHastaTS() {
        return strHastaTS;
    }

    public void setStrHastaTS(String strHastaTS) {
        this.strHastaTS = strHastaTS;
    }   

    public boolean isFiltrarTodasFechasCambioEstado() {
        return filtrarTodasFechasCambioEstado;
    }

    public void setFiltrarTodasFechasCambioEstado(boolean filtrarTodasFechasCambioEstado) {
        this.filtrarTodasFechasCambioEstado = filtrarTodasFechasCambioEstado;
    }
    //</editor-fold>

    /**
     * Creates a new instance of SuscripcionManBean
     */
    

    public SuscripcionManBean() {
        this.suscVar = new Suscripcion();
        this.lstSuscripciones = new ArrayList<>();
        this.paginador = new Paginador(this.lstSuscripciones);
        this.strFechaPrimerVenc = this.sdf.format(new Date());
        Calendar clDF = new GregorianCalendar();
        clDF.add(Calendar.MONTH, -6);
        this.strDesdeFiltro = this.sdfMesAnio.format(clDF.getTime());

        this.strHastaFiltro = this.sdfMesAnio.format(new Date());
        Calendar cdesdecobro = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
        this.strFechaDesdeCobroRes = this.sdf.format(cdesdecobro.getTime());

        Calendar chastacobro = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
        chastacobro.add(Calendar.MONTH, 1);
        chastacobro.add(Calendar.DAY_OF_MONTH, -1);
        this.strFechaHastaCobroRes = this.sdf.format(chastacobro.getTime());

        this.strDesdeTS = this.sdf.format(new Date());
        this.strHastaTS = this.sdf.format(new Date());

//        this.idEstadoFiltro=EntidadesEstaticas.ESTADO_ACTIVO.getIdestado();
//        this.idTipoSuscFiltro=EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL.getIdtipoSuscripcion();
    }

    @PostConstruct
    private void postConstruct() {
//        this.filtrarSuscripciones();
    }

    public void agregarSuscripcion() {
        try {
            Date priVenc = this.sdf.parse(this.strFechaPrimerVenc);
            System.out.println("Monto conexion" + this.montoPorConexion);
            this.cargarDatos();
            this.suscVar.setIdsuscripcion(null);
            this.suscVar.setEstado(EntidadesEstaticas.ESTADO_CONECTADO);
            this.suscripcionFacade.registrarSuscripcion(suscVar, priVenc, this.montoPrimeraCuota);
            this.msg = "Suscripción agregada. Código: " + this.suscVar.getIdsuscripcion();
            this.filtrarSuscripciones();
//            this.paginador.limpiarLista();
//            this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
        } catch (ParseException x) {
            LOG.log(Level.SEVERE, "error al convetir string a date", x);
        }
    }

    private void cargarDatos() {
        this.suscVar.setBarrio(this.barrioFacade.find(this.idBarrio));
        this.suscVar.setCliente(this.clienteFacade.find(this.idCliente));
        this.suscVar.setCobrador(this.funcionarioFacade.find(this.idCobrador));
        this.suscVar.setServicio(this.servicioFacade.find(this.idServicio));
        this.suscVar.setTipoVivienda(this.tipoViviendaFacade.find(this.idTipoVivienda));
        this.suscVar.setTipoSuscripcion(this.tipoSuscripcionFacade.find(this.idTipoSuscripcion));
    }

    public void modificarSuscripcion() {
        this.cargarDatos();
        this.suscripcionFacade.edit(this.suscVar);
        this.msg = "Suscripción Modificada. Código: " + this.suscVar.getIdsuscripcion();
        int pos = this.lstSuscripciones.indexOf(this.suscVar);
        this.filtrarSuscripciones();
//        this.paginador.limpiarLista();
//        this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
    }

    public void eliminarSuscripciones() {
        Gson gson = new Gson();
        Integer[] arrayIdsEliminar = gson.fromJson(this.seleccionJson, Integer[].class);
        List<Suscripcion> lstEliminar = new ArrayList<>();
        for (Integer id : arrayIdsEliminar) {
            Suscripcion s = this.suscripcionFacade.find(id);
            lstEliminar.add(s);
            this.suscripcionFacade.remove(s);
        }
        this.paginador.limpiarLista();
        this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
    }

    public void filtrarSuscripciones() {
        System.out.println("Se filtran suscripciones: " + this.filtrarTodasFechasCambioEstado + " " + this.strDesdeTS + " " + this.strHastaTS);
        System.out.println("cristerio: " + this.textBusq + ", idestado: " + this.idEstadoFiltro + ", idtipo: " + this.idTipoSuscFiltro);
        this.paginador.limpiarLista();
        if (this.textBusq == null) {
            this.textBusq = "";
        }
        if (this.textBusq.isEmpty()) {
            if (this.idEstadoFiltro == null && this.idTipoSuscFiltro == null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
            } else if (this.idTipoSuscFiltro == null && this.idEstadoFiltro != null) {
                EstadoSuscripcion e = this.estadoSuscripcionFacade.find(this.idEstadoFiltro);
                if (this.idEstadoFiltro == 1000) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesConServicio());
                } else if (this.idEstadoFiltro == 1001) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesSinServicio());
                } else {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesPorEstado(e));
                }
            } else if (this.idTipoSuscFiltro != null && this.idEstadoFiltro == null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesOrdenInverso(this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            } else {
                if (this.idEstadoFiltro == 1000) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesConServicio(this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                } else if (this.idEstadoFiltro == 1001) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesSinServicio(this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                } else {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesOrdenInverso(this.estadoSuscripcionFacade.find(this.idEstadoFiltro), this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                }
            }
        } else {
            System.out.print("Se busca suscripcion: "+this.textBusq);
            System.out.println("idestadoFiltro: "+this.idEstadoFiltro+", idtiposuscfiltro: "+this.idTipoSuscFiltro);
            if (this.idEstadoFiltro == null && this.idTipoSuscFiltro == null) {
                
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq));
            } else if (this.idEstadoFiltro != null && this.idTipoSuscFiltro == null) {
                System.out.println("filtro por criterio y estado");
                if (this.idEstadoFiltro == 1000) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesConServicio(this.textBusq));
                } else if (this.idEstadoFiltro == 1001) {
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesSinServicio(textBusq));
                } else {
                    EstadoSuscripcion e = this.estadoSuscripcionFacade.find(this.idEstadoFiltro);
                    this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(textBusq, e));
                }
            } else if (this.idEstadoFiltro == null && this.idTipoSuscFiltro != null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq, this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            } else {
                if(this.idEstadoFiltro==1000){
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesConServicio(this.textBusq, this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                }else if(this.idEstadoFiltro==1001){
                    this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesSinServicio(this.textBusq, this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                }else{
                    this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq, this.estadoSuscripcionFacade.find(this.idEstadoFiltro), this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
                }
            }
        }
        
        if(this.idServicioFiltro!=null){
            List<Suscripcion> lstserv=new ArrayList<>();
            for(Suscripcion s:this.paginador.getListaCompleta()){
                if(s.getServicio().getIdservicio().equals(this.idServicioFiltro)){
                    lstserv.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstserv);
        }
        
        if (this.idCobradorFiltro != null) {
            List<Suscripcion> lstcobra = new ArrayList<>();
            for (Suscripcion s : this.paginador.getListaCompleta()) {
                if (s.getCobrador().getIdfuncionario().equals(this.idCobradorFiltro)) {
                    lstcobra.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstcobra);
        }

        if (this.cantCuotasFiltro != null) {
            List<Suscripcion> lstSuscFiltCantCuo = new ArrayList<>();
            for (Suscripcion s : this.paginador.getListaCompleta()) {
                if (this.getCantidadCuotasPendientes(s.getIdsuscripcion()).equals(this.cantCuotasFiltro)) {
                    lstSuscFiltCantCuo.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstSuscFiltCantCuo);
        }
        if (!this.filtrarTodasFechasCambioEstado) {
            try {
                List<Suscripcion> lstcambioestado = new LinkedList<>();
                for (Suscripcion s : this.paginador.getListaCompleta()) {
                    if (s.getFechaCambioEstado() != null) {
                        Calendar fce = new GregorianCalendar();
                        fce.setTime(s.getFechaCambioEstado());
                        Date fceDesde = this.sdf.parse(this.strDesdeTS);
                        Date fceHasta = this.sdf.parse(this.strHastaTS);
                        Calendar cceDesde = new GregorianCalendar();
                        cceDesde.setTime(fceDesde);
                        Calendar cceHasta = new GregorianCalendar();
                        cceHasta.setTime(fceHasta);
                        if ((fce.after(cceDesde) || fce.equals(cceDesde)) && (fce.before(cceHasta) || fce.equals(cceHasta))) {
                            lstcambioestado.add(s);
                        }
                    } else {
                        System.out.println("Fecha cambio estado NULL ");
                    }
                }
                this.paginador.limpiarLista();
                this.paginador.agregarItemsLista(lstcambioestado);
            } catch (ParseException ex) {
                System.out.println("Error al convertir string a date: " + ex);
            }
        }
        if (this.idBarrioFiltro != null) {
            List<Suscripcion> lstbarrio = new LinkedList<>();
            for (Suscripcion s : this.paginador.getListaCompleta()) {
                if (s.getBarrio().getIdbarrio().equals(this.idBarrioFiltro)) {
                    lstbarrio.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstbarrio);
        }
        this.lstItemsResumen.clear();
    }

    public List<TipoSuscripcion> getLstTipoSuscripcion() {
        return this.tipoSuscripcionFacade.findAll();
    }

    public List<Servicio> getLstServiciosSuscribibles() {
        return this.servicioFacade.getServiciosSuscribibles();
    }

    public List<Funcionario> getLstCobradores() {
        return this.funcionarioFacade.getCobradores();
    }

    public void seleccionarCliFormulario() {
        System.out.println("Seleccion cliente");
        this.reiniciarFormulario();
        this.suscVar.setCliente(this.clienteFacade.find(this.idCliente));
        this.suscVar.setDireccion(this.suscVar.getCliente().getDireccion());
    }

    public void reiniciarFormulario() {
        this.idServicio = EntidadesEstaticas.ID_SERVICIO_TVCABLE;
        this.suscVar.setServicio(this.servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_TVCABLE));
        this.suscVar.setPrecio(this.suscVar.getServicio().getPrecio());
        this.suscVar.setDiaVencimientoMes((short) 1);
        this.suscVar.setCantidadTvs(1);
        this.idTipoVivienda = EntidadesEstaticas.TIPO_VIVIENDA_PROPIA.getIdtipoVivienda();
        this.suscVar.setTipoVivienda(EntidadesEstaticas.TIPO_VIVIENDA_PROPIA);
        this.suscVar.setCobrador(this.funcionarioFacade.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR));
        this.idCobrador = EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR;
        this.suscVar.setBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO);
        this.setIdBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO.getIdbarrio());
        this.suscVar.setTipoSuscripcion(EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL);
        this.idTipoSuscripcion = EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL.getIdtipoSuscripcion();
        this.suscVar.setDireccion("");
        this.suscVar.setNroMedidorElectrico("");
    }

    public Integer getCantidadCuotasPendientes(Integer idsuscripcion) {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
            return this.utilReportesSesBean.getCantidadCuotasPendientes(idsuscripcion, fdesde, fhasta);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a fecha", ex);
            return -1;
        }
    }

    public Integer getMontoDeuda(Integer idsuscripcion) {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
            return this.utilReportesSesBean.getMontoDeuda(idsuscripcion, fdesde, fhasta);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a fecha", ex);
            return -1;
        }
    }

    public void procesarResumen() {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
            this.lstItemsResumen.clear();
            this.lstItemsResumen.addAll(this.utilReportesSesBean.getResumen(this.paginador.getListaCompleta(), fdesde, fhasta));
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al procesar resumen", ex);
        }
    }

    public void verInforme(boolean conCobro) {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);

            Date fdesdeCobro = this.sdf.parse(this.strFechaDesdeCobroRes);
            Date fhastaCobro = this.sdf.parse(this.strFechaHastaCobroRes);

            Map<String, Object> par = new HashMap<>();
            if (this.idEstadoFiltro != null) {
                if(this.idEstadoFiltro==1000){
                    par.put("estadosusc", "Activo");
                }else if(this.idEstadoFiltro==1001){
                    par.put("estadosusc", "Inactivo");
                }else{
                    par.put("estadosusc", this.estadoSuscripcionFacade.find(this.idEstadoFiltro).getNombre());
                }
            } else {
                par.put("estadosusc", "(Todos los estados)");
            }
            if (this.idTipoSuscFiltro != null) {
                par.put("tiposusc", this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro).getNombre());
            } else {
                par.put("tiposusc", "(Todos los tipos)");
            }
            Funcionario cobra = null;
            if (this.idCobradorFiltro != null) {
                cobra = this.funcionarioFacade.find(this.idCobradorFiltro);
            }
            par.put("cuopend", this.cantCuotasFiltro);
            par.put("cuodesde", fdesde);
            par.put("cuohasta", fhasta);
            if (this.filtrarTodasFechasCambioEstado) {
                par.put("fechaCambioEstado", "(En cualquer fecha)");
            } else {
                try {
                    Date dd = this.sdf.parse(this.strDesdeTS);
                    Date dh = this.sdf.parse(this.strHastaTS);
                    SimpleDateFormat s = new SimpleDateFormat("dd/MMM/yy");
                    par.put("fechaCambioEstado", "(De " + s.format(dd) + " a " + s.format(dh) + ")");
                } catch (ParseException ex) {
                    System.err.println("Error al convertr String a Date: " + ex.getMessage());
                }
            }
            if (!conCobro) {
                if (this.cantCuotasFiltro == null) {
                    this.utilReportesSesBean.generarReporteSuscCuotasPend(this.paginador.getListaCompleta(), par, fdesde, fhasta, cobra, UtilReportesSesBean.REPORTE_SUSCRIPCIONES_CUOTAS_GRAL);
                } else {
                    this.utilReportesSesBean.generarReporteSuscCuotasPend(this.paginador.getListaCompleta(), par, fdesde, fhasta, cobra, UtilReportesSesBean.REPORTE_SUSCRIPCIONES_CUOTAS);
                }
            } else {
                if (this.cantCuotasFiltro == null) {
                    this.utilReportesSesBean.generarReporteSuscCuotasPend(this.paginador.getListaCompleta(), par, fdesde, fhasta, fdesdeCobro, fhastaCobro, cobra, UtilReportesSesBean.REPORTE_SUSCRIPCIONES_CUOTAS_GRAL_CON_COBRO);
                } else {
                    this.utilReportesSesBean.generarReporteSuscCuotasPend(this.paginador.getListaCompleta(), par, fdesde, fhasta, fdesdeCobro, fhastaCobro, cobra, UtilReportesSesBean.REPORTE_SUSCRIPCIONES_CUOTAS_CON_COBRO);
                }
            }
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a fecha", ex);
        }
    }

    public List<Funcionario> getLstFuncionarios() {
        return this.funcionarioFacade.getCobradores();
    }

    public Integer getTotalResumen() {
        Integer tot = 0;
        for (ItemResumenSuscripciones it : this.lstItemsResumen) {
            tot = tot + it.getMonto();
        }
        return tot;
    }

    public Integer getTotalSuscripcionesResumen() {
        Integer tot = 0;
        for (ItemResumenSuscripciones it : this.lstItemsResumen) {
            tot = tot + it.getCantSuscripciones();
        }
        return tot;
    }

    public void seleccionarSuscripciones() {
        System.out.println("SeleccionarSuscripciones: " + this.seleccionJson);
        Gson gson = new Gson();
        Integer[] idsSuscripciones = gson.fromJson(this.seleccionJson, Integer[].class);
        List<Suscripcion> lstEliminar = new ArrayList<>();
        this.lstSuscSeleccionadas.clear();
        for (Integer id : idsSuscripciones) {
            this.lstSuscSeleccionadas.add(this.suscripcionFacade.find(id));
        }
        if (this.lstSuscSeleccionadas.size() == 1) {
            this.suscVar = this.lstSuscSeleccionadas.get(0);
        } else {
            this.suscVar = new Suscripcion();
        }

    }

    public void verDatoFechaCobro() {
        System.out.println("rangos: " + this.strFechaDesdeCobroRes + " " + this.strFechaHastaCobroRes);
    }

    public Integer getMontoDeudaOtrosSerivicios(Integer idsuscripcion) {
        Suscripcion s = this.suscripcionFacade.find(idsuscripcion);
        return this.cuotaFacade.getMontoDeudaOtrosServicios(s);
    }
    
    public List<EstadoSuscripcion> getLstEstados(){
        return this.estadoSuscripcionFacade.findAll();
    }
    
    public Integer getCantidadPorEstadoServicio(Integer idservicio, Integer idestado){
        return this.suscripcionFacade.getCantidadSuscripciones(idservicio, idestado);
    }
    
    public void actualizarResumenCantidadSuscripciones(){
        this.lstServiciosResumenCantidad.clear();
        this.lstServiciosResumenCantidad.addAll(this.getLstServiciosSuscribibles());
    }

}
