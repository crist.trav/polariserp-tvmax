package com.exabit.polariserp.tvmax.util;

import com.exabit.polariserp.tvmax.cuotas.GenerarCuotasRunnable;
import com.exabit.polariserp.tvmax.entidades.CampoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.CategoriaCliente;
import com.exabit.polariserp.tvmax.entidades.CategoriaModulo;
import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoVivienda;
import com.exabit.polariserp.tvmax.sesbeans.CampoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.CargoFacade;
import com.exabit.polariserp.tvmax.sesbeans.CategoriaClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CategoriaModuloFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.ConceptoFacade;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.FacturaFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionalidadFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ModuloFacade;
import com.exabit.polariserp.tvmax.sesbeans.ParametroSistemaFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TerminalImpresionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedExecutorService;

/**
 *
 * @author cristhian-kn
 */
@Singleton
@Startup
public class UtilInicioSesBean {

    private static final Logger LOG = Logger.getLogger(UtilInicioSesBean.class.getName());
    
    @EJB
    private ConceptoFacade conceptoFacade;

    @EJB
    private CategoriaClienteFacade categoriaClienteFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private FacturaFacade facturaFacade;

    @EJB
    private CampoSuscripcionFacade campoSuscripcionFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private TerminalImpresionFacade terminalImpresionFacade;

    @EJB
    private CargoFacade cargoFacade;

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;

    @EJB
    private TipoViviendaFacade tipoViviendaFacade;

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private FuncionalidadFacade funcionalidadFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private CategoriaModuloFacade categoriaModuloFacade;

    @EJB
    private ModuloFacade moduloFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @Resource
    private ManagedExecutorService executor;

//    @Schedule(hour = "*/6", minute = "0", second = "0")
    public void generarCuotasDelMes() {
        System.out.println("tarea programada generar cuotas");
        String strEjecutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_SERVICIO_GENERACION_CUOTA_ACTIVO);
        boolean ejecutar = false;
        if (strEjecutar != null) {
            ejecutar = Boolean.parseBoolean(strEjecutar);
        }
        if (ejecutar) {
            this.executor.submit(new GenerarCuotasRunnable(new Date(), new Date(), this.suscripcionFacade.getSuscripcionesConServicio(), false, true));
        } else {
            System.out.println("no ejecutar tarea programada generacion de cuotas");
        }
    }

    @Schedule(dayOfMonth = "1", hour = "0", minute = "0", second = "0")
    public void generarCuotasInicioDelMes() {
        this.generarCuotasDelMes();
    }

    @Schedule(dayOfMonth = "Last", hour = "23", minute = "30", second = "0")
    private void anularFacturasPendientesMes() {
        System.out.println("tarea programada anular facturas");
        String strEjecutar = this.parametroSistemaFacade.getParametro(EntidadesEstaticas.PAR_SERVICIO_ANULACION_FACTURA_ACTIVO);
        boolean ejecutar = false;
        if (strEjecutar != null) {
            ejecutar = Boolean.parseBoolean(strEjecutar);
        }
        if (ejecutar) {
            Calendar cd = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
            Calendar ch = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
            ch.add(Calendar.MONTH, 1);
            ch.add(Calendar.DAY_OF_MONTH, -1);
            cd.set(Calendar.HOUR_OF_DAY, 23);
            cd.set(Calendar.MINUTE, 59);
            cd.set(Calendar.SECOND, 59);
            this.facturaFacade.anularFacturas(this.facturaFacade.getFacturasPendientes(cd.getTime(), ch.getTime()), "Anulación automática");
        } else {
            System.out.println("Ejecutar anulacion desactivada");
        }
    }
    
    @PostConstruct
    public void postConstruct() {
        System.out.println("INICIALIZACION DE APLICACION");
        for (CategoriaCliente cc : EntidadesEstaticas.getListaCategoriaClientes()) {
            if (this.categoriaClienteFacade.find(cc.getIdcategoria()) == null) {
                this.categoriaClienteFacade.create(cc);
            }
        }

        if (this.cargoFacade.find(EntidadesEstaticas.CARGO_COBRADOR.getIdcargo()) == null) {
            this.cargoFacade.create(EntidadesEstaticas.CARGO_COBRADOR);
        }

        for (EstadoSuscripcion e : EntidadesEstaticas.getListaEstadosSuscripcion()) {
            if (this.estadoSuscripcionFacade.find(e.getIdestado()) == null) {
                this.estadoSuscripcionFacade.create(e);
            }
        }

        for (TipoVivienda tv : EntidadesEstaticas.getListaTiposVivienda()) {
            if (this.tipoViviendaFacade.find(tv.getIdtipoVivienda()) == null) {
                this.tipoViviendaFacade.create(tv);
            }
        }

        for (TipoSuscripcion ts : EntidadesEstaticas.getListaTipoSuscripciones()) {
            if (this.tipoSuscripcionFacade.find(ts.getIdtipoSuscripcion()) == null) {
                this.tipoSuscripcionFacade.create(ts);
            }
        }

        for (CategoriaModulo cm : EntidadesEstaticas.getListaCategoriasModulos()) {
            if (this.categoriaModuloFacade.find(cm.getIdcategoriaModulo()) == null) {
                this.categoriaModuloFacade.create(cm);
            }
        }
        for (Modulo m : EntidadesEstaticas.getListaModulos()) {
            if (this.moduloFacade.find(m.getIdmodulo()) == null) {
                this.moduloFacade.create(m);
            }
        }

        for (Funcionalidad f : EntidadesEstaticas.getListaFuncionalidades()) {
            if (this.funcionalidadFacade.find(f.getIdfuncionalidad()) == null) {
                this.funcionalidadFacade.create(f);
            }
        }

        if (this.clienteFacade.find(EntidadesEstaticas.CLIENTE_ANULACION_FACTURA.getIdcliente()) == null) {
            Cliente caf=EntidadesEstaticas.CLIENTE_ANULACION_FACTURA;
            caf.setCategoria(this.categoriaClienteFacade.find(EntidadesEstaticas.CATEGORIA_CLIENTE_SIN_CATEGORIA.getIdcategoria()));
            this.clienteFacade.create(caf);
        }
        for (CampoSuscripcion cs : EntidadesEstaticas.getListaCamposSuscripcion()) {
            if (this.campoSuscripcionFacade.find(cs.getIdcampoSuscripcion()) == null) {
                this.campoSuscripcionFacade.create(cs);
            }
        }
        
        for(Concepto con:EntidadesEstaticas.getListaConceptoCobros()){
           if(this.conceptoFacade.find(con.getIdconcepto())==null){
               this.conceptoFacade.create(con);
           }
        }

        for (Servicio srv : EntidadesEstaticas.getListaServicios()) {
            if (this.servicioFacade.find(srv.getIdservicio()) == null) {
                this.servicioFacade.create(srv);
            }
        }

        this.funcionarioFacade.crearAdministrador();
    }

}
