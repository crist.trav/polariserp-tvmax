/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author frioss
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> {

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    public List<Cliente> buscarCliente(String tx){
        System.out.println("buscando String: "+tx);
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente C WHERE SQL('CAST(? AS CHAR(11))', c.idcliente) LIKE :tx OR SQL('CAST(? AS CHAR(11))', c.ci) LIKE :tx OR LOWER(c.razonSocial) LIKE :tx OR LOWER(c.nombres) LIKE :tx OR LOWER(c.apellidos) LIKE :tx ", Cliente.class);
        q.setParameter("tx", "%"+tx.toLowerCase()+"%");
//        q.setMaxResults(50);
        return q.getResultList();
    }
    
    public List<Cliente> busuqedaClientePorCI(Integer ci){
        System.out.println("buscando ci: "+ci);
        TypedQuery<Cliente> q = this.em.createQuery("SELECT c FROM Cliente C WHERE c.ci=:ci OR c.idcliente=:ci OR c.dvRuc=:ci", Cliente.class);
        q.setParameter("ci", ci);
//        q.setMaxResults(50);
        return q.getResultList();
    }
    
    public List<Cliente> getClienteEstado(){
//        Lis<Cliente
        TypedQuery<Cliente> query = this.em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        List<Cliente> listaCliente = new ArrayList<>();
        List<Cliente> listaClientePorEstadoSuscripcion = new LinkedList<>();
        listaCliente.addAll(query.getResultList());
        for (Cliente cliente : listaCliente) {
//            System.out.println("lista de clientes: "+cliente.getIdcliente());
            TypedQuery<Suscripcion> suscripcion = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cliente", Suscripcion.class);
            suscripcion.setParameter("cliente", cliente);
            suscripcion.getResultList();
            for (Suscripcion suscripc : suscripcion.getResultList()) {
//                System.out.println("realizando busqueda suscripcion");
                if (suscripc.getEstado().getRecibeServicio()==true) {
//                    listaSuscripcion.add(suscripc);
                    System.out.println("lista de suscripciones activos: "+suscripc.getEstado().getIdestado()+"---->"+suscripc.getCliente().getRazonSocial());
//                    Integer id = suscripc.getCliente().getIdcliente();
                    listaClientePorEstadoSuscripcion.add(suscripc.getCliente());
//                    TypedQuery<Cliente> clienteQuery = this.em.createQuery("SELECT cl FROM Cliente cl WHERE cl.idcliente=:id", Cliente.class);
//                    clienteQuery.setParameter("id", id);
//                    for (Cliente cliente1 : listaClientePorEstadoSuscripcion) {
////                        System.out.println("lista de clientes activos: "+cliente1.getRazonSocial());
//                    }
//                    listaClientePorEstadoSuscripcion.addAll(clienteQuery.getResultList());
                }else{
                    System.out.println("lista de clientes inactivos: "+ suscripc.getEstado().getIdestado()+", "+suscripc.getCliente().getRazonSocial());
                }
            }
        }
        return listaClientePorEstadoSuscripcion;
    }
    public List<Cliente> getClientes(){
        TypedQuery<Cliente> query = this.em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        return query.getResultList();
    }
    
    public Funcionario getCobrador(Cliente c){       
        TypedQuery<Suscripcion> q=this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli", Suscripcion.class);
        q.setParameter("cli", c);
        List<Suscripcion> lstSusc=q.getResultList();
        if(lstSusc.isEmpty()){
            return this.funcionarioFacade.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR);
        }else{
            return lstSusc.get(0).getCobrador();
        }        
    }
    
    public List<Cliente> getClientesPorCobrador(Funcionario cobrador){
        TypedQuery<Cliente> q=this.em.createQuery("SELECT c FROM Cliente c WHERE c.cobrador=:cobra ORDER BY c.razonSocial ASC", Cliente.class);
        q.setParameter("cobra", cobrador);
        return q.getResultList();
    }
    
    public List<Cliente> getClientesOrdenadoRazonSocial(){
        TypedQuery<Cliente> query = this.em.createQuery("SELECT c FROM Cliente c WHERE c.idcliente!=10000000 ORDER BY c.razonSocial ASC", Cliente.class);
        return query.getResultList();
    }
}
