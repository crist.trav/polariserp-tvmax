/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.HistorialSuscripcion;
import com.exabit.polariserp.tvmax.entidades.SolicitudCambioEstado;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class SuscripcionFacade extends AbstractFacade<Suscripcion> {

    @EJB
    private HistorialSuscripcionFacade historialSuscripcionFacade;

    @Inject
    SessionManBean sessionManBean;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private ParametroSistemaFacade parametroSistemaFacade;

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private SolicitudCambioEstadoFacade solicitudCambioEstadoFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SuscripcionFacade() {
        super(Suscripcion.class);
    }

    public List<Suscripcion> getSuscripcionOrdenInverso() {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
//        q.setMaxResults(100).getResultList();
        return q.getResultList();
    }

    public List<Suscripcion> buscarSuscripcion(String texto) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx OR s.nroMedidorElectrico LIKE :tx ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("tx", "%" + texto.toLowerCase() + "%");
        return q.getResultList();
    }

    public List<Suscripcion> buscarSuscripcion(String texto, EstadoSuscripcion e) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado=:estado AND (LOWER(s.estado.nombre) LIKE :tx OR SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx) OR s.nroMedidorElectrico LIKE :tx ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("tx", "%" + texto.toLowerCase() + "%");
        q.setParameter("estado", e);
        return q.getResultList();
    }

    public List<Suscripcion> buscarSuscripcion(String texto, TipoSuscripcion ts) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos AND (LOWER(s.estado.nombre) LIKE :tx OR SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx) or s.nroMedidorElectrico LIKE :tx ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("tx", "%" + texto.toLowerCase() + "%");
        q.setParameter("tipos", ts);
        return q.getResultList();
    }

    public List<Suscripcion> buscarSuscripcion(String texto, EstadoSuscripcion e, TipoSuscripcion ts) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos AND s.estado=:estado AND (LOWER(s.estado.nombre) LIKE :tx OR SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx) OR s.nroMedidorElectrico LIKE :tx ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("tx", "%" + texto.toLowerCase() + "%");
        q.setParameter("estado", e);
        q.setParameter("tipos", ts);
        return q.getResultList();
    }

    public List<Suscripcion> buscarSuscripcionporCobrador(Funcionario func, EstadoSuscripcion ets) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cobrador=:func AND s.estado=:ets", Suscripcion.class);
        q.setParameter("func", func);
        q.setParameter("ets", ets);
        return q.getResultList();
    }

    public List<Suscripcion> getSuscripcionesPorEstado(EstadoSuscripcion es) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado=:est ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("est", es);
        return q.getResultList();
    }

    public List<Suscripcion> getSuscripcionesOrdenInverso(EstadoSuscripcion es, TipoSuscripcion ts) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos AND s.estado=:est ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("est", es);
        q.setParameter("tipos", ts);
        return q.getResultList();
    }

    public List<Suscripcion> getSuscripcionesOrdenInverso(TipoSuscripcion ts) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        q.setParameter("tipos", ts);
        return q.getResultList();
    }

    public List<Suscripcion> getSuscripcionesConServicio() {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=true ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesConServicio(String busqueda) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=true AND (SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx OR s.nroMedidorElectrico LIKE :tx) ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tx", "%"+busqueda.toLowerCase()+"%");
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesConServicio(String busqueda, TipoSuscripcion ts) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos AND s.estado.recibeServicio=true AND (SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx OR s.nroMedidorElectrico LIKE :tx) ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tx", "%"+busqueda.toLowerCase()+"%");
        qs.setParameter("tipos", ts);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesConServicio(TipoSuscripcion ts) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=true AND s.tipoSuscripcion=:tipos ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tipos", ts);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesSinServicio() {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=false ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesSinServicio(String busqueda) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=false AND (SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx OR s.nroMedidorElectrico LIKE :tx) ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tx", "%"+busqueda.toLowerCase()+"%");
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesSinServicio(String busqueda, TipoSuscripcion ts) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.tipoSuscripcion=:tipos AND s.estado.recibeServicio=false AND (SQL('CAST(? AS CHAR(11))',s.idsuscripcion) LIKE :tx OR LOWER(s.cliente.razonSocial) LIKE :tx OR LOWER(s.cliente.apellidos) LIKE :tx OR LOWER(s.cliente.nombres) LIKE :tx OR SQL('CAST(? AS CHAR(11))', s.cliente.ci) LIKE :tx OR s.nroMedidorElectrico LIKE :tx) ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tx", "%"+busqueda.toLowerCase()+"%");
        qs.setParameter("tipos", ts);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesSinServicio(TipoSuscripcion ts) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.estado.recibeServicio=false AND s.tipoSuscripcion=:tipos ORDER BY s.cliente.razonSocial ASC", Suscripcion.class);
        qs.setParameter("tipos", ts);
        return qs.getResultList();
    }

    public List<Suscripcion> getSuscripcionesConServicio(Cliente cli) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli AND s.estado.recibeServicio=true", Suscripcion.class);
        qs.setParameter("cli", cli);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesOrdenInversoFecha(Cliente cli) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli ORDER BY s.fechaCambioEstado DESC", Suscripcion.class);
        qs.setParameter("cli", cli);
        return qs.getResultList();
    }
    public List<Suscripcion> getSuscripcionesOrdenDESCid(Cliente cli) {
        TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli ORDER BY s.idsuscripcion DESC", Suscripcion.class);
        qs.setParameter("cli", cli);
        return qs.getResultList();
    }

    public void registrarSuscripcion(Suscripcion s, Date primerVenc, Integer montoPrimeraCuota) {
        SolicitudCambioEstado sce = new SolicitudCambioEstado();
        sce.setConcretado(true);
        sce.setEstadoAnterior(this.estadoSuscripcionFacade.find(EntidadesEstaticas.ESTADO_NUEVO.getIdestado()));
        sce.setEstadoNuevo(this.estadoSuscripcionFacade.find(EntidadesEstaticas.ESTADO_CONECTADO.getIdestado()));
        sce.setFechaConcrecion(new Date());
        sce.setFechaSolicitud(new Date());
        sce.setSuscripcion(s);
        sce.setObservacion("Registro de suscripción");
        s.setFechaCambioEstado(new Date());
        s.setFechaSuscripcion(new Date());
        this.create(s);
        this.solicitudCambioEstadoFacade.create(sce);

        Cuota c = new Cuota();
        Calendar calVen = new GregorianCalendar();
        calVen.setTime(primerVenc);
        c.setFechaVencimiento(primerVenc);
        c.setMontoCuota(montoPrimeraCuota);
        c.setMontoEntrega(0);
        c.setAnioCuota((short) calVen.get(Calendar.YEAR));
        c.setMesCuota((short) (calVen.get(Calendar.MONTH) + 1));
        c.setCancelado(false);
        c.setSuscripcion(s);

        Calendar calIngreso = new GregorianCalendar();
        calIngreso.setTime(s.getFechaSuscripcion());

//        while(calVen.compareTo(calIngreso)>0){
        while (calIngreso.compareTo(calVen) < 0) {
            if (calVen.get(Calendar.MONTH) != calIngreso.get(Calendar.MONTH)) {
                this.cuotaFacade.exonerarCuota(s, calIngreso.get(Calendar.YEAR), calIngreso.get(Calendar.MONTH) + 1);
                calIngreso.add(Calendar.MONTH, 1);
            }
        }
        this.cuotaFacade.create(c);
    }

    public List<Suscripcion> getSuscripcionPorNumeroMedidor(String numeroMedidor) {
        TypedQuery<Suscripcion> q = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.nroMedidorElectrico=:numeroMedidor", Suscripcion.class);
        q.setParameter("numeroMedidor", numeroMedidor);
        return q.getResultList();
    }

    public List<Suscripcion> getSuscripcionesActivasPorCobrador(Funcionario c) {
        TypedQuery<Suscripcion> q = getEntityManager().createQuery("SELECT s FROM Suscripcion s WHERE s.cobrador=:cobrador AND s.estado=:estado ORDER BY s.cliente.razonSocial DESC", Suscripcion.class);
        q.setParameter("cobrador", c);
        q.setParameter("estado", EntidadesEstaticas.ESTADO_CONECTADO);
        return q.getResultList();
    }

    @Override
    public void edit(Suscripcion s) {
        if (s.getCobrador() == null) {
            if (s.getCliente().getCobrador() != null) {
                s.setCobrador(s.getCliente().getCobrador());
            } else {
                s.setCobrador(this.em.find(Funcionario.class, 1000));
            }
        }
        Suscripcion suscripcionOrig = this.find(s.getIdsuscripcion());
        //Cambio de barrio
        if (!s.getBarrio().equals(suscripcionOrig.getBarrio())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_BARRIO);
            hs.setClaveRefAnterior(suscripcionOrig.getBarrio().getIdbarrio());
            hs.setClaveRefNuevo(s.getBarrio().getIdbarrio());
            hs.setValorAnterior(suscripcionOrig.getBarrio().getNombre());
            hs.setValorNuevo(s.getBarrio().getNombre());
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            this.historialSuscripcionFacade.create(hs);
        }
        //Cambio de cantidad de tv
        if (s.getCantidadTvs() != null && suscripcionOrig.getCantidadTvs() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_CANTIDAD_TV);
            hs.setValorAnterior("(Sin valor)");
            hs.setValorNuevo(s.getCantidadTvs().toString());
            hs.setFechaHoraCambio(new Date());
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());
            hs.setSuscripcion(s);
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getCantidadTvs() == null && suscripcionOrig.getCantidadTvs() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_CANTIDAD_TV);
            hs.setValorAnterior(suscripcionOrig.getCantidadTvs().toString());
            hs.setValorNuevo("(Sin valor)");
            hs.setFechaHoraCambio(new Date());
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());
            hs.setSuscripcion(s);
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getCantidadTvs() != null && suscripcionOrig.getCantidadTvs() != null) {
            if (!s.getCantidadTvs().equals(suscripcionOrig.getCantidadTvs())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_CANTIDAD_TV);
                hs.setValorAnterior(suscripcionOrig.getCantidadTvs().toString());
                hs.setValorNuevo(s.getCantidadTvs().toString());
                hs.setFechaHoraCambio(new Date());
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());
                hs.setSuscripcion(s);
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //cambio de titular
        if (!s.getCliente().equals(suscripcionOrig.getCliente())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_CLIENTE);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            String clienteAnterior = suscripcionOrig.getCliente().getRazonSocial();
            if (suscripcionOrig.getCliente().getCi() != null) {
                clienteAnterior = clienteAnterior + "[CI:" + suscripcionOrig.getCliente().getCi() + "]";
            }
            hs.setValorAnterior(clienteAnterior);
            hs.setClaveRefAnterior(suscripcionOrig.getCliente().getIdcliente());

            String clienteNuevo = s.getCliente().getRazonSocial();
            if (s.getCliente().getCi() != null) {
                clienteNuevo = clienteNuevo + "[CI:" + s.getCliente().getCi() + "]";
            }
            hs.setValorNuevo(clienteNuevo);
            hs.setClaveRefNuevo(s.getCliente().getIdcliente());
            this.historialSuscripcionFacade.create(hs);
        }
        //cambio de cobrador
        if (!s.getCobrador().equals(suscripcionOrig.getCobrador())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_COBRADOR);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            String cobradorAnterior = suscripcionOrig.getCobrador().getNombres();
            if (suscripcionOrig.getCobrador().getApellidos() != null) {
                cobradorAnterior = cobradorAnterior + " " + suscripcionOrig.getCobrador().getApellidos();
            }
            if (suscripcionOrig.getCobrador().getCi() != null) {
                cobradorAnterior = cobradorAnterior + " [CI:" + suscripcionOrig.getCobrador().getCi() + "]";
            }
            hs.setValorAnterior(cobradorAnterior);
            hs.setClaveRefAnterior(suscripcionOrig.getCobrador().getIdfuncionario());

            String cobradorNuevo = s.getCobrador().getNombres();
            if (s.getCobrador().getApellidos() != null) {
                cobradorNuevo = cobradorNuevo + " " + s.getCobrador().getApellidos();
            }
            if (s.getCobrador().getCi() != null) {
                cobradorNuevo = cobradorNuevo + " [CI:" + s.getCobrador().getCi() + "]";
            }
            hs.setValorNuevo(cobradorNuevo);
            hs.setClaveRefNuevo(s.getCobrador().getIdfuncionario());
            this.historialSuscripcionFacade.create(hs);
        }
        //cambio de dia de vencimiento
        if (s.getDiaVencimientoMes() != null && suscripcionOrig.getDiaVencimientoMes() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIA_VENCIMIENTO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior("(Sin valor)");
            hs.setValorNuevo(s.getDiaVencimientoMes().toString());
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getDiaVencimientoMes() == null && suscripcionOrig.getDiaVencimientoMes() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIA_VENCIMIENTO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getDiaVencimientoMes().toString());
            hs.setValorNuevo("(Sin valor)");
            this.historialSuscripcionFacade.create(hs);
        }
        if (s.getDiaVencimientoMes() != null && suscripcionOrig.getDiaVencimientoMes() != null) {
            if (!s.getDiaVencimientoMes().equals(suscripcionOrig.getDiaVencimientoMes())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIA_VENCIMIENTO);
                hs.setFechaHoraCambio(new Date());
                hs.setSuscripcion(s);
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

                hs.setValorAnterior(suscripcionOrig.getDiaVencimientoMes().toString());
                hs.setValorNuevo(s.getDiaVencimientoMes().toString());
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //cambio de direccion
        if (s.getDireccion() != null && suscripcionOrig.getDireccion() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIRECCION);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior("(Sin Valor)");
            hs.setValorNuevo(s.getDireccion());
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getDireccion() == null && suscripcionOrig.getDireccion() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIRECCION);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getDireccion());
            hs.setValorNuevo("(Sin Valor)");
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getDireccion() != null && suscripcionOrig.getDireccion() != null) {
            if (!s.getDireccion().equals(suscripcionOrig.getDireccion())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_DIRECCION);
                hs.setFechaHoraCambio(new Date());
                hs.setSuscripcion(s);
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

                hs.setValorAnterior(suscripcionOrig.getDireccion());
                hs.setValorNuevo(s.getDireccion());
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //cambio de estado
        if (!s.getEstado().equals(suscripcionOrig.getEstado())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_ESTADO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getEstado().getNombre());
            hs.setClaveRefAnterior(suscripcionOrig.getEstado().getIdestado());

            hs.setValorNuevo(s.getEstado().getNombre());
            hs.setClaveRefNuevo(s.getEstado().getIdestado());
            this.historialSuscripcionFacade.create(hs);
        }
        //cambio de fecha de suscripcion
        if (s.getFechaSuscripcion() != null && suscripcionOrig.getFechaSuscripcion() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
            hs.setValorAnterior("(Sin valor)");
            hs.setValorNuevo(sdf.format(s.getFechaSuscripcion()));
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getFechaSuscripcion() == null && suscripcionOrig.getFechaSuscripcion() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
            hs.setValorAnterior(sdf.format(suscripcionOrig.getFechaSuscripcion()));
            hs.setValorNuevo("(Sin valor)");
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getFechaSuscripcion() != null && suscripcionOrig.getFechaSuscripcion() != null) {
            if (!s.getFechaSuscripcion().equals(suscripcionOrig.getFechaSuscripcion())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION);
                hs.setFechaHoraCambio(new Date());
                hs.setSuscripcion(s);
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMM/yyyy");
                hs.setValorAnterior(sdf.format(suscripcionOrig.getFechaSuscripcion()));
                hs.setValorNuevo(sdf.format(s.getFechaSuscripcion()));
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //cambio de numero de medidor
        if (s.getNroMedidorElectrico() != null && suscripcionOrig.getNroMedidorElectrico() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_NRO_MEDIDOR);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior("(Sin valor)");
            hs.setValorNuevo(s.getNroMedidorElectrico());
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getNroMedidorElectrico() == null && suscripcionOrig.getNroMedidorElectrico() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_NRO_MEDIDOR);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getNroMedidorElectrico());
            hs.setValorNuevo("(Sin valor)");
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getNroMedidorElectrico() != null && suscripcionOrig.getNroMedidorElectrico() != null) {
            if (!s.getNroMedidorElectrico().equals(suscripcionOrig.getNroMedidorElectrico())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_NRO_MEDIDOR);
                hs.setFechaHoraCambio(new Date());
                hs.setSuscripcion(s);
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

                hs.setValorAnterior(suscripcionOrig.getNroMedidorElectrico());
                hs.setValorNuevo(s.getNroMedidorElectrico());
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //cambio de precio de cuota
        if (s.getPrecio() != null && suscripcionOrig.getPrecio() == null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_PRECIO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior("(Sin valor)");
            hs.setValorNuevo(s.getPrecio().toString());
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getPrecio() == null && suscripcionOrig.getPrecio() != null) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_PRECIO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getPrecio().toString());
            hs.setValorNuevo("(Sin valor)");
            this.historialSuscripcionFacade.create(hs);
        } else if (s.getPrecio() != null && suscripcionOrig.getPrecio() != null) {
            if (!s.getPrecio().equals(suscripcionOrig.getPrecio())) {
                HistorialSuscripcion hs = new HistorialSuscripcion();
                hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_PRECIO);
                hs.setFechaHoraCambio(new Date());
                hs.setSuscripcion(s);
                hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

                hs.setValorAnterior(suscripcionOrig.getPrecio().toString());
                hs.setValorNuevo(s.getPrecio().toString());
                this.historialSuscripcionFacade.create(hs);
            }
        }
        //Cambio de servicio
        if (!s.getServicio().equals(suscripcionOrig.getServicio())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_SERVICIO);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getServicio().getNombre());
            hs.setClaveRefAnterior(suscripcionOrig.getServicio().getIdservicio());

            hs.setValorNuevo(s.getServicio().getNombre());
            hs.setClaveRefNuevo(s.getServicio().getIdservicio());
            this.historialSuscripcionFacade.create(hs);
        }
        //cambio de tipo de suscripcion
        if (!s.getTipoSuscripcion().equals(suscripcionOrig.getTipoSuscripcion())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_TIPO_SUSCRIPCION);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getTipoSuscripcion().getNombre());
            hs.setClaveRefAnterior(suscripcionOrig.getTipoSuscripcion().getIdtipoSuscripcion());

            hs.setValorNuevo(s.getTipoSuscripcion().getNombre());
            hs.setClaveRefNuevo(s.getTipoSuscripcion().getIdtipoSuscripcion());
            this.historialSuscripcionFacade.create(hs);
        }
        //cambio de tipo de vivienda
        if (!s.getTipoVivienda().equals(suscripcionOrig.getTipoVivienda())) {
            HistorialSuscripcion hs = new HistorialSuscripcion();
            hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_TIPO_VIVIENDA);
            hs.setFechaHoraCambio(new Date());
            hs.setSuscripcion(s);
            hs.setFuncionario(this.sessionManBean.getFuncionarioVar());

            hs.setValorAnterior(suscripcionOrig.getTipoVivienda().getNombre());
            hs.setClaveRefAnterior(suscripcionOrig.getTipoVivienda().getIdtipoVivienda());

            hs.setValorNuevo(s.getTipoVivienda().getNombre());
            hs.setClaveRefNuevo(s.getTipoVivienda().getIdtipoVivienda());

            this.historialSuscripcionFacade.create(hs);
        }
        super.edit(s);
    }

    public List<Suscripcion> getSuscripciones(Cliente c) {
        getEntityManager().refresh(getEntityManager().merge(c));
        TypedQuery<Suscripcion> q = getEntityManager().createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli", Suscripcion.class);
        q.setParameter("cli", c);
        List<Suscripcion> lst = q.getResultList();
        for (Suscripcion s : lst) {
            getEntityManager().refresh(s);
        }
        return lst;
    }

    public void cambiarEstado(Integer idSuscripcion, Integer idestado, Date fecha, String observacion, Funcionario func) {
        Suscripcion s = this.em.find(Suscripcion.class, idSuscripcion);
        EstadoSuscripcion e = this.em.find(EstadoSuscripcion.class, idestado);
        HistorialSuscripcion hs = new HistorialSuscripcion();
        hs.setCampoSuscripcion(EntidadesEstaticas.CAMPO_SUSCRIPCION_ESTADO);
        hs.setFechaHoraCambio(fecha);
        hs.setSuscripcion(s);
        hs.setFuncionario(func);

        hs.setValorAnterior(s.getEstado().getNombre());
        hs.setClaveRefAnterior(s.getEstado().getIdestado());

        EstadoSuscripcion estadoNuevo=this.em.find(EstadoSuscripcion.class, idestado);
        
        hs.setValorNuevo(estadoNuevo.getNombre());
        hs.setClaveRefNuevo(idestado);
        this.em.persist(hs);
        s.setEstado(e);
        s.setFechaCambioEstado(fecha);
        this.em.merge(s);
    }
    
    public Integer getCantidadSuscripciones(Integer idservicio, Integer idestado){
        TypedQuery<Suscripcion> q=this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.servicio.idservicio=:idservicio AND s.estado.idestado=:idestado", Suscripcion.class);
        q.setParameter("idservicio", idservicio);
        q.setParameter("idestado", idestado);
        return q.getResultList().size();
    }
}
