/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.Barrio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class BarrioFacade extends AbstractFacade<Barrio> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BarrioFacade() {
        super(Barrio.class);
    }
    
    public List<Barrio> buscarBarrio(String tx){
        TypedQuery<Barrio> q=this.em.createQuery("SELECT b FROM Barrio b WHERE SQL('CAST(? AS CHAR(11))', b.idbarrio) LIKE :tx OR LOWER(b.nombre) LIKE :tx OR LOWER(b.zona.nombre) LIKE :tx", Barrio.class);
        q.setParameter("tx", "%"+tx.toLowerCase()+"%");
        return q.getResultList();
    }
    
    public List<Barrio> getListaBarriosOrdenado(){
        TypedQuery<Barrio> q=this.em.createQuery("SELECT b FROM Barrio b ORDER BY b.nombre ASC", Barrio.class);
        return q.getResultList();
    }
    
}
