/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sorteo;

import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Sorteo;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.reportes.UtilReportesSesBean;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SorteoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import com.exabit.polariserp.tvmax.suscripciones.ItemResumenSuscripciones;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author frioss
 */
@Named(value = "excluidoManBean")
@ViewScoped
public class ExcluidosManBean implements Serializable {

    @EJB
    private SorteoFacade sorteoFacade;
    private static final Logger LOG = Logger.getLogger(ExcluidosManBean.class.getName());

    @EJB
    private UtilReportesSesBean utilReportesSesBean;

    @EJB
    private CuotaFacade cuotaFacade;

    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;

    @EJB
    private ServicioFacade servicioFacade;

    @EJB
    private ClienteFacade clienteFacade;

    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;

    @EJB
    private TipoViviendaFacade tipoViviendaFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private BarrioFacade barrioFacade;

    @EJB
    private SuscripcionFacade suscripcionFacade;
    
    
    
    private List<ItemResumenSuscripciones> lstItemsResumen = new ArrayList<>();

    public List<ItemResumenSuscripciones> getLstItemsResumen() {
        return lstItemsResumen;
    }

    public void setLstItemsResumen(List<ItemResumenSuscripciones> lstItemsResumen) {
        this.lstItemsResumen = lstItemsResumen;
    }

    private Suscripcion suscSeleccionada;

    public Suscripcion getSuscSeleccionada() {
        return suscSeleccionada;
    }

    public void setSuscSeleccionada(Suscripcion suscSeleccionada) {
        this.suscSeleccionada = suscSeleccionada;
    }

    private List<Suscripcion> lstSuscSeleccionadas = new LinkedList<>();

    public List<Suscripcion> getLstSuscSeleccionadas() {
        return lstSuscSeleccionadas;
    }

    public void setLstSuscSeleccionadas(List<Suscripcion> lstSuscSeleccionadas) {
        this.lstSuscSeleccionadas = lstSuscSeleccionadas;
    }

    private Integer montoPorConexion;

    public Integer getMontoPorConexion() {
        return montoPorConexion;
    }
    
    private Integer idFiltroSorteo;

    public Integer getIdFiltroSorteo() {
        return idFiltroSorteo;
    }

    public void setIdFiltroSorteo(Integer idFiltroSorteo) {
        this.idFiltroSorteo = idFiltroSorteo;
    }

    public void setMontoPorConexion(Integer montoPorConexion) {
        this.montoPorConexion = montoPorConexion;
    }

    public Paginador<Suscripcion> getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador<Suscripcion> paginador) {
        this.paginador = paginador;
    }
    private Paginador<Suscripcion> paginador;
    private List<Suscripcion> lstSuscripciones;
    private Integer idBarrio;
    private Integer idCobrador;
    private Integer idTipoVivienda;
    private Integer idEstadoSusc;
    private Integer idCliente;
    private Suscripcion suscVar;
    private Integer idServicio;
    private String msg;
    private String strFechaDesdeCobroRes;
    private String strFechaHastaCobroRes;
    private boolean chkMostrarTodosEstados = true;
    private Integer idEstadoFiltro=1;
    private String seleccionJson;
    private String textBusq;

    public String getStrFechaDesdeCobroRes() {
        return strFechaDesdeCobroRes;
    }

    public void setStrFechaDesdeCobroRes(String strFechaDesdeCobroRes) {
        this.strFechaDesdeCobroRes = strFechaDesdeCobroRes;
    }

    public String getStrFechaHastaCobroRes() {
        return strFechaHastaCobroRes;
    }

    public void setStrFechaHastaCobroRes(String strFechaHastaCobroRes) {
        this.strFechaHastaCobroRes = strFechaHastaCobroRes;
    }

    public boolean isChkMostrarTodosEstados() {
        return chkMostrarTodosEstados;
    }

    public void setChkMostrarTodosEstados(boolean chkMostrarTodosEstados) {
        this.chkMostrarTodosEstados = chkMostrarTodosEstados;
    }

    public Integer getIdEstadoFiltro() {
        return idEstadoFiltro;
    }

    public void setIdEstadoFiltro(Integer idEstadoFiltro) {
        this.idEstadoFiltro = idEstadoFiltro;
    }

    public List<Suscripcion> getLstSuscripciones() {
        return lstSuscripciones;
    }

    public void setLstSuscripciones(List<Suscripcion> lstSuscripciones) {
        this.lstSuscripciones = lstSuscripciones;
    }

    public Integer getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Integer idBarrio) {
        this.idBarrio = idBarrio;
    }

    public Integer getIdCobrador() {
        return idCobrador;
    }

    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }

    public Integer getIdTipoVivienda() {
        return idTipoVivienda;
    }

    public void setIdTipoVivienda(Integer idTipoVivienda) {
        this.idTipoVivienda = idTipoVivienda;
    }

    public Integer getIdEstadoSusc() {
        return idEstadoSusc;
    }

    public void setIdEstadoSusc(Integer idEstadoSusc) {
        this.idEstadoSusc = idEstadoSusc;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Suscripcion getSuscVar() {
        return suscVar;
    }

    public void setSuscVar(Suscripcion suscVar) {
        this.suscVar = suscVar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSeleccionJson() {
        return seleccionJson;
    }

    public void setSeleccionJson(String seleccionJson) {
        this.seleccionJson = seleccionJson;
    }

    public String getTextBusq() {
        return textBusq;
    }

    public void setTextBusq(String textBusq) {
        this.textBusq = textBusq;
    }

    private Integer idTipoSuscripcion;

    public Integer getIdTipoSuscripcion() {
        return idTipoSuscripcion;
    }

    public void setIdTipoSuscripcion(Integer idTipoSuscripcion) {
        this.idTipoSuscripcion = idTipoSuscripcion;
    }

    private Integer idTipoSuscFiltro=1;

    public Integer getIdTipoSuscFiltro() {
        return idTipoSuscFiltro;
    }

    public void setIdTipoSuscFiltro(Integer idTipoSuscFiltro) {
        this.idTipoSuscFiltro = idTipoSuscFiltro;
    }

    private Integer cantCuotasFiltro=0;

    public Integer getCantCuotasFiltro() {
        return cantCuotasFiltro;
    }

    public void setCantCuotasFiltro(Integer cantCuotasFiltro) {
        this.cantCuotasFiltro = cantCuotasFiltro;
    }

    private String strFechaPrimerVenc;

    public String getStrFechaPrimerVenc() {
        return strFechaPrimerVenc;
    }

    public void setStrFechaPrimerVenc(String strFechaPrimerVenc) {
        this.strFechaPrimerVenc = strFechaPrimerVenc;
    }

    private Integer montoPrimeraCuota;

    public Integer getMontoPrimeraCuota() {
        return montoPrimeraCuota;
    }

    public void setMontoPrimeraCuota(Integer montoPrimeraCuota) {
        this.montoPrimeraCuota = montoPrimeraCuota;
    }

    private Integer idCobradorFiltro;

    public Integer getIdCobradorFiltro() {
        return idCobradorFiltro;
    }

    public void setIdCobradorFiltro(Integer idCobradorFiltro) {
        this.idCobradorFiltro = idCobradorFiltro;
    }

    private String strDesdeFiltro;

    public String getStrDesdeFiltro() {
        return strDesdeFiltro;
    }

    public void setStrDesdeFiltro(String strDesdeFiltro) {
        this.strDesdeFiltro = strDesdeFiltro;
    }

    private String strHastaFiltro;

    public String getStrHastaFiltro() {
        return strHastaFiltro;
    }

    public void setStrHastaFiltro(String strHastaFiltro) {
        this.strHastaFiltro = strHastaFiltro;
    }

    private Integer cantCuotasResaltar = 4;

    public Integer getCantCuotasResaltar() {
        return cantCuotasResaltar;
    }

    public void setCantCuotasResaltar(Integer cantCuotasResaltar) {
        this.cantCuotasResaltar = cantCuotasResaltar;
    }

    /**
     * Creates a new instance of SuscripcionManBean
     */
    private final SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
    private final SimpleDateFormat sdfMesAnio = new SimpleDateFormat("MMMM yyyy");

    public ExcluidosManBean() {
        this.suscVar = new Suscripcion();
        this.lstSuscripciones = new ArrayList<>();
        this.paginador = new Paginador(this.lstSuscripciones);
        this.strFechaPrimerVenc = this.sdf.format(new Date());
        Calendar clDF = new GregorianCalendar();
        clDF.add(Calendar.MONTH, -1);
        this.strDesdeFiltro = this.sdfMesAnio.format(clDF.getTime());

        this.strHastaFiltro = this.sdfMesAnio.format(new Date());
        Calendar cdesdecobro = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
        this.strFechaDesdeCobroRes = this.sdf.format(cdesdecobro.getTime());

        Calendar chastacobro = new GregorianCalendar(new GregorianCalendar().get(Calendar.YEAR), new GregorianCalendar().get(Calendar.MONTH), 1);
        chastacobro.add(Calendar.MONTH, 1);
        chastacobro.add(Calendar.DAY_OF_MONTH, -1);
        this.strFechaHastaCobroRes = this.sdf.format(chastacobro.getTime());
        
//        this.idEstadoFiltro=EntidadesEstaticas.ESTADO_ACTIVO.getIdestado();
//        this.idTipoSuscFiltro=EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL.getIdtipoSuscripcion();

    }

    @PostConstruct
    private void postConstruct() {
//        this.filtrarSuscripciones();
    }

    private void cargarDatos() {
        this.suscVar.setBarrio(this.barrioFacade.find(this.idBarrio));
        this.suscVar.setCliente(this.clienteFacade.find(this.idCliente));
        this.suscVar.setCobrador(this.funcionarioFacade.find(this.idCobrador));
        this.suscVar.setServicio(this.servicioFacade.find(this.idServicio));
        this.suscVar.setTipoVivienda(this.tipoViviendaFacade.find(this.idTipoVivienda));
        this.suscVar.setTipoSuscripcion(this.tipoSuscripcionFacade.find(this.idTipoSuscripcion));
    }

//    public void eliminarSuscripciones() {
//        Gson gson = new Gson();
//        Integer[] arrayIdsEliminar = gson.fromJson(this.seleccionJson, Integer[].class);
//        List<Suscripcion> lstEliminar = new ArrayList<>();
//        for (Integer id : arrayIdsEliminar) {
//            Suscripcion s = this.suscripcionFacade.find(id);
//            lstEliminar.add(s);
//            this.suscripcionFacade.remove(s);
//        }
//        this.paginador.limpiarLista();
//        this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
//    }

    public void filtrarSuscripciones() {
        System.out.println("Se filtran suscripciones");
        System.out.println("cristerio: " + this.textBusq + ", idestado: " + this.idEstadoFiltro + ", idtipo: " + this.idTipoSuscFiltro+", idFiltroSorteo: "+idFiltroSorteo);
                
        this.paginador.limpiarLista();
        if (this.textBusq == null) {
            this.textBusq = "";
        }
        if (this.textBusq.isEmpty()) {
            if (this.idEstadoFiltro == null && this.idTipoSuscFiltro == null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionOrdenInverso());
            } else if (this.idTipoSuscFiltro == null && this.idEstadoFiltro != null) {
                EstadoSuscripcion e = this.estadoSuscripcionFacade.find(this.idEstadoFiltro);
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesPorEstado(e));
            } else if (this.idTipoSuscFiltro != null && this.idEstadoFiltro == null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesOrdenInverso(this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            } else {
                this.paginador.agregarItemsLista(this.suscripcionFacade.getSuscripcionesOrdenInverso(this.estadoSuscripcionFacade.find(this.idEstadoFiltro), this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            }
        } else {
            if (this.idEstadoFiltro == null && this.idTipoSuscFiltro == null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq));

            } else if (this.idEstadoFiltro != null && this.idTipoSuscFiltro == null) {
                System.out.println("filtro por criterio y estado");
                EstadoSuscripcion e = this.estadoSuscripcionFacade.find(this.idEstadoFiltro);
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(textBusq, e));
            } else if (this.idEstadoFiltro == null && this.idTipoSuscFiltro != null) {
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq, this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            } else {
                this.paginador.agregarItemsLista(this.suscripcionFacade.buscarSuscripcion(this.textBusq, this.estadoSuscripcionFacade.find(this.idEstadoFiltro), this.tipoSuscripcionFacade.find(this.idTipoSuscFiltro)));
            }
        }
        if (this.idCobradorFiltro != null) {
            List<Suscripcion> lstcobra = new ArrayList<>();
            for (Suscripcion s : this.paginador.getListaCompleta()) {
                if (s.getCobrador().getIdfuncionario().equals(this.idCobradorFiltro)) {
                    lstcobra.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstcobra);
        }

        if (this.cantCuotasFiltro != null) {
            cantCuotasFiltro=0;
            List<Suscripcion> lstSuscFiltCantCuo = new ArrayList<>();
            for (Suscripcion s : this.paginador.getListaCompleta()) {
                if (this.getCantidadCuotasPendientes(s.getIdsuscripcion()).equals(this.cantCuotasFiltro)) {
                    lstSuscFiltCantCuo.add(s);
                }
            }
            this.paginador.limpiarLista();
            this.paginador.agregarItemsLista(lstSuscFiltCantCuo);
        }
        
        if (idFiltroSorteo == null) {
            System.out.println("idFiltroSorteo is null");
            List<Suscripcion> suscRe = new ArrayList<>();
            List<Sorteo> sorRe = new ArrayList<>();
            suscRe.addAll(paginador.getListaCompleta());
//       sorRe.addAll(sorteoFacade.findAll());
            for (Sorteo sorteo : sorteoFacade.findAll()) {
                for (Suscripcion suscripcion : sorteo.getSuscripcionList()) {
                    suscRe.remove(suscripcion);
                    paginador.limpiarLista();
                    paginador.agregarItemsLista(suscRe);
                }
            }
        }else{
            System.out.println("idFiltroSorteo is: "+idFiltroSorteo);
            List<Suscripcion> suscRe = new ArrayList<>();
            List<Sorteo> sorRe = new ArrayList<>();
            suscRe.addAll(paginador.getListaCompleta());
            Sorteo sorteo = sorteoFacade.find(idFiltroSorteo);
//       sorRe.addAll(sorteoFacade.findAll());
            if (sorteo!=null) {
                for (Suscripcion suscripcion : sorteo.getSuscripcionList()) {
                    suscRe.remove(suscripcion);
                    paginador.limpiarLista();
                    paginador.agregarItemsLista(suscRe);
                }
            }
        }
        
//        this.listaFiltrada();
        this.lstItemsResumen.clear();
    }

    public List<TipoSuscripcion> getLstTipoSuscripcion() {
        return this.tipoSuscripcionFacade.findAll();
    }

    public List<Servicio> getLstServiciosSuscribibles() {
        return this.servicioFacade.getServiciosSuscribibles();
    }

    public List<Funcionario> getLstCobradores() {
        return this.funcionarioFacade.getCobradores();
    }
    
    public void listaFiltrada(){
        lstSuscSeleccionadas.clear();
        List<Suscripcion> listTmp = new ArrayList<>();
        List<Suscripcion> listIncluidas = new LinkedList<>();
        List<Sorteo> lstSor = sorteoFacade.findAll();
//        Sorteo sorteo = sorteoFacade.find(1);
        for (Sorteo sorteo : lstSor) {
            for (Suscripcion suscripcion : sorteo.getSuscripcionList()) {
                lstSuscripciones.remove(suscripcion);
                this.paginador.limpiarLista();
                this.paginador.agregarItemsLista(lstSuscripciones);
            }
        }
    }

    public void seleccionarCliFormulario() {
        System.out.println("Seleccion cliente");
        this.reiniciarFormulario();
        this.suscVar.setCliente(this.clienteFacade.find(this.idCliente));
        this.suscVar.setDireccion(this.suscVar.getCliente().getDireccion());
    }

    public void reiniciarFormulario() {
        this.idServicio = EntidadesEstaticas.ID_SERVICIO_TVCABLE;
        this.suscVar.setServicio(this.servicioFacade.find(EntidadesEstaticas.ID_SERVICIO_TVCABLE));
        this.suscVar.setPrecio(this.suscVar.getServicio().getPrecio());
        this.suscVar.setDiaVencimientoMes((short) 1);
        this.suscVar.setCantidadTvs(1);
        this.idTipoVivienda = EntidadesEstaticas.TIPO_VIVIENDA_PROPIA.getIdtipoVivienda();
        this.suscVar.setTipoVivienda(EntidadesEstaticas.TIPO_VIVIENDA_PROPIA);
        this.suscVar.setCobrador(this.funcionarioFacade.find(EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR));
        this.idCobrador = EntidadesEstaticas.ID_COBRADOR_SIN_COBRADOR;
        this.suscVar.setBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO);
        this.setIdBarrio(EntidadesEstaticas.BARRIO_SIN_BARRIO.getIdbarrio());
        this.suscVar.setTipoSuscripcion(EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL);
        this.idTipoSuscripcion = EntidadesEstaticas.TIPO_SUSCRIPCION_NORMAL.getIdtipoSuscripcion();
        this.suscVar.setDireccion("");
        this.suscVar.setNroMedidorElectrico("");
    }

    public Integer getCantidadCuotasPendientes(Integer idsuscripcion) {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
            return this.utilReportesSesBean.getCantidadCuotasPendientes(idsuscripcion, fdesde, fhasta);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a fecha", ex);
            return -1;
        }
    }

    public Integer getMontoDeuda(Integer idsuscripcion) {
        try {
            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
            return this.utilReportesSesBean.getMontoDeuda(idsuscripcion, fdesde, fhasta);
        } catch (ParseException ex) {
            LOG.log(Level.SEVERE, "Error al convertir string a fecha", ex);
            return -1;
        }
    }

//    public void procesarResumen() {
//        try {
//            Date fdesde = this.sdfMesAnio.parse(this.strDesdeFiltro);
//            Date fhasta = this.sdfMesAnio.parse(this.strHastaFiltro);
//            this.lstItemsResumen.clear();
//            this.lstItemsResumen.addAll(this.utilReportesSesBean.getResumen(this.paginador.getListaCompleta(), fdesde, fhasta));
//        } catch (ParseException ex) {
//            LOG.log(Level.SEVERE, "Error al procesar resumen", ex);
//        }
//    }

    public List<Funcionario> getLstFuncionarios() {
        return this.funcionarioFacade.getCobradores();
    }

//    public Integer getTotalResumen() {
//        Integer tot = 0;
//        for (ItemResumenSuscripciones it : this.lstItemsResumen) {
//            tot = tot + it.getMonto();
//        }
//        return tot;
//    }

//    public Integer getTotalSuscripcionesResumen() {
//        Integer tot = 0;
//        for (ItemResumenSuscripciones it : this.lstItemsResumen) {
//            tot = tot + it.getCantSuscripciones();
//        }
//        return tot;
//    }

    public void seleccionarSuscripciones() {
        System.out.println("SeleccionarSuscripciones: " + this.seleccionJson);
        Gson gson = new Gson();
        Integer[] idsSuscripciones = gson.fromJson(this.seleccionJson, Integer[].class);
        List<Suscripcion> lstEliminar = new ArrayList<>();
        this.lstSuscSeleccionadas.clear();
        for (Integer id : idsSuscripciones) {
            this.lstSuscSeleccionadas.add(this.suscripcionFacade.find(id));
        }
        if (this.lstSuscSeleccionadas.size() == 1) {
            this.suscVar = this.lstSuscSeleccionadas.get(0);
        } else {
            this.suscVar = new Suscripcion();
        }

    }

    public void verDatoFechaCobro() {
        System.out.println("rangos: " + this.strFechaDesdeCobroRes + " " + this.strFechaHastaCobroRes);
    }
}
