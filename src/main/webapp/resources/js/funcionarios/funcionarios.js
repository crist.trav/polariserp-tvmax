$(document).ready(function () {
    limpiarSeleccion();
    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    limpiarCampos();
    $("#in-busqueda").val("");
});
function seleccionUnica(id) {
    limpiarSeleccion();
    $("#rw-" + id).addClass("active");
    $("#chk-" + id).prop("checked", true);
    $("#in-idfuncionario").val(id);
    $("#cml-cargardatos").click();
    evaluarMostrarBtnEliminar();
}

function limpiarSeleccion() {
    $("[id^='rw-']").removeClass("active");
    $("[id^='chk-']").prop("checked", false);
}

function agregarSeleccion(id) {
    if ($("#rw-" + id).hasClass("active")) {
        $("#rw-" + id).removeClass("active");
        $("#chk-" + id).prop("checked", false);
    } else {
        $("#rw-" + id).addClass("active");
        $("#chk-" + id).prop("checked", true);
    }
    evaluarMostrarBtnEliminar();
}

function seleccionarTodo() {
    if ($("#chk-gral").prop("checked")) {
        $("[id^='rw-']").addClass("active");
        $("[id^='chk-']").prop("checked", true);
    } else {
        $("[id^='rw-']").removeClass("active");
        $("[id^='chk-']").prop("checked", false);
    }
    evaluarMostrarBtnEliminar();
}

function mostrarModalEliminacion() {
    $("#dlg-conf-elim").modal("show");
    var ids = new Array;
    $("#tablafuncionario tbody tr.active [id^='col-idfunc']").each(function (index, value) {
        ids.push(parseInt($(this).html()));
    });
    $("#selbar").val(JSON.stringify(ids));
}

function ajaxCargarFuncionario(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#cmbCargo").dropdown();
            break;
        }
    }
}

function setTipoMsg(tipo) {
    $("#msg").removeClass("positive");
    $("#msg").removeClass("negative");
    $("#msg").removeClass("error");
    $("#msg").removeClass("warning");
    $("#msg").removeClass("info");
    $("#msg").addClass(tipo);
}

function mostrarMsg(tipo, cabecera, cuerpo) {
    setTipoMsg(tipo);
    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);
    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);
    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function validar() {
    var validado = true;
    if ($("#in-nombres").val() === "") {
        validado = false;
        $("#fieldNombres").addClass("error");
    } else {
        $("#fieldNombres").removeClass("error");
    }
//    if ($("#in-apellidos").val() === "") {
//        validado = false;
//        $("#fieldApellidos").addClass("error");
//    } else {
//        $("#fieldApellidos").removeClass("error");
//    }
    if ($("#in-idcargo").val() === "") {
        validado = false;
        $("#fieldCargo").addClass("error");
    } else {
        $("#fieldCargo").removeClass("error");
    }
//    if($("#in-idfuncionario").val()===""){
//        if($("#in-pass").val()===""){
//            validado=false;
//            $("#fieldPassword").addClass("error");
//            $("#fieldPassword2").addClass("error");
//        }else{
//            $("#fieldPassword").removeClass("error");
//            $("#fieldPassword2").removeClass("error");
//        }
//    }
    if (!validado) {
        mostrarMsg("negative", "Error de validación", "Complete los campos obligatorios");
    } else {
        if ($("#in-pass").val() !== $("#in-pass2").val()) {
            validado = false;
            $("#fieldPassword").addClass("error");
            $("#fieldPassword2").addClass("error");
            mostrarMsg("negative", "Error de validación", "Las contraseñas no coinciden");
        } else {
            $("#fieldPassword").removeClass("error");
            $("#fieldPassword2").removeClass("error");
        }
    }
    return validado;
}

function ajaxGuardarFuncionario(data) {
    var status = data.status;
    switch (status) {
        case "begin":
        {
            $("#btnGuardar").addClass("disabled");
            $("#btnGuardar").addClass("loading");
            break;
        }
        case "success":
        {
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $("#btnGuardar").removeClass("disabled");
            $("#btnGuardar").removeClass("loading");
            break;
        }
    }
}

function limpiarCampos() {
    $("#in-idfuncionario").val("");
    $("#in-nombres").val("");
    $("#in-apellidos").val("");
    $("#cmbCargo").dropdown("clear");
    $("#in-telefono1").val("");
    $("#in-telefono2").val("");
    $("#in-ci").val("");
    $("#in-pass").val("");
    $("#in-pass2").val("");
    $("#in-estado").prop("checked", false);
    if (!$("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
    $("#in-nombres").focus();
}

function ajaxEliminarFuncionarios(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#dlg-conf-elim").modal("hide");
            setTipoMsg($("#tipoMsg").val());
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            break;
        }
    }
}

function evaluarMostrarBtnEliminar(){
    if($("#tablafuncionario tbody tr.active [id^='col-idfunc']").length!==0){
        $("#btnEliminar").show();
    }else{
        $("#btnEliminar").hide();
    }
}

var timeOut = 0;
$('#in-busqueda').on('input', function () {
    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        $('#cml-buscar').click();
    }, 500);
});