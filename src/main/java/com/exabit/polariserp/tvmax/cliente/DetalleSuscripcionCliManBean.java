package com.exabit.polariserp.tvmax.cliente;

import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.login.SessionManBean;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ClienteFacade;
import com.exabit.polariserp.tvmax.sesbeans.CuotaFacade;
import com.exabit.polariserp.tvmax.sesbeans.EstadoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.FuncionarioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ServicioFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoSuscripcionFacade;
import com.exabit.polariserp.tvmax.sesbeans.TipoViviendaFacade;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import com.exabit.polariserp.tvmax.util.UtilConversion;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

/**
 *
 * @author traver
 */
@Named(value = "detalleSuscripcionCliManBean")
@ViewScoped
public class DetalleSuscripcionCliManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(DetalleSuscripcionCliManBean.class.getName());

    @Inject
    private SessionManBean sessionBean;

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private CuotaFacade cuotaFacade;
    @EJB
    private ClienteFacade clienteFacade;
    @EJB
    private SuscripcionFacade suscripcionFacade;
    @EJB
    private TipoViviendaFacade tipoViviendaFacade;
    @EJB
    private TipoSuscripcionFacade tipoSuscripcionFacade;
    @EJB
    private FuncionarioFacade funcionarioFacade;
    @EJB
    private BarrioFacade barrioFacade;
    @EJB
    private ServicioFacade servicioFacade;
    @EJB
    private EstadoSuscripcionFacade estadoSuscripcionFacade;
    //</editor-fold>

    private SimpleDateFormat sdfMes = new SimpleDateFormat("MMMM yyyy");
    private SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    private Integer idSuscripcion;
    private Integer montoMensual;
    private Integer idServicio;
    private Integer idTipoVivienda;
    private Integer idTipoSuscripcion;
    private String direccion;
    private String nroMedidor;
    private Integer idBarrio;
    private Integer idCobrador;
    private String tipoMsgSusc;
    private String detalleMsgSusc;
    private String cabeceraMsgSusc;
    private Integer idCliente;
    private String strFechaSuscripcion;
    //Eliminar suscripciones
    private Integer idSuscripcionEliminar;
    private Integer idCuotaEliminar;
    //Generar cuotas otros servicios
    private Integer idOtrosServiciosGenerar;
    private String strFechaOtrosServicios;
    private Integer montoOtrosServicios;
    private Integer cantCuotasOtrosServicios;
    //Generar cuotas
    private Integer montoCuotaGenerar;
    private String strFechaDesdeGenerarCuota;
    private String strFechaHastaGenerarCuota;
    //Cambio de estado
    private Integer idSuscripcionCambioEstado;
    private Integer idEstadoCambio;
    private String strFechaCambioEstado;
    private String observacionCambioEstado;
    //Edicion de cuota
    private Integer idCuotaEditar;
    private Integer montoCuotaEditar;
    private String strFechaCuotaEditar;
    private String observacionCuotaEditar;
    //Cmabio de titular
    private Integer idSuscripcionCambioTitular;
    private Integer idClienteCambioTitular;

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getStrFechaSuscripcion() {
        return strFechaSuscripcion;
    }

    public void setStrFechaSuscripcion(String strFechaSuscripcion) {
        this.strFechaSuscripcion = strFechaSuscripcion;
    }

    public Integer getIdClienteCambioTitular() {
        return idClienteCambioTitular;
    }

    public void setIdClienteCambioTitular(Integer idClienteCambioTitular) {
        this.idClienteCambioTitular = idClienteCambioTitular;
    }

    public Integer getIdSuscripcionCambioTitular() {
        return idSuscripcionCambioTitular;
    }

    public void setIdSuscripcionCambioTitular(Integer idSuscripcionCambioTitular) {
        this.idSuscripcionCambioTitular = idSuscripcionCambioTitular;
    }

    public String getObservacionCuotaEditar() {
        return observacionCuotaEditar;
    }

    public void setObservacionCuotaEditar(String observacionCuotaEditar) {
        this.observacionCuotaEditar = observacionCuotaEditar;
    }

    public String getStrFechaCuotaEditar() {
        return strFechaCuotaEditar;
    }

    public void setStrFechaCuotaEditar(String strFechaCuotaEditar) {
        this.strFechaCuotaEditar = strFechaCuotaEditar;
    }

    public Integer getMontoCuotaEditar() {
        return montoCuotaEditar;
    }

    public void setMontoCuotaEditar(Integer montoCuotaEditar) {
        this.montoCuotaEditar = montoCuotaEditar;
    }

    public Integer getIdCuotaEditar() {
        return idCuotaEditar;
    }

    public void setIdCuotaEditar(Integer idCuotaEditar) {
        this.idCuotaEditar = idCuotaEditar;
    }

    public Integer getIdSuscripcionCambioEstado() {
        return idSuscripcionCambioEstado;
    }

    public void setIdSuscripcionCambioEstado(Integer idSuscripcionCambioEstado) {
        this.idSuscripcionCambioEstado = idSuscripcionCambioEstado;
    }

    public String getObservacionCambioEstado() {
        return observacionCambioEstado;
    }

    public void setObservacionCambioEstado(String observacionCambioEstado) {
        this.observacionCambioEstado = observacionCambioEstado;
    }

    public String getStrFechaCambioEstado() {
        return strFechaCambioEstado;
    }

    public void setStrFechaCambioEstado(String strFechaCambioEstado) {
        this.strFechaCambioEstado = strFechaCambioEstado;
    }

    public Integer getIdEstadoCambio() {
        return idEstadoCambio;
    }

    public void setIdEstadoCambio(Integer idEstadoCambio) {
        this.idEstadoCambio = idEstadoCambio;
    }

    public String getStrFechaHastaGenerarCuota() {
        return strFechaHastaGenerarCuota;
    }

    public void setStrFechaHastaGenerarCuota(String strFechaHastaGenerarCuota) {
        this.strFechaHastaGenerarCuota = strFechaHastaGenerarCuota;
    }

    public Integer getMontoCuotaGenerar() {
        return montoCuotaGenerar;
    }

    public void setMontoCuotaGenerar(Integer montoCuotaGenerar) {
        this.montoCuotaGenerar = montoCuotaGenerar;
    }

    public String getStrFechaDesdeGenerarCuota() {
        return strFechaDesdeGenerarCuota;
    }

    public void setStrFechaDesdeGenerarCuota(String strFechaDesdeGenerarCuota) {
        this.strFechaDesdeGenerarCuota = strFechaDesdeGenerarCuota;
    }

    public Integer getIdCuotaEliminar() {
        return idCuotaEliminar;
    }

    public void setIdCuotaEliminar(Integer idCuotaEliminar) {
        this.idCuotaEliminar = idCuotaEliminar;
    }

    public Integer getCantCuotasOtrosServicios() {
        return cantCuotasOtrosServicios;
    }

    public void setCantCuotasOtrosServicios(Integer cantCuotasOtrosServicios) {
        this.cantCuotasOtrosServicios = cantCuotasOtrosServicios;
    }

    public Integer getMontoOtrosServicios() {
        return montoOtrosServicios;
    }

    public void setMontoOtrosServicios(Integer montoOtrosServicios) {
        this.montoOtrosServicios = montoOtrosServicios;
    }

    public String getStrFechaOtrosServicios() {
        return strFechaOtrosServicios;
    }

    public void setStrFechaOtrosServicios(String strFechaOtrosServicios) {
        this.strFechaOtrosServicios = strFechaOtrosServicios;
    }

    public Integer getIdOtrosServiciosGenerar() {
        return idOtrosServiciosGenerar;
    }

    public void setIdOtrosServiciosGenerar(Integer idOtrosServiciosGenerar) {
        this.idOtrosServiciosGenerar = idOtrosServiciosGenerar;
    }

    public Integer getIdSuscripcionEliminar() {
        return idSuscripcionEliminar;
    }

    public void setIdSuscripcionEliminar(Integer idSuscripcionEliminar) {
        this.idSuscripcionEliminar = idSuscripcionEliminar;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getTipoMsgSusc() {
        return tipoMsgSusc;
    }

    public void setTipoMsgSusc(String tipoMsgSusc) {
        this.tipoMsgSusc = tipoMsgSusc;
    }

    public String getCabeceraMsgSusc() {
        return cabeceraMsgSusc;
    }

    public void setCabeceraMsgSusc(String cabeceraMsgSusc) {
        this.cabeceraMsgSusc = cabeceraMsgSusc;
    }

    public String getDetalleMsgSusc() {
        return detalleMsgSusc;
    }

    public void setDetalleMsgSusc(String detalleMsgSusc) {
        this.detalleMsgSusc = detalleMsgSusc;
    }

    public Integer getIdCobrador() {
        return idCobrador;
    }

    public void setIdCobrador(Integer idCobrador) {
        this.idCobrador = idCobrador;
    }

    public Integer getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Integer idBarrio) {
        this.idBarrio = idBarrio;
    }

    public String getNroMedidor() {
        return nroMedidor;
    }

    public void setNroMedidor(String nroMedidor) {
        this.nroMedidor = nroMedidor;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdTipoSuscripcion() {
        return idTipoSuscripcion;
    }

    public void setIdTipoSuscripcion(Integer idTipoSuscripcion) {
        this.idTipoSuscripcion = idTipoSuscripcion;
    }

    public Integer getIdTipoVivienda() {
        return idTipoVivienda;
    }

    public void setIdTipoVivienda(Integer idTipoVivienda) {
        this.idTipoVivienda = idTipoVivienda;
    }

    public Integer getIdSuscripcion() {
        return idSuscripcion;
    }

    public void setIdSuscripcion(Integer idSuscripcion) {
        this.idSuscripcion = idSuscripcion;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicioStr) {
        this.idServicio = idServicioStr;
    }

    public Integer getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(Integer montoMensual) {
        this.montoMensual = montoMensual;
    }

    //</editor-fold>
    /**
     * Creates a new instance of DetalleSuscripcionCliManBean
     */
    public DetalleSuscripcionCliManBean() {
        this.strFechaDesdeGenerarCuota = this.sdfMes.format(new Date());
        this.strFechaHastaGenerarCuota = this.sdfMes.format(new Date());
        this.strFechaCambioEstado = this.sdf.format(new Date());
    }

    public void cargarDatosSuscripcion() {
        if (this.idSuscripcion != null) {
            Suscripcion s = this.suscripcionFacade.find(this.idSuscripcion);
            this.idServicio = s.getServicio().getIdservicio();
            this.idTipoSuscripcion = s.getTipoSuscripcion().getIdtipoSuscripcion();
            this.montoMensual = s.getPrecio();
            this.direccion = s.getDireccion();
            this.nroMedidor = s.getNroMedidorElectrico();
            this.idTipoVivienda = s.getTipoVivienda().getIdtipoVivienda();
            this.idBarrio = s.getBarrio().getIdbarrio();
            this.idCobrador = s.getCobrador().getIdfuncionario();
            if(s.getFechaSuscripcion()!=null){
                this.strFechaSuscripcion=this.sdf.format(s.getFechaSuscripcion());
            }
        } else {
            this.limpiarCampos();
        }
    }

    public void guardarSuscripcion() {
        if (this.idCliente != null) {
            Suscripcion s;
            if (this.idSuscripcion != null) {
                s = this.suscripcionFacade.find(this.idSuscripcion);
            } else {
                s = new Suscripcion();
            }
            try {
                s.setBarrio(this.barrioFacade.find(this.idBarrio));
                s.setCantidadTvs(1);
                s.setCliente(this.clienteFacade.find(this.idCliente));
                s.setCobrador(s.getCliente().getCobrador());
                if (s.getCobrador() == null) {
                    s.setCobrador(this.funcionarioFacade.find(1000));
                }
//                s.setCobrador(this.funcionarioFacade.find(this.idCobrador));
                s.setDiaVencimientoMes((short) 1);
                s.setDireccion(this.direccion);
                if (s.getEstado() == null) {
                    s.setEstado(EntidadesEstaticas.ESTADO_CONECTADO);
                }
                if (s.getFechaCambioEstado() == null) {
                    s.setFechaCambioEstado(new Date());
                }
                if (s.getFechaSuscripcion() == null) {
                    s.setFechaSuscripcion(new Date());
                }

                s.setNroMedidorElectrico(this.nroMedidor);
                s.setPrecio(this.montoMensual);
                s.setServicio(this.servicioFacade.find(this.idServicio));
                s.setTipoSuscripcion(this.tipoSuscripcionFacade.find(this.idTipoSuscripcion));
                s.setTipoVivienda(this.tipoViviendaFacade.find(this.idTipoVivienda));
                s.setFechaSuscripcion(this.sdf.parse(this.strFechaSuscripcion));
                if (this.idSuscripcion != null) {
                    this.suscripcionFacade.edit(s);
                } else {
                    this.suscripcionFacade.create(s);
                    this.idSuscripcion = s.getIdsuscripcion();
                }
                this.tipoMsgSusc = "info";
                this.cabeceraMsgSusc = "Exito";
                this.detalleMsgSusc = "Guardado correctamente";
            } catch (Exception ex) {
                LOG.log(Level.SEVERE, "Error al guardar suscripción", ex);
                this.tipoMsgSusc = "error";
                this.cabeceraMsgSusc = "Error";
                this.detalleMsgSusc = "No se pudo guardar la suscripción: " + ex.getMessage();
            }
        } else {
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error";
            this.detalleMsgSusc = "No se pudo identificar al cliente";
        }

    }

    public void limpiarCampos() {
        this.idServicio = null;
        this.idTipoSuscripcion = null;
        this.montoMensual = null;
        this.direccion = "";
        this.idBarrio = null;
        this.idCobrador = null;
        this.idSuscripcion = null;
        this.nroMedidor = "";
        this.idTipoVivienda = null;
        this.strFechaSuscripcion=null;
    }

    public void eliminarSuscripcion() {
        System.out.println("Eliminar: " + this.idSuscripcionEliminar);
        this.suscripcionFacade.remove(this.suscripcionFacade.find(this.idSuscripcionEliminar));
    }

    public List<Cuota> getCuotas() {

        if (this.idSuscripcion != null) {
            return this.cuotaFacade.getCuotaOrdenVencimiento(this.suscripcionFacade.find(this.idSuscripcion));
        } else {
            return new ArrayList<>();
        }
    }

    public List<Cuota> getCuotasAnio(Short anio) {
        System.out.println("Get cuotas, anio: " + anio + ", idsuscripcion: " + this.idSuscripcion);
        if (this.idSuscripcion != null) {
            return this.cuotaFacade.getCuotaOrdenVencimiento(this.suscripcionFacade.find(this.idSuscripcion), anio);
        } else {
            return new ArrayList<>();
        }
    }

    public List<Short> getAniosCuotas() {
        System.out.println("getAnioscuotas, idsuscripcion: " + this.idSuscripcion);
        List<Short> lstAnios = new ArrayList<>();
        if (this.idSuscripcion != null) {
            List<Cuota> lstcuo = this.cuotaFacade.getCuotaOrdenVencimiento(this.suscripcionFacade.find(this.idSuscripcion));
            for (Cuota c : lstcuo) {
                boolean enc = false;
                for (Short anio : lstAnios) {
                    if (anio.equals(c.getAnioCuota())) {
                        enc = true;
                    }
                }
                if (!enc) {
                    lstAnios.add(c.getAnioCuota());
                }
            }
        }
        return lstAnios;
    }

    public List<Servicio> getServiciosNoSuscribibles() {
        return this.servicioFacade.getServiciosNoSuscribibles();
    }

    public void generarCuotasOtrosServicios() {
        try {
            Suscripcion s = this.suscripcionFacade.find(this.idSuscripcion);
            Servicio serv = this.servicioFacade.find(this.idOtrosServiciosGenerar);
            SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");

            Date fpc = sdf.parse(this.strFechaOtrosServicios);

            this.cuotaFacade.generarCuotasOtrosServicios(s, serv, fpc, this.montoOtrosServicios, this.cantCuotasOtrosServicios);
            this.tipoMsgSusc = "info";
            this.cabeceraMsgSusc = "Cuotas otros servicios";
            this.detalleMsgSusc = "Cuotas generadas exitosamente";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al generar cuotas otros servicios", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error al generar cuotas";
            this.detalleMsgSusc = ex.getMessage();
        }
    }

    public List<Cuota> getCuotasOtrosServicios() {
        if (this.idSuscripcion != null) {
            return this.cuotaFacade.getCuotasOtrosServicios(this.suscripcionFacade.find(this.idSuscripcion));
        } else {
            return new ArrayList<>();
        }
    }

    public void eliminarCuota() {
        try {
            System.out.println("Eliminar cuota: " + this.idCuotaEliminar);
            this.tipoMsgSusc = "info";
            this.cabeceraMsgSusc = "Eliminar cuota";
            this.detalleMsgSusc = "Cuota eliminada exitosamente";
            this.cuotaFacade.remove(this.cuotaFacade.find(this.idCuotaEliminar));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al eliminar cuota", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error";
            this.detalleMsgSusc = "Error al eliminar cuota: " + ex.getMessage();
        }
    }

    public void generarCuotas() {
        int cantidadgenerada = 0;
        try {
            Date fd = this.sdfMes.parse(this.strFechaDesdeGenerarCuota);
            Date fh = this.sdfMes.parse(this.strFechaHastaGenerarCuota);
            List<Cuota> lstcuo = this.cuotaFacade.generarCuotas(this.suscripcionFacade.find(this.idSuscripcion), fd, fh, this.montoCuotaGenerar);
            cantidadgenerada = lstcuo.size();
            this.tipoMsgSusc = "info";
            this.cabeceraMsgSusc = "Cuotas";
            this.detalleMsgSusc = "Cuotas generadas exitosamente. Total: " + cantidadgenerada;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al generar cuotas", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error";
            this.detalleMsgSusc = "Error al generar cuotas: " + ex.getMessage();
        }
        System.out.println("Generar cuotas " + this.montoCuotaGenerar + " " + this.strFechaDesdeGenerarCuota + " " + this.strFechaHastaGenerarCuota);
    }

    public int getCantidadMesesPendientes(Integer ids) {
        Calendar desde = new GregorianCalendar();
        desde.add(Calendar.YEAR, -1);
        Calendar hasta = new GregorianCalendar();
        return this.cuotaFacade.getCantMesesConCuotaPend(this.suscripcionFacade.find(ids), desde.getTime(), hasta.getTime());
    }

    public int getTotalCuotasPendientes(Integer ids) {
        return this.cuotaFacade.getCuotasPendientes(this.suscripcionFacade.find(ids)).size();
    }

    public int getCantCuotasPendientesPrincipal(Integer ids) {
        return this.cuotaFacade.getCuotasPendientesPrincipal(this.suscripcionFacade.find(ids)).size();
    }

    public int getCantCuotasPendientesSecundario(Integer ids) {
        return this.cuotaFacade.getCuotasPendientesSecundASCFechaVenc(this.suscripcionFacade.find(ids)).size();
    }

    public int getMontoDeuda(Integer ids) {
        int total = 0;
        for (Cuota c : this.cuotaFacade.getCuotasPendientes(this.suscripcionFacade.find(ids))) {
            total = total + (c.getMontoCuota() - c.getMontoEntrega());
        }
        return total;
    }

    public void cambiarEstadoSuscripcion() {
        try {
            Date fechac = this.sdf.parse(this.strFechaCambioEstado);
            this.suscripcionFacade.cambiarEstado(this.idSuscripcionCambioEstado, this.idEstadoCambio, fechac, this.observacionCambioEstado, this.sessionBean.getFuncionarioVar());
            this.tipoMsgSusc = "info";
            this.cabeceraMsgSusc = "Exito";
            this.detalleMsgSusc = "Estado cambiado";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cambiar estado de suscripción", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error al cambiar estado";
            this.detalleMsgSusc = ex.getMessage();
        }

    }

    public List<EstadoSuscripcion> getLstEstados() {
        return this.estadoSuscripcionFacade.findAll();
    }

    public void cargarDatosCuota(Integer idcuota) {
        Cuota c = this.cuotaFacade.find(idcuota);
        this.idCuotaEditar = idcuota;
        this.montoCuotaEditar = c.getMontoCuota();
        this.strFechaCuotaEditar = this.sdf.format(c.getFechaVencimiento());
        this.observacionCuotaEditar = c.getObservacion();
    }

    public void editarCuota() {
        try {
            Cuota c = this.cuotaFacade.find(this.idCuotaEditar);
            Date fv = this.sdf.parse(this.strFechaCuotaEditar);
            Calendar cv = new GregorianCalendar();
            cv.setTime(fv);
            Calendar cReg = new GregorianCalendar();
            cReg.setTime(c.getFechaVencimiento());
            boolean existeEnMesAnio = false;
            System.out.println(cv.get(Calendar.MONTH) + " " + cReg.get(Calendar.MONTH) + " " + cv.get(Calendar.YEAR) + " " + cReg.get(Calendar.YEAR));
            if (cv.get(Calendar.MONTH) != cReg.get(Calendar.MONTH) || cv.get(Calendar.YEAR) != cReg.get(Calendar.YEAR)) {
                System.out.println("comprobar existencia: " + (cv.get(Calendar.MONTH) + 1) + " " + cv.get(Calendar.YEAR));
                existeEnMesAnio = this.cuotaFacade.existenCuotasPeriodo(c.getSuscripcion(), cv.get(Calendar.MONTH) + 1, cv.get(Calendar.YEAR));
            }
            if (existeEnMesAnio) {
                this.tipoMsgSusc = "Error";
                this.cabeceraMsgSusc = "Error al editar cuota";
                UtilConversion uc = new UtilConversion();
                this.detalleMsgSusc = "La cuota para el periodo " + uc.getNombreMes(cv.get(Calendar.MONTH) + 1) + "/" + cv.get(Calendar.YEAR) + " ya existe.";
            } else {
                c.setFechaVencimiento(fv);
                c.setMesCuota((short) (cv.get(Calendar.MONTH) + 1));
                c.setAnioCuota((short) cv.get(Calendar.YEAR));
                c.setMontoCuota(this.montoCuotaEditar);
                c.setObservacion(this.observacionCuotaEditar);
                this.cuotaFacade.edit(c);
                this.tipoMsgSusc = "info";
                this.cabeceraMsgSusc = "Éxito";
                this.detalleMsgSusc = "Cuota editada";
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al editar cuota", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error al editar cuota";
            this.detalleMsgSusc = ex.getMessage();
        }
    }

    public void cargarPrecioCuotaGenerar() {
        if (this.idSuscripcion != null) {
            Suscripcion s = this.suscripcionFacade.find(this.idSuscripcion);
            this.montoCuotaGenerar = s.getPrecio();
        }
    }

    public void cargarPrecioServicio() {
        if (this.idServicio != null) {
            Servicio se = this.servicioFacade.find(this.idServicio);
            this.montoMensual = se.getPrecio();
        }
    }

    public void cambiarTitular() {
        System.out.println("Cambio titular> idcliente: " + this.idClienteCambioTitular + ", idsuscripcion: " + this.idSuscripcionCambioTitular);
        try {
            Suscripcion s = this.suscripcionFacade.find(this.idSuscripcionCambioTitular);
            s.setCliente(this.clienteFacade.find(this.idClienteCambioTitular));
            this.suscripcionFacade.edit(s);
            this.tipoMsgSusc = "info";
            this.cabeceraMsgSusc = "Éxito";
            this.detalleMsgSusc = "Titular cambiado correctamente";
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al cambiar titular", ex);
            this.tipoMsgSusc = "error";
            this.cabeceraMsgSusc = "Error al cambiar titular";
            this.detalleMsgSusc = ex.getMessage();
        }
    }

}
