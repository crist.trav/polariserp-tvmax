package com.exabit.polariserp.tvmax.barrios;

import com.exabit.polariserp.tvmax.entidades.Barrio;
import com.exabit.polariserp.tvmax.sesbeans.BarrioFacade;
import com.exabit.polariserp.tvmax.sesbeans.ZonaFacade;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "barriosManBean")
@ViewScoped
public class BarriosManBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(BarriosManBean.class.getName());

    //<editor-fold desc="EJBs" defaultstate="collapsed">
    @EJB
    private ZonaFacade zonaFacade;

    @EJB
    private BarrioFacade barrioFacade;
    //</editor-fold>

    //<editor-fold desc="Properties" defaultstate="collapsed">
    private List<Barrio> lstBarrios = new ArrayList<>();
    private String txtBusqueda="";
    private String seleccionBarriosIDJson;
    private String msg = "n";
    private Integer idZonaTmp;
    private Integer idBarrio;
    private String nombreBarrio;
    private String tipoMsg;
    private String cabeceraMsg;
    private String cuerpoMsg;
    private final Paginador<Barrio> paginador;

    public Paginador<Barrio> getPaginador() {
        return paginador;
    }
    
    //</editor-fold>

    //<editor-fold desc="Getters and Setters" defaultstate="collapsed">
    public String getCuerpoMsg() {
        return cuerpoMsg;
    }

    public void setCuerpoMsg(String cuerpoMsg) {
        this.cuerpoMsg = cuerpoMsg;
    }

    public String getCabeceraMsg() {
        return cabeceraMsg;
    }

    public void setCabeceraMsg(String cabeceraMsg) {
        this.cabeceraMsg = cabeceraMsg;
    }

    public String getTipoMsg() {
        return tipoMsg;
    }

    public void setTipoMsg(String tipoMsg) {
        this.tipoMsg = tipoMsg;
    }

    public String getNombreBarrio() {
        return nombreBarrio;
    }

    public void setNombreBarrio(String nombreBarrio) {
        this.nombreBarrio = nombreBarrio;
    }

    public Integer getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Integer idBarrio) {
        this.idBarrio = idBarrio;
    }

    public List<Barrio> getLstBarrios() {
        return lstBarrios;
    }

    public void setLstBarrios(List<Barrio> lstBarrios) {
        this.lstBarrios = lstBarrios;
    }

    public String getTxtBusqueda() {
        return txtBusqueda;
    }

    public void setTxtBusqueda(String txtBusqueda) {
        this.txtBusqueda = txtBusqueda;
    }

    public String getSeleccionBarriosIDJson() {
        return seleccionBarriosIDJson;
    }

    public void setSeleccionBarriosIDJson(String seleccionBarriosIDJson) {
        this.seleccionBarriosIDJson = seleccionBarriosIDJson;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getIdZonaTmp() {
        return idZonaTmp;
    }

    public void setIdZonaTmp(Integer idZonaTmp) {
        this.idZonaTmp = idZonaTmp;
    }
    //</editor-fold>

    /**
     * Creates a new instance of BarriosManBean
     */
    public BarriosManBean() {
        this.paginador=new Paginador(this.lstBarrios);
    }

    @PostConstruct
    private void postConstruct() {
        this.paginador.agregarItemsLista(this.barrioFacade.findAll());
    }

    public void eliminarBarrios() {
        try {
            Gson gson = new Gson();
            Integer[] idsEliminar = gson.fromJson(this.seleccionBarriosIDJson, Integer[].class);
            for (Integer ide : idsEliminar) {
                System.out.println("eliminar: " + ide);
                this.barrioFacade.remove(this.barrioFacade.find(ide));
            }
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Barrios eliminados correctamente";
            
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al eliminar", ex);
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error al eliminar";
            this.cuerpoMsg = ex.getMessage();
        }
        this.buscarBarrio();
    }

    public void buscarBarrio() {
        this.paginador.limpiarLista();
        if (this.txtBusqueda.isEmpty()) {
            this.paginador.agregarItemsLista(this.barrioFacade.findAll());
//            this.lstBarrios.addAll(this.barrioFacade.findAll());
        } else {
            this.paginador.agregarItemsLista(this.barrioFacade.buscarBarrio(this.txtBusqueda));
//            this.lstBarrios.addAll(this.barrioFacade.buscarBarrio(this.txtBusqueda));
        }
        System.out.println("Buscar: " + this.txtBusqueda);
    }

    public List<Barrio> getLstBarriosOrdenados() {
        return this.barrioFacade.getListaBarriosOrdenado();
    }

    public void cargarDatosBarrio() {
        System.out.println("Cargar datos bario");
        if (this.idBarrio != null) {
            Barrio b = this.barrioFacade.find(this.idBarrio);
            this.nombreBarrio = b.getNombre();
            this.idZonaTmp = b.getZona().getIdzona();
        }
    }

    public void guardarBarrio() {
        try {
            Barrio b = new Barrio();
            b.setIdbarrio(this.idBarrio);
            b.setNombre(this.nombreBarrio);
            b.setZona(this.zonaFacade.find(this.idZonaTmp));
            if (b.getIdbarrio() != null) {
                this.barrioFacade.edit(b);
            } else {
                this.barrioFacade.create(b);
            }
            this.tipoMsg = "info";
            this.cabeceraMsg = "Éxito";
            this.cuerpoMsg = "Barrio guardado correctamente.";
            
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error al guardar barrio", ex);
            this.tipoMsg = "error";
            this.cabeceraMsg = "Error al guardar";
        }
        this.buscarBarrio();
    }

    public void limpiar() {
        this.idBarrio = null;
        this.nombreBarrio = null;
        this.idZonaTmp = null;
    }
}
