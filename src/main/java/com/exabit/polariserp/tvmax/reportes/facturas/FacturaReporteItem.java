/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.reportes.facturas;

import com.exabit.polariserp.tvmax.entidades.Factura;
import java.util.Date;

/**
 *
 * @author cristhian-kn
 */
public class FacturaReporteItem {

    public FacturaReporteItem(Factura f) {
        this.idfactura=f.getIdfactura();
        this.anulado=f.getAnulado();
        this.pagado=f.getCancelado();
        this.cliente=f.getCliente().getRazonSocial();
        this.fecha=f.getFecha();
        this.monto=f.getTotal();
        this.nrofactura=f.getTalonarioFactura().getCodEstablecimiento()+"-"+f.getNroFactura();
    }
    
    
    
    private Integer idfactura;

    public Integer getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(Integer idfactura) {
        this.idfactura = idfactura;
    }

    private String cliente;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    
    private String nrofactura;
        
    public String getNrofactura() {
        return nrofactura;
    }

    public void setNrofactura(String nrofactura) {
        this.nrofactura = nrofactura;
    }

    private Integer monto;

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }
    
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    private Boolean anulado;

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    private Boolean pagado;

    public Boolean getPagado() {
        return pagado;
    }

    public void setPagado(Boolean pagado) {
        this.pagado = pagado;
    }

    
}
