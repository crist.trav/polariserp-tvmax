/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.service;

import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import java.text.DecimalFormat;

/**
 *
 * @author cristhian-kn
 */
public class SuscripcionBasic {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    private String nombresCliente;

    public String getNombresCliente() {
        return nombresCliente;
    }

    public void setNombresCliente(String nombresCliente) {
        this.nombresCliente = nombresCliente;
    }

    private String apellidosCliente;

    public String getApellidosCliente() {
        return apellidosCliente;
    }

    public void setApellidosCliente(String apellidosCliente) {
        this.apellidosCliente = apellidosCliente;
    }

        private Integer ciCliente;

    public Integer getCiCliente() {
        return ciCliente;
    }

    public void setCiCliente(Integer ciCliente) {
        this.ciCliente = ciCliente;
    }

        private String servicio;

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

        private Integer cuota;

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    

    public SuscripcionBasic(Suscripcion susc) {
        DecimalFormat df=new DecimalFormat("###,###,###,###");
        this.id=susc.getIdsuscripcion();
        this.nombresCliente=susc.getCliente().getNombres();
        this.apellidosCliente=susc.getCliente().getApellidos();
        this.ciCliente=susc.getCliente().getCi();
        this.servicio=susc.getServicio().getNombre();
        this.cuota=susc.getPrecio();
        this.tituloItem=this.id+"-"+this.nombresCliente+" "+this.apellidosCliente+" ("+df.format(this.ciCliente)+")";
        this.descripcionItem=this.servicio+"-"+df.format(this.cuota)+" gs./mes";
    }
    
    private String tituloItem;

    public String getTituloItem() {
        return tituloItem;
    }

    public void setTituloItem(String tituloItem) {
        this.tituloItem = tituloItem;
    }
    
        private String descripcionItem;

    public String getDescripcionItem() {
        return descripcionItem;
    }

    public void setDescripcionItem(String descripcionItem) {
        this.descripcionItem = descripcionItem;
    }


}
