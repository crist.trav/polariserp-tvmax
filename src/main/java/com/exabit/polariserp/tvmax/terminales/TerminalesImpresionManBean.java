/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.terminales;

import com.exabit.polariserp.tvmax.entidades.TerminalImpresion;
import com.exabit.polariserp.tvmax.sesbeans.TerminalImpresionFacade;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.Cookie;

/**
 *
 * @author cristhian-kn
 */
@Named(value = "terminalesImpresionManBean")
@ViewScoped
public class TerminalesImpresionManBean implements Serializable {

    @EJB
    private TerminalImpresionFacade terminalImpresionFacade;

    private TerminalImpresion terminalImpresionVar = new TerminalImpresion();

    public TerminalImpresion getTerminalImpresionVar() {
        return terminalImpresionVar;
    }

    public void setTerminalImpresionVar(TerminalImpresion terminalImpresionVar) {
        this.terminalImpresionVar = terminalImpresionVar;
    }

    private List<TerminalImpresion> lstTerminales = new LinkedList<>();

    public List<TerminalImpresion> getLstTerminales() {
        return lstTerminales;
    }

    public void setLstTerminales(List<TerminalImpresion> lstTerminales) {
        this.lstTerminales = lstTerminales;
    }

    private Paginador<TerminalImpresion> paginador;

    public Paginador<TerminalImpresion> getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador<TerminalImpresion> paginador) {
        this.paginador = paginador;
    }

    public void filtrar() {
        this.paginador.limpiarLista();
        this.paginador.agregarItemsLista(this.terminalImpresionFacade.findAll());
    }

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String jsonSeleccion;

    public String getJsonSeleccion() {
        return jsonSeleccion;
    }

    public void setJsonSeleccion(String jsonSeleccion) {
        this.jsonSeleccion = jsonSeleccion;
    }

    private List<TerminalImpresion> lstTerminalesSeleccionadas = new LinkedList<>();

    public List<TerminalImpresion> getLstTerminalesSeleccionadas() {
        return lstTerminalesSeleccionadas;
    }

    public void setLstTerminalesSeleccionadas(List<TerminalImpresion> lstTerminalesSeleccionadas) {
        this.lstTerminalesSeleccionadas = lstTerminalesSeleccionadas;
    }
    
        private Integer tmpIdTerminal;

    public Integer getTmpIdTerminal() {
        return tmpIdTerminal;
    }

    public void setTmpIdTerminal(Integer tmpIdTerminal) {
        this.tmpIdTerminal = tmpIdTerminal;
    }
    
        private Long tmpFechaHoraAsignacion;

    public Long getTmpFechaHoraAsignacion() {
        return tmpFechaHoraAsignacion;
    }

    public void setTmpFechaHoraAsignacion(Long tmpFechaHoraAsignacion) {
        this.tmpFechaHoraAsignacion = tmpFechaHoraAsignacion;
    }



    /**
     * Creates a new instance of TerminalesImpresionManBean
     */
    public TerminalesImpresionManBean() {
        paginador = new Paginador<>(this.lstTerminales);
        
    }

    @PostConstruct
    private void postConstruct() {
//        this.verificarTerminal();
//        this.tmpIdTerminal=this.getIdTerminalCookie();
//        this.tmpFechaHoraAsignacion=this.getFechaHoraAsigCookie();
        this.filtrar();
    }

    public void registrarTerminal() {
        this.terminalImpresionFacade.create(terminalImpresionVar);
        this.msg = "Agregada terminal de impresión con código: " + this.terminalImpresionVar.getIdterminalImpresion();
        this.limpiar();
        this.filtrar();
    }

    public void guardarTerminal() {
        this.terminalImpresionFacade.edit(this.terminalImpresionVar);
        this.msg="Cambios guardados para Terminal de Impresión con código "+this.terminalImpresionVar.getIdterminalImpresion();
        this.filtrar();
        System.out.println("Guardar terminal");
    }

    public void limpiar() {
        this.terminalImpresionVar = new TerminalImpresion();
    }

    public void seleccionTerminales() {
        System.out.println(this.jsonSeleccion);
        Gson gson = new Gson();
        Integer[] idsTerminales = gson.fromJson(this.jsonSeleccion, Integer[].class);
        this.lstTerminalesSeleccionadas.clear();
        for (Integer id : idsTerminales) {
            this.lstTerminalesSeleccionadas.add(this.terminalImpresionFacade.find(id));
        }
        if (this.lstTerminalesSeleccionadas.size() == 1) {
            this.terminalImpresionVar = lstTerminalesSeleccionadas.get(0);
        }else{
            this.limpiar();
        }
    }

//    public void asociarTerminal() {
//        Map<String, Object> pars=new HashMap<>();
//        pars.put("maxAge", 2592000);
//        pars.put("path", "/");
//        
//        FacesContext.getCurrentInstance().getExternalContext().addResponseCookie("polariserp-idterminal", this.terminalImpresionVar.getIdterminalImpresion().toString(), pars);
//        Date fechahoraasignacion=new Date();
//        FacesContext.getCurrentInstance().getExternalContext().addResponseCookie("polariserp-asignacion-timestamp", fechahoraasignacion.getTime()+"", pars);
//        this.terminalImpresionVar.setFechaAsignacion(fechahoraasignacion);
//        this.terminalImpresionFacade.edit(terminalImpresionVar);
//        this.tmpIdTerminal=this.terminalImpresionVar.getIdterminalImpresion();
//        this.tmpFechaHoraAsignacion=terminalImpresionVar.getFechaAsignacion().getTime();
//        this.filtrar();
//    }
    
    public void asociarTerminal(){
        System.out.println("id: "+this.tmpIdTerminal+", "+this.tmpFechaHoraAsignacion );
        TerminalImpresion tf=this.terminalImpresionFacade.find(this.tmpIdTerminal);
        if(tf!=null){
            Date d=new Date(this.tmpFechaHoraAsignacion);
            System.out.println(d);
            tf.setFechaAsignacion(d);
            this.terminalImpresionFacade.edit(tf);
        }
        this.filtrar();
    }

    public void verificarTerminal() {
        System.out.println("Verificar cookie");
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        for(String key:requestCookieMap.keySet()){
            Cookie c=(Cookie) requestCookieMap.get(key);
            
            System.out.println("key galleta: "+key+", objeto galleta: "+requestCookieMap.get(key)+", valor galleta: "+c.getValue());
        }
    }
    
    private Integer getIdTerminalCookie(){
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        if(requestCookieMap.get("polariserp-idterminal")==null){
            return null;
        }else{
            Cookie c=(Cookie) requestCookieMap.get("polariserp-idterminal");
            System.out.println("Dominio galleta: "+c.getDomain());
            return Integer.parseInt(c.getValue());
        }
    }
    private Long getFechaHoraAsigCookie(){
        Map<String, Object> requestCookieMap = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestCookieMap();
        if(requestCookieMap.get("polariserp-asignacion-timestamp")==null){
            return null;
        }else{
            Cookie c=(Cookie) requestCookieMap.get("polariserp-asignacion-timestamp");
            return Long.parseLong(c.getValue());
        }
    }
    
    public void eliminarTerminales(){
        Integer cant=this.lstTerminalesSeleccionadas.size();
        for(TerminalImpresion t:this.lstTerminalesSeleccionadas){
            this.terminalImpresionFacade.remove(t);
        }
        this.lstTerminalesSeleccionadas.clear();
        this.limpiar();
        this.filtrar();
        this.msg="Eliminadas correctamente "+cant+" terminales de impresión";
    }

}
