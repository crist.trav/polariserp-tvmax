$(document).ready(function () {
    $('#cal-fechafactura').calendar({
        type: "date",
        monthFirst: false,
        formatter: {
            date: function (date, settings) {
                if (!date)
                    return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        }
    });
    $('#cmbCliente').dropdown({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/buscarClienteDropdown?q={query}'
        },
        showOnFocus: false
    });
    $('#cmbCobrador').dropdown({
        apiSettings: {
            url: '//' + location.host + restRootPath + '/buscarcobradoractivodropdown?q={query}'
        },
        showOnFocus: false
    });
    $('#rdbAgreCli').change(function () {
        $("#cmbCliente").show();
        $("#cmbCobrador").hide();
    });
    $("#rdbAgreCobra").change(function () {
        $("#cmbCobrador").show();
        $("#cmbCliente").hide();
    });

    $("#cmbCliente").keypress(function (e) {
        if (e.which == 13) {
            $("#cmlAgregar").click();
        }
    });
    $("#cmbCobrador").keypress(function (e) {
        if (e.which == 13) {
            $("#cmlAgregar").click();
        }
    });

    $('.message .close').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
});

function agregarPor() {
    if ($('#rdbAgreCli').prop("checked")) {
        $("#in-agregarporcliente").val("true");

    } else {
        $("#in-agregarporcliente").val("false");
    }
}

function verSuscripciones(idcli, nombrecli) {
    $("#brcIconSusc").hide();
    $("#brcSuscripcion").hide();

    $("#brcIconCliente").show();
    $("#brcCliente").empty();
    $("#brcCliente").append(nombrecli);
    $("#brcCliente").show();
    $("#inIdClienteVer").val(idcli);
    $("#cmlVerCliente").click();
}

function retrocederVerSuscripciones() {
    $("#brcCliente").show();
    $("#brcIconSusc").hide();
    $("#brcSuscripcion").hide();
    $("#cmlVerCliente").click();
}

function verCuotas(idsusc, nombreserv) {
    $("#brcIconSusc").show();
    $("#brcSuscripcion").empty();
    $("#brcSuscripcion").append(idsusc + "-" + nombreserv);
    $("#brcSuscripcion").show();
    $("#idSuscripcionVer").val(idsusc);
    $("#cmlVerSuscripcion").click();
}

function ajaxVerCliente(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#tablaClientes").hide();
            $("#tablaCuotas").hide();
            $("#tablaSuscripciones").show();
            break;
        }
    }
}

function ajaxVerSuscripcion(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#tablaClientes").hide();
            $("#tablaSuscripciones").hide();
            $("#tablaCuotas").show();
            break;
        }
    }
}

function ajaxVerTodosClientes(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#brcIconSusc").hide();
            $("#brcSuscripcion").hide();

            $("#brcIconCliente").hide();
            $("#brcCliente").hide();

            $("#tablaClientes").show();
            $("#tablaSuscripciones").hide();
            $("#tablaCuotas").hide();
            break;
        }
    }
}

function ajaxAgregar(data) {
    var status = data.status;
    switch (status) {
        case "begin":
        {
            $("#cmlAgregar").addClass("loading");
            break;
        }
        case "success":
        {
            $("#brcIconSusc").hide();
            $("#brcSuscripcion").hide();

            $("#brcIconCliente").hide();
            $("#brcCliente").hide();

            $("#tablaClientes").show();
            $("#tablaSuscripciones").hide();
            $("#tablaCuotas").hide();
        }
        default:
        {
            $("#cmlAgregar").removeClass("loading");
            break;
        }
    }
}

function ajaxIncluirExcluir(data) {
    var status = data.status;
    switch (status) {
        case "success":
        {
            $("#tablaCuotas").show();
            break;
        }
    }
}

function mostrarMsg(tipo, cabecera, cuerpo) {
    $("#msg").removeClass("info");
    $("#msg").removeClass("error");
    $("#msg").removeClass("warning");
    $("#msg").addClass(tipo);

    $("#cabeceraMsg").empty();
    $("#cabeceraMsg").append(cabecera);

    $("#cuerpoMsg").empty();
    $("#cuerpoMsg").append(cuerpo);
    if ($("#msg").hasClass("hidden")) {
        $("#msg").transition("fade");
    }
}

function validarGeneracion() {
    var validado = true;
    if ($("#in-idtalonario").val() === "") {
        $("#fieldTalonario").addClass("error");
        validado = false;
    } else {
        $("#fieldTalonario").removeClass("error");
    }
    if ($("#in-fechafacturas").val() === "") {
        $("#fieldFechaFacturas").addClass("error");
        validado = false;
    } else {
        $("#fieldFechaFacturas").removeClass("error");
    }
    if ($("#colVaciaCliente").length !== 0) {
        validado = false;
        mostrarMsg("error", "Error de validación", "No se agregaron clientes");
    } else {
        if (!validado) {
            mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
        }
    }
    return validado;
}

function ajaxGenerarFacturas(data) {
    var status = data.status;
    switch (status) {
        case "begin":
        {
            $("#btn-generar").addClass("disabled");
            $("#btn-generar").addClass("loading");
            $("#cmlAgregar").addClass("disabled");
            break;
        }
        case "success":
        {
            if ($("#msg").hasClass("hidden")) {
                $("#msg").transition("fade");
            }
            $('.message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
            $("#btn-generar").removeClass("loading");

            $("#brcIconSusc").hide();
            $("#brcSuscripcion").hide();

            $("#brcIconCliente").hide();
            $("#brcCliente").hide();

            $("#tablaClientes").show();
            $("#tablaSuscripciones").hide();
            $("#tablaCuotas").hide();
            break;
        }
    }
}

function validarAgregar() {
    agregarPor();
    var validado = true;
    if ($("#in-cant-cuotas").val() === "") {
        validado = false;
        $("#fieldCantidadCuotas").addClass("error");
    } else {
        $("#fieldCantidadCuotas").removeClass("error");
    }
    if($("#in-max-items-factura").val()===""){
        validado=false;
        $("#fieldItemsPorFact").addClass("error");
    }else{
        $("#fieldItemsPorFact").removeClass("error");
    }
    if ($('#rdbAgreCli').prop("checked") && $("#in-idclienteagregar").val() === "") {
        validado = false;
        $("#fieldClienteCobrador").addClass("error");
    } else if ($("#rdbAgreCobra").prop("checked") && $("#in-idcobradoragregar").val() === "") {
        validado = false;
        $("#fieldClienteCobrador").addClass("error");
    } else {
        $("#fieldClienteCobrador").removeClass("error");
    }
    if (!validado) {
        mostrarMsg("error", "Error de validación", "Complete los campos obligatorios");
    } else {
        if (!$("#msg").hasClass("hidden")) {
            $("#msg").transition("fade");
        }
    }
    return validado;
}