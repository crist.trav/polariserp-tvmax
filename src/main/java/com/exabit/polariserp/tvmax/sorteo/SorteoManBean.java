/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sorteo;

import com.exabit.polariserp.tvmax.entidades.Sorteo;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.sesbeans.SorteoFacade;
import com.exabit.polariserp.tvmax.sesbeans.SuscripcionFacade;
import com.exabit.polariserp.tvmax.util.Paginador;
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author frioss
 */
@Named(value="sorteoManBeanPremio")
@ViewScoped
public class SorteoManBean implements Serializable{

    @EJB
    private SuscripcionFacade suscripcionFacade;

    @EJB
    private SorteoFacade sorteoFacade;
    private Paginador<Suscripcion> paginador;
    private List<Sorteo> listExcluidos = new LinkedList<>();
    private List<Sorteo> listIncluir = new LinkedList<>();
    private List<Suscripcion> listExcluidoS = new LinkedList<>();
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;
    private Sorteo sorteoVar = new Sorteo();
    private String seleccionarSorteoJSON;
    private String seleccionarRemoverSorteoJSON;
    private Integer idEliminar;
    private Integer idSorteoTmp;
    private Integer codSus;
    private String msg;

    public Paginador<Suscripcion> getPaginador() {
        return paginador;
    }

    public void setPaginador(Paginador<Suscripcion> paginador) {
        this.paginador = paginador;
    }

    public List<Suscripcion> getListExcluidoS() {
        listExcluidoS.clear();
        listExcluidos.clear();
        listExcluidos.addAll(sorteoFacade.findAll());
        
//        for (Sorteo sorteo : listExcluidos) {
            if (idSorteoTmp==null) {
                System.out.println("ListaExcluidos null");
            }else{
                Sorteo s = sorteoFacade.find(idSorteoTmp);
                listExcluidoS.addAll(s.getSuscripcionList());
            }
            
//        }
        return listExcluidoS;
    }

    public List<Sorteo> getListIncluir() {
        return listIncluir;
    }

    public void setListIncluir(List<Sorteo> listIncluir) {
        this.listIncluir = listIncluir;
    }

    public void setListExcluidoS(List<Suscripcion> listExcluidoS) {
        this.listExcluidoS = listExcluidoS;
    }

    public List<Sorteo> getListExcluidos() {
        return listExcluidos;
    }

    public void setListExcluidos(List<Sorteo> listExcluidos) {
        this.listExcluidos = listExcluidos;
    }

    public String getSeleccionarRemoverSorteoJSON() {
        return seleccionarRemoverSorteoJSON;
    }

    public void setSeleccionarRemoverSorteoJSON(String seleccionarRemoverSorteoJSON) {
        this.seleccionarRemoverSorteoJSON = seleccionarRemoverSorteoJSON;
    }

    public Integer getCodSus() {
        return codSus;
    }

    public void setCodSus(Integer codSus) {
        this.codSus = codSus;
    }

    public Integer getIdSorteoTmp() {
        return idSorteoTmp;
    }

    public void setIdSorteoTmp(Integer idSorteoTmp) {
        this.idSorteoTmp = idSorteoTmp;
    }

    public Integer getIdEliminar() {
        return idEliminar;
    }

    public void setIdEliminar(Integer idEliminar) {
        this.idEliminar = idEliminar;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    

    public Sorteo getSorteoVar() {
        return sorteoVar;
    }

    public String getSeleccionarSorteoJSON() {
        return seleccionarSorteoJSON;
    }

    public void setSeleccionarSorteoJSON(String seleccionarSorteoJSON) {
        this.seleccionarSorteoJSON = seleccionarSorteoJSON;
    }

    public void setSorteoVar(Sorteo sorteoVar) {
        this.sorteoVar = sorteoVar;
    }

    public SorteoManBean() {
//        this.listExcluidoS = new ArrayList<>();
//        this.paginador = new Paginador(this.listExcluidoS);
    }
    
    @PostConstruct
    private void postConstruct() {
//        try {
//            this.paginador.limpiarLista();
//            this.paginador.agregarItemsLista(getListExcluidoS());
//        } catch (Exception e) {
//            System.out.println("error al obtener lista excluidos, "+e);
//        }
    }
    
    public List<Sorteo>getListSorteo(){
        return this.sorteoFacade.findAll();
    }
    
    
    public void registrarSorteo(){
        sorteoVar.setFecha(new Date());
        this.sorteoFacade.create(sorteoVar);
    }
    public void modificarSorteo(){
        sorteoVar.setFecha(new Date());
        this.sorteoFacade.edit(sorteoVar);
    }
    
    public void eliminarSorteo() {
        System.out.println("idEliminar: "+idEliminar);
//        if (idEliminar!= null) {
            System.out.println("buscando sorteo a eliminar");
            Sorteo s = sorteoFacade.find(this.idEliminar);
//            TypedQuery<Premio> query = this.em.createQuery("SELECT p FROM Premio p WHERE p.sorteoIdsorteo=:s", Premio.class);
//            query.setParameter("s", s);
//            System.out.println("getResulList: "+query.getResultList().iterator().next().getDescripcion());
//            if (query.getResultList().isEmpty()) {
//                System.out.println("i'm here");
//                if (query.getResultList()==null) {
//                    System.out.println("Eliminando Sorteo");
                    sorteoFacade.remove(s);
//                }else{
//                    msg = "No se pudo eliminar el SORTEO, elimine los premios aignados primero!!";
//                    System.out.println("No se pudo eliminar el SORTEO, elimine los premios aignados primero!!");
//                }
//                
//        }else{
//                msg = "No se pudo eliminar el SORTEO, elimine los premios aignados primero!!";
//                System.out.println("No se pudo eliminar el SORTEO, elimine los premios aignados primero!!");
//            }
//                
    }
    
    public void registrarSuscripcionesExcluidas() {
        System.out.println("seleccion Json: "+seleccionarSorteoJSON);
        System.out.println("codSus: "+idSorteoTmp);
        if (idSorteoTmp == null) {
            System.out.println("valor null");
        } else {
            System.out.println("idSorteoTmp: "+idSorteoTmp);
            Sorteo sorteoex = sorteoFacade.find(idSorteoTmp);
            Suscripcion suscripcionex;
            Gson gson = new Gson();
            Integer[] idRegistro = gson.fromJson(this.seleccionarSorteoJSON, Integer[].class);
            for (Integer idSus : idRegistro) {
                suscripcionex = suscripcionFacade.find(idSus);
                System.out.println("suscripciones seleccionadas: "+suscripcionex.getIdsuscripcion());
                System.out.println("sorteo :"+sorteoex.getDescripcion());
                sorteoex.getSuscripcionList().add(suscripcionex);
                sorteoFacade.edit(sorteoex);
            }
        }
    }
    
    public void incluirSuscripcionSoreteo(Integer i){
        Suscripcion s = suscripcionFacade.find(i);
        System.out.println("integer: "+i);
        System.out.println("seleccionarRemoverSorteoJSON: "+i);
        if (s==null) {
            System.out.println("Valor idsuscripcion== NULL");
        }else{
            for (Sorteo incluir : s.getSorteoList()) {
                incluir.getSuscripcionList().remove(s);
                sorteoFacade.edit(incluir);
            }
        }
    }
}
