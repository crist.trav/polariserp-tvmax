/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "tipo_vivienda")
@NamedQueries({
    @NamedQuery(name = "TipoVivienda.findAll", query = "SELECT t FROM TipoVivienda t")
    , @NamedQuery(name = "TipoVivienda.findByIdtipoVivienda", query = "SELECT t FROM TipoVivienda t WHERE t.idtipoVivienda = :idtipoVivienda")
    , @NamedQuery(name = "TipoVivienda.findByNombre", query = "SELECT t FROM TipoVivienda t WHERE t.nombre = :nombre")})
public class TipoVivienda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idtipovivienda", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idtipovivienda", table = "idsequences", allocationSize = 1, initialValue = 4)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipo_vivienda")
    private Integer idtipoVivienda;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(mappedBy = "tipoVivienda")
    private List<Suscripcion> suscripcionList;

    public TipoVivienda() {
    }

    public TipoVivienda(Integer idtipoVivienda) {
        this.idtipoVivienda = idtipoVivienda;
    }

    public Integer getIdtipoVivienda() {
        return idtipoVivienda;
    }

    public void setIdtipoVivienda(Integer idtipoVivienda) {
        this.idtipoVivienda = idtipoVivienda;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoVivienda != null ? idtipoVivienda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoVivienda)) {
            return false;
        }
        TipoVivienda other = (TipoVivienda) object;
        if ((this.idtipoVivienda == null && other.idtipoVivienda != null) || (this.idtipoVivienda != null && !this.idtipoVivienda.equals(other.idtipoVivienda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.TipoVivienda[ idtipoVivienda=" + idtipoVivienda + " ]";
    }
    
}
