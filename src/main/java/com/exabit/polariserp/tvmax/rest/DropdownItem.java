package com.exabit.polariserp.tvmax.rest;

/**
 *
 * @author cristhian-kn
 */
public class DropdownItem {

    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
