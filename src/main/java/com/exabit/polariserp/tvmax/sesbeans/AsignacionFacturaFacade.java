/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.AsignacionFactura;
import com.exabit.polariserp.tvmax.entidades.Factura;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author cristhian-kn
 */
@Stateless
public class AsignacionFacturaFacade extends AbstractFacade<AsignacionFactura> {

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AsignacionFacturaFacade() {
        super(AsignacionFactura.class);
    }
    
    public List<Funcionario> getCobradoresPeriodo(Date desde, Date hasta){
        TypedQuery<Funcionario> q=this.em.createQuery("SELECT af.cobrador FROM AsignacionFactura af WHERE af.fechaHoraRegistro>=:desde AND af.fechaHoraRegistro<=:hasta GROUP BY af.cobrador", Funcionario.class);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        List<Funcionario> lstf=q.getResultList();
        System.out.println("CAntidad de cobradores: "+lstf.size());
        return lstf;
    }
    
    public List<Factura> getFacturas(Date desde, Date hasta, Funcionario cobrador, boolean pagado){
        TypedQuery<Factura> q=this.em.createQuery("SELECT af.factura FROM AsignacionFactura af WHERE af.factura.fecha>=:desde AND af.factura.fecha<=:hasta AND af.cobrador=:cobra AND af.factura.cancelado=:pagado AND af.factura.anulado=false", Factura.class);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        q.setParameter("cobra", cobrador);
        q.setParameter("pagado", pagado);
        return q.getResultList();
    }
    public List<Factura> getFacturas(Date desde, Date hasta, Funcionario cobrador){
        TypedQuery<Factura> q=this.em.createQuery("SELECT af.factura FROM AsignacionFactura af WHERE af.factura.fecha>=:desde AND af.factura.fecha<=:hasta AND af.cobrador=:cobra AND af.factura.anulado=false", Factura.class);
        q.setParameter("desde", desde);
        q.setParameter("hasta", hasta);
        q.setParameter("cobra", cobrador);
        return q.getResultList();
    }
   
    public Funcionario getCobradorAsinado(Factura f){
        TypedQuery<Funcionario> q=this.em.createQuery("SELECT af.cobrador FROM AsignacionFactura af WHERE af.factura=:factura", Funcionario.class);
        q.setParameter("factura", f);
        List<Funcionario> lstf=q.getResultList();
        if(!lstf.isEmpty()){
            return lstf.get(0);
        }else{
            return null;
        }
    }
}
