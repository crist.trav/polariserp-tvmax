/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "campo_suscripcion")
@NamedQueries({
    @NamedQuery(name = "CampoSuscripcion.findAll", query = "SELECT c FROM CampoSuscripcion c")
    , @NamedQuery(name = "CampoSuscripcion.findByIdcampoSuscripcion", query = "SELECT c FROM CampoSuscripcion c WHERE c.idcampoSuscripcion = :idcampoSuscripcion")
    , @NamedQuery(name = "CampoSuscripcion.findByNombre", query = "SELECT c FROM CampoSuscripcion c WHERE c.nombre = :nombre")})
public class CampoSuscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idcamposuscripcion", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idcamposuscripcion", table = "idsequences", allocationSize = 1, initialValue = 14)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcampo_suscripcion")
    private Integer idcampoSuscripcion;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "campoSuscripcion")
    private List<HistorialSuscripcion> historialSuscripcionList;

    public CampoSuscripcion() {
    }

    public CampoSuscripcion(Integer idcampoSuscripcion) {
        this.idcampoSuscripcion = idcampoSuscripcion;
    }

    public Integer getIdcampoSuscripcion() {
        return idcampoSuscripcion;
    }

    public void setIdcampoSuscripcion(Integer idcampoSuscripcion) {
        this.idcampoSuscripcion = idcampoSuscripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<HistorialSuscripcion> getHistorialSuscripcionList() {
        return historialSuscripcionList;
    }

    public void setHistorialSuscripcionList(List<HistorialSuscripcion> historialSuscripcionList) {
        this.historialSuscripcionList = historialSuscripcionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcampoSuscripcion != null ? idcampoSuscripcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampoSuscripcion)) {
            return false;
        }
        CampoSuscripcion other = (CampoSuscripcion) object;
        if ((this.idcampoSuscripcion == null && other.idcampoSuscripcion != null) || (this.idcampoSuscripcion != null && !this.idcampoSuscripcion.equals(other.idcampoSuscripcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.CampoSuscripcion[ idcampoSuscripcion=" + idcampoSuscripcion + " ]";
    }
    
}
