/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.util;

import com.exabit.polariserp.tvmax.entidades.Barrio;
import com.exabit.polariserp.tvmax.entidades.CampoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Cargo;
import com.exabit.polariserp.tvmax.entidades.CategoriaCliente;
import com.exabit.polariserp.tvmax.entidades.CategoriaModulo;
import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Concepto;
import com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.Funcionalidad;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.MedioPago;
import com.exabit.polariserp.tvmax.entidades.Modulo;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.TipoSuscripcion;
import com.exabit.polariserp.tvmax.entidades.TipoVivienda;
import com.exabit.polariserp.tvmax.entidades.Zona;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cristhian-kn
 */
public class EntidadesEstaticas {

    public static final int PAR_RUTA_REPORTES = 1;
    public static final int PAR_YA_IMPORTADO = 2;
    public static final int PAR_SERVICIO_GENERACION_CUOTA_ACTIVO = 3;
    public static final int PAR_SERVICIO_ANULACION_FACTURA_ACTIVO = 4;
//    public static final int PAR_DIA_CUOTA_PARCIAL=4;

    public static final Concepto CONCEPTO_COBRO_CUOTA = new Concepto();
    public static final Concepto CONCEPTO_OTROS_COBROS = new Concepto();

    public static final MedioPago MEDIO_PAGO_EFECTIVO = new MedioPago();

    public static final EstadoSuscripcion ESTADO_CONECTADO = new EstadoSuscripcion();
    public static final EstadoSuscripcion ESTADO_RECONECTADO = new EstadoSuscripcion();
    public static final EstadoSuscripcion ESTADO_DESCONECTADO = new EstadoSuscripcion();
    public static final EstadoSuscripcion ESTADO_NUEVO = new EstadoSuscripcion();

    public static final int ID_SERVICIO_TVCABLE = 1;
    public static final int ID_SERVICIO_CONEXION = 2;
    public static final int ID_SERVICIO_RECONEXION = 3;

    public static final int ID_COBRADOR_SIN_COBRADOR = 1000;

    public static final Cargo CARGO_COBRADOR = new Cargo();

    public static final int ID_TIPO_SUSCRIPCION_NORMAL = 1;
    public static final int ID_TIPO_SUSCRIPCION_GENTILEZA = 2;

//    public static final TipoVivienda TIPO_VIVIENDA_SIN_TIPO=new TipoVivienda();
    public static final TipoVivienda TIPO_VIVIENDA_PROPIA = new TipoVivienda();
    public static final TipoVivienda TIPO_VIVIENDA_ALQUILER = new TipoVivienda();
    public static final TipoVivienda TIPO_VIVIENDA_PRESTAMO = new TipoVivienda();

    public static final Barrio BARRIO_SIN_BARRIO = new Barrio();
    public static final Zona ZONA_SIN_ZONA = new Zona();

    public static final CategoriaCliente CATEGORIA_CLIENTE_SIN_CATEGORIA = new CategoriaCliente();

    public static final TipoSuscripcion TIPO_SUSCRIPCION_NORMAL = new TipoSuscripcion();
    public static final TipoSuscripcion TIPO_SUSCRIPCION_GENTILEZA = new TipoSuscripcion();

//    public static final List<Modulo> MODULOS_ESTATICOS_LISTA=new LinkedList<>();
//    private final List<Funcionalidad> lstFuncionalidades=new LinkedList<>();
    public static final Modulo MODULO_CLIENTES = new Modulo();
    public static final Modulo MODULO_SERVICIOS = new Modulo();
    public static final Modulo MODULO_CATEGORIA_CLIENTES = new Modulo();
    public static final Modulo MODULO_PERMISOS = new Modulo();
    public static final Modulo MODULO_FUNCIONALIDAD = new Modulo();
    public static final Modulo MODULO_MODULOS = new Modulo();
    public static final Modulo MODULO_SUSCRIPCIONES = new Modulo();
    public static final Modulo MODULO_COBROS = new Modulo();
    public static final Modulo MODULO_FACTURAS_COBRADOR = new Modulo();
    public static final Modulo MODULO_FACTURAS = new Modulo();
    public static final Modulo MODULO_TALONARIOS = new Modulo();
    public static final Modulo MODULO_BARRIOS = new Modulo();
    public static final Modulo MODULO_ZONAS = new Modulo();
    public static final Modulo MODULO_ESTADOS_SUSCRIPCIONES = new Modulo();
    public static final Modulo MODULO_FUNCIONARIOS = new Modulo();
    public static final Modulo MODULO_CARGOS = new Modulo();
    public static final Modulo MODULO_AJUSTES = new Modulo();
    public static final Modulo MODULO_IMPORTAR = new Modulo();
    public static final Modulo MODULO_TERMINALES_IMPRESION = new Modulo();
    public static final Modulo MODULO_PREMIO = new Modulo();
    public static final Modulo MODULO_EXCLUIDOS = new Modulo();
    public static final Modulo MODULO_SORTEO = new Modulo();

    public static final Funcionalidad REGISTRAR_CLIENTES = new Funcionalidad();
    public static final Funcionalidad MODIFICAR_CLIENTES = new Funcionalidad();
    public static final Funcionalidad ELIMINAR_CLIENTES = new Funcionalidad();

    public static final Funcionalidad REGISTRAR_CATEGORIA_CLIENTES = new Funcionalidad();
    public static final Funcionalidad EDITAR_CATEGORIA_CLIENTES = new Funcionalidad();
    public static final Funcionalidad ELIMINAR_CATEGORIA_CLIENTES = new Funcionalidad();

    public static final Funcionalidad REGISTRAR_FUNCIONALIDADES = new Funcionalidad();
    public static final Funcionalidad EDITAR_FUNCIONALIDADES = new Funcionalidad();
    public static final Funcionalidad ELIMINAR_FUNCIONALIDADES = new Funcionalidad();

    public static final Funcionalidad REGISTRAR_SERVICIOS = new Funcionalidad();
    public static final Funcionalidad EDITAR_SERVICIOS = new Funcionalidad();
    public static final Funcionalidad ELIMINAR_SERVICIOS = new Funcionalidad();

    public static final Funcionalidad REGISTRAR_MODULOS = new Funcionalidad();
    public static final Funcionalidad EDITAR_MODULOS = new Funcionalidad();
    public static final Funcionalidad ELIMINAR_MODULOS = new Funcionalidad();

    public static final Funcionalidad ASIGNAR_PERMISOS = new Funcionalidad();
    public static final Funcionalidad REMOVER_PERMISOS = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_COBROS = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_FACTURAS = new Funcionalidad();
    public static final Funcionalidad F_GENERAR_FACTURAS = new Funcionalidad();
    public static final Funcionalidad F_IMPRIMIR_FACTURAS = new Funcionalidad();
    public static final Funcionalidad F_VER_FACTURAS_PENDIENTES = new Funcionalidad();
    public static final Funcionalidad F_VER_FACTURA_PAGADAS = new Funcionalidad();
    public static final Funcionalidad VER_FACTURAS_ANULADAS = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_TALONARIOS = new Funcionalidad();
    public static final Funcionalidad F_EDITAR_TALONARIOS = new Funcionalidad();
    public static final Funcionalidad F_ELIMINAR_TALONARIOS = new Funcionalidad();

    public static final Funcionalidad F_VER_FACTURAS_POR_COBRADOR = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_BARRIOS = new Funcionalidad();
    public static final Funcionalidad F_REGISTRAR_ZONAS = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_MODIFICAR_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_ELIMINAR_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_GENERAR_CUOTAS_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_VISTA_PREVIA_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_RESUMEN_SUSCRIPCIONES = new Funcionalidad();

    public static final Funcionalidad F_CAMBIAR_ESTADOS_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_REGISTRAR_FUNCIONARIOS = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_FUNCIONALIDAD = new Funcionalidad();
    public static final Funcionalidad F_REGISTRAR_CARGOS = new Funcionalidad();

    public static final Funcionalidad F_CAMBIAR_RUTA_REPORTES = new Funcionalidad();

    public static final Funcionalidad F_IMPORTAR_SISTEMA_ANTERIOR = new Funcionalidad();
    public static final Funcionalidad F_REGISTRAR_TERMINALES_IMPRESION = new Funcionalidad();

    public static final Funcionalidad F_MODIFICAR_BARRIOS = new Funcionalidad();
    public static final Funcionalidad F_ELIMINAR_BARRIOS = new Funcionalidad();

    public static final Funcionalidad F_MODIFICAR_ZONAS = new Funcionalidad();
    public static final Funcionalidad F_ELIMINAR_ZONAS = new Funcionalidad();

    public static final Funcionalidad F_ELIMINAR_D_SUSCRIPCIONES = new Funcionalidad();
    public static final Funcionalidad F_EDITAR_D_SUSCRIPCIONES = new Funcionalidad();

    public static final Funcionalidad F_REGISTRAR_SUSCRIPCION_CLIENTES = new Funcionalidad();
    public static final Funcionalidad F_VER_CLIENTES_ACTIVOS = new Funcionalidad();

    public static final Funcionalidad F_VISTA_P_FACTURA = new Funcionalidad();
    public static final Funcionalidad F_PAGO_FACTURA = new Funcionalidad();
    public static final Funcionalidad F_RESUMEN_FACTURA = new Funcionalidad();
    public static final Funcionalidad F_VER_FACTURAS_ANULADAS = new Funcionalidad();
    public static final Funcionalidad F_ANULAR_FACTURAS = new Funcionalidad();
    
    public static final Funcionalidad F_REGISTRAR_PREMIO = new Funcionalidad();
    public static final Funcionalidad F_MODIFICAR_PREMIO = new Funcionalidad();
    public static final Funcionalidad F_ELIMINAR_PREMIO = new Funcionalidad();
    
    public static final Funcionalidad F_EXCLUIR_SUSCRIPCION = new Funcionalidad();
    public static final Funcionalidad F_INCLUIR_SUSCRIPCION = new Funcionalidad();
    
    public static final Funcionalidad F_REALIZAR_SORTEO = new Funcionalidad();

    //Categorias de modulos
    public static final CategoriaModulo CATEGORIA_MODULO_AJUSTES = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_COBROS = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_CLIENTES = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_FACTURAS = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_FUNCIONARIOS = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_MODULOS = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_SERVICIOS = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_UBICACIONES = new CategoriaModulo();
    public static final CategoriaModulo CATEGORIA_MODULO_SORTEO = new CategoriaModulo();
    
    //Campos de suscripcion
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_CLIENTE = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_SERVICIO = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_PRECIO = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_ESTADO = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_NRO_MEDIDOR = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_COBRADOR = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_TIPO_VIVIENDA = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_CANTIDAD_TV = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_BARRIO = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_DIRECCION = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_TIPO_SUSCRIPCION = new CampoSuscripcion();
    public static final CampoSuscripcion CAMPO_SUSCRIPCION_DIA_VENCIMIENTO = new CampoSuscripcion();
    
    //Servicios
    public static final Servicio SERVICIO_TV_CABLE=new Servicio();

    public static final Funcionario ADMIN_DEL_SISTEMA = new Funcionario();

    public static final Cliente CLIENTE_ANULACION_FACTURA = new Cliente();

    static {
        TIPO_SUSCRIPCION_NORMAL.setIdtipoSuscripcion(1);
        TIPO_SUSCRIPCION_NORMAL.setNombre("Normal");
        TIPO_SUSCRIPCION_NORMAL.setPorcentajeDescuento(0.0f);

        TIPO_SUSCRIPCION_GENTILEZA.setIdtipoSuscripcion(2);
        TIPO_SUSCRIPCION_GENTILEZA.setNombre("Gentileza");
        TIPO_SUSCRIPCION_GENTILEZA.setPorcentajeDescuento(100.0f);

        CONCEPTO_COBRO_CUOTA.setIdconcepto(1);
        CONCEPTO_COBRO_CUOTA.setNombre("Cobro cuota");

        CONCEPTO_OTROS_COBROS.setIdconcepto(2);
        CONCEPTO_OTROS_COBROS.setNombre("Otros cobros");

        MEDIO_PAGO_EFECTIVO.setIdmedioPago(1);
        MEDIO_PAGO_EFECTIVO.setNombre("Efectivo");

        ESTADO_CONECTADO.setIdestado(1);
        ESTADO_CONECTADO.setNombre("Conectado");
        ESTADO_CONECTADO.setAlias("Conectado");
        ESTADO_CONECTADO.setRecibeServicio(true);

        ESTADO_RECONECTADO.setIdestado(2);
        ESTADO_RECONECTADO.setNombre("Reconectado");
        ESTADO_RECONECTADO.setAlias("Reconectado");
        ESTADO_RECONECTADO.setRecibeServicio(true);

        ESTADO_DESCONECTADO.setIdestado(3);
        ESTADO_DESCONECTADO.setNombre("Desconectado");
        ESTADO_DESCONECTADO.setAlias("Desconectado");
        ESTADO_DESCONECTADO.setRecibeServicio(false);

        ESTADO_NUEVO.setIdestado(4);
        ESTADO_NUEVO.setNombre("Nuevo");
        ESTADO_NUEVO.setAlias("Nuevo");
        ESTADO_NUEVO.setRecibeServicio(false);

        CARGO_COBRADOR.setIdcargo(1);
        CARGO_COBRADOR.setNombre("Cobrador");

//        TIPO_VIVIENDA_SIN_TIPO.setIdtipoVivienda(1);
//        TIPO_VIVIENDA_SIN_TIPO.setNombre("(Sin Tipo)");
        TIPO_VIVIENDA_PROPIA.setIdtipoVivienda(1);
        TIPO_VIVIENDA_PROPIA.setNombre("Propia");

        TIPO_VIVIENDA_ALQUILER.setIdtipoVivienda(2);
        TIPO_VIVIENDA_ALQUILER.setNombre("Alquiler");

        TIPO_VIVIENDA_PRESTAMO.setIdtipoVivienda(3);
        TIPO_VIVIENDA_PRESTAMO.setNombre("Préstamo");

        ZONA_SIN_ZONA.setIdzona(1);
        ZONA_SIN_ZONA.setNombre("(Sin Zona)");

        BARRIO_SIN_BARRIO.setIdbarrio(1);
        BARRIO_SIN_BARRIO.setNombre("(Sin Barrio)");
        BARRIO_SIN_BARRIO.setZona(ZONA_SIN_ZONA);

        CATEGORIA_CLIENTE_SIN_CATEGORIA.setIdcategoria(1);
        CATEGORIA_CLIENTE_SIN_CATEGORIA.setNombre("(Sin Categoría)");
        CATEGORIA_CLIENTE_SIN_CATEGORIA.setPorcentajeDescuento((float) 0);
        
        //Servicios
        SERVICIO_TV_CABLE.setIdservicio(ID_SERVICIO_TVCABLE);
        SERVICIO_TV_CABLE.setNombre("TV Cable");
        SERVICIO_TV_CABLE.setConceptoCobro(CONCEPTO_COBRO_CUOTA);
        SERVICIO_TV_CABLE.setPorcentajeIva((short)10);
        SERVICIO_TV_CABLE.setPrecio(90000);
        SERVICIO_TV_CABLE.setSuscribible(true);

        //Modulos
        MODULO_CLIENTES.setIdmodulo(1);
        MODULO_CLIENTES.setNombre("Clientes");
        MODULO_CLIENTES.setCategoriaModulo(CATEGORIA_MODULO_CLIENTES);
        MODULO_CLIENTES.setUrl("cliente/cliente.xhtml");

        MODULO_CATEGORIA_CLIENTES.setIdmodulo(2);
        MODULO_CATEGORIA_CLIENTES.setNombre("Categoria de clientes");
        MODULO_CATEGORIA_CLIENTES.setCategoriaModulo(CATEGORIA_MODULO_CLIENTES);
        MODULO_CATEGORIA_CLIENTES.setUrl("cliente/categoria_Cliente.xhtml");

        MODULO_SERVICIOS.setIdmodulo(3);
        MODULO_SERVICIOS.setNombre("Servicios");
        MODULO_SERVICIOS.setCategoriaModulo(CATEGORIA_MODULO_SERVICIOS);
        MODULO_SERVICIOS.setUrl("servicios/servicios.xhtml");

        MODULO_FUNCIONALIDAD.setIdmodulo(4);
        MODULO_FUNCIONALIDAD.setNombre("Funcionalidades");
        MODULO_FUNCIONALIDAD.setCategoriaModulo(CATEGORIA_MODULO_MODULOS);
        MODULO_FUNCIONALIDAD.setUrl("modulos/funcionalidad.xhtml");

        MODULO_PERMISOS.setIdmodulo(5);
        MODULO_PERMISOS.setNombre("Permisos");
        MODULO_PERMISOS.setCategoriaModulo(CATEGORIA_MODULO_MODULOS);
        MODULO_PERMISOS.setUrl("modulos/permisos.xhtml");

        MODULO_MODULOS.setIdmodulo(6);
        MODULO_MODULOS.setNombre("Módulos");
        MODULO_MODULOS.setCategoriaModulo(CATEGORIA_MODULO_MODULOS);
        MODULO_MODULOS.setUrl("modulos/modulos.xhtml");

        MODULO_SUSCRIPCIONES.setIdmodulo(7);
        MODULO_SUSCRIPCIONES.setNombre("Suscripciones");
        MODULO_SUSCRIPCIONES.setCategoriaModulo(CATEGORIA_MODULO_SERVICIOS);
        MODULO_SUSCRIPCIONES.setUrl("servicios/suscripciones.xhtml");

        MODULO_COBROS.setIdmodulo(8);
        MODULO_COBROS.setNombre("Cobros");
        MODULO_COBROS.setCategoriaModulo(CATEGORIA_MODULO_COBROS);
        MODULO_COBROS.setUrl("cobros/cobros.xhtml");

        MODULO_FACTURAS_COBRADOR.setIdmodulo(9);
        MODULO_FACTURAS_COBRADOR.setNombre("Facturas por Cobrador");
        MODULO_FACTURAS_COBRADOR.setCategoriaModulo(CATEGORIA_MODULO_FACTURAS);
        MODULO_FACTURAS_COBRADOR.setUrl("facturas/asignacioncobradores.xhtml");

        MODULO_FACTURAS.setIdmodulo(10);
        MODULO_FACTURAS.setNombre("Facturas");
        MODULO_FACTURAS.setCategoriaModulo(CATEGORIA_MODULO_FACTURAS);
        MODULO_FACTURAS.setUrl("facturas/facturas.xhtml");

        MODULO_TALONARIOS.setIdmodulo(11);
        MODULO_TALONARIOS.setNombre("Talonarios");
        MODULO_TALONARIOS.setCategoriaModulo(CATEGORIA_MODULO_FACTURAS);
        MODULO_TALONARIOS.setUrl("facturas/talonarios.xhtml");

        MODULO_BARRIOS.setIdmodulo(12);
        MODULO_BARRIOS.setNombre("Barrios");
        MODULO_BARRIOS.setCategoriaModulo(CATEGORIA_MODULO_UBICACIONES);
        MODULO_BARRIOS.setUrl("ubicaciones/barrios.xhtml");

        MODULO_ZONAS.setIdmodulo(13);
        MODULO_ZONAS.setNombre("Zonas");
        MODULO_ZONAS.setCategoriaModulo(CATEGORIA_MODULO_UBICACIONES);
        MODULO_ZONAS.setUrl("ubicaciones/zonas.xhtml");

        MODULO_ESTADOS_SUSCRIPCIONES.setIdmodulo(14);
        MODULO_ESTADOS_SUSCRIPCIONES.setNombre("Estados de suscripciones");
        MODULO_ESTADOS_SUSCRIPCIONES.setCategoriaModulo(CATEGORIA_MODULO_SERVICIOS);
        MODULO_ESTADOS_SUSCRIPCIONES.setUrl("servicios/estados-suscripciones.xhtml");

        MODULO_FUNCIONARIOS.setIdmodulo(15);
        MODULO_FUNCIONARIOS.setNombre("Funcionarios");
        MODULO_FUNCIONARIOS.setCategoriaModulo(CATEGORIA_MODULO_FUNCIONARIOS);
        MODULO_FUNCIONARIOS.setUrl("funcionario/funcionario.xhtml");

        MODULO_CARGOS.setIdmodulo(16);
        MODULO_CARGOS.setNombre("Cargos");
        MODULO_CARGOS.setCategoriaModulo(CATEGORIA_MODULO_FUNCIONARIOS);
        MODULO_CARGOS.setUrl("funcionario/cargo.xhtml");

        MODULO_AJUSTES.setIdmodulo(17);
        MODULO_AJUSTES.setNombre("Ajustes");
        MODULO_AJUSTES.setCategoriaModulo(CATEGORIA_MODULO_AJUSTES);
        MODULO_AJUSTES.setUrl("ajustes/ajustes.xhtml");

        MODULO_IMPORTAR.setIdmodulo(18);
        MODULO_IMPORTAR.setNombre("Importar");
        MODULO_IMPORTAR.setCategoriaModulo(CATEGORIA_MODULO_AJUSTES);
        MODULO_IMPORTAR.setUrl("ajustes/importar.xhtml");

        MODULO_TERMINALES_IMPRESION.setIdmodulo(19);
        MODULO_TERMINALES_IMPRESION.setNombre("Terminales de impresión");
        MODULO_TERMINALES_IMPRESION.setCategoriaModulo(CATEGORIA_MODULO_AJUSTES);
        MODULO_TERMINALES_IMPRESION.setUrl("ajustes/terminales.xhtml");
        
        MODULO_SORTEO.setIdmodulo(20);
        MODULO_SORTEO.setNombre("Sorteos");
        MODULO_SORTEO.setCategoriaModulo(CATEGORIA_MODULO_SORTEO);
        MODULO_SORTEO.setUrl("sorteo/sorteo.xhtml");
        
        MODULO_PREMIO.setIdmodulo(22);
        MODULO_PREMIO.setNombre("Premios");
        MODULO_PREMIO.setCategoriaModulo(CATEGORIA_MODULO_SORTEO);
        MODULO_PREMIO.setUrl("premio/premio.xhtml");
        
        
        
        MODULO_EXCLUIDOS.setIdmodulo(23);
        MODULO_EXCLUIDOS.setNombre("Excluidos");
        MODULO_EXCLUIDOS.setCategoriaModulo(CATEGORIA_MODULO_SORTEO);
        MODULO_EXCLUIDOS.setUrl("premio/excluidos.xhtml");

        //Funcionalidades
        REGISTRAR_CLIENTES.setIdfuncionalidad(1);
        REGISTRAR_CLIENTES.setNombre("Registrar");
        REGISTRAR_CLIENTES.setModulo(MODULO_CLIENTES);

        MODIFICAR_CLIENTES.setIdfuncionalidad(2);
        MODIFICAR_CLIENTES.setNombre("Editar");
        MODIFICAR_CLIENTES.setModulo(MODULO_CLIENTES);

        ELIMINAR_CLIENTES.setIdfuncionalidad(3);
        ELIMINAR_CLIENTES.setNombre("Eliminar");
        ELIMINAR_CLIENTES.setModulo(MODULO_CLIENTES);

        REGISTRAR_CATEGORIA_CLIENTES.setIdfuncionalidad(4);
        REGISTRAR_CATEGORIA_CLIENTES.setNombre("Registrar");
        REGISTRAR_CATEGORIA_CLIENTES.setModulo(MODULO_CATEGORIA_CLIENTES);

        EDITAR_CATEGORIA_CLIENTES.setIdfuncionalidad(5);
        EDITAR_CATEGORIA_CLIENTES.setNombre("Editar");
        EDITAR_CATEGORIA_CLIENTES.setModulo(MODULO_CATEGORIA_CLIENTES);

        ELIMINAR_CATEGORIA_CLIENTES.setIdfuncionalidad(6);
        ELIMINAR_CATEGORIA_CLIENTES.setNombre("Eliminar");
        ELIMINAR_CATEGORIA_CLIENTES.setModulo(MODULO_CATEGORIA_CLIENTES);

        REGISTRAR_FUNCIONALIDADES.setIdfuncionalidad(7);
        REGISTRAR_FUNCIONALIDADES.setNombre("Registrar");
        REGISTRAR_FUNCIONALIDADES.setModulo(MODULO_FUNCIONALIDAD);

        EDITAR_FUNCIONALIDADES.setIdfuncionalidad(8);
        EDITAR_FUNCIONALIDADES.setNombre("Editar");
        EDITAR_FUNCIONALIDADES.setModulo(MODULO_FUNCIONALIDAD);

        ELIMINAR_FUNCIONALIDADES.setIdfuncionalidad(9);
        ELIMINAR_FUNCIONALIDADES.setNombre("Eliminar");
        ELIMINAR_FUNCIONALIDADES.setModulo(MODULO_FUNCIONALIDAD);

        REGISTRAR_MODULOS.setIdfuncionalidad(10);
        REGISTRAR_MODULOS.setNombre("Registrar");
        REGISTRAR_MODULOS.setModulo(MODULO_MODULOS);

        EDITAR_MODULOS.setIdfuncionalidad(11);
        EDITAR_MODULOS.setNombre("Editar");
        EDITAR_MODULOS.setModulo(MODULO_MODULOS);

        ELIMINAR_MODULOS.setIdfuncionalidad(12);
        ELIMINAR_MODULOS.setNombre("Eliminar");
        ELIMINAR_MODULOS.setModulo(MODULO_MODULOS);

        REGISTRAR_SERVICIOS.setIdfuncionalidad(13);
        REGISTRAR_SERVICIOS.setNombre("Registrar");
        REGISTRAR_SERVICIOS.setModulo(MODULO_SERVICIOS);

        EDITAR_SERVICIOS.setIdfuncionalidad(14);
        EDITAR_SERVICIOS.setNombre("Editar");
        EDITAR_SERVICIOS.setModulo(MODULO_SERVICIOS);

        ELIMINAR_SERVICIOS.setIdfuncionalidad(15);
        ELIMINAR_SERVICIOS.setNombre("Eliminar");
        ELIMINAR_SERVICIOS.setModulo(MODULO_SERVICIOS);

        ASIGNAR_PERMISOS.setIdfuncionalidad(16);
        ASIGNAR_PERMISOS.setNombre("Asignar");
        ASIGNAR_PERMISOS.setModulo(MODULO_PERMISOS);

        REMOVER_PERMISOS.setIdfuncionalidad(17);
        REMOVER_PERMISOS.setNombre("Remover");
        REMOVER_PERMISOS.setModulo(MODULO_PERMISOS);

        F_REGISTRAR_COBROS.setIdfuncionalidad(18);
        F_REGISTRAR_COBROS.setNombre("Registrar");
        F_REGISTRAR_COBROS.setModulo(MODULO_COBROS);

        F_REGISTRAR_FACTURAS.setIdfuncionalidad(19);
        F_REGISTRAR_FACTURAS.setNombre("Registrar");
        F_REGISTRAR_FACTURAS.setModulo(MODULO_FACTURAS);

        F_REGISTRAR_TALONARIOS.setIdfuncionalidad(20);
        F_REGISTRAR_TALONARIOS.setNombre("Registrar");
        F_REGISTRAR_TALONARIOS.setModulo(MODULO_TALONARIOS);

        F_VER_FACTURAS_POR_COBRADOR.setIdfuncionalidad(21);
        F_VER_FACTURAS_POR_COBRADOR.setNombre("Ver");
        F_VER_FACTURAS_POR_COBRADOR.setModulo(MODULO_FACTURAS_COBRADOR);

        F_REGISTRAR_BARRIOS.setIdfuncionalidad(22);
        F_REGISTRAR_BARRIOS.setNombre("Registrar");
        F_REGISTRAR_BARRIOS.setModulo(MODULO_BARRIOS);

        F_REGISTRAR_ZONAS.setIdfuncionalidad(23);
        F_REGISTRAR_ZONAS.setNombre("Registrar");
        F_REGISTRAR_ZONAS.setModulo(MODULO_ZONAS);

        F_REGISTRAR_SUSCRIPCIONES.setIdfuncionalidad(24);
        F_REGISTRAR_SUSCRIPCIONES.setNombre("Registrar");
        F_REGISTRAR_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_CAMBIAR_ESTADOS_SUSCRIPCIONES.setIdfuncionalidad(25);
        F_CAMBIAR_ESTADOS_SUSCRIPCIONES.setNombre("Cambiar estados");
        F_CAMBIAR_ESTADOS_SUSCRIPCIONES.setModulo(MODULO_ESTADOS_SUSCRIPCIONES);

        F_REGISTRAR_FUNCIONARIOS.setIdfuncionalidad(26);
        F_REGISTRAR_FUNCIONARIOS.setNombre("Registrar");
        F_REGISTRAR_FUNCIONARIOS.setModulo(MODULO_FUNCIONARIOS);

        F_REGISTRAR_CARGOS.setIdfuncionalidad(27);
        F_REGISTRAR_CARGOS.setNombre("Registrar");
        F_REGISTRAR_CARGOS.setModulo(MODULO_CARGOS);

        F_CAMBIAR_RUTA_REPORTES.setIdfuncionalidad(28);
        F_CAMBIAR_RUTA_REPORTES.setNombre("Cambiar ruta de reportes");
        F_CAMBIAR_RUTA_REPORTES.setModulo(MODULO_AJUSTES);

        F_IMPORTAR_SISTEMA_ANTERIOR.setIdfuncionalidad(29);
        F_IMPORTAR_SISTEMA_ANTERIOR.setNombre("Importar del sistema anterior");
        F_IMPORTAR_SISTEMA_ANTERIOR.setModulo(MODULO_IMPORTAR);

        F_MODIFICAR_SUSCRIPCIONES.setIdfuncionalidad(30);
        F_MODIFICAR_SUSCRIPCIONES.setNombre("Modificar");
        F_MODIFICAR_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);
        F_REGISTRAR_TERMINALES_IMPRESION.setIdfuncionalidad(31);
        F_REGISTRAR_TERMINALES_IMPRESION.setNombre("Registrar");
        F_REGISTRAR_TERMINALES_IMPRESION.setModulo(MODULO_TERMINALES_IMPRESION);

        F_MODIFICAR_BARRIOS.setIdfuncionalidad(32);
        F_MODIFICAR_BARRIOS.setNombre("Modificar");
        F_MODIFICAR_BARRIOS.setModulo(MODULO_BARRIOS);

        F_ELIMINAR_BARRIOS.setIdfuncionalidad(33);
        F_ELIMINAR_BARRIOS.setNombre("Eliminar");
        F_ELIMINAR_BARRIOS.setModulo(MODULO_BARRIOS);

        F_ELIMINAR_ZONAS.setIdfuncionalidad(34);
        F_ELIMINAR_ZONAS.setNombre("ELIMINAR");
        F_ELIMINAR_ZONAS.setModulo(MODULO_ZONAS);

        F_MODIFICAR_ZONAS.setIdfuncionalidad(35);
        F_MODIFICAR_ZONAS.setNombre("Modificar");
        F_MODIFICAR_ZONAS.setModulo(MODULO_ZONAS);

        F_ELIMINAR_SUSCRIPCIONES.setIdfuncionalidad(36);
        F_ELIMINAR_SUSCRIPCIONES.setNombre("Eliminar");
        F_ELIMINAR_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_GENERAR_CUOTAS_SUSCRIPCIONES.setIdfuncionalidad(37);
        F_GENERAR_CUOTAS_SUSCRIPCIONES.setNombre("Generar_Cuotas");
        F_GENERAR_CUOTAS_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_VISTA_PREVIA_SUSCRIPCIONES.setIdfuncionalidad(38);
        F_VISTA_PREVIA_SUSCRIPCIONES.setNombre("Vista_Previa");
        F_VISTA_PREVIA_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_RESUMEN_SUSCRIPCIONES.setIdfuncionalidad(39);
        F_RESUMEN_SUSCRIPCIONES.setNombre("Resumen_Suscripcion");
        F_RESUMEN_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_ELIMINAR_D_SUSCRIPCIONES.setIdfuncionalidad(40);
        F_ELIMINAR_D_SUSCRIPCIONES.setNombre("Eliminar Detalle Suscripciones");
        F_ELIMINAR_D_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_EDITAR_D_SUSCRIPCIONES.setIdfuncionalidad(41);
        F_EDITAR_D_SUSCRIPCIONES.setNombre("Editar Detalle Suscripcion");
        F_EDITAR_D_SUSCRIPCIONES.setModulo(MODULO_SUSCRIPCIONES);

        F_REGISTRAR_SUSCRIPCION_CLIENTES.setIdfuncionalidad(42);
        F_REGISTRAR_SUSCRIPCION_CLIENTES.setNombre("Registrar Suscripcion Cliente");
        F_REGISTRAR_SUSCRIPCION_CLIENTES.setModulo(MODULO_CLIENTES);

        F_VER_CLIENTES_ACTIVOS.setIdfuncionalidad(43);
        F_VER_CLIENTES_ACTIVOS.setNombre("Ver Clientes Activos");
        F_VER_CLIENTES_ACTIVOS.setModulo(MODULO_CLIENTES);

        F_VISTA_P_FACTURA.setIdfuncionalidad(44);
        F_VISTA_P_FACTURA.setNombre("Vista Previa");
        F_VISTA_P_FACTURA.setModulo(MODULO_COBROS);

        F_PAGO_FACTURA.setIdfuncionalidad(45);
        F_PAGO_FACTURA.setNombre("Pago Factura");
        F_PAGO_FACTURA.setModulo(MODULO_COBROS);

        F_RESUMEN_FACTURA.setIdfuncionalidad(46);
        F_RESUMEN_FACTURA.setNombre("Resumen Factura");
        F_RESUMEN_FACTURA.setModulo(MODULO_COBROS);

        F_VER_FACTURAS_ANULADAS.setIdfuncionalidad(47);
        F_VER_FACTURAS_ANULADAS.setNombre("Ver Facturas Anulados");
        F_VER_FACTURAS_ANULADAS.setModulo(MODULO_COBROS);

        F_EDITAR_TALONARIOS.setIdfuncionalidad(48);
        F_EDITAR_TALONARIOS.setNombre("Editar");
        F_EDITAR_TALONARIOS.setModulo(MODULO_TALONARIOS);

        F_ELIMINAR_TALONARIOS.setIdfuncionalidad(49);
        F_ELIMINAR_TALONARIOS.setNombre("Eliminar");
        F_ELIMINAR_TALONARIOS.setModulo(MODULO_TALONARIOS);

        F_GENERAR_FACTURAS.setIdfuncionalidad(50);
        F_GENERAR_FACTURAS.setNombre("Generar Facturas");
        F_GENERAR_FACTURAS.setModulo(MODULO_FACTURAS);

        F_IMPRIMIR_FACTURAS.setIdfuncionalidad(51);
        F_IMPRIMIR_FACTURAS.setNombre("Imprimir Facturas");
        F_IMPRIMIR_FACTURAS.setModulo(MODULO_FACTURAS);

        F_VER_FACTURAS_PENDIENTES.setIdfuncionalidad(52);
        F_VER_FACTURAS_PENDIENTES.setNombre("Ver Facturas Pendientes");
        F_VER_FACTURAS_PENDIENTES.setModulo(MODULO_FACTURAS);

        F_VER_FACTURA_PAGADAS.setIdfuncionalidad(53);
        F_VER_FACTURA_PAGADAS.setNombre("Ver Facturas Pagadas");
        F_VER_FACTURA_PAGADAS.setModulo(MODULO_FACTURAS);

        VER_FACTURAS_ANULADAS.setIdfuncionalidad(54);
        VER_FACTURAS_ANULADAS.setNombre("Ver Facturas Anuladas");
        VER_FACTURAS_ANULADAS.setModulo(MODULO_FACTURAS);

        F_ANULAR_FACTURAS.setIdfuncionalidad(55);
        F_ANULAR_FACTURAS.setNombre("Anular Facturas");
        F_ANULAR_FACTURAS.setModulo(MODULO_FACTURAS);
        
        F_REGISTRAR_PREMIO.setIdfuncionalidad(56);
        F_REGISTRAR_PREMIO.setNombre("Registrar Premio");
        F_REGISTRAR_PREMIO.setModulo(MODULO_PREMIO);
        
        F_MODIFICAR_PREMIO.setIdfuncionalidad(57);
        F_MODIFICAR_PREMIO.setNombre("MODIFICAR PREMIO");
        F_MODIFICAR_PREMIO.setModulo(MODULO_PREMIO);
        
        F_ELIMINAR_PREMIO.setIdfuncionalidad(58);
        F_ELIMINAR_PREMIO.setNombre("ELIMINAR PREMIO");
        F_ELIMINAR_PREMIO.setModulo(MODULO_PREMIO);
        
        F_EXCLUIR_SUSCRIPCION.setIdfuncionalidad(59);
        F_EXCLUIR_SUSCRIPCION.setNombre("EXCLUIR SUSCRIPCION");
        F_EXCLUIR_SUSCRIPCION.setModulo(MODULO_EXCLUIDOS);
        
        F_INCLUIR_SUSCRIPCION.setIdfuncionalidad(60);
        F_INCLUIR_SUSCRIPCION.setNombre("INCLUIR SUSCRIPCION");
        F_INCLUIR_SUSCRIPCION.setModulo(MODULO_EXCLUIDOS);
        
        F_REALIZAR_SORTEO.setIdfuncionalidad(61);
        F_REALIZAR_SORTEO.setNombre("REALIZAR SORTEO");
        F_REALIZAR_SORTEO.setModulo(MODULO_SORTEO);
                
        //Categorias de modulos
        CATEGORIA_MODULO_AJUSTES.setIdcategoriaModulo(1);
        CATEGORIA_MODULO_AJUSTES.setNombre("Ajustes del sistema");
        CATEGORIA_MODULO_AJUSTES.setIcono("options icon");

        CATEGORIA_MODULO_CLIENTES.setIdcategoriaModulo(2);
        CATEGORIA_MODULO_CLIENTES.setNombre("Clientes");
        CATEGORIA_MODULO_CLIENTES.setIcono("users icon");

        CATEGORIA_MODULO_COBROS.setIdcategoriaModulo(3);
        CATEGORIA_MODULO_COBROS.setNombre("Cobros");
        CATEGORIA_MODULO_COBROS.setIcono("money icon");

        CATEGORIA_MODULO_FACTURAS.setIdcategoriaModulo(4);
        CATEGORIA_MODULO_FACTURAS.setNombre("Facturas");
        CATEGORIA_MODULO_FACTURAS.setIcono("newspaper icon");

        CATEGORIA_MODULO_FUNCIONARIOS.setIdcategoriaModulo(5);
        CATEGORIA_MODULO_FUNCIONARIOS.setNombre("Funcionarios");
        CATEGORIA_MODULO_FUNCIONARIOS.setIcono("user icon");

        CATEGORIA_MODULO_MODULOS.setIdcategoriaModulo(6);
        CATEGORIA_MODULO_MODULOS.setNombre("Modulos");
        CATEGORIA_MODULO_MODULOS.setIcono("object group icon");

        CATEGORIA_MODULO_SERVICIOS.setIdcategoriaModulo(7);
        CATEGORIA_MODULO_SERVICIOS.setNombre("Servicios");
        CATEGORIA_MODULO_SERVICIOS.setIcono("suitcase icon");

        CATEGORIA_MODULO_UBICACIONES.setIdcategoriaModulo(8);
        CATEGORIA_MODULO_UBICACIONES.setNombre("Ubicaciones");
        CATEGORIA_MODULO_UBICACIONES.setIcono("map icon");
        
        CATEGORIA_MODULO_SORTEO.setIdcategoriaModulo(9);
        CATEGORIA_MODULO_SORTEO.setNombre("Sorteo");
        CATEGORIA_MODULO_SORTEO.setIcono("game icon");

        ADMIN_DEL_SISTEMA.setIdfuncionario(1001);
        ADMIN_DEL_SISTEMA.setCi(99999);
        ADMIN_DEL_SISTEMA.setNombres("ADMIN");
        ADMIN_DEL_SISTEMA.setApellidos("ADMIN");
        ADMIN_DEL_SISTEMA.setPassword("dd1c7f8ee2709bd31432f17bb8852207a17b4a66f33b7e2cda4d84bce52384ba");
        ADMIN_DEL_SISTEMA.setActivo(true);
        ADMIN_DEL_SISTEMA.setFuncionalidadList(new LinkedList<Funcionalidad>());
        ADMIN_DEL_SISTEMA.getFuncionalidadList().addAll(EntidadesEstaticas.getListaFuncionalidades());

        CLIENTE_ANULACION_FACTURA.setIdcliente(10000000);
        CLIENTE_ANULACION_FACTURA.setRazonSocial("(Anulación de factura)");
        CLIENTE_ANULACION_FACTURA.setCategoria(CATEGORIA_CLIENTE_SIN_CATEGORIA);
        
    //Campos de suscripcion    
    CAMPO_SUSCRIPCION_CLIENTE.setIdcampoSuscripcion(1);
    CAMPO_SUSCRIPCION_CLIENTE.setNombre("Titular");
    
    CAMPO_SUSCRIPCION_SERVICIO.setIdcampoSuscripcion(2);
    CAMPO_SUSCRIPCION_SERVICIO.setNombre("Servicio");
    
    CAMPO_SUSCRIPCION_PRECIO.setIdcampoSuscripcion(3);
    CAMPO_SUSCRIPCION_PRECIO.setNombre("Monto de cuota");
    
    CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION.setIdcampoSuscripcion(4);
    CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION.setNombre("Fecha de suscripción");
    
    CAMPO_SUSCRIPCION_ESTADO.setIdcampoSuscripcion(5);
    CAMPO_SUSCRIPCION_ESTADO.setNombre("Estado");
    
    CAMPO_SUSCRIPCION_NRO_MEDIDOR.setIdcampoSuscripcion(6);
    CAMPO_SUSCRIPCION_NRO_MEDIDOR.setNombre("Número de medidor");
    
    CAMPO_SUSCRIPCION_COBRADOR.setIdcampoSuscripcion(7);
    CAMPO_SUSCRIPCION_COBRADOR.setNombre("Cobrador");
    
    CAMPO_SUSCRIPCION_TIPO_VIVIENDA.setIdcampoSuscripcion(8);
    CAMPO_SUSCRIPCION_TIPO_VIVIENDA.setNombre("Tipo de vivienda");
    
    CAMPO_SUSCRIPCION_CANTIDAD_TV.setIdcampoSuscripcion(9);
    CAMPO_SUSCRIPCION_CANTIDAD_TV.setNombre("Cantidad de TVs");
    
    CAMPO_SUSCRIPCION_BARRIO.setIdcampoSuscripcion(10);
    CAMPO_SUSCRIPCION_BARRIO.setNombre("Barrio");
    
    CAMPO_SUSCRIPCION_DIRECCION.setIdcampoSuscripcion(11);
    CAMPO_SUSCRIPCION_DIRECCION.setNombre("Dirección");
    
    CAMPO_SUSCRIPCION_TIPO_SUSCRIPCION.setIdcampoSuscripcion(12);
    CAMPO_SUSCRIPCION_TIPO_SUSCRIPCION.setNombre("Tipo de suscripción");
    
    CAMPO_SUSCRIPCION_DIA_VENCIMIENTO.setIdcampoSuscripcion(13);
    CAMPO_SUSCRIPCION_DIA_VENCIMIENTO.setNombre("Día de vencimiento de cada mes");
}

public static String getNombreMes(int nroMes){
        switch(nroMes){
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return null;
        }
    }
public static String getNombreMesCorto(int nroMes){
        switch(nroMes){
            case 1:
                return "ene";
            case 2:
                return "feb";
            case 3:
                return "mar";
            case 4:
                return "abr";
            case 5:
                return "may";
            case 6:
                return "jun";
            case 7:
                return "jul";
            case 8:
                return "ago";
            case 9:
                return "sep";
            case 10:
                return "oct";
            case 11:
                return "nov";
            case 12:
                return "dic";
            default:
                return null;
        }
    }
    
    public static List<Modulo> getListaModulos(){
        List<Modulo> lstModulos=new LinkedList<>();
        lstModulos.add(MODULO_CLIENTES);
        lstModulos.add(MODULO_SERVICIOS);
        lstModulos.add(MODULO_SUSCRIPCIONES);
        lstModulos.add(MODULO_CATEGORIA_CLIENTES);
        lstModulos.add(MODULO_FUNCIONALIDAD);
        lstModulos.add(MODULO_PERMISOS);
        lstModulos.add(MODULO_MODULOS);
        lstModulos.add(MODULO_COBROS);
        lstModulos.add(MODULO_FACTURAS_COBRADOR);
        lstModulos.add(MODULO_FACTURAS);
        lstModulos.add(MODULO_TALONARIOS);
        lstModulos.add(MODULO_ZONAS);
        lstModulos.add(MODULO_BARRIOS);
        lstModulos.add(MODULO_ESTADOS_SUSCRIPCIONES);
        lstModulos.add(MODULO_FUNCIONARIOS);
        lstModulos.add(MODULO_CARGOS);
        lstModulos.add(MODULO_AJUSTES);
        lstModulos.add(MODULO_IMPORTAR);
        lstModulos.add(MODULO_TERMINALES_IMPRESION);
        lstModulos.add(MODULO_PREMIO);
        lstModulos.add(MODULO_EXCLUIDOS);
        lstModulos.add(MODULO_SORTEO);
        return lstModulos;
    }
    
    public static List<Funcionalidad> getListaFuncionalidades(){
        List<Funcionalidad> lstFuncionalidades=new LinkedList<>();
        lstFuncionalidades.add(ASIGNAR_PERMISOS);
        lstFuncionalidades.add(EDITAR_CATEGORIA_CLIENTES);
        lstFuncionalidades.add(EDITAR_FUNCIONALIDADES);
        lstFuncionalidades.add(EDITAR_MODULOS);
        lstFuncionalidades.add(EDITAR_SERVICIOS);
        lstFuncionalidades.add(ELIMINAR_CATEGORIA_CLIENTES);
        lstFuncionalidades.add(ELIMINAR_CLIENTES);
        lstFuncionalidades.add(ELIMINAR_FUNCIONALIDADES);
        lstFuncionalidades.add(ELIMINAR_MODULOS);
        lstFuncionalidades.add(ELIMINAR_SERVICIOS);
        lstFuncionalidades.add(MODIFICAR_CLIENTES);
        lstFuncionalidades.add(REGISTRAR_CATEGORIA_CLIENTES);
        lstFuncionalidades.add(REGISTRAR_CLIENTES);
        lstFuncionalidades.add(REGISTRAR_FUNCIONALIDADES);
        lstFuncionalidades.add(REGISTRAR_MODULOS);
        lstFuncionalidades.add(REGISTRAR_SERVICIOS);
        lstFuncionalidades.add(REMOVER_PERMISOS);
        lstFuncionalidades.add(F_REGISTRAR_FACTURAS);
        lstFuncionalidades.add(F_REGISTRAR_COBROS);
        lstFuncionalidades.add(F_REGISTRAR_TALONARIOS);
        lstFuncionalidades.add(F_VER_FACTURAS_POR_COBRADOR);
        lstFuncionalidades.add(F_REGISTRAR_BARRIOS);
        lstFuncionalidades.add(F_MODIFICAR_BARRIOS);
        lstFuncionalidades.add(F_ELIMINAR_BARRIOS);
        lstFuncionalidades.add(F_REGISTRAR_ZONAS);
        lstFuncionalidades.add(F_MODIFICAR_ZONAS);
        lstFuncionalidades.add(F_ELIMINAR_ZONAS);
        lstFuncionalidades.add(F_REGISTRAR_SUSCRIPCIONES);
        lstFuncionalidades.add(F_CAMBIAR_ESTADOS_SUSCRIPCIONES);
        lstFuncionalidades.add(F_REGISTRAR_FUNCIONARIOS);
        lstFuncionalidades.add(F_REGISTRAR_CARGOS);
        lstFuncionalidades.add(F_CAMBIAR_RUTA_REPORTES);
        lstFuncionalidades.add(F_IMPORTAR_SISTEMA_ANTERIOR);
        lstFuncionalidades.add(F_MODIFICAR_SUSCRIPCIONES);
        lstFuncionalidades.add(F_REGISTRAR_TERMINALES_IMPRESION);
        lstFuncionalidades.add(F_ELIMINAR_SUSCRIPCIONES);
        lstFuncionalidades.add(F_RESUMEN_SUSCRIPCIONES);
        lstFuncionalidades.add(F_VISTA_PREVIA_SUSCRIPCIONES);
        lstFuncionalidades.add(F_GENERAR_CUOTAS_SUSCRIPCIONES);
        lstFuncionalidades.add(F_EDITAR_D_SUSCRIPCIONES);
        lstFuncionalidades.add(F_ELIMINAR_D_SUSCRIPCIONES);
        lstFuncionalidades.add(F_REGISTRAR_SUSCRIPCION_CLIENTES);
        lstFuncionalidades.add(F_VER_CLIENTES_ACTIVOS);
        lstFuncionalidades.add(F_RESUMEN_FACTURA);
        lstFuncionalidades.add(F_VISTA_P_FACTURA);
        lstFuncionalidades.add(F_PAGO_FACTURA);
        lstFuncionalidades.add(F_VER_FACTURAS_ANULADAS);
        lstFuncionalidades.add(F_EDITAR_TALONARIOS);
        lstFuncionalidades.add(F_ELIMINAR_TALONARIOS);
        lstFuncionalidades.add(VER_FACTURAS_ANULADAS);
        lstFuncionalidades.add(F_VER_FACTURAS_PENDIENTES);
        lstFuncionalidades.add(F_VER_FACTURA_PAGADAS);
        lstFuncionalidades.add(F_IMPRIMIR_FACTURAS);
        lstFuncionalidades.add(F_GENERAR_FACTURAS);
        lstFuncionalidades.add(F_ANULAR_FACTURAS);
        lstFuncionalidades.add(F_REGISTRAR_PREMIO);
        lstFuncionalidades.add(F_MODIFICAR_PREMIO);
        lstFuncionalidades.add(F_ELIMINAR_PREMIO);
        lstFuncionalidades.add(F_EXCLUIR_SUSCRIPCION);
        lstFuncionalidades.add(F_INCLUIR_SUSCRIPCION);
        lstFuncionalidades.add(F_REALIZAR_SORTEO);
        return lstFuncionalidades;
    }
    
    public static List<CategoriaModulo> getListaCategoriasModulos(){
        List<CategoriaModulo> lstc=new LinkedList<>();
        lstc.add(CATEGORIA_MODULO_AJUSTES);
        lstc.add(CATEGORIA_MODULO_CLIENTES);
        lstc.add(CATEGORIA_MODULO_COBROS);
        lstc.add(CATEGORIA_MODULO_FACTURAS);
        lstc.add(CATEGORIA_MODULO_FUNCIONARIOS);
        lstc.add(CATEGORIA_MODULO_MODULOS);
        lstc.add(CATEGORIA_MODULO_SERVICIOS);
        lstc.add(CATEGORIA_MODULO_UBICACIONES);
        lstc.add(CATEGORIA_MODULO_SORTEO);
        return lstc;
    }
    
    public static List<EstadoSuscripcion> getListaEstadosSuscripcion(){
        List<EstadoSuscripcion> lst=new LinkedList<>();
//        lst.add(ESTADO_NUEVO);
        lst.add(ESTADO_DESCONECTADO);
        lst.add(ESTADO_RECONECTADO);
        lst.add(ESTADO_CONECTADO);
        return lst;
    }
    
    public static List<TipoVivienda> getListaTiposVivienda(){
        List<TipoVivienda> lst=new LinkedList<>();
        lst.add(TIPO_VIVIENDA_PROPIA);
        lst.add(TIPO_VIVIENDA_ALQUILER);
        lst.add(TIPO_VIVIENDA_PRESTAMO);
        return lst;
    }
    
    public static List<TipoSuscripcion> getListaTipoSuscripciones(){
        List<TipoSuscripcion> lst=new LinkedList<>();
        lst.add(TIPO_SUSCRIPCION_NORMAL);
        lst.add(TIPO_SUSCRIPCION_GENTILEZA);
        return lst;
    }
    
    public static List<CampoSuscripcion> getListaCamposSuscripcion(){
        List<CampoSuscripcion> lst=new LinkedList<>();
        lst.add(CAMPO_SUSCRIPCION_PRECIO);
        lst.add(CAMPO_SUSCRIPCION_BARRIO);
        lst.add(CAMPO_SUSCRIPCION_CANTIDAD_TV);
        lst.add(CAMPO_SUSCRIPCION_CLIENTE);
        lst.add(CAMPO_SUSCRIPCION_COBRADOR);
        lst.add(CAMPO_SUSCRIPCION_DIRECCION);
        lst.add(CAMPO_SUSCRIPCION_ESTADO);
        lst.add(CAMPO_SUSCRIPCION_FECHA_SUSCRIPCION);
        lst.add(CAMPO_SUSCRIPCION_NRO_MEDIDOR);
        lst.add(CAMPO_SUSCRIPCION_SERVICIO);
        lst.add(CAMPO_SUSCRIPCION_TIPO_SUSCRIPCION);
        lst.add(CAMPO_SUSCRIPCION_TIPO_VIVIENDA);
        lst.add(CAMPO_SUSCRIPCION_DIA_VENCIMIENTO);
        return lst;
    }
    
    public static List<Servicio> getListaServicios(){
        List<Servicio> lst=new LinkedList<>();
        lst.add(SERVICIO_TV_CABLE);
        return lst;
    }
    
    public static List<CategoriaCliente> getListaCategoriaClientes(){
        List<CategoriaCliente> lst=new LinkedList<>();
        lst.add(CATEGORIA_CLIENTE_SIN_CATEGORIA);
        return lst;
    }
    
    public static List<Concepto> getListaConceptoCobros(){
        List<Concepto> lst=new LinkedList<>();
        lst.add(CONCEPTO_COBRO_CUOTA);
        lst.add(CONCEPTO_OTROS_COBROS);
        return lst;
    }
    
}
