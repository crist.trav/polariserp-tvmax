/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.reportes;

import com.exabit.polariserp.tvmax.entidades.DetalleFactura;

/**
 *
 * @author cristhian-kn
 */
public class DetalleFacturaReporteBean {

    public DetalleFacturaReporteBean(DetalleFactura df) {
        this.item=df.getDescripcion().toUpperCase();
        this.monto=df.getMonto();
        Short porceIva=df.getServicio().getPorcentajeIva();
        
        switch (porceIva) {
            case 10:
                this.monto10=df.getSubtotal();
                this.monto5=null;
                this.montoExe=null;
                break;
            case 5:
                this.monto10=null;
                this.monto5=df.getSubtotal();
                this.montoExe=null;
                break;
            default:
                this.monto10=null;
                this.monto5=null;
                this.montoExe=df.getSubtotal();
                break;
        }
        this.cantidad=df.getCantidad();
    }

    public DetalleFacturaReporteBean() {
    }
    
        private Integer montoExe;

    public Integer getMontoExe() {
        return montoExe;
    }

    public void setMontoExe(Integer montoExe) {
        this.montoExe = montoExe;
    }

    
    private String item;

    public String getItem() {
        return item;
        
    }

    public void setItem(String item) {
        this.item = item;
    }

        private Integer cantidad;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

        private Integer monto;

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

        private Integer monto5;

    public Integer getMonto5() {
        return monto5;
    }

    public void setMonto5(Integer monto5) {
        this.monto5 = monto5;
    }

        private Integer monto10;

    public Integer getMonto10() {
        return monto10;
    }

    public void setMonto10(Integer monto10) {
        this.monto10 = monto10;
    }
    
    

}
