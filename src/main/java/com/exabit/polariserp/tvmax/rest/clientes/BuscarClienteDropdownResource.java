/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.rest.clientes;

import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.google.gson.Gson;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author cristhian-kn
 */
@Path("buscarClienteDropdown")
public class BuscarClienteDropdownResource {
    
    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of BuscarClienteDropdownResource
     */
    public BuscarClienteDropdownResource() {
    }

    /**
     * Retrieves representation of an instance of com.polariserp.web.rest.clientes.BuscarClienteDropdownResource
     * @param q
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getText(@QueryParam("q") String q) {
        TypedQuery<Cliente> queryfiltro=this.em.createQuery("SELECT c FROM Cliente c WHERE SQL('CAST(? AS CHAR(11))',c.idcliente) LIKE :tx OR LOWER(c.nombres) LIKE :tx OR LOWER(c.apellidos) LIKE :tx OR LOWER(c.razonSocial) LIKE :tx OR SQL('CAST(? AS CHAR(11))', c.ci) LIKE :tx", Cliente.class);
        TypedQuery<Cliente> querytodos=this.em.createQuery("SELECT c FROM Cliente c", Cliente.class);
        queryfiltro.setParameter("tx", "%"+q.toLowerCase()+"%");
        List<Cliente> lstClientes=new LinkedList<>();
        if(q.isEmpty()){
            System.out.println("todos: "+q);
            lstClientes.addAll(querytodos.setMaxResults(200).getResultList());
        }else{
            System.out.println("filtrar: "+q);
            lstClientes.addAll(queryfiltro.setMaxResults(200).getResultList());
        }
        
        RespuestaBusquedaCliente r=new RespuestaBusquedaCliente();
        r.setSuccess(true);
        DecimalFormat df=new DecimalFormat("###,###,###,###");
        for(Cliente c:lstClientes){
            ClienteBasicDropdown clib=new ClienteBasicDropdown();
            String tmpName=c.getRazonSocial();
            if(c.getCi()!=null){
                tmpName=tmpName+" ("+df.format(c.getCi())+")";
            }
            clib.setName(tmpName);
            clib.setValue(c.getIdcliente());
            r.getResults().add(clib);
        }
        System.out.println("hay "+r.getResults().size()+" resultados");
        Gson gson=new Gson();
        return gson.toJson(r);
    }

    /**
     * PUT method for updating or creating an instance of BuscarClienteDropdownResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void putText(String content) {
    }
}
