/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "sorteo")
@NamedQueries({
    @NamedQuery(name = "Sorteo.findAll", query = "SELECT s FROM Sorteo s")
    , @NamedQuery(name = "Sorteo.findByIdsorteo", query = "SELECT s FROM Sorteo s WHERE s.idsorteo = :idsorteo")
    , @NamedQuery(name = "Sorteo.findByDescripcion", query = "SELECT s FROM Sorteo s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "Sorteo.findByFecha", query = "SELECT s FROM Sorteo s WHERE s.fecha = :fecha")})
public class Sorteo implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sorteo")
    private List<SuscripcionesExcluidasSorteo> suscripcionesExcluidasSorteoList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idsorteo", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idsorteo", table = "idsequences", allocationSize = 1, initialValue = 3)
    @Basic(optional = false)
    @Column(name = "idsorteo")
    private Integer idsorteo;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinTable(name = "suscripciones_excluidas_sorteo", joinColumns = {
        @JoinColumn(name = "sorteo_idsorteo", referencedColumnName = "idsorteo")}, inverseJoinColumns = {
        @JoinColumn(name = "suscripcion_idsuscripcion", referencedColumnName = "idsuscripcion")})
    @ManyToMany
    private List<Suscripcion> suscripcionList;
    @OneToMany(mappedBy = "sorteoIdsorteo")
    private List<Premio> premioList;

    public Sorteo() {
    }

    public Sorteo(Integer idsorteo) {
        this.idsorteo = idsorteo;
    }

    public Integer getIdsorteo() {
        return idsorteo;
    }

    public void setIdsorteo(Integer idsorteo) {
        this.idsorteo = idsorteo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    public List<Premio> getPremioList() {
        return premioList;
    }

    public void setPremioList(List<Premio> premioList) {
        this.premioList = premioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsorteo != null ? idsorteo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sorteo)) {
            return false;
        }
        Sorteo other = (Sorteo) object;
        if ((this.idsorteo == null && other.idsorteo != null) || (this.idsorteo != null && !this.idsorteo.equals(other.idsorteo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.Sorteo[ idsorteo=" + idsorteo + " ]";
    }

    public List<SuscripcionesExcluidasSorteo> getSuscripcionesExcluidasSorteoList() {
        return suscripcionesExcluidasSorteoList;
    }

    public void setSuscripcionesExcluidasSorteoList(List<SuscripcionesExcluidasSorteo> suscripcionesExcluidasSorteoList) {
        this.suscripcionesExcluidasSorteoList = suscripcionesExcluidasSorteoList;
    }
    
}
