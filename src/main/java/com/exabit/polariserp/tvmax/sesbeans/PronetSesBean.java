package com.exabit.polariserp.tvmax.sesbeans;

import com.exabit.polariserp.tvmax.entidades.AnulacionReqPronet;
import com.exabit.polariserp.tvmax.entidades.AnulacionResPronet;
import com.exabit.polariserp.tvmax.entidades.Cliente;
import com.exabit.polariserp.tvmax.entidades.Cobro;
import com.exabit.polariserp.tvmax.entidades.ConsultaReqPronet;
import com.exabit.polariserp.tvmax.entidades.ConsultaResDetallePronet;
import com.exabit.polariserp.tvmax.entidades.ConsultaResPronet;
import com.exabit.polariserp.tvmax.entidades.Cuota;
import com.exabit.polariserp.tvmax.entidades.Funcionario;
import com.exabit.polariserp.tvmax.entidades.PagoReqPronet;
import com.exabit.polariserp.tvmax.entidades.PagoResPronet;
import com.exabit.polariserp.tvmax.entidades.Servicio;
import com.exabit.polariserp.tvmax.entidades.Suscripcion;
import com.exabit.polariserp.tvmax.util.EntidadesEstaticas;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author traver
 */
@Stateless
public class PronetSesBean {

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @PersistenceContext(unitName = "polariserp-tvmax-pu")
    private EntityManager em;

    public ConsultaResPronet guardarConsultaRes(ConsultaResPronet cr) {
        ConsultaResPronet crManaged = null;
        if (this.validarConsultaRes(cr)) {
            crManaged = this.em.merge(cr);
        } else {
            System.out.println("No validado consulta response");
        }
        return crManaged;
    }

    public ConsultaReqPronet guardarConsultaReq(ConsultaReqPronet cr) {
        ConsultaReqPronet crManaged = null;
        if (this.validarConsultaReq(cr)) {
            cr.setFechaHora(new Date());
            crManaged = this.em.merge(cr);
        } else {
            System.out.println("No validado consulta request");
        }
        return crManaged;
    }

    private boolean validarConsultaReq(ConsultaReqPronet c) {
        boolean validado = true;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<ConsultaReqPronet>> constraintViolations = validator.validate(c);
        if (constraintViolations.size() > 0) {
            validado = false;
            Iterator<ConstraintViolation<ConsultaReqPronet>> iterator = constraintViolations.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<ConsultaReqPronet> cv = iterator.next();
                System.err.println(cv.getRootBeanClass().getName() + "." + cv.getPropertyPath() + " " + cv.getMessage());
            }
        }
        return validado;
    }

    private boolean validarConsultaRes(ConsultaResPronet c) {
        boolean validado = true;
        ValidatorFactory factoryCabecera = Validation.buildDefaultValidatorFactory();
        Validator validatorCabecera = factoryCabecera.getValidator();
        Set<ConstraintViolation<ConsultaResPronet>> constraintViolationsCabecera = validatorCabecera.validate(c);
        if (constraintViolationsCabecera.size() > 0) {
            validado = false;
            Iterator<ConstraintViolation<ConsultaResPronet>> iterator = constraintViolationsCabecera.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<ConsultaResPronet> cv = iterator.next();
                System.err.println(cv.getRootBeanClass().getName() + "." + cv.getPropertyPath() + " " + cv.getMessage());
            }
        }
        if (c.getDetalles() != null) {
            for (ConsultaResDetallePronet detalle : c.getDetalles()) {
                ValidatorFactory factoryDetalle = Validation.buildDefaultValidatorFactory();
                Validator validatorDetalle = factoryDetalle.getValidator();
                Set<ConstraintViolation<ConsultaResDetallePronet>> constraintViolationsDetalle = validatorDetalle.validate(detalle);
                if (constraintViolationsDetalle.size() > 0) {
                    validado = false;
                    Iterator<ConstraintViolation<ConsultaResDetallePronet>> iterator = constraintViolationsDetalle.iterator();
                    while (iterator.hasNext()) {
                        ConstraintViolation<ConsultaResDetallePronet> cv = iterator.next();
                        System.err.println(cv.getRootBeanClass().getName() + "." + cv.getPropertyPath() + " " + cv.getMessage());
                    }
                }
            }
        }
        return validado;
    }

    public ConsultaResPronet getDeudaCliente(Integer ci, ConsultaReqPronet cons) {
        ConsultaResPronet cr = new ConsultaResPronet();
        cr.setConsultaReqPronet(cons);
        TypedQuery qcli = this.em.createQuery("SELECT c FROM Cliente c WHERE c.ci=:ci", Cliente.class);
        qcli.setParameter("ci", ci);
        List<Cliente> lstcli = qcli.getResultList();
        if (!lstcli.isEmpty()) {
            Cliente cl = lstcli.get(0);
            cr.setTipoTrx(5);
            cr.setNombreApellido(cl.getRazonSocial());
            cr.setCodRetorno("000");
            List<Suscripcion> lsts = new ArrayList<>();
            for(Cliente cli : lstcli){
                TypedQuery<Suscripcion> qs = this.em.createQuery("SELECT s FROM Suscripcion s WHERE s.cliente=:cli ORDER BY s.estado ASC", Suscripcion.class);
                qs.setParameter("cli", cli);
                lsts.addAll(qs.getResultList());
            }
            
            Map<String, List<Suscripcion>> mapaUbiSusc = new HashMap<>();
            for (Suscripcion su : lsts) {
                String ubi = su.getBarrio().getIdbarrio().toString() + su.getDireccion().replace(" ", "").toLowerCase();
                if (mapaUbiSusc.get(ubi) == null) {
                    List<Suscripcion> lstsu = new ArrayList<>();
                    mapaUbiSusc.put(ubi, lstsu);
                }
                mapaUbiSusc.get(ubi).add(su);
            }

            List<List<Cuota>> lstGrupoCuota = new ArrayList<>();
            Map<String, List<List<Cuota>>> mapGrupoCuotaUbi = new HashMap<>();
            for (String ubi : mapaUbiSusc.keySet()) {

                System.out.println("ubicacion key: " + ubi);
                Map<Integer, List<List<Cuota>>> mpCuotasSusc = new HashMap<>();
                for (Suscripcion susc : mapaUbiSusc.get(ubi)) {
                    Map<Integer, List<Cuota>> mapCuotaGpServicio = new HashMap<>();
                    System.out.println("Suscripcion " + susc.getIdsuscripcion() + " ubi: " + ubi);
                    TypedQuery<Cuota> tqc = this.em.createQuery("SELECT c FROM Cuota c WHERE c.suscripcion=:sus AND c.cancelado=false AND c.fechaVencimiento<=:fv ORDER BY c.fechaVencimiento ASC", Cuota.class);
                    tqc.setParameter("sus", susc);
                    tqc.setParameter("fv", new Date());
                    List<Cuota> lstcuo = tqc.getResultList();
                    for (Cuota cu : lstcuo) {
                        if (mapCuotaGpServicio.get(cu.getServicio().getIdservicio()) == null) {
                            mapCuotaGpServicio.put(cu.getServicio().getIdservicio(), new ArrayList<>());
                        }
                        mapCuotaGpServicio.get(cu.getServicio().getIdservicio()).add(cu);
                    }
                    List<List<Cuota>> lstGSCuota = new ArrayList<>();
                    lstGSCuota.addAll(mapCuotaGpServicio.values());
                    mpCuotasSusc.put(susc.getIdsuscripcion(), lstGSCuota);
                }
                int cantMax = 0;
                for (List<List<Cuota>> lstgcs : mpCuotasSusc.values()) {
                    for (List<Cuota> lstc : lstgcs) {
                        if (lstc.size() > cantMax) {
                            cantMax = lstc.size();
                        }
                    }
                }
                for (int i = 0; i < cantMax; i++) {
                    List<Cuota> lstgc = new ArrayList<>();
                    for (List<List<Cuota>> lstgcs : mpCuotasSusc.values()) {
                        for (List<Cuota> lstc : lstgcs) {
                            if (i < lstc.size()) {
                                lstgc.add(lstc.get(i));
                            }
                        }
                    }
                    if (mapGrupoCuotaUbi.get(ubi) == null) {
                        mapGrupoCuotaUbi.put(ubi, new ArrayList<>());
                    }
                    mapGrupoCuotaUbi.get(ubi).add(lstgc);
                }
            }
            for (List<List<Cuota>> lstGrupoUbi : mapGrupoCuotaUbi.values()) {
                lstGrupoCuota.addAll(lstGrupoUbi);
            }
            SimpleDateFormat sdfx = new SimpleDateFormat("dd.MM.yyyy");
            if (!lstGrupoCuota.isEmpty()) {
                if (cr.getDetalles() == null) {
                    cr.setDetalles(new LinkedList<>());
                }
                DecimalFormat df = new DecimalFormat("0000000000000.00");
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

                for (List<Cuota> lstc : lstGrupoCuota) {
                    int totalDetalle = 0;
                    int totalIva10 = 0;
                    int totalIva5 = 0;
                    int totalExento = 0;

                    ConsultaResDetallePronet crde = new ConsultaResDetallePronet();
                    crde.setConsultaResPronet(cr);
                    SimpleDateFormat sdfDet = new SimpleDateFormat("MMM yyyy");
                    crde.setMoneda("1");
                    crde.setDireccionDomicilio(lstc.get(0).getSuscripcion().getBarrio().getNombre() + "-" + lstc.get(0).getSuscripcion().getDireccion());
                    List<Integer> lstIdSuscripciones = new ArrayList<>();
                    if (lstc.size() == 1) {
                        Cuota c = lstc.get(0);
                        lstIdSuscripciones.add(c.getSuscripcion().getIdsuscripcion());
                        totalDetalle = c.getMontoCuota();
                        crde.setFechaVencimiento(sdf.format(c.getFechaVencimiento()));

                        crde.setDesOperacion("[" + c.getServicio().getIdservicio() + "]" + c.getSuscripcion().getServicio().getNombre().replace(" ", "") + " " + sdfDet.format(c.getFechaVencimiento()));
                        crde.setDesOperacion(crde.getDesOperacion().toUpperCase());
                        if (null != c.getServicio().getPorcentajeIva()) {
                            switch (c.getServicio().getPorcentajeIva()) {
                                case 10:
                                    totalIva10 = c.getMontoCuota();
                                    break;
                                case 5:
                                    totalIva5 = c.getMontoCuota();
                                    break;
                                case 0:
                                    totalExento = c.getMontoCuota();
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        crde.setFechaVencimiento(sdf.format(new Date()));
                        List<Cuota> lstCuotaPrimaria = new ArrayList<>();
                        List<Cuota> lstCuotaSecundaria = new ArrayList<>();
                        for (Cuota c : lstc) {
                            lstIdSuscripciones.add(c.getSuscripcion().getIdsuscripcion());
                            System.out.println("Susc: " + c.getSuscripcion().getIdsuscripcion() + " " + c.getServicio().getNombre() + " " + sdfx.format(c.getFechaVencimiento()));
                            if (c.getServicio().getIdservicio().equals(c.getSuscripcion().getServicio().getIdservicio())) {
                                lstCuotaPrimaria.add(c);
                            } else {
                                lstCuotaSecundaria.add(c);
                            }
                            totalDetalle = totalDetalle + c.getMontoCuota();
                            if (null != c.getServicio().getPorcentajeIva()) {
                                switch (c.getServicio().getPorcentajeIva()) {
                                    case 10:
                                        totalIva10 = totalIva10 + c.getMontoCuota();
                                        break;
                                    case 5:
                                        totalIva5 = totalIva5 + c.getMontoCuota();
                                        break;
                                    case 0:
                                        totalExento = totalExento + 0;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        String dsc = "";
                        for (Cuota cp : lstCuotaPrimaria) {
                            if (!dsc.isEmpty()) {
                                dsc = dsc + "+";
                            }
                            dsc=dsc+cp.getServicio().getNombre();

                            dsc = dsc +" "+ sdfDet.format(cp.getFechaVencimiento());
                        }
                        for (Cuota cs : lstCuotaSecundaria) {
                            if (!dsc.isEmpty()) {
                                dsc = dsc + "+";
                            }
                              dsc = dsc + cs.getServicio().getNombre()+" "+cs.getNroCuota();
                        }
                        System.out.println("Descripcion: " + dsc);
                        if (dsc.length() > 100) {
                            dsc = dsc.substring(0, 100);
                        }
                        crde.setDesOperacion(dsc.toUpperCase());
                    }
                    crde.setCodigoSuscripcion(lstIdSuscripciones.toString());
                    crde.setTotalDetalle(df.format(totalDetalle).replace(",", "").replace(".", ""));
                    double iva10 = 0.0;
                    double iva5 = 0.0;
                    if (totalIva10 != 0) {
                        iva10 = (((double) totalIva10) * 0.1) / 1.1;
                    }
                    if (totalIva10 != 0) {
                        iva5 = (((double) totalIva5) * 0.05) / 1.05;
                    }
                    BigDecimal bdIva10 = new BigDecimal(iva10);
                    bdIva10 = bdIva10.setScale(2, RoundingMode.HALF_UP);
                    BigDecimal bdIva5 = new BigDecimal(iva5);
                    bdIva5 = bdIva5.setScale(2, RoundingMode.HALF_UP);
                    crde.setIva10(df.format(bdIva10.doubleValue()).replace(",", "").replace(".", ""));
                    crde.setIva5(df.format(bdIva5.doubleValue()).replace(",", "").replace(".", ""));
                    crde.setCuotaList(lstc);

                    cr.getDetalles().add(crde);
                }
                //Agregar reconexion a las suscripciones desconectadas
                for (Suscripcion sus : lsts) {
                    if (sus.getEstado().getIdestado().equals(3) && sus.getServicio().getIdservicio().equals(1)) {
                        ConsultaResDetallePronet detalleRecon = new ConsultaResDetallePronet();
                        detalleRecon.setConsultaResPronet(cr);
                        detalleRecon.setMoneda("1");
                        detalleRecon.setDireccionDomicilio(sus.getBarrio().getNombre() + "-" + sus.getDireccion());
                        detalleRecon.setFechaVencimiento(sdf.format(new Date()));
                        Servicio ser = this.em.find(Servicio.class, 3);
                        detalleRecon.setDesOperacion("[" + ser.getIdservicio() + "]" + ser.getNombre().toUpperCase() + " " + sus.getServicio().getNombre().toUpperCase());
                        int totalIva10 = 0;
                        int totalIva5 = 0;
                        //int totalExento = 0;
                        if (null != ser.getPorcentajeIva()) {
                            switch (ser.getPorcentajeIva()) {
                                case 10:
                                    totalIva10 = ser.getPrecio();
                                    break;
                                case 5:
                                    totalIva5 = ser.getPrecio();
                                    break;
                                /*case 0:
                                    totalExento = ser.getPrecio();
                                    break;*/
                                default:
                                    break;
                            }
                        }

                        double iva10 = 0.0;
                        double iva5 = 0.0;
                        if (totalIva10 != 0) {
                            iva10 = (((double) totalIva10) * 0.1) / 1.1;
                        }
                        if (totalIva10 != 0) {
                            iva5 = (((double) totalIva5) * 0.05) / 1.05;
                        }
                        BigDecimal bdIva10 = new BigDecimal(iva10);
                        bdIva10 = bdIva10.setScale(2, RoundingMode.HALF_UP);
                        BigDecimal bdIva5 = new BigDecimal(iva5);
                        bdIva5 = bdIva5.setScale(2, RoundingMode.HALF_UP);
                        detalleRecon.setIva10(df.format(bdIva10.doubleValue()).replace(",", "").replace(".", ""));
                        detalleRecon.setIva5(df.format(bdIva5.doubleValue()).replace(",", "").replace(".", ""));
                        List<Integer> lstIdSusc = new ArrayList<>();
                        lstIdSusc.add(sus.getIdsuscripcion());
                        detalleRecon.setCodigoSuscripcion(lstIdSusc.toString());
                        detalleRecon.setTotalDetalle(df.format(ser.getPrecio()).replace(",", "").replace(".", ""));
                        cr.getDetalles().add(detalleRecon);
                    }
                }
            } else {
                cr.setCodRetorno("001");
                cr.setCantFilas(0);
                cr.setDesRetorno("SIN CUOTAS");
                cr.setNombreApellido(cl.getRazonSocial());
                cr.setTipoTrx(5);
            }
        } else {
            cr.setCantFilas(0);
            cr.setCodRetorno("999");
            cr.setDesRetorno("CLIE NO ENCONTRADO");
            cr.setTipoTrx(5);
        }

        return this.guardarConsultaRes(cr);
    }

    public boolean validarPassword(Integer ci, String password) {
        TypedQuery<Funcionario> q = this.em.createQuery("SELECT f FROM Funcionario f WHERE f.ci=:usu", Funcionario.class);
        q.setParameter("usu", ci);
        List<Funcionario> lstf = q.getResultList();
        if (!lstf.isEmpty()) {
            Funcionario f = lstf.get(0);
            String pwdEncrypt = DigestUtils.sha256Hex(password);
            if (f.getPassword().equals(pwdEncrypt)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public PagoResPronet pagar(PagoReqPronet pr) {
        ConsultaResDetallePronet conDetalle = this.em.find(ConsultaResDetallePronet.class, pr.getNroOperacion());
        if (conDetalle != null) {
            if (!conDetalle.getPagoReqPronetList().isEmpty()) {
                PagoResPronet pagoResponse = new PagoResPronet();
                pagoResponse.setCodRetorno("999");
                pagoResponse.setDesRetorno("OPER YA PROCESADA");
                pagoResponse.setTipoTrx(3);
                return pagoResponse;
            } else {
                DecimalFormat df = new DecimalFormat("0000000000000");
                Integer montopago = 0;
                Integer montoconsulta = 0;
                try {
                    montopago = df.parse(pr.getImporte().substring(0, 13)).intValue();
                    montoconsulta = df.parse(conDetalle.getTotalDetalle().substring(0, 13)).intValue();
                } catch (ParseException ex) {
                    Logger.getLogger(PronetSesBean.class.getName()).log(Level.SEVERE, "Error al parsear importe de pago", ex);
                    PagoResPronet pagoResponse = new PagoResPronet();
                    pagoResponse.setCodRetorno("999");
                    pagoResponse.setDesRetorno("ERR INTER FORMAT NUM");
                    pagoResponse.setTipoTrx(3);
                    return pagoResponse;
                }
                System.out.println("Monto consulta: " + montoconsulta + ", monto pago: " + montopago);
                if (!montopago.equals(montoconsulta)) {
                    PagoResPronet pagoResponse = new PagoResPronet();
                    pagoResponse.setCodRetorno("999");
                    pagoResponse.setDesRetorno("MONTOS NO COINCIDEN");
                    pagoResponse.setTipoTrx(3);
                    return pagoResponse;
                } else {
                    //Cuota
                    //Pago Request
//                    pr.setCobroList(lstCobros);
                    pr.setConsultaResDetallePronet(conDetalle);
                    pr.setFechaHora(new Date());
                    this.em.persist(pr);
                    this.em.merge(conDetalle);

                    String strIdServicio = "";

                    if (conDetalle.getDesOperacion().contains("[") && conDetalle.getDesOperacion().contains("]")) {
                        String desOpe = conDetalle.getDesOperacion();
                        strIdServicio = desOpe.substring(desOpe.indexOf("[") + 1, desOpe.indexOf("]"));
                    }

                    if (strIdServicio.equals("3")) {

                        Integer idServ = Integer.parseInt(strIdServicio);
                        Servicio serv = this.em.find(Servicio.class, idServ);

                        String strSus = conDetalle.getCodigoSuscripcion();
                        String strIdsus = strSus.substring(strSus.indexOf("[") + 1, strSus.indexOf("]"));
                        Integer idsus = Integer.parseInt(strIdsus);
                        Suscripcion sus = this.em.find(Suscripcion.class, idsus);

                        Cobro cobReco = new Cobro();
                        cobReco.setPagoReqPronetList(new ArrayList<>());
                        cobReco.setServicio(serv);

                        try {
                            cobReco.setMonto(Integer.parseInt(conDetalle.getTotalDetalle().substring(0, conDetalle.getTotalDetalle().length()-2)));
                        } catch (NumberFormatException ex) {
                            System.out.println("Error al parsear monto para reconexion: " + ex.getMessage());
                        }
                        cobReco.setCodTransBanco(pr.getCodTransaccion().toString());
                        cobReco.setSuscripcion(sus);
                        cobReco.setCobrador(sus.getCobrador());
                        cobReco.setConcepto(serv.getConceptoCobro());
                        cobReco.setFecha(new Date());
                        cobReco.setFechaHoraRegistro(new Date());
                        cobReco.setAnulado(false);

                        Integer cifun = Integer.parseInt(conDetalle.getConsultaResPronet().getConsultaReqPronet().getUsuario());
                        Funcionario fun = this.funcionarioFacade.getFuncionarioPorDocumento(cifun);
                        cobReco.setFuncionarioRegistro(fun);
                        cobReco.setMedioPago(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO);
                        cobReco.setPorceComisionAplicado(0.0f);
                        cobReco.getPagoReqPronetList().add(pr);
                        
                        this.em.persist(cobReco);

                    } else {
                        List<Cobro> lstCobros = new ArrayList<>();
                        for (Cuota cuota : conDetalle.getCuotaList()) {
                            Cobro cobro = new Cobro();
                            cobro.setPagoReqPronetList(new ArrayList<>());
                            cobro.setAnulado(false);
                            Integer cifun = Integer.parseInt(conDetalle.getConsultaResPronet().getConsultaReqPronet().getUsuario());
                            Funcionario fun = this.funcionarioFacade.getFuncionarioPorDocumento(cifun);
                            cobro.setSuscripcion(cuota.getSuscripcion());
                            cobro.setCobrador(cuota.getSuscripcion().getCobrador());
                            cobro.setCodTransBanco(pr.getCodTransaccion().toString());
                            cobro.setConcepto(cuota.getServicio().getConceptoCobro());
                            cobro.setServicio(cuota.getServicio());
//                        cobro.setCuotaList(new ArrayList<>());
//                        cobro.getCuotaList().add(cuota);
                            cobro.setCuota(cuota);
                            cobro.setFecha(new Date());
                            cobro.setFechaHoraRegistro(new Date());
                            cobro.setFuncionarioRegistro(fun);
                            cobro.setMedioPago(EntidadesEstaticas.MEDIO_PAGO_EFECTIVO);
                            cobro.setPorceComisionAplicado(0.0f);
                            cobro.setMonto(cuota.getMontoCuota());
                            cobro.getPagoReqPronetList().add(pr);
                            this.em.persist(cobro);
                            lstCobros.add(cobro);
                            cuota.setFechaPago(new Date());
                            cuota.setMontoEntrega(cuota.getMontoCuota());
                            cuota.setCancelado(true);
//                        cuota.getCobroList().add(cobro);
                            this.em.merge(cuota);
                        }
                    }
                    //Pago response
                    PagoResPronet pRes = new PagoResPronet();
                    pRes.setCodRetorno("000");
                    pRes.setDesRetorno("PAGO EXITOSO");
                    pRes.setTipoTrx(3);
                    pRes.setPagoReqPronet(pr);
                    PagoResPronet prManaged = this.em.merge(pRes);
                    return prManaged;
                }
            }
        } else {
            PagoResPronet pagoResponse = new PagoResPronet();
            pagoResponse.setCodRetorno("999");
            pagoResponse.setDesRetorno("OPER NO ENCONTRADA");
            pagoResponse.setTipoTrx(3);
            return pagoResponse;
        }
    }

    public AnulacionResPronet anular(AnulacionReqPronet anulacionReq) {
        Integer nroOp = anulacionReq.getNroOperacion();
        if (nroOp != null) {
            ConsultaResDetallePronet conDet = this.em.find(ConsultaResDetallePronet.class, nroOp);
            if (conDet != null) {
                this.em.refresh(conDet);
                List<PagoReqPronet> lst = conDet.getPagoReqPronetList();
                if (!lst.isEmpty()) {
                    PagoReqPronet pagoReq = lst.get(0);
                    if (!pagoReq.getAnulacionReqPronetList().isEmpty()) {
                        AnulacionResPronet anulRes = new AnulacionResPronet();
                        anulRes.setCodRetorno("999");
                        anulRes.setDesRetorno("OPER YA ANULADA");
                        anulRes.setTipoTrx(4);
                        return anulRes;
                    } else {
                        DecimalFormat df = new DecimalFormat("0000000000000");
                        if (!pagoReq.getCodTransaccion().equals(anulacionReq.getCodTransaccionAnular())) {
                            AnulacionResPronet anulRes = new AnulacionResPronet();
                            anulRes.setCodRetorno("999");
                            anulRes.setDesRetorno("CODTRANS NO COINCIDE");
                            anulRes.setTipoTrx(4);
                            return anulRes;
                        } else {
                            Integer montopagoanular = 0;
                            Integer montopago = 0;
                            try {
                                montopago = df.parse(pagoReq.getImporte().substring(0, 13)).intValue();
                                montopagoanular = df.parse(anulacionReq.getImporte().substring(0, 13)).intValue();
                            } catch (Exception ex) {
                                System.out.println("Error al convetir monto a int: " + ex);
                                AnulacionResPronet anulRes = new AnulacionResPronet();
                                anulRes.setCodRetorno("999");
                                anulRes.setDesRetorno("ERR INTER FORMAT NUM");
                                anulRes.setTipoTrx(4);
                                return anulRes;
                            }
                            if (!montopagoanular.equals(montopago)) {
                                AnulacionResPronet anulRes = new AnulacionResPronet();
                                anulRes.setCodRetorno("999");
                                anulRes.setDesRetorno("MONTOS NO COINCIDEN");
                                anulRes.setTipoTrx(4);
                                return anulRes;
                            } else {
                                for (Cobro co : pagoReq.getCobroList()) {
                                    co.setAnulado(true);
                                    this.em.merge(co);
                                }
                                for (Cuota cuo : pagoReq.getConsultaResDetallePronet().getCuotaList()) {
                                    cuo.setCancelado(false);
                                    String strImporteAnul = anulacionReq.getImporte().substring(0, 13);

                                    Integer impAnul = 0;
                                    try {
                                        impAnul = df.parse(strImporteAnul).intValue();
                                    } catch (ParseException ex) {
                                        System.out.println("Error al convertir a Integer monto de anulacion: " + ex);
                                    }
                                    if (cuo.getMontoEntrega() >= impAnul) {
                                        cuo.setMontoEntrega(cuo.getMontoEntrega() - impAnul);
                                    } else {
                                        cuo.setMontoEntrega(0);
                                    }
                                    cuo.setFechaPago(null);
                                    this.em.merge(cuo);
                                }
                                anulacionReq.setFechaHora(new Date());
                                anulacionReq.setPagoReqPronet(pagoReq);
                                this.em.persist(anulacionReq);
                                AnulacionResPronet anulRes = new AnulacionResPronet();
                                anulRes.setCodRetorno("000");
                                anulRes.setDesRetorno("OPERACION EXITOSA");
                                anulRes.setAnulacionReqPronet(anulacionReq);
                                anulRes.setTipoTrx(4);
                                AnulacionResPronet arMerged = this.em.merge(anulRes);
                                return arMerged;
                            }
                        }
                    }
                } else {
                    AnulacionResPronet anulRes = new AnulacionResPronet();
                    anulRes.setCodRetorno("999");
                    anulRes.setDesRetorno("NO SE ENCUENTRA PAGO");
                    anulRes.setTipoTrx(4);
                    return anulRes;
                }
            } else {
                AnulacionResPronet anulRes = new AnulacionResPronet();
                anulRes.setCodRetorno("999");
                anulRes.setDesRetorno("NO SE ENCUENTRA OPER");
                anulRes.setTipoTrx(4);
                return anulRes;
            }
        } else {
            AnulacionResPronet anulRes = new AnulacionResPronet();
            anulRes.setCodRetorno("999");
            anulRes.setDesRetorno("NRO OPER INVALIDO");
            anulRes.setTipoTrx(4);
            return anulRes;
        }
    }

    public PagoReqPronet getPago(Integer codTransaccion) {
        TypedQuery<PagoReqPronet> q = this.em.createQuery("SELECT p FROM PagoReqPronet p WHERE p.codTransaccion=:cod", PagoReqPronet.class);
        q.setParameter("cod", codTransaccion);
        List<PagoReqPronet> lst = q.getResultList();
        if (!lst.isEmpty()) {
            return lst.get(0);
        } else {
            return null;
        }
    }
}
