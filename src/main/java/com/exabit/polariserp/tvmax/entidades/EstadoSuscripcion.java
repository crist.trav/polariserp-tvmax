/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "estado_suscripcion")
@NamedQueries({
    @NamedQuery(name = "EstadoSuscripcion.findAll", query = "SELECT e FROM EstadoSuscripcion e")
    , @NamedQuery(name = "EstadoSuscripcion.findByIdestado", query = "SELECT e FROM EstadoSuscripcion e WHERE e.idestado = :idestado")
    , @NamedQuery(name = "EstadoSuscripcion.findByAlias", query = "SELECT e FROM EstadoSuscripcion e WHERE e.alias = :alias")
    , @NamedQuery(name = "EstadoSuscripcion.findByNombre", query = "SELECT e FROM EstadoSuscripcion e WHERE e.nombre = :nombre")
    , @NamedQuery(name = "EstadoSuscripcion.findByRecibeServicio", query = "SELECT e FROM EstadoSuscripcion e WHERE e.recibeServicio = :recibeServicio")})
public class EstadoSuscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idestado", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idestado", table = "idsequences", allocationSize = 1, initialValue = 5)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestado")
    private Integer idestado;
    @Size(max = 255)
    @Column(name = "alias")
    private String alias;
    @Size(max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "recibe_servicio")
    private Boolean recibeServicio;
    @OneToMany(mappedBy = "estado")
    private List<Suscripcion> suscripcionList;
    @OneToMany(mappedBy = "estadoAnterior")
    private List<SolicitudCambioEstado> solicitudCambioEstadoList;
    @OneToMany(mappedBy = "estadoNuevo")
    private List<SolicitudCambioEstado> solicitudCambioEstadoList1;

    public EstadoSuscripcion() {
    }

    public EstadoSuscripcion(Integer idestado) {
        this.idestado = idestado;
    }

    public Integer getIdestado() {
        return idestado;
    }

    public void setIdestado(Integer idestado) {
        this.idestado = idestado;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getRecibeServicio() {
        return recibeServicio;
    }

    public void setRecibeServicio(Boolean recibeServicio) {
        this.recibeServicio = recibeServicio;
    }

    public List<Suscripcion> getSuscripcionList() {
        return suscripcionList;
    }

    public void setSuscripcionList(List<Suscripcion> suscripcionList) {
        this.suscripcionList = suscripcionList;
    }

    public List<SolicitudCambioEstado> getSolicitudCambioEstadoList() {
        return solicitudCambioEstadoList;
    }

    public void setSolicitudCambioEstadoList(List<SolicitudCambioEstado> solicitudCambioEstadoList) {
        this.solicitudCambioEstadoList = solicitudCambioEstadoList;
    }

    public List<SolicitudCambioEstado> getSolicitudCambioEstadoList1() {
        return solicitudCambioEstadoList1;
    }

    public void setSolicitudCambioEstadoList1(List<SolicitudCambioEstado> solicitudCambioEstadoList1) {
        this.solicitudCambioEstadoList1 = solicitudCambioEstadoList1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestado != null ? idestado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoSuscripcion)) {
            return false;
        }
        EstadoSuscripcion other = (EstadoSuscripcion) object;
        if ((this.idestado == null && other.idestado != null) || (this.idestado != null && !this.idestado.equals(other.idestado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.EstadoSuscripcion[ idestado=" + idestado + " ]";
    }
    
}
