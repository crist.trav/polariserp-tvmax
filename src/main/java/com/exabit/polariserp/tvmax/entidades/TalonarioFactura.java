/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exabit.polariserp.tvmax.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author traver
 */
@Entity
@Table(name = "talonario_factura")
@NamedQueries({
    @NamedQuery(name = "TalonarioFactura.findAll", query = "SELECT t FROM TalonarioFactura t")
    , @NamedQuery(name = "TalonarioFactura.findByIdtalonario", query = "SELECT t FROM TalonarioFactura t WHERE t.idtalonario = :idtalonario")
    , @NamedQuery(name = "TalonarioFactura.findByActivo", query = "SELECT t FROM TalonarioFactura t WHERE t.activo = :activo")
    , @NamedQuery(name = "TalonarioFactura.findByCodEstablecimiento", query = "SELECT t FROM TalonarioFactura t WHERE t.codEstablecimiento = :codEstablecimiento")
    , @NamedQuery(name = "TalonarioFactura.findByFinVigencia", query = "SELECT t FROM TalonarioFactura t WHERE t.finVigencia = :finVigencia")
    , @NamedQuery(name = "TalonarioFactura.findByInicioVigencia", query = "SELECT t FROM TalonarioFactura t WHERE t.inicioVigencia = :inicioVigencia")
    , @NamedQuery(name = "TalonarioFactura.findByNroActual", query = "SELECT t FROM TalonarioFactura t WHERE t.nroActual = :nroActual")
    , @NamedQuery(name = "TalonarioFactura.findByNroFin", query = "SELECT t FROM TalonarioFactura t WHERE t.nroFin = :nroFin")
    , @NamedQuery(name = "TalonarioFactura.findByNroInicio", query = "SELECT t FROM TalonarioFactura t WHERE t.nroInicio = :nroInicio")
    , @NamedQuery(name = "TalonarioFactura.findByTimbrado", query = "SELECT t FROM TalonarioFactura t WHERE t.timbrado = :timbrado")
    , @NamedQuery(name = "TalonarioFactura.findByNombreFormato", query = "SELECT t FROM TalonarioFactura t WHERE t.nombreFormato = :nombreFormato")})
public class TalonarioFactura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "idtalonariofactura", strategy = GenerationType.TABLE)
    @TableGenerator(name = "idtalonariofactura", table = "idsequences", allocationSize = 1, initialValue = 18)
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtalonario")
    private Integer idtalonario;
    @Column(name = "activo")
    private Boolean activo;
    @Size(max = 255)
    @Column(name = "cod_establecimiento")
    private String codEstablecimiento;
    @Column(name = "fin_vigencia")
    @Temporal(TemporalType.DATE)
    private Date finVigencia;
    @Column(name = "inicio_vigencia")
    @Temporal(TemporalType.DATE)
    private Date inicioVigencia;
    @Column(name = "nro_actual")
    private Integer nroActual;
    @Column(name = "nro_fin")
    private Integer nroFin;
    @Column(name = "nro_inicio")
    private Integer nroInicio;
    @Column(name = "timbrado")
    private Integer timbrado;
    @Size(max = 100)
    @Column(name = "nombre_formato")
    private String nombreFormato;
    @OneToMany(mappedBy = "talonarioFactura")
    private List<Factura> facturaList;
    @JoinColumn(name = "terminal_impresion", referencedColumnName = "idterminal_impresion")
    @ManyToOne
    private TerminalImpresion terminalImpresion;

    public TalonarioFactura() {
    }

    public TalonarioFactura(Integer idtalonario) {
        this.idtalonario = idtalonario;
    }

    public Integer getIdtalonario() {
        return idtalonario;
    }

    public void setIdtalonario(Integer idtalonario) {
        this.idtalonario = idtalonario;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getCodEstablecimiento() {
        return codEstablecimiento;
    }

    public void setCodEstablecimiento(String codEstablecimiento) {
        this.codEstablecimiento = codEstablecimiento;
    }

    public Date getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(Date finVigencia) {
        this.finVigencia = finVigencia;
    }

    public Date getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(Date inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public Integer getNroActual() {
        return nroActual;
    }

    public void setNroActual(Integer nroActual) {
        this.nroActual = nroActual;
    }

    public Integer getNroFin() {
        return nroFin;
    }

    public void setNroFin(Integer nroFin) {
        this.nroFin = nroFin;
    }

    public Integer getNroInicio() {
        return nroInicio;
    }

    public void setNroInicio(Integer nroInicio) {
        this.nroInicio = nroInicio;
    }

    public Integer getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public String getNombreFormato() {
        return nombreFormato;
    }

    public void setNombreFormato(String nombreFormato) {
        this.nombreFormato = nombreFormato;
    }

    public List<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<Factura> facturaList) {
        this.facturaList = facturaList;
    }

    public TerminalImpresion getTerminalImpresion() {
        return terminalImpresion;
    }

    public void setTerminalImpresion(TerminalImpresion terminalImpresion) {
        this.terminalImpresion = terminalImpresion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtalonario != null ? idtalonario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TalonarioFactura)) {
            return false;
        }
        TalonarioFactura other = (TalonarioFactura) object;
        if ((this.idtalonario == null && other.idtalonario != null) || (this.idtalonario != null && !this.idtalonario.equals(other.idtalonario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.exabit.polariserp.tvmax.entidades.TalonarioFactura[ idtalonario=" + idtalonario + " ]";
    }
    
}
